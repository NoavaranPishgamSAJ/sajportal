﻿[assembly: Microsoft.Owin.OwinStartupAttribute(typeof(SAJPortal.Startup))]
namespace SAJPortal
{

    #region Assembly

    using Owin;
    using System.Web.Helpers;
    using System.Security.Claims;

    #endregion

    /// <summary>
    /// این کلاس برای مقداردهی اولیه OWIN استفاده می شود 
    /// </summary>
    /// <remarks>
    /// از OWIN برای تعریف یک استاندارد مابین برنامه ها و وب سرویس های دات نت استفاده می شود
    /// </remarks>
    public class Startup
    {

        #region Method

        /// <summary>
        /// این متد تنظیمات اولیه را انجام می دهد
        /// </summary>
        public void Configuration(IAppBuilder app)
        {
            AntiForgeryConfig.SuppressXFrameOptionsHeader = true;
            AntiForgeryConfig.UniqueClaimTypeIdentifier = ClaimTypes.NameIdentifier;
        }

        #endregion

    }
}
