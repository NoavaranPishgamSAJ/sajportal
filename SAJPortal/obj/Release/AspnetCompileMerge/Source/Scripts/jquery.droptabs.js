/* Copyright (c) 2014 Alexandru Boboc
 * Droptabs v.1.1 Jquery Plugin
 * Tested with JQuery 1.11.1
 */

(function ($) {

    $.fn.droptabs = function (o) {

        //Default options
        var s = $.extend({
            dropdownSelector: "li.more",
            dropdownMenuSelector: "ul.more-menu",
            dropdownTabsSelector: "li.top",
            visibleTabsSelector: ">li:not(.more)",
            autoArrangeTabs: true
        }, o);

        return this.each(function () {

            var $container = $(this);
            var dropdown = $(s.dropdownSelector, this);
            var dropdownMenu = $(s.dropdownMenuSelector, dropdown);

            var $dropdownTabs = function () {
                return $(s.dropdownTabsSelector, dropdownMenu);
            }

            var $visibleTabs = function () {
                return $(s.visibleTabsSelector, $container);
            }

            function getFirstHiddenElementWidth() {
                var tempElem = $dropdownTabs().last().clone().appendTo($container).css("position", "fixed");
                var hiddenElementWidth = $(tempElem).outerWidth();
                $(tempElem).remove();
                return hiddenElementWidth;
            }

            function getHiddenElementWidth(elem) {
                var tempElem = $(elem).clone().appendTo($container).css("position", "fixed");
                var hiddenElementWidth = $(tempElem).outerWidth();
                $(tempElem).remove();
                return hiddenElementWidth;
            }

            var visibleTabsWidth = function () {
                var visibleTabsWidth = 0;
                $($visibleTabs()).each(function (index) {
                    visibleTabsWidth += parseInt($(this).outerWidth(), 10);
                });
                visibleTabsWidth = visibleTabsWidth + parseInt($(dropdown).outerWidth(), 10);
                return visibleTabsWidth;
            }

            var availableSpace = function () {
                return $container.outerWidth() - visibleTabsWidth();
            }

            var arrangeTabs = function () {
                //End Development info	

                if (availableSpace() < 0) {//we will hide tabs here
                    var x = availableSpace();
                    $($visibleTabs().get().reverse()).each(function (index) {
                        if (!($(this).hasClass('always-in-visible'))) {
                            $(this).prependTo(dropdownMenu);
                            x = x + $(this).outerWidth();
                        }
                        if (x >= 0) { return false; }
                    });
                }

                if (availableSpace() > getFirstHiddenElementWidth()) { //and here we bring the tabs out
                    var x = availableSpace();
                    $($dropdownTabs()).each(function (index) {
                        if (getHiddenElementWidth(this) < x && !($(this).hasClass('always-in-dropdown'))) {
                            $(this).appendTo($container);
                            x = x - $(this).outerWidth();
                        } else { return false; }
                    });
                }

                if ($dropdownTabs().length <= 0) { dropdown.hide(); } else { dropdown.show(); }
            }

            //init

            if (s.autoArrangeTabs) {
                var tempTabs = [];
                $($visibleTabs().get().reverse()).each(function (index) {
                    if ($(this).hasClass('always-in-visible')) {
                        tempTabs.push($(this));
                        $(this).remove();
                    }
                });
                for (var i = 0; i < tempTabs.length; i++) {
                    $container.prepend(tempTabs[i]);
                }
            }

            $(document).ready(function () {
                arrangeTabs();
            });

            $(window).resize(function () {
                arrangeTabs();
            });

            $('.droptabs .more > a').on('click', function () {
                if (dropdownMenu.css('display') === 'none') {
                    $('.droptabs .more > a').addClass('active');
                }
                else {
                    $('.droptabs .more > a').removeClass('active');
                }
                dropdownMenu.toggle();
            });
            $('.droptabs .menu > a').on('click', function () {
                var parent = $(this).parent('.menu');
                var submenu = $(' > ul.sub-menu', parent);
                if (submenu.css('display') === 'none') {
                    $(this).addClass('active');
                }
                else {
                    $(this).removeClass('active');
                    $('.menu > a', submenu).removeClass('active');
                    $('ul.sub-menu', submenu).hide();
                }
                submenu.toggle();
            });
            return this;
        });
    }
}(jQuery));
