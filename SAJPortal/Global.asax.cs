﻿namespace SAJPortal
{

    #region Assembly

    using ORM.Condition;
    using System.Web.Mvc;
    using System.Web.Optimization;
    using System.Web.Routing;
    using DataService;
    using Mapping;
    using CMS.Sitemap;
    using CMS.Routing;
    using ORM.Model;
    using System.Web;
    using FluentScheduler;
    using System;
    using Utility;
    using CMS.Helper;
    using ORM.Enums;
    using Resources;

    #endregion

    /// <summary>
    /// کلاس برنامه که مسئول پاسخوگی به رویدادهای برنامه می باشد
    /// </summary>
    public class MvcApplication : HttpApplication
    {

        #region Method

        /// <summary>
        /// این متد زمان شروع برنامه برای بار اول اجرا می شود
        /// </summary>
        protected void Application_Start()
        {
            // تنظیم موتور پردازش نماهای پرتال
            ViewEngines.Engines.Clear();
            ViewEngines.Engines.Insert(0, new CmsViewEngine());

            //CMS.ModuleSetup.Setup();

            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            // نگاشت مدل ها به مدل های نما
            AutoMapperConfiguration.Configure();

            // تنطیم بیدار نگه دار پرتال
            TaskManager.UnobservedTaskException += TaskManager_UnobservedTaskException;
            TaskManager.Initialize(new UpTimeRegistry());

            // برای جلوگیری از تشخیص تکنولوزی سایت
            MvcHandler.DisableMvcResponseHeader = true;
        }

        /// <summary>
        /// این متد زمانی که هر کاربر به سایت درخواست می دهد یکبار اجرا می شود
        /// </summary>
        protected void Session_Start()
        {
            SitemapProvider.BindNodeFromDatabase(1, string.Empty);

            //var baseUrl = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + "/";
            //var basesUrl = Request.Url.GetComponents(UriComponents.SchemeAndServer, UriFormat.UriEscaped) + "/";
            var das = DynamicService.Current;
            var host = Request.ServerVariables["HTTP_HOST"];
            var criterias = new FilterExpressionList();
            var siteList = das.GetList<Site>(criterias);
            if (siteList != null)
            {
                foreach (var site in siteList)
                {
                    foreach (var domain in site.DomainsList)
                    {
                        if (domain.IsNotEmpty())
                        {
                            var domainPath = domain;
                            if (host.ToLower().Contains("localhost") && domainPath.Contains("localhost") && site.IsOnline == Toggle.True)
                            {
                                CmsVariables.ConfigureSite(site);
                            }
                            else if (domainPath.Equals(host) && site.IsOnline == Toggle.True)
                            {
                                CmsVariables.ConfigureSite(site);
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// این متد زمانی بیدار نگه دار پرتال با خطا مواجه شود اجرا می شود
        /// </summary>
        /// <param name="sender">اطلاعات مربوط به خطا: FluentScheduler.Model.TaskExceptionInformation</param>
        /// <param name="e">رویداد خطا: UnhandledExceptionEventArgs</param>
        public void TaskManager_UnobservedTaskException(FluentScheduler.Model.TaskExceptionInformation sender, UnhandledExceptionEventArgs e)
        {
            Logger.Log(new Exception(@"An error happened with a scheduled task: " + e.ExceptionObject));
        }

        /// <summary>
        /// این متد برای مدیریت وضعیت درخواست ها می باشد و زمانی که یک درخواست اجرا شود فعال می شود
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Application_AcquireRequestState(object sender, EventArgs e)
        {
            // تنظیم کردن اطلاعات کاربری که به سیستم وارد شده است
            if (CmsVariables.FormsAuthentication.Get() != null && CmsVariables.FormsAuthentication.Get().Identity.Name.IsNotEmpty())
            {
                HttpContext.Current.User = CmsVariables.FormsAuthentication.Get();
            }

            // تنظیم فرهنگ پرتال
            var baseSite = ModuleHelper.GetBaseSite();
            if (baseSite != null && baseSite.Culture.IsNotEmpty())
            {
                CultureHelper.SetCurrentCulture(baseSite.Culture);
            }

            // برای جلوگیری از تشخیص تکنولوزی سایت
            Response.AddHeader("X-WebKit-CSP", "default-src 'self'");
            // experimental header introduced into Google Chrome
            // and other WebKit based browsers (Safari) in 2011

            Response.AddHeader("X-Content-Security-Policy", "default-src 'self'");
            //experimental header introduced in Gecko 2 based browsers
            // (Firefox 4 to Firefox 22, Thunderbird 3.3, SeaMonkey 2.1).
        }

        #endregion

    }
}

//Sanitizer.GetSafeHtmlFragment(model.SiteName)