﻿namespace SAJPortal.Areas.Repositories.ViewModels
{

    #region Assembly

    using System.ComponentModel.DataAnnotations;
    using Resources;

    #endregion

    /// <summary>
    /// مدل مخزن
    /// </summary>
    public class RepositoryViewModel
    {

        /// <summary>
        /// شناسه
        /// </summary>
        public int ItemId { get; set; }

        /// <summary>
        /// نام مخزن
        /// </summary>
        [Required(ErrorMessageResourceType = typeof(Global), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(Global), Name = "RepositoryName")]
        public string RepositoryName { get; set; }

        /// <summary>
        /// نمایش پوشه های مخفی
        /// </summary>
        [Required(ErrorMessageResourceType = typeof(Global), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(Global), Name = "ShowHiddenFolder")]
        public bool ShowHiddenFolder { get; set; }

        /// <summary>
        /// فعال سازی گردش کار
        /// </summary>
        [Required(ErrorMessageResourceType = typeof(Global), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(Global), Name = "EnableWorkFlow")]
        public bool EnableWorkFlow { get; set; }

    }
}