﻿namespace SAJPortal.Areas.Repositories.Controllers
{

    #region Assembly

    using DataService;
    using ORM.Model;
    using System.Web.Mvc;
    using ViewModels;
    using System;
    using ORM.Condition;
    using AutoMapper;
    using System.Linq;
    using ORM.Enums;
    using Portable.IO;
    using CMS.Attribute;
    using CMS.Helper;

    #endregion

    /// <summary>
    /// کنترل کننده مخازن
    /// </summary>
    [OutputCache(VaryByParam = "*", Duration = 0, NoStore = true)]
    [LoggedOrAuthorized]
    public class HomeController : Controller
    {

        #region Attribute

        private const int PageSize = 10;

        private int PageIndex
        {
            get
            {
                if (TempData["PageIndex"] == null)
                    TempData["PageIndex"] = 1;
                return Convert.ToInt32(TempData["PageIndex"]);
            }
            set { TempData["PageIndex"] = value; }
        }

        private long PageCount
        {
            set { TempData["PageCount"] = value; }
        }

        private FilterExpressionList Criterias
        {
            get
            {
                if (TempData["Criterias"] == null)
                    TempData["Criterias"] = new FilterExpressionList();
                return TempData["Criterias"] as FilterExpressionList;
            }
            set { TempData["Criterias"] = value; }
        }

        #endregion

        #region Method

        /// <summary>
        /// لیست مخازن
        /// </summary>
        /// <returns>ActionResult</returns>
        [DapperAuthorize(Roles = "RepositoriesList")]
        public ActionResult Index()
        {
            PageIndex = 1;
            Criterias = new FilterExpressionList();
            if (!User.IsInRole("IsAdmin") && !Criterias.ContainFilter("ItemId"))
            {
                Criterias.Add(new ORM.Condition.Filter("ItemId", SqlOperators.Equal, ModuleHelper.GetBaseSite().RepositoryId));
            }
            else if (!Criterias.ContainFilter("ItemId"))
                Criterias.Add(new ORM.Condition.Filter("ItemId", SqlOperators.NotEqual, 1));
            var list = DynamicService.Current.GetList<Repository>(PageIndex - 1, PageSize, Criterias, string.Empty);
            PageCount = Convert.ToInt64(Math.Ceiling((double)DynamicService.Current.GetCount<Repository>(Criterias, string.Empty) / PageSize));
            return View(list);
        }

        /// <summary>
        /// لیست مخازن
        /// </summary>
        /// <param name="search">فیلتر لیست: string</param>
        /// <param name="formType">نوع فیلتر: string</param>
        /// <returns>ActionResult</returns>
        [DapperAuthorize(Roles = "RepositoriesList")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index(string search, string formType)
        {
            if (formType.Equals("pager") && Request.Form.AllKeys.Contains("PageIndex"))
                PageIndex = Convert.ToInt32(Request.Form["PageIndex"]);
            else if (formType.Equals("search"))
            {
                PageIndex = 1;
                Criterias.Clear();
                Criterias.Add(new ORM.Condition.Filter("RepositoryName", SqlOperators.Like, search, SqlOperand.And, DbType.NVarChar));
            }
            if (!User.IsInRole("IsAdmin") && !Criterias.ContainFilter("ItemId"))
            {
                Criterias.Add(new ORM.Condition.Filter("ItemId", SqlOperators.Equal, ModuleHelper.GetBaseSite().RepositoryId));
            }
            else if (!Criterias.ContainFilter("ItemId"))
                Criterias.Add(new ORM.Condition.Filter("ItemId", SqlOperators.NotEqual, 1));
            var list = DynamicService.Current.GetList<Repository>(PageIndex - 1, PageSize, Criterias, string.Empty);
            PageCount = Convert.ToInt64(Math.Ceiling((double)DynamicService.Current.GetCount<Repository>(Criterias, string.Empty) / PageSize));
            return View(list);
        }

        /// <summary>
        /// فرم مخزن جدید
        /// </summary>
        /// <returns>ActionResult</returns>
        [DapperAuthorize(Roles = "NewRepository")]
        [Admin]
        [HttpGet]
        public ActionResult New()
        {
            return View();
        }

        /// <summary>
        /// ثبت مخزن
        /// </summary>
        /// <param name="model">مددل مخزن: RepositoryViewModel</param>
        /// <returns>ActionResult</returns>
        [DapperAuthorize(Roles = "NewRepository")]
        [Admin]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult New(RepositoryViewModel model)
        {
            if (ModelState.IsValid)
            {
                DynamicService.Current.CreateItem(new Repository
                {
                    RepositoryName = model.RepositoryName,
                    ShowHiddenFolder = model.ShowHiddenFolder.GetToggle(),
                    EnableWorkFlow = model.EnableWorkFlow.GetToggle(),
                    AddedDate = DateTime.Today
                });

                // تعیین مسیر مخزن
                var path = HttpContext.Server.MapPath($"~/CmsData/Repository/{model.RepositoryName}");
                if (!FileSystem.Current.ExistsDirectory(path))
                {
                    // ایجاد پوشه مخزن
                    FileSystem.Current.AppDirectory.CreateDirectory(path);
                }

                return RedirectToAction("Index", "Home");
            }
            return View(model);
        }

        /// <summary>
        /// فرم ویرایش مخزن
        /// </summary>
        /// <param name="id">شناسه مخزن: int</param>
        /// <returns>ActionResult</returns>
        [DapperAuthorize(Roles = "EditRepository")]
        [HttpGet]
        public ActionResult Edit(int id)
        {
            var repository = DynamicService.Current.GetItem<Repository>(id);
            var repositoryVm = Mapper.Map<RepositoryViewModel>(repository);
            return View(repositoryVm);
        }

        /// <summary>
        /// ویرایش مخزن
        /// </summary>
        /// <param name="id">شناسه مخزن: int</param>
        /// <param name="model">مددل مخزن: RepositoryViewModel</param>
        /// <returns>ActionResult</returns>
        [DapperAuthorize(Roles = "EditRepository")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, RepositoryViewModel model)
        {
            if (ModelState.IsValid)
            {
                var repository = DynamicService.Current.GetItem<Repository>(id);
                
                // تعیین مسیر قبلی مخزن
                var pathOld = HttpContext.Server.MapPath($"~/CmsData/Repository/{repository.RepositoryName}");

                repository.RepositoryName = model.RepositoryName;
                repository.ShowHiddenFolder = model.ShowHiddenFolder.GetToggle();
                repository.EnableWorkFlow = model.EnableWorkFlow.GetToggle();
                repository.ModifiedDate = DateTime.Today;

                DynamicService.Current.UpdateItem(repository);

                // تعیین مسیر مخزن
                var path = HttpContext.Server.MapPath($"~/CmsData/Repository/{model.RepositoryName}");
                if (FileSystem.Current.ExistsDirectory(pathOld))
                {
                    // حذف پوشه مخزن قبلی
                    var directory = FileSystem.Current.GetDirectory(pathOld);
                    directory.Delete();

                    // ایجاد پوشه مخزن
                    FileSystem.Current.AppDirectory.CreateDirectory(path);
                }

                return RedirectToAction("Index", "Home");
            }
            return View(model);
        }

        /// <summary>
        /// حذف مخزن
        /// </summary>
        /// <param name="id">شناسه مخزن: int</param>
        /// <returns>ActionResult</returns>
        [DapperAuthorize(Roles = "DeleteRepository")]
        [Admin]
        [HttpGet]
        public ActionResult Delete(int id)
        {
            var repository = DynamicService.Current.GetItem<Repository>(id);
            DynamicService.Current.DeleteItem(repository);

            // تععین مسیر مخزن
            var path = HttpContext.Server.MapPath($"~/CmsData/Repository/{repository.RepositoryName}");
            if (FileSystem.Current.ExistsDirectory(path))
            {
                // حذف پوشه مخزن
                var directory = FileSystem.Current.GetDirectory(path);
                directory.Delete();
            }

            return RedirectToAction("Index", "Home", new { area = "Repositories" });
        }

        #endregion 

    }
}