﻿namespace SAJPortal.Areas.Memberships
{

    #region Assembly

    using System.Web.Mvc;

    #endregion

    /// <summary>
    /// ثبت ناحیه عضویت
    /// </summary>
    public class MembershipsAreaRegistration : AreaRegistration 
    {

        #region Attribute

        /// <summary>
        /// نام ناحیه
        /// </summary>
        public override string AreaName => "Memberships";

        #endregion

        #region Method

        /// <summary>
        /// متد ثبت ناحیه
        /// </summary>
        /// <param name="context"></param>
        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                name: "Memberships",
                url: "admin/memberships/{action}/{id}",
                defaults: new { controller = "home", action = "index", id = UrlParameter.Optional },
                namespaces: new[] { "SAJPortal.Areas.Memberships.Controllers" }
            );

            context.MapRoute(
                name: "Roles",
                url: "admin/memberships/roles/{action}/{mid}/{id}",
                defaults: new { controller = "roles", action = "index", mid = UrlParameter.Optional, id = UrlParameter.Optional },
                namespaces: new[] { "SAJPortal.Areas.Memberships.Controllers" }
            );

            context.MapRoute(
                name: "Permissions",
                url: "admin/memberships/permissions/{action}/{mid}/{id}",
                defaults: new { controller = "permissions", action = "index", mid = UrlParameter.Optional, id = UrlParameter.Optional },
                namespaces: new[] { "SAJPortal.Areas.Memberships.Controllers" }
            );

            context.MapRoute(
                name: "RolePermissions",
                url: "admin/memberships/rolepermissions/{action}/{mid}/{id}",
                defaults: new { controller = "rolepermissions", action = "index", mid = UrlParameter.Optional, id = UrlParameter.Optional },
                namespaces: new[] { "SAJPortal.Areas.Memberships.Controllers" }
            );

            context.MapRoute(
                name: "Positions",
                url: "admin/memberships/positions/{action}/{mid}/{id}",
                defaults: new { controller = "positions", action = "index", mid = UrlParameter.Optional, id = UrlParameter.Optional },
                namespaces: new[] { "SAJPortal.Areas.Memberships.Controllers" }
            );

            context.MapRoute(
                name: "Customers",
                url: "admin/memberships/customers/{action}/{mid}/{id}",
                defaults: new { controller = "customers", action = "index", mid = UrlParameter.Optional, id = UrlParameter.Optional },
                namespaces: new[] { "SAJPortal.Areas.Memberships.Controllers" }
            );

        }

        #endregion

    }
}