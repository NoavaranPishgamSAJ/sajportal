﻿namespace SAJPortal.Areas.Memberships.Controllers
{

    #region Assembly

    using DataService;
    using ORM.Model;
    using System.Web.Mvc;
    using ViewModels;
    using System;
    using ORM.Condition;
    using AutoMapper;
    using System.Linq;
    using System.Collections.Generic;
    using CMS.Attribute;

    #endregion

    /// <summary>
    /// کنترل کننده نقش ها
    /// </summary>
    [OutputCache(VaryByParam = "*", Duration = 0, NoStore = true)]
    [LoggedOrAuthorized]
    public class RolesController : Controller
    {

        #region Attribute

        private const int PageSize = 10;

        private int PageIndex
        {
            get
            {
                if (TempData["PageIndex"] == null)
                    TempData["PageIndex"] = 1;
                return Convert.ToInt32(TempData["PageIndex"]);
            }
            set { TempData["PageIndex"] = value; }
        }

        private long PageCount
        {
            set { TempData["PageCount"] = value; }
        }

        private FilterExpressionList Criterias
        {
            get
            {
                if (TempData["Criterias"] == null)
                    TempData["Criterias"] = new FilterExpressionList();
                return TempData["Criterias"] as FilterExpressionList;
            }
            set { TempData["Criterias"] = value; }
        }

        #endregion

        #region Method

        /// <summary>
        /// لیست نقش ها
        /// </summary>
        /// <param name="mid">شناسه عضویت</param>
        /// <returns>ActionResult</returns>
        [DapperAuthorize(Roles = "RolesList")]
        public ActionResult Index(int mid)
        {
            PageIndex = 1;
            Criterias = new FilterExpressionList();
            if (!Criterias.ContainFilter("MembershipId"))
                Criterias.Add(new ORM.Condition.Filter("MembershipId", SqlOperators.Equal, mid));
            var list = DynamicService.Current.GetList<Role>(PageIndex - 1, PageSize, Criterias, string.Empty);
            PageCount = Convert.ToInt64(Math.Ceiling((double)DynamicService.Current.GetCount<Role>(Criterias, string.Empty) / PageSize));
            return View(list);
        }

        /// <summary>
        /// لیست نقش ها
        /// </summary>
        /// <param name="mid">شناسه عضویت</param>
        /// <param name="search">فیلتر لیست: string</param>
        /// <param name="formType">نوع فیلتر: string</param>
        /// <returns>ActionResult</returns>
        [DapperAuthorize(Roles = "RolesList")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index(int mid, string search, string formType)
        {
            if (formType.Equals("pager") && Request.Form.AllKeys.Contains("PageIndex"))
                PageIndex = Convert.ToInt32(Request.Form["PageIndex"]);
            else if (formType.Equals("search"))
            {
                PageIndex = 1;
                Criterias.Clear();
                Criterias.Add(new ORM.Condition.Filter("DisplayName", SqlOperators.Like, search, SqlOperand.And, DbType.NVarChar));
            }
            if (!Criterias.ContainFilter("MembershipId"))
                Criterias.Add(new ORM.Condition.Filter("MembershipId", SqlOperators.Equal, mid));
            var list = DynamicService.Current.GetList<Role>(PageIndex - 1, PageSize, Criterias, string.Empty);
            PageCount = Convert.ToInt64(Math.Ceiling((double)DynamicService.Current.GetCount<Role>(Criterias, string.Empty) / PageSize));
            return View(list);
        }

        /// <summary>
        /// فرم نقش جدید
        /// </summary>
        /// <param name="mid">شناسه عضویت: int</param>
        /// <returns>ActionResult</returns>
        [DapperAuthorize(Roles = "NewRole")]
        [HttpGet]
        public ActionResult New(int mid)
        {
            return View();
        }

        /// <summary>
        /// ثبت نقش
        /// </summary>
        /// <param name="mid">شناسه عضویت: int</param>
        /// <param name="model">مدل نقش: RoleViewModel</param>
        /// <returns>ActionResult</returns>
        [DapperAuthorize(Roles = "NewRole")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult New(int mid, RoleViewModel model)
        {
            if (ModelState.IsValid)
            {
                DynamicService.Current.CreateItem(new Role
                {
                    DisplayName = model.DisplayName,
                    MembershipId = mid,
                    AddedDate = DateTime.Today
                });
                return RedirectToAction("Index", "Roles", new { mid });
            }
            return View(model);
        }

        /// <summary>
        /// فرم ویرایش نقش
        /// </summary>
        /// <param name="mid">شناسه عضویت: int</param>
        /// <param name="id">شناسه نقش: int</param>
        /// <returns>ActionResult</returns>
        [DapperAuthorize(Roles = "EditRole")]
        [HttpGet]
        public ActionResult Edit(int mid, int id)
        {
            var role = DynamicService.Current.GetItem<Role>(id);
            var roleVm = Mapper.Map<RoleViewModel>(role);
            return View(roleVm);
        }

        /// <summary>
        /// ویرایش نقش
        /// </summary>
        /// <param name="mid">شناسه عضویت: int</param>
        /// <param name="id">شناسه نقش: int</param>
        /// <param name="model">مدل نقش: RoleViewModel</param>
        /// <returns>ActionResult</returns>
        [DapperAuthorize(Roles = "EditRole")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int mid, int id, RoleViewModel model)
        {
            if (ModelState.IsValid)
            {
                var role = DynamicService.Current.GetItem<Role>(id);
                role.DisplayName = model.DisplayName;
                role.ModifiedDate = DateTime.Today;

                DynamicService.Current.UpdateItem(role);
                return RedirectToAction("Index", "Roles", new { mid });
            }
            return View(model);
        }

        /// <summary>
        /// حذف نقش
        /// </summary>
        /// <param name="mid">شناسه عضویت: int</param>
        /// <param name="id">شناسه نقش: int</param>
        /// <returns>ActionResult</returns>
        [DapperAuthorize(Roles = "DeleteRole")]
        [HttpGet]
        public ActionResult Delete(int mid, int id)
        {
            DynamicService.Current.DeleteItem<Role>(id);

            return RedirectToAction("Index", "Roles", new { mid });
        }

        #endregion

        #region Permissions

        /// <summary>
        /// لیست مجوزها
        /// </summary>
        /// <param name="mid">شناسه عضویت: int</param>
        /// <param name="id">شناسه نقش: int</param>
        /// <returns>ActionResult</returns>
        [DapperAuthorize(Roles = "RolePermissionsList")]
        public ActionResult Permissions(int mid, int id)
        {
            return View(GetPermissionsInitialModel(mid, id));
        }

        /// <summary>
        /// لیست مجوزها
        /// </summary>
        /// <param name="mid">شناسه عضویت: int</param>
        /// <param name="id">شناسه نقش: int</param>
        /// <param name="postedPermissions">مجوزهای تعیین شده: PostedPermissions</param>
        /// <returns>ActionResult</returns>
        [DapperAuthorize(Roles = "EditRolePermission")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Permissions(int mid, int id, PostedPermissions postedPermissions)
        {
            return View(GetPermissionsModel(mid, id, postedPermissions));
        }

        /// <summary>
        /// دریافت لیست مجوزها
        /// </summary>
        /// <param name="mid">شناسه عضویت: int</param>
        /// <param name="id">شناسه کاربر: int</param>
        /// <param name="postedPermissions">مجوزهای تعیین شده: PostedPermissions</param>
        /// <returns>CustomerPermissionsViewModel</returns>
        private RolePermissionsViewModel GetPermissionsModel(int mid, int id, PostedPermissions postedPermissions)
        {
            var model = new RolePermissionsViewModel();
            var selectedPermissions = new List<RolePermissions>();
            var postedPermissionItemIds = new string[0];
            if (postedPermissions == null)
                postedPermissions = new PostedPermissions();

            Criterias = new FilterExpressionList();
            if (!Criterias.ContainFilter("MembershipId"))
                Criterias.Add(new ORM.Condition.Filter("MembershipId", SqlOperators.Equal, mid));
            var list = DynamicService.Current.GetList<Permission>(Criterias, string.Empty);

            MembershipService.Current.DeleteRolePermissions(id);

            if (postedPermissions.PermissionItemIds != null && postedPermissions.PermissionItemIds.Any())
            {
                postedPermissionItemIds = postedPermissions.PermissionItemIds;
            }

            var permissions = list as IList<Permission> ?? list.ToList();
            if (postedPermissionItemIds.Any())
            {
                foreach (var permissionId in postedPermissionItemIds)
                {
                    MembershipService.Current.AddRolePermission(id, int.Parse(permissionId));
                }
                selectedPermissions = permissions.Where(x => postedPermissionItemIds.Any(s => x.ItemId.ToString().Equals(s))).Select(p => new RolePermissions { ItemId = p.ItemId, Name = p.DisplayName, IsSelected = false }).ToList();
            }

            model.AvailablePermissions = permissions.Select(p => new RolePermissions { ItemId = p.ItemId, Name = p.DisplayName, IsSelected = false }).ToList();
            model.SelectedPermissions = selectedPermissions;
            model.PostedPermissions = postedPermissions;
            return model;
        }

        /// <summary>
        /// دریافت لیست مجوزها
        /// </summary>
        /// <param name="mid">شناسه عضویت: int</param>
        /// <param name="id">شناسه کاربر: int</param>
        /// <returns>CustomerPermissionsViewModel</returns>
        private RolePermissionsViewModel GetPermissionsInitialModel(int mid, int id)
        {
            Criterias = new FilterExpressionList();
            if (!Criterias.ContainFilter("MembershipId"))
                Criterias.Add(new ORM.Condition.Filter("MembershipId", SqlOperators.Equal, mid));
            var list = DynamicService.Current.GetList<Permission>(Criterias, string.Empty);

            var rolePermissionsList = MembershipService.Current.RolePermissions(id);

            var model = new RolePermissionsViewModel
            {
                AvailablePermissions = list.Select(p => new RolePermissions { ItemId = p.ItemId, Name = p.DisplayName, IsSelected = false }).ToList(),
                SelectedPermissions = rolePermissionsList.Select(p => new RolePermissions
                {
                    ItemId = p.PermissionId,
                    Name = p.PermissionDisplayName,
                    IsSelected = true
                }).ToList()
            };
            return model;
        }

        #endregion

    }
}