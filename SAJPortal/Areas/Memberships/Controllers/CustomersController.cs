﻿namespace SAJPortal.Areas.Memberships.Controllers
{

    #region Assembly

    using System.Collections.Generic;
    using DataService;
    using ORM.Model;
    using System.Web.Mvc;
    using ViewModels;
    using System;
    using ORM.Condition;
    using AutoMapper;
    using System.Linq;
    using ORM.Enums;
    using CMS.Attribute;

    #endregion

    /// <summary>
    /// کنترل کننده مدیریت کاربران
    /// </summary>
    [OutputCache(VaryByParam = "*", Duration = 0, NoStore = true)]
    [LoggedOrAuthorized]
    public class CustomersController : Controller
    {

        #region Attribute

        private const int PageSize = 10;

        private int PageIndex
        {
            get
            {
                if (TempData["PageIndex"] == null)
                    TempData["PageIndex"] = 1;
                return Convert.ToInt32(TempData["PageIndex"]);
            }
            set { TempData["PageIndex"] = value; }
        }

        private long PageCount
        {
            set { TempData["PageCount"] = value; }
        }

        private FilterExpressionList Criterias
        {
            get
            {
                if (TempData["Criterias"] == null)
                    TempData["Criterias"] = new FilterExpressionList();
                return TempData["Criterias"] as FilterExpressionList;
            }
            set { TempData["Criterias"] = value; }
        }

        #endregion

        #region Method

        /// <summary>
        /// لیست کاربران
        /// </summary>
        /// <param name="mid">شناسه عضویت: int</param>
        /// <returns>ActionResult</returns>
        [DapperAuthorize(Roles = "CustomersList")]
        public ActionResult Index(int mid)
        {
            PageIndex = 1;
            Criterias = new FilterExpressionList();
            if (!Criterias.ContainFilter("MembershipId"))
                Criterias.Add(new ORM.Condition.Filter("MembershipId", SqlOperators.Equal, mid));
            var list = DynamicService.Current.GetList<Customer>(PageIndex - 1, PageSize, Criterias, string.Empty);
            PageCount = Convert.ToInt64(Math.Ceiling((double)DynamicService.Current.GetCount<Customer>(Criterias, string.Empty) / PageSize));
            return View(list);
        }

        /// <summary>
        /// لیست کاربران
        /// </summary>
        /// <param name="mid">شناسه عضویت: int</param>
        /// <param name="search">فیلتر لیست: string</param>
        /// <param name="formType">نوع فیلتر: string</param>
        /// <returns>ActionResult</returns>
        [DapperAuthorize(Roles = "CustomersList")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index(int mid, string search, string formType)
        {
            if (formType.Equals("pager") && Request.Form.AllKeys.Contains("PageIndex"))
                PageIndex = Convert.ToInt32(Request.Form["PageIndex"]);
            else if (formType.Equals("search"))
            {
                PageIndex = 1;
                Criterias.Clear();
                Criterias.Add(new ORM.Condition.Filter("Username", SqlOperators.Like, search, SqlOperand.And, DbType.NVarChar));
            }
            if (!Criterias.ContainFilter("MembershipId"))
                Criterias.Add(new ORM.Condition.Filter("MembershipId", SqlOperators.Equal, mid));
            var list = DynamicService.Current.GetList<Customer>(PageIndex - 1, PageSize, Criterias, string.Empty);
            PageCount = Convert.ToInt64(Math.Ceiling((double)DynamicService.Current.GetCount<Customer>(Criterias, string.Empty) / PageSize));
            return View(list);
        }

        /// <summary>
        /// فرم کاربر جدید
        /// </summary>
        /// <param name="mid">شناسه عضویت: int</param>
        /// <returns>ActionResult</returns>
        [DapperAuthorize(Roles = "NewCustomer")]
        [HttpGet]
        public ActionResult New(int mid)
        {
            return View();
        }

        /// <summary>
        /// ثبت کاربر جدید
        /// </summary>
        /// <param name="mid">شناسه عضویت: int</param>
        /// <param name="model">مدل کاربر: CustomerViewModel</param>
        /// <returns>ActionResult</returns>
        [DapperAuthorize(Roles = "NewCustomer")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult New(int mid, CustomerViewModel model)
        {
            if (ModelState.IsValid)
            {
                DynamicService.Current.CreateItem(new Customer
                {
                    Username = model.Username,
                    Password = model.Password,
                    FirstName = model.FirstName,
                    LastName = model.LastName,
                    FatherName = model.FatherName,
                    NationalCode = model.NationalCode,
                    IdentificationNumber = model.IdentificationNumber,
                    IsAdmin = model.IsAdmin.GetToggle(),
                    BoycottUser = model.BoycottUser.GetToggle(),
                    UseOperationDate = model.UseOperationDate.GetToggle(),
                    OperationDate = model.OperationDate,
                    MembershipId = mid,
                    LastLoginDate = null,
                    EmailConfirmed = Toggle.False,
                    PhoneNumberConfirmed = Toggle.False,
                    PasswordQuestion = "PasswordQuestion",
                    PasswordAnswer = "PasswordAnswer",
                    IsApproved = Toggle.False,
                    AccessFailedCount = 0,
                    AddedDate = DateTime.Today
                });
                return RedirectToAction("Index", "Customers", new { mid });
            }
            return View(model);
        }

        /// <summary>
        /// فرم ویرایش کاربر
        /// </summary>
        /// <param name="mid">شناسه عضویت: int</param>
        /// <param name="id">شناسه کاربر: int</param>
        /// <returns>ActionResult</returns>
        [DapperAuthorize(Roles = "EditCustomer")]
        [HttpGet]
        public ActionResult Edit(int mid, int id)
        {
            var customer = DynamicService.Current.GetItem<Customer>(id);
            var customerVm = Mapper.Map<CustomerViewModel>(customer);
            return View(customerVm);
        }

        /// <summary>
        /// ویرایش کاربر
        /// </summary>
        /// <param name="mid">شناسه عضویت: int</param>
        /// <param name="id">شناسه کاربر: int</param>
        /// <param name="model">مدل کاربر: CustomerViewModel</param>
        /// <returns>ActionResult</returns>
        [DapperAuthorize(Roles = "EditCustomer")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int mid, int id, CustomerViewModel model)
        {
            if (ModelState.IsValid)
            {
                var customer = DynamicService.Current.GetItem<Customer>(id);
                customer.Username = model.Username;
                customer.Password = model.Password;
                customer.FirstName = model.FirstName;
                customer.LastName = model.LastName;
                customer.FatherName = model.FatherName;
                customer.NationalCode = model.NationalCode;
                customer.IdentificationNumber = model.IdentificationNumber;
                customer.IsAdmin = model.IsAdmin.GetToggle();
                customer.BoycottUser = model.BoycottUser.GetToggle();
                customer.UseOperationDate = model.UseOperationDate.GetToggle();
                customer.OperationDate = model.OperationDate;
                customer.ModifiedDate = DateTime.Today;

                DynamicService.Current.UpdateItem(customer);
                return RedirectToAction("Index", "Customers", new { mid });
            }
            return View(model);
        }

        /// <summary>
        /// حذف کاربر
        /// </summary>
        /// <param name="mid">شناسه عضویت: int</param>
        /// <param name="id">شناسه کاربر: int</param>
        /// <returns>ActionResult</returns>
        [DapperAuthorize(Roles = "DeleteCustomer")]
        [HttpGet]
        public ActionResult Delete(int mid, int id)
        {
            DynamicService.Current.DeleteItem<Customer>(id);

            return RedirectToAction("Index", "Customers", new { mid });
        }

        #endregion 

        #region Roles

        /// <summary>
        /// لیست نقش های کاربر
        /// </summary>
        /// <param name="mid">شناسه عضویت: int</param>
        /// <param name="id">شناسه کاربر: int</param>
        /// <returns>ActionResult</returns>
        [DapperAuthorize(Roles = "CustomerRolesList")]
        public ActionResult Roles(int mid, int id)
        {
            return View(GetRolesInitialModel(mid, id));
        }

        /// <summary>
        /// ویرایش نقش های کاربر
        /// </summary>
        /// <param name="mid">شناسه عضویت: int</param>
        /// <param name="id">شناسه کاربر: int</param>
        /// <param name="postedRoles">نقش های تعیین شده: PostedRoles</param>
        /// <returns>ActionResult</returns>
        [DapperAuthorize(Roles = "EditCustomerRole")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Roles(int mid, int id, PostedRoles postedRoles)
        {
            return View(GetRolesModel(mid, id, postedRoles));
        }

        /// <summary>
        /// دریافت لیست نقش ها
        /// </summary>
        /// <param name="mid">شناسه عضویت: int</param>
        /// <param name="id">شناسه کاربر: int</param>
        /// <param name="postedRoles">نقش های تعیین شده: PostedRoles</param>
        /// <returns>CustomerRolesViewModel</returns>
        private CustomerRolesViewModel GetRolesModel(int mid, int id, PostedRoles postedRoles)
        {
            var model = new CustomerRolesViewModel();
            var selectedRoles = new List<CustomerRoles>();
            var postedRoleItemIds = new string[0];
            if (postedRoles == null)
                postedRoles = new PostedRoles();

            Criterias = new FilterExpressionList();
            if (!Criterias.ContainFilter("MembershipId"))
                Criterias.Add(new ORM.Condition.Filter("MembershipId", SqlOperators.Equal, mid));
            var list = DynamicService.Current.GetList<Role>(Criterias, string.Empty);

            MembershipService.Current.DeleteCustomerRoles(id);

            if (postedRoles.RoleItemIds != null && postedRoles.RoleItemIds.Any())
            {
                postedRoleItemIds = postedRoles.RoleItemIds;
            }

            var roles = list as IList<Role> ?? list.ToList();
            if (postedRoleItemIds.Any())
            {
                foreach (var roleId in postedRoleItemIds)
                {
                    MembershipService.Current.AddUserRole(id, int.Parse(roleId));
                }
                selectedRoles = roles.Where(x => postedRoleItemIds.Any(s => x.ItemId.ToString().Equals(s))).Select(p => new CustomerRoles { ItemId = p.ItemId, Name = p.DisplayName, IsSelected = false }).ToList();
            }

            model.AvailableRoles = roles.Select(p => new CustomerRoles { ItemId = p.ItemId, Name = p.DisplayName, IsSelected = false }).ToList();
            model.SelectedRoles = selectedRoles;
            model.PostedRoles = postedRoles;
            return model;
        }

        /// <summary>
        /// دریافت لیست نقش ها
        /// </summary>
        /// <param name="mid">شناسه عضویت: int</param>
        /// <param name="id">شناسه کاربر: int</param>
        /// <returns>CustomerRolesViewModel</returns>
        private CustomerRolesViewModel GetRolesInitialModel(int mid, int id)
        {
            Criterias = new FilterExpressionList();
            if (!Criterias.ContainFilter("MembershipId"))
                Criterias.Add(new ORM.Condition.Filter("MembershipId", SqlOperators.Equal, mid));
            var list = DynamicService.Current.GetList<Role>(Criterias, string.Empty);

            var roleRolesList = MembershipService.Current.UserRoles(id);

            var model = new CustomerRolesViewModel
            {
                AvailableRoles = list.Select(p => new CustomerRoles { ItemId = p.ItemId, Name = p.DisplayName, IsSelected = false }).ToList(),
                SelectedRoles = roleRolesList.Select(p => new CustomerRoles
                {
                    ItemId = p.RoleId,
                    Name = p.RoleDisplayName,
                    IsSelected = true
                }).ToList()
            };
            return model;
        }

        #endregion

        #region Positions

        /// <summary>
        /// لیست سمت های کاربر
        /// </summary>
        /// <param name="mid">شناسه عضویت: int</param>
        /// <param name="id">شناسه کاربر: int</param>
        /// <returns>ActionResult</returns>
        [DapperAuthorize(Roles = "CustomerPositionsList")]
        public ActionResult Positions(int mid, int id)
        {
            return View(GetPositionsInitialModel(mid, id));
        }

        /// <summary>
        /// ویرایش سمت های کاربر
        /// </summary>
        /// <param name="mid">شناسه عضویت: int</param>
        /// <param name="id">شناسه کاربر: int</param>
        /// <param name="postedPositions">سمت های تعیین شده: PostedPositions</param>
        /// <returns>ActionResult</returns>
        [DapperAuthorize(Roles = "EditCustomerPosition")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Positions(int mid, int id, PostedPositions postedPositions)
        {
            return View(GetPositionsModel(mid, id, postedPositions));
        }

        /// <summary>
        /// دریافت لیست سمت ها
        /// </summary>
        /// <param name="mid">شناسه عضویت: int</param>
        /// <param name="id">شناسه کاربر: int</param>
        /// <param name="postedPositions">سمت های تعیین شده: PostedPositions</param>
        /// <returns>CustomerPositionsViewModel</returns>
        private CustomerPositionsViewModel GetPositionsModel(int mid, int id, PostedPositions postedPositions)
        {
            var model = new CustomerPositionsViewModel();
            var selectedPositions = new List<CustomerPositions>();
            var postedPositionItemIds = new string[0];
            if (postedPositions == null)
                postedPositions = new PostedPositions();

            Criterias = new FilterExpressionList();
            if (!Criterias.ContainFilter("MembershipId"))
                Criterias.Add(new ORM.Condition.Filter("MembershipId", SqlOperators.Equal, mid));
            var list = DynamicService.Current.GetList<Position>(Criterias, string.Empty);

            MembershipService.Current.DeleteCustomerPositions(id);

            if (postedPositions.PositionItemIds != null && postedPositions.PositionItemIds.Any())
            {
                postedPositionItemIds = postedPositions.PositionItemIds;
            }

            var positions = list as IList<Position> ?? list.ToList();
            if (postedPositionItemIds.Any())
            {
                foreach (var positionId in postedPositionItemIds)
                {
                    MembershipService.Current.AddUserPosition(id, int.Parse(positionId));
                }
                selectedPositions = positions.Where(x => postedPositionItemIds.Any(s => x.ItemId.ToString().Equals(s))).Select(p => new CustomerPositions { ItemId = p.ItemId, Name = p.DisplayName, IsSelected = false }).ToList();
            }

            model.AvailablePositions = positions.Select(p => new CustomerPositions { ItemId = p.ItemId, Name = p.DisplayName, IsSelected = false }).ToList();
            model.SelectedPositions = selectedPositions;
            model.PostedPositions = postedPositions;
            return model;
        }

        /// <summary>
        /// دریافت لیست سمت ها
        /// </summary>
        /// <param name="mid">شناسه عضویت: int</param>
        /// <param name="id">شناسه کاربر: int</param>
        /// <returns>CustomerPositionsViewModel</returns>
        private CustomerPositionsViewModel GetPositionsInitialModel(int mid, int id)
        {
            Criterias = new FilterExpressionList();
            if (!Criterias.ContainFilter("MembershipId"))
                Criterias.Add(new ORM.Condition.Filter("MembershipId", SqlOperators.Equal, mid));
            var list = DynamicService.Current.GetList<Position>(Criterias, string.Empty);

            var positionPositionsList = MembershipService.Current.UserPositions(id);

            var model = new CustomerPositionsViewModel
            {
                AvailablePositions = list.Select(p => new CustomerPositions { ItemId = p.ItemId, Name = p.DisplayName, IsSelected = false }).ToList(),
                SelectedPositions = positionPositionsList.Select(p => new CustomerPositions
                {
                    ItemId = p.PositionId,
                    Name = p.PositionDisplayName,
                    IsSelected = true
                }).ToList()
            };
            return model;
        }

        #endregion

        #region Permissions

        /// <summary>
        /// لیست مجوزهای کاربر
        /// </summary>
        /// <param name="mid">شناسه عضویت: int</param>
        /// <param name="id">شناسه کاربر: int</param>
        /// <returns>ActionResult</returns>
        [DapperAuthorize(Roles = "CustomerPermissionsList")]
        public ActionResult Permissions(int mid, int id)
        {
            return View(GetPermissionsInitialModel(mid, id));
        }

        /// <summary>
        /// ویرایش مجوزهای کاربر
        /// </summary>
        /// <param name="mid">شناسه عضویت: int</param>
        /// <param name="id">شناسه کاربر: int</param>
        /// <param name="postedPermissions">مجوزهای تعیین شده: PostedPermissions</param>
        /// <returns>ActionResult</returns>
        [DapperAuthorize(Roles = "EditCustomerPermission")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Permissions(int mid, int id, PostedPermissions postedPermissions)
        {
            return View(GetPermissionsModel(mid, id, postedPermissions));
        }

        /// <summary>
        /// دریافت لیست مجوزها
        /// </summary>
        /// <param name="mid">شناسه عضویت: int</param>
        /// <param name="id">شناسه کاربر: int</param>
        /// <param name="postedPermissions">مجوزهای تعیین شده: PostedPermissions</param>
        /// <returns>CustomerPermissionsViewModel</returns>
        private CustomerPermissionsViewModel GetPermissionsModel(int mid, int id, PostedPermissions postedPermissions)
        {
            var model = new CustomerPermissionsViewModel();
            var selectedPermissions = new List<ViewModels.CustomerPermissions>();
            var postedPermissionItemIds = new string[0];
            if (postedPermissions == null)
                postedPermissions = new PostedPermissions();

            Criterias = new FilterExpressionList();
            if (!Criterias.ContainFilter("MembershipId"))
                Criterias.Add(new ORM.Condition.Filter("MembershipId", SqlOperators.Equal, mid));
            var list = DynamicService.Current.GetList<Permission>(Criterias, string.Empty);

            if (postedPermissions.PermissionItemIds != null && postedPermissions.PermissionItemIds.Any())
            {
                postedPermissionItemIds = postedPermissions.PermissionItemIds;
            }

            var permissions = list as IList<Permission> ?? list.ToList();
            foreach (var permission in permissions)
            {
                if (postedPermissionItemIds.Contains(permission.ItemId.ToString()))
                {
                    if (!MembershipService.Current.UserHasPermission(id, permission.ItemId))
                        MembershipService.Current.AddUserPermission(id, permission.ItemId);
                }
                else
                {
                    if (MembershipService.Current.UserHasPermission(id, permission.ItemId))
                        MembershipService.Current.DeleteUserPermission(id, permission.ItemId);
                }
            }
            if (postedPermissionItemIds.Any())
            {
                selectedPermissions = permissions.Where(x => postedPermissionItemIds.Any(s => x.ItemId.ToString().Equals(s))).Select(p => new ViewModels.CustomerPermissions { ItemId = p.ItemId, Name = p.DisplayName, IsSelected = false }).ToList();
            }

            model.AvailablePermissions = permissions.Select(p => new ViewModels.CustomerPermissions { ItemId = p.ItemId, Name = p.DisplayName, IsSelected = false }).ToList();
            model.SelectedPermissions = selectedPermissions;
            model.PostedPermissions = postedPermissions;
            return model;
        }

        /// <summary>
        /// دریافت لیست مجوزها
        /// </summary>
        /// <param name="mid">شناسه عضویت: int</param>
        /// <param name="id">شناسه کاربر: int</param>
        /// <returns>CustomerPermissionsViewModel</returns>
        private CustomerPermissionsViewModel GetPermissionsInitialModel(int mid, int id)
        {
            Criterias = new FilterExpressionList();
            if (!Criterias.ContainFilter("MembershipId"))
                Criterias.Add(new ORM.Condition.Filter("MembershipId", SqlOperators.Equal, mid));
            var list = DynamicService.Current.GetList<Permission>(Criterias, string.Empty);

            var permissionPermissionsList = MembershipService.Current.UserExistPermissions(id);

            var model = new CustomerPermissionsViewModel
            {
                AvailablePermissions = list.Select(p => new ViewModels.CustomerPermissions { ItemId = p.ItemId, Name = p.DisplayName, IsSelected = false }).ToList(),
                SelectedPermissions = permissionPermissionsList.Select(p => new ViewModels.CustomerPermissions
                {
                    ItemId = p.PermissionId,
                    Name = p.DisplayName,
                    IsSelected = true
                }).ToList()
            };
            return model;
        }

        #endregion

    }
}