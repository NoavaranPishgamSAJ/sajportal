﻿namespace SAJPortal.Areas.Memberships.Controllers
{

    #region Assembly

    using DataService;
    using ORM.Model;
    using System.Web.Mvc;
    using ViewModels;
    using System;
    using ORM.Condition;
    using AutoMapper;
    using System.Linq;
    using CMS.Attribute;
    using CMS.Helper;

    #endregion

    /// <summary>
    /// کنترل کننده مدیریت عضویت ها
    /// </summary>
    [OutputCache(VaryByParam = "*", Duration = 0, NoStore = true)]
    [LoggedOrAuthorized]
    public class HomeController : Controller
    {

        #region Attribute

        private const int PageSize = 10;

        private int PageIndex
        {
            get
            {
                if (TempData["PageIndex"] == null)
                    TempData["PageIndex"] = 1;
                return Convert.ToInt32(TempData["PageIndex"]);
            }
            set { TempData["PageIndex"] = value; }
        }

        private long PageCount
        {
            set { TempData["PageCount"] = value; }
        }

        private FilterExpressionList Criterias
        {
            get
            {
                if (TempData["Criterias"] == null)
                    TempData["Criterias"] = new FilterExpressionList();
                return TempData["Criterias"] as FilterExpressionList;
            }
            set { TempData["Criterias"] = value; }
        }

        #endregion

        #region Method

        /// <summary>
        /// لیست عضویت ها
        /// </summary>
        /// <returns>ActionResult</returns>
        [DapperAuthorize(Roles = "MembershipsList")]
        public ActionResult Index()
        {
            PageIndex = 1;
            Criterias = new FilterExpressionList();
            if (!User.IsInRole("IsAdmin") && !Criterias.ContainFilter("ItemId"))
            {
                Criterias.Add(new ORM.Condition.Filter("ItemId", SqlOperators.Equal, ModuleHelper.GetBaseSite().MembershipId));
            }
            var list = DynamicService.Current.GetList<Membership>(PageIndex - 1, PageSize, Criterias, string.Empty);
            PageCount = Convert.ToInt64(Math.Ceiling((double)DynamicService.Current.GetCount<Membership>(Criterias, string.Empty) / PageSize));
            return View(list);
        }

        /// <summary>
        /// لیست عضویت ها
        /// </summary>
        /// <param name="search">فیلتر لیست: string</param>
        /// <param name="formType">نوع فیلتر: string</param>
        /// <returns>ActionResult</returns>
        [DapperAuthorize(Roles = "MembershipsList")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index(string search, string formType)
        {
            if (formType.Equals("pager") && Request.Form.AllKeys.Contains("PageIndex"))
                PageIndex = Convert.ToInt32(Request.Form["PageIndex"]);
            else if (formType.Equals("search"))
            {
                PageIndex = 1;
                Criterias.Clear();
                Criterias.Add(new ORM.Condition.Filter("MembershipName", SqlOperators.Like, search, SqlOperand.And, DbType.NVarChar));
            }
            if (!User.IsInRole("IsAdmin") && !Criterias.ContainFilter("ItemId"))
            {
                Criterias.Add(new ORM.Condition.Filter("ItemId", SqlOperators.Equal, ModuleHelper.GetBaseSite().MembershipId));
            }
            var list = DynamicService.Current.GetList<Membership>(PageIndex - 1, PageSize, Criterias, string.Empty);
            PageCount = Convert.ToInt64(Math.Ceiling((double)DynamicService.Current.GetCount<Membership>(Criterias, string.Empty) / PageSize));
            return View(list);
        }

        /// <summary>
        /// فرم عضویت جدید
        /// </summary>
        /// <returns>ActionResult</returns>
        [DapperAuthorize(Roles = "NewMembership")]
        [Admin]
        [HttpGet]
        public ActionResult New()
        {
            return View();
        }

        /// <summary>
        /// ثبت عضویت جدید
        /// </summary>
        /// <param name="model">مدل عضویت: MembershipViewModel</param>
        /// <returns>ActionResult</returns>
        [DapperAuthorize(Roles = "NewMembership")]
        [Admin]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult New(MembershipViewModel model)
        {
            if (ModelState.IsValid)
            {
                DynamicService.Current.CreateItem(new Membership
                {
                    MembershipName = model.MembershipName,
                    AddedDate = DateTime.Today
                });

                return RedirectToAction("Index", "Home");
            }
            return View(model);
        }

        /// <summary>
        /// فرم ویرایش عضویت
        /// </summary>
        /// <param name="id">شناسه عضویت: int</param>
        /// <returns>ActionResult</returns>
        [DapperAuthorize(Roles = "EditMembership")]
        [HttpGet]
        public ActionResult Edit(int id)
        {
            var membership = DynamicService.Current.GetItem<Membership>(id);
            var membershipVm = Mapper.Map<MembershipViewModel>(membership);
            return View(membershipVm);
        }

        /// <summary>
        /// ویرایش عضویت
        /// </summary>
        /// <param name="id">شناسه عضویت: int</param>
        /// <param name="model">مدل عضویت: MembershipViewModel</param>
        /// <returns>ActionResult</returns>
        [DapperAuthorize(Roles = "EditMembership")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, MembershipViewModel model)
        {
            if (id != 1)
            {
                if (ModelState.IsValid)
                {
                    var membership = DynamicService.Current.GetItem<Membership>(id);
                    membership.MembershipName = model.MembershipName;
                    membership.ModifiedDate = DateTime.Today;

                    DynamicService.Current.UpdateItem(membership);
                    return RedirectToAction("Index", "Home");
                }
            }
            return View(model);
        }

        /// <summary>
        /// حذف عضویت
        /// </summary>
        /// <param name="id">شناسه عضویت: int</param>
        /// <returns>ActionResult</returns>
        [DapperAuthorize(Roles = "DeleteMembership")]
        [Admin]
        [HttpGet]
        public ActionResult Delete(int id)
        {
            if (id != 1)
            {
                var membership = DynamicService.Current.GetItem<Membership>(id);
                DynamicService.Current.DeleteItem(membership);
            }
            return RedirectToAction("Index", "Home", new { area = "Memberships" });
        }

        #endregion 

    }
}