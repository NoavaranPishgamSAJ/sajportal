﻿namespace SAJPortal.Areas.Memberships.Controllers
{

    #region Assembly

    using DataService;
    using ORM.Model;
    using System.Web.Mvc;
    using ViewModels;
    using System;
    using ORM.Condition;
    using AutoMapper;
    using System.Linq;
    using CMS.Attribute;

    #endregion

    /// <summary>
    /// کنترل کننده سمت ها
    /// </summary>
    [OutputCache(VaryByParam = "*", Duration = 0, NoStore = true)]
    [LoggedOrAuthorized]
    public class PositionsController : Controller
    {

        #region Attribute

        private const int PageSize = 10;

        private int PageIndex
        {
            get
            {
                if (TempData["PageIndex"] == null)
                    TempData["PageIndex"] = 1;
                return Convert.ToInt32(TempData["PageIndex"]);
            }
            set { TempData["PageIndex"] = value; }
        }

        private long PageCount
        {
            set { TempData["PageCount"] = value; }
        }

        private FilterExpressionList Criterias
        {
            get
            {
                if (TempData["Criterias"] == null)
                    TempData["Criterias"] = new FilterExpressionList();
                return TempData["Criterias"] as FilterExpressionList;
            }
            set { TempData["Criterias"] = value; }
        }

        #endregion

        #region Method

        /// <summary>
        /// لیست سمت ها
        /// </summary>
        /// <param name="mid">شناسه عضویت</param>
        /// <returns>ActionResult</returns>
        [DapperAuthorize(Roles = "PositionsList")]
        public ActionResult Index(int mid)
        {
            PageIndex = 1;
            Criterias = new FilterExpressionList();
            if (!Criterias.ContainFilter("MembershipId"))
                Criterias.Add(new ORM.Condition.Filter("MembershipId", SqlOperators.Equal, mid));
            var list = DynamicService.Current.GetList<Position>(PageIndex - 1, PageSize, Criterias, string.Empty);
            PageCount = Convert.ToInt64(Math.Ceiling((double)DynamicService.Current.GetCount<Position>(Criterias, string.Empty) / PageSize));
            return View(list);
        }

        /// <summary>
        /// لیست سمت ها
        /// </summary>
        /// <param name="mid">شناسه عضویت</param>
        /// <param name="search">فیلتر لیست: string</param>
        /// <param name="formType">نوع فیلتر: string</param>
        /// <returns>ActionResult</returns>
        [DapperAuthorize(Roles = "PositionsList")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index(int mid, string search, string formType)
        {
            if (formType.Equals("pager") && Request.Form.AllKeys.Contains("PageIndex"))
                PageIndex = Convert.ToInt32(Request.Form["PageIndex"]);
            else if (formType.Equals("search"))
            {
                PageIndex = 1;
                Criterias.Clear();
                Criterias.Add(new ORM.Condition.Filter("DisplayName", SqlOperators.Like, search, SqlOperand.And, DbType.NVarChar));
            }
            if (!Criterias.ContainFilter("MembershipId"))
                Criterias.Add(new ORM.Condition.Filter("MembershipId", SqlOperators.Equal, mid));
            var list = DynamicService.Current.GetList<Position>(PageIndex - 1, PageSize, Criterias, string.Empty);
            PageCount = Convert.ToInt64(Math.Ceiling((double)DynamicService.Current.GetCount<Position>(Criterias, string.Empty) / PageSize));
            return View(list);
        }

        /// <summary>
        /// فرم سمت جدید
        /// </summary>
        /// <param name="mid">شناسه عضویت: int</param>
        /// <returns>ActionResult</returns>
        [DapperAuthorize(Roles = "NewPosition")]
        [HttpGet]
        public ActionResult New(int mid)
        {
            return View();
        }

        /// <summary>
        /// ثبت سمت
        /// </summary>
        /// <param name="mid">شناسه عضویت: int</param>
        /// <param name="model">مدل سمت: PositionViewModel</param>
        /// <returns>ActionResult</returns>
        [DapperAuthorize(Roles = "NewPosition")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult New(int mid, PositionViewModel model)
        {
            if (ModelState.IsValid)
            {
                DynamicService.Current.CreateItem(new Position
                {
                    DisplayName = model.DisplayName,
                    MembershipId = mid,
                    AddedDate = DateTime.Today
                });
                return RedirectToAction("Index", "Positions", new { mid });
            }
            return View(model);
        }

        /// <summary>
        /// فرم ویرایش سمت
        /// </summary>
        /// <param name="mid">شناسه عضویت: int</param>
        /// <param name="id">شناسه سمت: int</param>
        /// <returns>ActionResult</returns>
        [DapperAuthorize(Roles = "EditPosition")]
        [HttpGet]
        public ActionResult Edit(int mid, int id)
        {
            var position = DynamicService.Current.GetItem<Position>(id);
            var positionVm = Mapper.Map<PositionViewModel>(position);
            return View(positionVm);
        }

        /// <summary>
        /// ویرایش سمت
        /// </summary>
        /// <param name="mid">شناسه عضویت: int</param>
        /// <param name="id">شناسه سمت: int</param>
        /// <param name="model">مدل سمت: PositionViewModel</param>
        /// <returns>ActionResult</returns>
        [DapperAuthorize(Roles = "EditPosition")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int mid, int id, PositionViewModel model)
        {
            if (ModelState.IsValid)
            {
                var position = DynamicService.Current.GetItem<Position>(id);
                position.DisplayName = model.DisplayName;
                position.ModifiedDate = DateTime.Today;

                DynamicService.Current.UpdateItem(position);
                return RedirectToAction("Index", "Positions", new { mid });
            }
            return View(model);
        }

        /// <summary>
        /// حذف سمت
        /// </summary>
        /// <param name="mid">شناسه عضویت: int</param>
        /// <param name="id">شناسه سمت: int</param>
        /// <returns>ActionResult</returns>
        [DapperAuthorize(Roles = "DeletePosition")]
        [HttpGet]
        public ActionResult Delete(int mid, int id)
        {
            DynamicService.Current.DeleteItem<Position>(id);

            return RedirectToAction("Index", "Positions", new { mid });
        }

        #endregion 

    }
}