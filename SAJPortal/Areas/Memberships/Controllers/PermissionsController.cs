﻿namespace SAJPortal.Areas.Memberships.Controllers
{

    #region Assembly

    using DataService;
    using ORM.Model;
    using System.Web.Mvc;
    using ViewModels;
    using System;
    using ORM.Condition;
    using AutoMapper;
    using System.Linq;
    using CMS.Attribute;

    #endregion

    /// <summary>
    /// کنترل کننده مدیریت مجوزها
    /// </summary>
    [OutputCache(VaryByParam = "*", Duration = 0, NoStore = true)]
    [LoggedOrAuthorized]
    public class PermissionsController : Controller
    {

        #region Attribute

        private const int PageSize = 10;

        private int PageIndex
        {
            get
            {
                if (TempData["PageIndex"] == null)
                    TempData["PageIndex"] = 1;
                return Convert.ToInt32(TempData["PageIndex"]);
            }
            set { TempData["PageIndex"] = value; }
        }

        private long PageCount
        {
            set { TempData["PageCount"] = value; }
        }

        private FilterExpressionList Criterias
        {
            get
            {
                if (TempData["Criterias"] == null)
                    TempData["Criterias"] = new FilterExpressionList();
                return TempData["Criterias"] as FilterExpressionList;
            }
            set { TempData["Criterias"] = value; }
        }

        #endregion

        #region Method

        /// <summary>
        /// لیست مجوزها
        /// </summary>
        /// <param name="mid">شناسه عضویت: int</param>
        /// <returns>ActionResult</returns>
        [DapperAuthorize(Roles = "PermissionsList")]
        public ActionResult Index(int mid)
        {
            PageIndex = 1;
            Criterias = new FilterExpressionList();
            if (!Criterias.ContainFilter("MembershipId"))
                Criterias.Add(new ORM.Condition.Filter("MembershipId", SqlOperators.Equal, mid));
            var list = DynamicService.Current.GetList<Permission>(PageIndex - 1, PageSize, Criterias, string.Empty);
            PageCount = Convert.ToInt64(Math.Ceiling((double)DynamicService.Current.GetCount<Permission>(Criterias, string.Empty) / PageSize));
            return View(list);
        }

        /// <summary>
        /// لیست مجوزها
        /// </summary>
        /// <param name="mid">شناسه عضویت: int</param>
        /// <param name="search">فیلتر لیست: string</param>
        /// <param name="formType">نوع فیلتر: string</param>
        /// <returns>ActionResult</returns>
        [DapperAuthorize(Roles = "PermissionsList")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index(int mid, string search, string formType)
        {
            if (formType.Equals("pager") && Request.Form.AllKeys.Contains("PageIndex"))
                PageIndex = Convert.ToInt32(Request.Form["PageIndex"]);
            else if (formType.Equals("search"))
            {
                PageIndex = 1;
                Criterias.Clear();
                Criterias.Add(new OrFilter(
                    new ORM.Condition.Filter("DisplayName", SqlOperators.Like, search, SqlOperand.And, DbType.NVarChar),
                    new ORM.Condition.Filter("DevName", SqlOperators.Like, search, SqlOperand.And, DbType.NVarChar)
                    ));
            }
            if (!Criterias.ContainFilter("MembershipId"))
                Criterias.Add(new ORM.Condition.Filter("MembershipId", SqlOperators.Equal, mid));
            var list = DynamicService.Current.GetList<Permission>(PageIndex - 1, PageSize, Criterias, string.Empty);
            PageCount = Convert.ToInt64(Math.Ceiling((double)DynamicService.Current.GetCount<Permission>(Criterias, string.Empty) / PageSize));
            return View(list);
        }

        /// <summary>
        /// فرم مجوز جدید
        /// </summary>
        /// <param name="mid">شناسه عضویت: int</param>
        /// <returns>ActionResult</returns>
        [DapperAuthorize(Roles = "NewPermission")]
        [HttpGet]
        public ActionResult New(int mid)
        {
            return View();
        }

        /// <summary>
        /// ثبت مجوز جدید
        /// </summary>
        /// <param name="mid">شناسه عضویت: int</param>
        /// <param name="model">مدل مجوز: PermissionViewModel</param>
        /// <returns>ActionResult</returns>
        [DapperAuthorize(Roles = "NewPermission")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult New(int mid, PermissionViewModel model)
        {
            if (ModelState.IsValid)
            {
                DynamicService.Current.CreateItem(new Permission
                {
                    DisplayName = model.DisplayName,
                    DevName = model.DevName,
                    MembershipId = mid,
                    AddedDate = DateTime.Today
                });
                return RedirectToAction("Index", "Permissions", new { mid });
            }
            return View(model);
        }

        /// <summary>
        /// فرم ویرایش مجوز
        /// </summary>
        /// <param name="mid">شناسه عضویت: int</param>
        /// <param name="id">شناسه مجوز: int</param>
        /// <returns>ActionResult</returns>
        [DapperAuthorize(Roles = "EditPermission")]
        [HttpGet]
        public ActionResult Edit(int mid, int id)
        {
            var permission = DynamicService.Current.GetItem<Permission>(id);
            var permissionVm = Mapper.Map<PermissionViewModel>(permission);
            return View(permissionVm);
        }

        /// <summary>
        /// ویرایش مجوز
        /// </summary>
        /// <param name="mid">شناسه عضویت: int</param>
        /// <param name="id">شناسه مجوز: int</param>
        /// <param name="model">مدل مجوز: PermissionViewModel</param>
        /// <returns>ActionResult</returns>
        [DapperAuthorize(Roles = "EditPermission")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int mid, int id, PermissionViewModel model)
        {
            if (ModelState.IsValid)
            {
                var permission = DynamicService.Current.GetItem<Permission>(id);
                permission.DisplayName = model.DisplayName;
                permission.DevName = model.DevName;
                permission.ModifiedDate = DateTime.Today;

                DynamicService.Current.UpdateItem(permission);
                return RedirectToAction("Index", "Permissions", new { mid });
            }
            return View(model);
        }

        /// <summary>
        /// حذف مجوز
        /// </summary>
        /// <param name="mid">شناسه عضویت: int</param>
        /// <param name="id">شناسه مجوز: int</param>
        /// <returns>ActionResult</returns>
        [DapperAuthorize(Roles = "DeletePermission")]
        [HttpGet]
        public ActionResult Delete(int mid, int id)
        {
            DynamicService.Current.DeleteItem<Permission>(id);

            return RedirectToAction("Index", "Permissions", new { mid });
        }

        #endregion 

    }
}