﻿namespace SAJPortal.Areas.Memberships.ViewModels
{

    #region Assembly

    using System.Collections.Generic;

    #endregion

    /// <summary>
    /// مدل مجوز های کاربر
    /// </summary>
    public class CustomerPermissionsViewModel
    {

        /// <summary>
        /// لیست مجوزهای موجود
        /// </summary>
        public List<CustomerPermissions> AvailablePermissions { get; set; }

        /// <summary>
        /// لیست مجوزهای فعال
        /// </summary>
        public List<CustomerPermissions> SelectedPermissions { get; set; }

        /// <summary>
        /// لیست مجوزهای انتخاب شده
        /// </summary>
        public PostedPermissions PostedPermissions { get; set; }

    }

    /// <summary>
    /// مجوزهای کاربر
    /// </summary>
    public class CustomerPermissions
    {

        /// <summary>
        /// شناسه
        /// </summary>
        public int ItemId { get; set; }

        /// <summary>
        /// نام
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// انتخاب شده یا نه
        /// </summary>
        public bool IsSelected { get; set; }

    }

}