﻿namespace SAJPortal.Areas.Memberships.ViewModels
{

    #region Assembly

    using System.Collections.Generic;

    #endregion

    /// <summary>
    /// مدل نقش های کاربر
    /// </summary>
    public class CustomerRolesViewModel
    {

        /// <summary>
        /// لیست نقش های موجود
        /// </summary>
        public List<CustomerRoles> AvailableRoles { get; set; }

        /// <summary>
        /// لیست نقش های فعال
        /// </summary>
        public List<CustomerRoles> SelectedRoles { get; set; }

        /// <summary>
        /// لیست نقش های انتخاب شده
        /// </summary>
        public PostedRoles PostedRoles { get; set; }

    }

    /// <summary>
    /// نقش های انتخاب شده
    /// </summary>
    public class PostedRoles
    {
        /// <summary>
        /// شناسه نقش ها
        /// </summary>
        public string[] RoleItemIds { get; set; }
    }

    /// <summary>
    /// نقش های کاربر
    /// </summary>
    public class CustomerRoles
    {

        /// <summary>
        /// شناسه
        /// </summary>
        public int ItemId { get; set; }

        /// <summary>
        /// نام
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// انتخاب شده یا نه
        /// </summary>
        public bool IsSelected { get; set; }

    }

}