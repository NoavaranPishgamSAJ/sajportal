﻿namespace SAJPortal.Areas.Memberships.ViewModels
{

    #region Assembly

    using System.ComponentModel.DataAnnotations;
    using Resources;
    using System;

    #endregion

    /// <summary>
    /// مدل کاربران
    /// </summary>
    public class CustomerViewModel
    {

        /// <summary>
        /// شناسه
        /// </summary>
        public int ItemId { get; set; }

        /// <summary>
        /// نام کاربری
        /// </summary>
        [Required(ErrorMessageResourceType = typeof(Global), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(Global), Name = "Username")]
        public string Username { get; set; }

        /// <summary>
        /// رمزعبور
        /// </summary>
        [Required(ErrorMessageResourceType = typeof(Global), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(Global), Name = "Password")]
        public string Password { get; set; }

        /// <summary>
        /// نام
        /// </summary>
        [Display(ResourceType = typeof(Global), Name = "FirstName")]
        public string FirstName { get; set; }

        /// <summary>
        /// نام خانوادگی
        /// </summary>
        [Display(ResourceType = typeof(Global), Name = "LastName")]
        public string LastName { get; set; }

        /// <summary>
        /// نام پدر
        /// </summary>
        [Display(ResourceType = typeof(Global), Name = "FatherName")]
        public string FatherName { get; set; }

        /// <summary>
        /// شناسه ملی
        /// </summary>
        [Display(ResourceType = typeof(Global), Name = "NationalCode")]
        public string NationalCode { get; set; }

        /// <summary>
        /// راهبر بودن
        /// </summary>
        [Required(ErrorMessageResourceType = typeof(Global), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(Global), Name = "IsAdmin")]
        public bool IsAdmin { get; set; }

        /// <summary>
        /// مسدود بودن
        /// </summary>
        [Required(ErrorMessageResourceType = typeof(Global), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(Global), Name = "BoycottUser")]
        public bool BoycottUser { get; set; }

        /// <summary>
        /// استفاده از تاریخ سفارشی
        /// </summary>
        [Required(ErrorMessageResourceType = typeof(Global), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(Global), Name = "UseOperationDate")]
        public bool UseOperationDate { get; set; }

        /// <summary>
        /// تاریخ سفارشی
        /// </summary>
        [Display(ResourceType = typeof(Global), Name = "OperationDate")]
        public DateTime? OperationDate { get; set; }

        /// <summary>
        /// شماره شناسایی
        /// </summary>
        [Display(ResourceType = typeof(Global), Name = "IdentificationNumber")]
        public string IdentificationNumber { get; set; }

    }
}