﻿namespace SAJPortal.Areas.Memberships.ViewModels
{

    #region Assembly

    using System.Collections.Generic;

    #endregion

    /// <summary>
    /// مدل سمت های کاربر
    /// </summary>
    public class CustomerPositionsViewModel
    {

        /// <summary>
        /// لیست سمت های موجود
        /// </summary>
        public List<CustomerPositions> AvailablePositions { get; set; }

        /// <summary>
        /// لیست سمت های فعال
        /// </summary>
        public List<CustomerPositions> SelectedPositions { get; set; }

        /// <summary>
        /// لیست سمت های انتخاب شده
        /// </summary>
        public PostedPositions PostedPositions { get; set; }

    }

    /// <summary>
    /// سمت های انتخاب شده
    /// </summary>
    public class PostedPositions
    {
        /// <summary>
        /// شناسه سمت ها
        /// </summary>
        public string[] PositionItemIds { get; set; }
    }

    /// <summary>
    /// سمت های کاربر
    /// </summary>
    public class CustomerPositions
    {

        /// <summary>
        /// شناسه
        /// </summary>
        public int ItemId { get; set; }

        /// <summary>
        /// نام
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// انتخاب شده یا نه
        /// </summary>
        public bool IsSelected { get; set; }

    }

}