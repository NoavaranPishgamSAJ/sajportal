﻿namespace SAJPortal.Areas.Memberships.ViewModels
{

    #region Assembly

    using System.ComponentModel.DataAnnotations;
    using Resources;

    #endregion

    /// <summary>
    /// مدل عضویت
    /// </summary>
    public class MembershipViewModel
    {

        /// <summary>
        /// شناسه
        /// </summary>
        public int ItemId { get; set; }

        /// <summary>
        /// نام عضویت
        /// </summary>
        [Required(ErrorMessageResourceType = typeof(Global), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(Global), Name = "MembershipName")]
        public string MembershipName { get; set; }

    }
}