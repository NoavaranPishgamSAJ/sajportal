﻿namespace SAJPortal.Areas.Memberships.ViewModels
{

    #region Assembly

    using System.Collections.Generic;

    #endregion

    /// <summary>
    /// مدل مجوزهای نقش
    /// </summary>
    public class RolePermissionsViewModel
    {

        /// <summary>
        /// لیست مجوزهای موجود
        /// </summary>
        public List<RolePermissions> AvailablePermissions { get; set; }

        /// <summary>
        /// لیست مجوزهای فعال
        /// </summary>
        public List<RolePermissions> SelectedPermissions { get; set; }

        /// <summary>
        /// لیست مجوزهای انتخاب شده
        /// </summary>
        public PostedPermissions PostedPermissions { get; set; }

    }

    /// <summary>
    /// مجوزهای انتخاب شده
    /// </summary>
    public class PostedPermissions
    {
        /// <summary>
        /// لیست شناسه مجوزها
        /// </summary>
        public string[] PermissionItemIds { get; set; }
    }

    /// <summary>
    /// مجوزهای نقش
    /// </summary>
    public class RolePermissions
    {

        /// <summary>
        /// شناسه
        /// </summary>
        public int ItemId { get; set; }

        /// <summary>
        /// نام
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// انتخاب شده یا نه
        /// </summary>
        public bool IsSelected { get; set; }

    }

}