﻿namespace SAJPortal.Areas.Layouts.ViewModels
{

    #region Assembly

    using System.ComponentModel.DataAnnotations;
    using Resources;

    #endregion

    /// <summary>
    /// مدل قالب ها
    /// </summary>
    public class LayoutViewModel
    {

        /// <summary>
        /// شناسه
        /// </summary>
        public int ItemId { get; set; }

        /// <summary>
        /// نام قالب
        /// </summary>
        [Required(ErrorMessageResourceType = typeof(Global), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(Global), Name = "LayoutName")]
        public string LayoutName { get; set; }

        /// <summary>
        /// مسیر نسبی
        /// </summary>
        [Required(ErrorMessageResourceType = typeof(Global), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(Global), Name = "VirtualPath")]
        public string VirtualPath { get; set; }

        /// <summary>
        /// شناسه سایت
        /// </summary>
        [Required(ErrorMessageResourceType = typeof(Global), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(Global), Name = "SiteId")]
        public int SiteId { get; set; }

        /// <summary>
        /// محتوای قالب
        /// </summary>
        public string Content { get; set; }

    }
}