﻿namespace SAJPortal.Areas.Layouts.Controllers
{

    #region Assembly

    using CMS.Attribute;
    using DataService;
    using ORM.Model;
    using System.Web.Mvc;
    using ViewModels;
    using System;
    using ORM.Condition;
    using AutoMapper;
    using System.Linq;
    using Portable.IO;
    using CMS.Helper;

    #endregion

    /// <summary>
    /// کنترل کننده مدیریت قالب ها
    /// </summary>
    [OutputCache(VaryByParam = "*", Duration = 0, NoStore = true)]
    [LoggedOrAuthorized]
    public class HomeController : Controller
    {

        #region Attribute

        private const int PageSize = 10;

        private int PageIndex
        {
            get
            {
                if (TempData["PageIndex"] == null)
                    TempData["PageIndex"] = 1;
                return Convert.ToInt32(TempData["PageIndex"]);
            }
            set { TempData["PageIndex"] = value; }
        }

        private long PageCount
        {
            set { TempData["PageCount"] = value; }
        }

        private FilterExpressionList Criterias
        {
            get
            {
                if (TempData["Criterias"] == null)
                    TempData["Criterias"] = new FilterExpressionList();
                return TempData["Criterias"] as FilterExpressionList;
            }
            set { TempData["Criterias"] = value; }
        }

        #endregion

        #region Method

        /// <summary>
        /// لیست قالب ها
        /// </summary>
        /// <returns>ActionResult</returns>
        [DapperAuthorize(Roles = "LayoutsList")]
        public ActionResult Index()
        {
            PageIndex = 1;
            Criterias = new FilterExpressionList();
            if (!User.IsInRole("IsAdmin") && !Criterias.ContainFilter("SiteId"))
            {
                Criterias.Add(new ORM.Condition.Filter("SiteId", SqlOperators.Equal, ModuleHelper.GetBaseSite().ItemId));
            }
            var list = DynamicService.Current.GetList<Layout>(PageIndex - 1, PageSize, Criterias, string.Empty);
            PageCount = Convert.ToInt64(Math.Ceiling((double)DynamicService.Current.GetCount<Layout>(Criterias, string.Empty) / PageSize));
            return View(list);
        }

        /// <summary>
        /// لیست قالب ها
        /// </summary>
        /// <param name="search">فیلتر لیست: string</param>
        /// <param name="formType">نوع فیلتر: string</param>
        /// <returns>ActionResult</returns>
        [DapperAuthorize(Roles = "LayoutsList")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index(string search, string formType)
        {
            if (formType.Equals("pager") && Request.Form.AllKeys.Contains("PageIndex"))
                PageIndex = Convert.ToInt32(Request.Form["PageIndex"]);
            else if (formType.Equals("search"))
            {
                PageIndex = 1;
                Criterias.Clear();
                Criterias.Add(new ORM.Condition.Filter("LayoutName", SqlOperators.Like, search, SqlOperand.And, DbType.NVarChar));
            }
            if (!User.IsInRole("IsAdmin") && !Criterias.ContainFilter("SiteId"))
            {
                Criterias.Add(new ORM.Condition.Filter("SiteId", SqlOperators.Equal, ModuleHelper.GetBaseSite().ItemId));
            }
            var list = DynamicService.Current.GetList<Layout>(PageIndex - 1, PageSize, Criterias, string.Empty);
            PageCount = Convert.ToInt64(Math.Ceiling((double)DynamicService.Current.GetCount<Layout>(Criterias, string.Empty) / PageSize));
            return View(list);
        }


        /// <summary>
        /// فرم قالب جدید
        /// </summary>
        /// <returns>ActionResult</returns>
        [DapperAuthorize(Roles = "NewLayout")]
        [HttpGet]
        public ActionResult New()
        {
            GetSites();
            return View();
        }

        /// <summary>
        /// ثبت قالب جدید
        /// </summary>
        /// <param name="model">مدل قالب: LayoutViewModel</param>
        /// <returns>ActionResult</returns>
        [DapperAuthorize(Roles = "NewLayout")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult New(LayoutViewModel model)
        {
            if (ModelState.IsValid)
            {
                DynamicService.Current.CreateItem(new Layout
                {
                    LayoutName = model.LayoutName,
                    VirtualPath = model.VirtualPath,
                    SiteId = model.SiteId,
                    AddedDate = DateTime.Today
                });

                // تعیین مسیر قالب
                var path = HttpContext.Server.MapPath($"~/CmsData/Layouts/{model.VirtualPath}");
                if (!FileSystem.Current.ExistsFile(path))
                {
                    // ساخت فایل قالب
                    FileSystem.Current.AppDirectory.CreateFile(path);
                    if (FileSystem.Current.ExistsFile(path))
                    {
                        // نوشتن محتوای قالب
                        System.IO.File.WriteAllText(path, System.Web.HttpUtility.UrlDecode(model.Content));
                    }
                }

                return RedirectToAction("Index", "Home");
            }
            GetSites();
            return View(model);
        }

        /// <summary>
        /// فرم ویرایش قالب
        /// </summary>
        /// <param name="id">شناسه قالب: int</param>
        /// <returns>ActionResult</returns>
        [DapperAuthorize(Roles = "EditLayout")]
        [HttpGet]
        public ActionResult Edit(int id)
        {
            var layout = DynamicService.Current.GetItem<Layout>(id);
            var layoutVm = Mapper.Map<LayoutViewModel>(layout);
            GetSites();

            // تعیین مسیر قالب
            var path = HttpContext.Server.MapPath($"~/CmsData/Layouts/{layout.VirtualPath}");
            if (FileSystem.Current.ExistsFile(path))
            {
                // خواندن محتوای قالب
                layoutVm.Content = System.IO.File.ReadAllText(path);
            }
            return View(layoutVm);
        }

        /// <summary>
        /// ویرایش قالب
        /// </summary>
        /// <param name="id">شناسه قالب: int</param>
        /// <param name="model">مدل قالب: LayoutViewModel</param>
        /// <returns>ActionResult</returns>
        [DapperAuthorize(Roles = "EditLayout")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, LayoutViewModel model)
        {
            if (ModelState.IsValid)
            {
                var layout = DynamicService.Current.GetItem<Layout>(id);
                // تعیین مسیر قبلی قالب
                var pathOld = HttpContext.Server.MapPath($"~/CmsData/Layouts/{layout.VirtualPath}");

                layout.LayoutName = model.LayoutName;
                layout.VirtualPath = model.VirtualPath;
                layout.SiteId = model.SiteId;
                layout.ModifiedDate = DateTime.Today;

                DynamicService.Current.UpdateItem(layout);

                // تعیین مسیر قالب
                var path = HttpContext.Server.MapPath($"~/CmsData/Layouts/{model.VirtualPath}");
                if (FileSystem.Current.ExistsFile(pathOld))
                {
                    // حذف قالب قبلی
                    var file = FileSystem.Current.GetFile(pathOld);
                    file.Delete();

                    // ساخت قالب جدید
                    FileSystem.Current.AppDirectory.CreateFile(path);
                    if (FileSystem.Current.ExistsFile(path))
                    {
                        // نوشتن محتوای قالب
                        System.IO.File.WriteAllText(path, System.Web.HttpUtility.UrlDecode(model.Content));
                    }
                }

                return RedirectToAction("Index", "Home");
            }
            GetSites();
            return View(model);
        }

        /// <summary>
        /// حذف قالب
        /// </summary>
        /// <param name="id">شناسه قالب: int</param>
        /// <returns>ActionResult</returns>
        [DapperAuthorize(Roles = "DeleteLayout")]
        [HttpGet]
        public ActionResult Delete(int id)
        {
            var layout = DynamicService.Current.GetItem<Layout>(id);
            DynamicService.Current.DeleteItem(layout);

            // تعیین مسیر قالب
            var path = HttpContext.Server.MapPath($"~/CmsData/Layouts/{layout.VirtualPath}");
            if (FileSystem.Current.ExistsFile(path))
            {
                // حذف فابل قالب
                var file = FileSystem.Current.GetFile(path);
                file.Delete();
            }

            return RedirectToAction("Index", "Home", new { area = "Layouts" });
        }

        /// <summary>
        /// لیست سایت ها
        /// </summary>
        public void GetSites()
        {
            var criterias = new FilterExpressionList();
            if (!User.IsInRole("IsAdmin"))
            {
                criterias.Add(new ORM.Condition.Filter("ItemId", SqlOperators.Equal, ModuleHelper.GetBaseSite().ItemId));
            }
            var sites = DynamicService.Current.GetList<Site>(criterias);
            ViewBag.Sites = sites.Select(s => new SelectListItem { Text = s.Label, Value = s.ItemId.ToString() }).ToList();
        }

        #endregion 

    }
}