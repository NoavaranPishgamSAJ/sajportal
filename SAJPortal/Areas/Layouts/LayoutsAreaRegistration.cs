﻿namespace SAJPortal.Areas.Layouts
{

    #region Assembly

    using System.Web.Mvc;

    #endregion

    /// <summary>
    /// ثبت ناحیه لایه ها
    /// </summary>
    public class LayoutsAreaRegistration : AreaRegistration 
    {

        #region Attribute

        /// <summary>
        /// نام ناحیه
        /// </summary>
        public override string AreaName => "Layouts";

        #endregion

        #region Method

        /// <summary>
        /// متد ثبت ناحیه
        /// </summary>
        /// <param name="context"></param>
        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                name: "Layouts",
                url: "admin/layouts/{action}/{id}",
                defaults: new { controller = "home", action = "index", id = UrlParameter.Optional },
                namespaces: new[] { "SAJPortal.Areas.Layouts.Controllers" }
            );
        }

        #endregion

    }
}