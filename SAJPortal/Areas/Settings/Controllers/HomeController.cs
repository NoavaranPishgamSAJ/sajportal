﻿namespace SAJPortal.Areas.Settings.Controllers
{

    #region Assembly

    using CMS.Helper;
    /*using Microsoft.Web.Administration;
    using System.Collections.Generic;
    using System.DirectoryServices;
    using System.Linq;
    using System.Management;*/
    using System;
    using System.Web.Mvc;
    using System.Web;
    using CMS.Attribute;
    using Utility;

    #endregion

    /// <summary>
    /// کنترل کننده تنظیمات پرتال
    /// </summary>
    [OutputCache(VaryByParam = "*", Duration = 0, NoStore = true)]
    [LoggedOrAuthorized]
    public class HomeController : Controller
    {

        #region Method

        /// <summary>
        /// فرم تنظیمات
        /// </summary>
        /// <returns>ActionResult</returns>
        [DapperAuthorize(Roles = "Settings")]
        [HttpGet]
        public ActionResult RestartPortal()
        {
            return View();
        }

        /// <summary>
        /// راه اندازی مجدد پرتال
        /// </summary>
        /// <param name="isRestart">پارامتر تصمیم گیرنده: bool</param>
        /// <returns>ActionResult</returns>
        [DapperAuthorize(Roles = "Settings")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult RestartPortal(bool isRestart)
        {
            if (isRestart)
            {
                CmsVariables.ActiveSite = null;
                Session.Abandon();
                RestartServerMethod1();
                /*RestartServerMethod2();
                RestartServerMethod3();
                RestartServerMethod4();
                RestartServerMethod5();
                RestartServerMethod6();
                RestartServerMethod7();*/
                return Redirect(Url.Content("~/"));
            }
            return View();
        }

        private static void RestartServerMethod1()
        {
            try
            {
                HttpRuntime.UnloadAppDomain();
            }
            catch (Exception ex)
            {
                Logger.Log(ex);
            }
        }

        /*private void RestartServerMethod2()
        {
            try
            {
                var server = new ServerManager();
                var site = server.Sites.FirstOrDefault(s => s.Name.ToLower().Equals(GetApplicationPoolNames().ToLower()));
                if (site != null)
                {
                    //stop the site...
                    site.Stop();
                    if (site.State == ObjectState.Stopped)
                    {
                        //do deployment tasks...
                    }
                    //restart the site...
                    site.Start();
                }
            }
            catch (Exception)
            {
            }
        }

        private void RestartServerMethod3()
        {
            try
            {
                RecycleCurrentApplicationPool();
            }
            catch (Exception)
            {
            }
        }

        private void RestartServerMethod4()
        {
            try
            {
                var list = new List<string>();
                var w3Svc = new DirectoryEntry("IIS://LocalHost/w3svc", "", "");

                foreach (DirectoryEntry site in w3Svc.Children)
                {
                    if (site.Name.ToLower().Equals(GetApplicationPoolNames().ToLower()))
                    {
                        list.AddRange(from DirectoryEntry child in site.Children select child.Name);
                    }
                }
            }
            catch (Exception)
            {
            }
        }

        private void RestartServerMethod5()
        {
            try
            {
                RecycleAppPool();
            }
            catch (Exception)
            {
            }
        }

        private void RestartServerMethod6()
        {
            try
            {
                RecycleCuAppPool();
            }
            catch (Exception)
            {
            }
        }

        private void RestartServerMethod7()
        {
            try
            {
                RecycleAppPools();
            }
            catch (Exception)
            {
            }
        }

        /// <summary>
        /// دریافت نام مخزن نرم افزار IIS
        /// </summary>
        /// <returns></returns>
        public string GetApplicationPoolNames()
        {
            var manager = new ServerManager();
            var defaultSiteName = System.Web.Hosting.HostingEnvironment.ApplicationHost.GetSiteName();
            var defaultSite = manager.Sites[defaultSiteName];
            var appVirtaulPath = HttpRuntime.AppDomainAppVirtualPath;

            var appPoolName = string.Empty;
            foreach (var app in defaultSite.Applications)
            {
                var appPath = app.Path;
                if (appPath == appVirtaulPath)
                {
                    appPoolName = app.ApplicationPoolName;
                }
            }
            return appPoolName;
        }

        public bool RecycleCurrentApplicationPool()
        {
            try
            {
                // Application hosted on IIS that supports App Pools, like 6.0 and 7.0
                if (IsApplicationRunningOnAppPool())
                {
                    // Get current application pool name
                    var appPoolId = GetCurrentApplicationPoolId();
                    // Recycle current application pool
                    RecycleApplicationPool(appPoolId);
                    return true;
                }
                return false;
            }
            catch
            {
                return false;
            }
        }

        private bool IsApplicationRunningOnAppPool()
        {
            // Application is not hosted on IIS
            if (!AppDomain.CurrentDomain.FriendlyName.StartsWith("/LM/"))
                return false;
            // Application hosted on IIS that doesn't support App Pools, like 5.1
            if (!DirectoryEntry.Exists("IIS://Localhost/W3SVC/AppPools"))
                return false;
            return true;
        }

        private string GetCurrentApplicationPoolId()
        {
            var virtualDirPath = AppDomain.CurrentDomain.FriendlyName;
            virtualDirPath = virtualDirPath.Substring(4);
            var index = virtualDirPath.Length + 1;
            index = virtualDirPath.LastIndexOf("-", index - 1, index - 1, StringComparison.Ordinal);
            index = virtualDirPath.LastIndexOf("-", index - 1, index - 1, StringComparison.Ordinal);
            virtualDirPath = "IIS://localhost/" + virtualDirPath.Remove(index);
            var virtualDirEntry = new DirectoryEntry(virtualDirPath);
            return virtualDirEntry.Properties["AppPoolId"].Value.ToString();
        }

        private void RecycleApplicationPool(string appPoolId)
        {
            try
            {
                var appPoolPath = "IIS://localhost/W3SVC/AppPools/" + appPoolId;
                var appPoolEntry = new DirectoryEntry(appPoolPath);
                appPoolEntry.Invoke("Recycle");
            }
            catch { }
        }

        private void RecycleCuAppPool()
        {
            try
            {
                var scope = new ManagementScope(@"\\localhost\root\MicrosoftIISv2");
                scope.Connect();
                var appPool = new ManagementObject(scope, new ManagementPath("IIsApplicationPool.Name='W3SVC/AppPools/" + GetApplicationPoolNames() + "'"), null);

                appPool.InvokeMethod("Recycle", null, null);
            }
            catch { }
        }

        private void RecycleAppPool()
        {
            try
            {
                var serverManager = new ServerManager();
                var appPool = serverManager.ApplicationPools[GetApplicationPoolNames()];
                if (appPool != null)
                {
                    if (appPool.State == ObjectState.Stopped)
                    {
                        appPool.Start();
                    }
                    else
                    {
                        appPool.Recycle();
                    }
                }
            }
            catch { }
        }

        private void RecycleAppPools()
        {
            try
            {
                var serverManager = new ServerManager();
                var appPools = serverManager.ApplicationPools;
                foreach (var ap in appPools)
                {
                    if (ap.Name.ToLower().Equals(GetApplicationPoolNames().ToLower()))
                        ap.Recycle();
                }
            }
            catch { }
        }
        */
        #endregion

    }
}