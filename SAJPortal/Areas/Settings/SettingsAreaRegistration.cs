﻿namespace SAJPortal.Areas.Settings
{

    #region Assembly

    using System.Web.Mvc;

    #endregion

    /// <summary>
    /// ثبت ناحیه تنظیمات
    /// </summary>
    public class SettingsAreaRegistration : AreaRegistration
    {

        #region Attribute

        /// <summary>
        /// نام ناحیه
        /// </summary>
        public override string AreaName => "Settings";

        #endregion

        #region Method

        /// <summary>
        /// متد ثبت ناحیه
        /// </summary>
        /// <param name="context">AreaRegistrationContext</param>
        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                name: "Settings",
                url: "admin/settings/{action}/{id}",
                defaults: new { controller = "home", action = "RestartPortal", id = UrlParameter.Optional },
                namespaces: new[] { "SAJPortal.Areas.Settings.Controllers" }
            );
        }

        #endregion

    }
}