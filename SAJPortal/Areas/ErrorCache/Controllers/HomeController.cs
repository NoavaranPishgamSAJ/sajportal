﻿namespace SAJPortal.Areas.ErrorCache.Controllers
{

    #region Assembly

    using System.Web.Mvc;
    using Resources;

    #endregion

    /// <summary>
    /// کنترل کننده خطاهای پرتال
    /// </summary>
    [OutputCache(VaryByParam = "*", Duration = 0, NoStore = true)]
    public class HomeController : Controller
    {

        #region Method

        /// <summary>
        /// رویداد رسیدگی به خطاها
        /// </summary>
        /// <param name="id">شناسه خطا: int</param>
        /// <returns>ActionResult</returns>
        public ActionResult Index(int id)
        {
            ViewBag.ErrorCode = id;
            switch (id)
            {
                case 1000:// خطای ناشناخته
                    ViewBag.Error = Global.Error1000;
                    break;
                case 1001:// خطای نداستن مجوز مورد نیاز
                    ViewBag.Error = Global.Error1001;
                    break;
                case 1002:// خطای عدم ورود به سیستم
                    ViewBag.Error = Global.Error1002;
                    break;
                case 404:// خطای پبدا نکردن صفحه
                    ViewBag.Error = Global.Error404;
                    break;
                case 500:// خطای داخلی پرتال
                    ViewBag.Error = Global.Error500;
                    break;
            }
            return View();
        }

       #endregion

    }
}