﻿namespace SAJPortal.Areas.ErrorCache
{

    #region Assembly

    using System.Web.Mvc;

    #endregion

    /// <summary>
    /// ثبت ناحیه خطایابی
    /// </summary>
    public class ErrorCacheAreaRegistration : AreaRegistration
    {

        #region Attribute

        /// <summary>
        /// نام ناحیه
        /// </summary>
        public override string AreaName => "ErrorCache";

        #endregion

        #region Method

        /// <summary>
        /// متد ثبت ناحیه
        /// </summary>
        /// <param name="context">AreaRegistrationContext</param>
        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                name: "ErrorCache",
                url: "error/{id}",
                defaults: new { controller = "home", action = "index", id = UrlParameter.Optional },
                namespaces: new[] { "SAJPortal.Areas.ErrorCache.Controllers" }
            );
        }

        #endregion

    }
}