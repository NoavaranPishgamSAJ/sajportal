﻿namespace SAJPortal.Areas.Contents.Controllers
{

    #region Assembly

    using CMS.Helper;
    using CMS.Attribute;
    using Utility;
    using DataService;
    using ORM.Model;
    using System.Web.Mvc;
    using ViewModels;
    using System;
    using ORM.Condition;
    using AutoMapper;
    using System.Linq;
    using ORM.Enums;
    using System.Collections.Generic;
    using ORM.DescriptiveModel;
    using Newtonsoft.Json.Linq;

    #endregion

    /// <summary>
    /// کنترل کننده مدیریت محتوا
    /// </summary>
    [OutputCache(VaryByParam = "*", Duration = 0, NoStore = true)]
    [LoggedOrAuthorized]
    public class HomeController : Controller
    {

        #region Attribute

        private const int PageSize = 10;

        private int PageIndex
        {
            get
            {
                if (TempData["PageIndex"] == null)
                    TempData["PageIndex"] = 1;
                return Convert.ToInt32(TempData["PageIndex"]);
            }
            set { TempData["PageIndex"] = value; }
        }

        private long PageCount
        {
            set { TempData["PageCount"] = value; }
        }

        private FilterExpressionList Criterias
        {
            get
            {
                if (TempData["Criterias"] == null)
                    TempData["Criterias"] = new FilterExpressionList();
                return TempData["Criterias"] as FilterExpressionList;
            }
            set { TempData["Criterias"] = value; }
        }

        #endregion

        #region Method

        /// <summary>
        /// لیست محتواها
        /// </summary>
        /// <returns>ActionResult</returns>
        [DapperAuthorize(Roles = "ContentsList")]
        public ActionResult Index()
        {
            PageIndex = 1;
            Criterias = new FilterExpressionList();
            if (!User.IsInRole("IsAdmin") && !Criterias.ContainFilter("RepositoryId"))
            {
                Criterias.Add(new ORM.Condition.Filter("RepositoryId", SqlOperators.Equal, ModuleHelper.GetBaseSite().RepositoryId));
            }
            var list = DynamicService.Current.GetList<Content>(PageIndex - 1, PageSize, Criterias, string.Empty);
            PageCount = Convert.ToInt64(Math.Ceiling((double)DynamicService.Current.GetCount<Content>(Criterias, string.Empty) / PageSize));
            return View(list);
        }

        /// <summary>
        /// لیست محتواها
        /// </summary>
        /// <param name="search">فیلتر لیست: string</param>
        /// <param name="formType">نوع فیلتر: string</param>
        /// <returns>ActionResult</returns>
        [DapperAuthorize(Roles = "ContentsList")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index(string search, string formType)
        {
            if (formType.Equals("pager") && Request.Form.AllKeys.Contains("PageIndex"))
                PageIndex = Convert.ToInt32(Request.Form["PageIndex"]);
            else if (formType.Equals("search"))
            {
                PageIndex = 1;
                Criterias.Clear();
                Criterias.Add(new ORM.Condition.Filter("Label", SqlOperators.Like, search, SqlOperand.And, DbType.NVarChar));
            }
            if (!User.IsInRole("IsAdmin") && !Criterias.ContainFilter("RepositoryId"))
            {
                Criterias.Add(new ORM.Condition.Filter("RepositoryId", SqlOperators.Equal, ModuleHelper.GetBaseSite().RepositoryId));
            }
            var list = DynamicService.Current.GetList<Content>(PageIndex - 1, PageSize, Criterias, string.Empty);
            PageCount = Convert.ToInt64(Math.Ceiling((double)DynamicService.Current.GetCount<Content>(Criterias, string.Empty) / PageSize));
            return View(list);
        }

        /// <summary>
        /// فرم محتوای جدید
        /// </summary>
        /// <returns>ActionResult</returns>
        [DapperAuthorize(Roles = "NewContent")]
        [HttpGet]
        public ActionResult New()
        {
            GetRepositories();
            return View();
        }

        /// <summary>
        /// ثبت محتوای جدید
        /// </summary>
        /// <param name="model">مدل محتوا: ContentViewModel</param>
        /// <returns>ActionResult</returns>
        [DapperAuthorize(Roles = "NewContent")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult New(ContentViewModel model)
        {
            if (ModelState.IsValid)
            {
                DynamicService.Current.CreateItem(new Content
                {
                    ContentName = model.ContentName,
                    Label = model.Label,
                    Hidden = model.Hidden.GetToggle(),
                    Sortable = model.Sortable.GetToggle(),
                    Paging = model.Paging.GetToggle(),
                    PageSize = model.PageSize,
                    FieldsObject = GetFields(model.Fields),
                    CategoriesList = model.CategoriesList,
                    CategoryFieldsList = model.CategoryFieldsList,
                    CategoryFieldsValueList = model.CategoryFieldsValueList,
                    RoleName = model.RoleName,
                    RepositoryId = model.RepositoryId,
                    AddedDate = DateTime.Today
                });

                // دریافت مخزن محتوا
                var repository = DynamicService.Current.GetItem<Repository>(model.RepositoryId);
                if (repository != null)
                {
                    // ساخت دستور ایجاد جدول دیتابیس مربوطه 
                    var tableName = $"{repository.RepositoryName}_{ model.ContentName}";
                    var command = $@"CREATE TABLE [dbo].[{tableName}](" + Environment.NewLine;
                    // اضافه کردن کلید اصلی
                    command += @"[ItemId] [int] IDENTITY(1,1) NOT NULL," + Environment.NewLine;
                    // اضافه کردن فیلدهای جدول
                    command = model.FieldsObject.Aggregate(command, (current, field) => current + $@"[{field.FieldName}] [{field.DataType}]{(field.Length.Equal(0) && field.DataType.Equal("nvarchar") ? "(max)" : field.Length > 0 && field.DataType.Equal("nvarchar") ? $"({field.Length})" : string.Empty)}{(field.AllowNull.Equal(Toggle.False) ? " NOT NULL" : string.Empty)}," + Environment.NewLine);
                    // اضافه کردن کلید های خارجی
                    command = model.CategoriesList.Where(foreignFields => foreignFields.IsNotEmpty()).Aggregate(command, (current, foreignField) => current + $@"[{foreignField}Id] [int] NOT NULL," + Environment.NewLine);
                    command += @"[AddedDate] [date] NOT NULL," + Environment.NewLine;
                    command += @"[ModifiedDate] [date]," + Environment.NewLine;
                    // تنظیم کلید اصلی
                    command += $@"CONSTRAINT [PK_{tableName}] PRIMARY KEY  CLUSTERED([ItemId] ASC)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]) ON [PRIMARY]";
                    DynamicService.Current.Execute(command);

                    /*// ساخت دستور ایجاد کلید های خارجی جدول
                    foreach (var foreignField in model.CategoriesList)
                    {
                        if (foreignField.IsNotEmpty())
                        {
                            command = $@"ALTER TABLE [dbo].[{repository.RepositoryName}_{model.ContentName}]  WITH CHECK ADD  CONSTRAINT [FK_{model.ContentName}_{foreignField}] FOREIGN KEY([{foreignField}Id])" + Environment.NewLine;
                            command += $@"REFERENCES [dbo].[{foreignField}] ([ItemId])" + Environment.NewLine;
                            command += @"ON DELETE CASCADE" + Environment.NewLine;
                            command += $@"ALTER TABLE [dbo].[{repository.RepositoryName}_{model.ContentName}] CHECK CONSTRAINT [FK_{model.ContentName}_{foreignField}]";
                            DynamicService.Current.Execute(command);
                        }
                    }*/
                }

                return RedirectToAction("Index", "Home");
            }
            GetRepositories();
            return View(model);
        }

        /// <summary>
        /// فرم ویرایش محتوا
        /// </summary>
        /// <param name="id">شناسه محتوای مورد نظر: int</param>
        /// <returns>ActionResult</returns>
        [DapperAuthorize(Roles = "EditContent")]
        [HttpGet]
        public ActionResult Edit(int id)
        {
            var content = DynamicService.Current.GetItem<Content>(id);
            var contentVm = Mapper.Map<ContentViewModel>(content);
            GetRepositories();
            return View(contentVm);
        }

        /// <summary>
        /// ویرایش محتوا
        /// </summary>
        /// <param name="id">شناسه محتوا: int</param>
        /// <param name="model">مدل محتوا: ContentViewModel</param>
        /// <returns>ActionResult</returns>
        [DapperAuthorize(Roles = "EditContent")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, ContentViewModel model)
        {
            if (ModelState.IsValid)
            {
                var content = DynamicService.Current.GetItem<Content>(id);
                
                // دریافت مخزن قبلی محتوا
                var repositoryOld = DynamicService.Current.GetItem<Repository>(content.RepositoryId);
                if (repositoryOld != null)
                {
                    // حذف جدول محتوای قبلی از دیتابیس
                    var command = $@"DROP TABLE [dbo].[{repositoryOld.RepositoryName}_{content.ContentName}]";
                    DynamicService.Current.Execute(command);
                }

                content.ContentName = model.ContentName;
                content.Label = model.Label;
                content.Hidden = model.Hidden.GetToggle();
                content.Sortable = model.Sortable.GetToggle();
                content.Paging = model.Paging.GetToggle();
                content.PageSize = model.PageSize;
                content.FieldsObject = GetFields(model.Fields);
                content.CategoriesList = model.CategoriesList;
                content.CategoryFieldsList = model.CategoryFieldsList;
                content.CategoryFieldsValueList = model.CategoryFieldsValueList;
                content.RoleName = model.RoleName;
                content.RepositoryId = model.RepositoryId;
                content.ModifiedDate = DateTime.Today;

                DynamicService.Current.UpdateItem(content);

                // دریافت مخزن محتوا
                var repository = DynamicService.Current.GetItem<Repository>(model.RepositoryId);
                if (repository != null)
                {
                    // ساخت دستور ایجاد جدول دیتابیس مربوطه 
                    var tableName = $"{repository.RepositoryName}_{ model.ContentName}";
                    var command = $@"CREATE TABLE [dbo].[{tableName}](" + Environment.NewLine;
                    // اضافه کردن کلید اصلی
                    command += @"[ItemId] [int] IDENTITY(1,1) NOT NULL," + Environment.NewLine;
                    // اضافه کردن فیلدهای جدول
                    command = model.FieldsObject.Aggregate(command, (current, field) => current + ($@"[{field.FieldName}] [{field.DataType}]{(field.Length.Equal(0) && field.DataType.Equal("nvarchar") ? "(max)" : field.Length > 0 && field.DataType.Equal("nvarchar") ? $"({field.Length})" : string.Empty)}{(field.AllowNull.Equal(Toggle.False) ? " NOT NULL" : string.Empty)}," + Environment.NewLine));
                    // اضافه کردن کلید های خارجی
                    command = model.CategoriesList.Where(foreignFields => foreignFields.IsNotEmpty()).Aggregate(command, (current, foreignField) => current + $@"[{foreignField}Id] [int] NOT NULL," + Environment.NewLine);
                    command += @"[AddedDate] [date] NOT NULL," + Environment.NewLine;
                    command += @"[ModifiedDate] [date]," + Environment.NewLine;
                    // تنظیم کلید اصلی
                    command += $@"CONSTRAINT [PK_{tableName}] PRIMARY KEY  CLUSTERED([ItemId] ASC)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]) ON [PRIMARY]";
                    DynamicService.Current.Execute(command);

                   /* // ساخت دستور ایجاد کلید های خارجی جدول
                    foreach (var foreignField in model.CategoriesList)
                    {
                        if (foreignField.IsNotEmpty())
                        {
                            command = $@"ALTER TABLE [dbo].[{repository.RepositoryName}_{model.ContentName}]  WITH CHECK ADD  CONSTRAINT [FK_{model.ContentName}_{foreignField}] FOREIGN KEY([{foreignField}Id])" + Environment.NewLine;
                            command += $@"REFERENCES [dbo].[{foreignField}] ([ItemId])" + Environment.NewLine;
                            command += @"ON DELETE CASCADE" + Environment.NewLine;
                            command += $@"ALTER TABLE [dbo].[{repository.RepositoryName}_{model.ContentName}] CHECK CONSTRAINT [FK_{model.ContentName}_{foreignField}]";
                            DynamicService.Current.Execute(command);
                        }
                    }*/
                }

                return RedirectToAction("Index", "Home");
            }
            GetRepositories();
            return View(model);
        }

        /// <summary>
        /// حذف محتوا
        /// </summary>
        /// <param name="id">شناسه محتوا: int</param>
        /// <returns>ActionResult</returns>
        [DapperAuthorize(Roles = "DeleteContent")]
        [HttpGet]
        public ActionResult Delete(int id)
        {
            var content = DynamicService.Current.GetItem<Content>(id);
            DynamicService.Current.DeleteItem(content);

            // دریافت مخزن محتوا
            var repository = DynamicService.Current.GetItem<Repository>(content.RepositoryId);
            if (repository != null)
            {
                // حذف جدول محتوای قبلی از دیتابیس
                var command = $@"DROP TABLE [dbo].[{repository.RepositoryName}_{content.ContentName}]";
                DynamicService.Current.Execute(command);

            }

            return RedirectToAction("Index", "Home", new { area = "Contents" });
        }

        /// <summary>
        /// لیست مخازن
        /// </summary>
        public void GetRepositories()
        {
            var criterias = new FilterExpressionList();
            if (!User.IsInRole("IsAdmin"))
            {
                criterias.Add(new ORM.Condition.Filter("ItemId", SqlOperators.Equal, ModuleHelper.GetBaseSite().RepositoryId));
            }
            var repositories = DynamicService.Current.GetList<Repository>(criterias);
            ViewBag.Repositories = repositories.Select(r => new SelectListItem { Text = r.RepositoryName, Value = r.ItemId.ToString() }).ToList();
        }

        /// <summary>
        /// دریافت فیلدها در قالب لیست
        /// </summary>
        /// <param name="fieldsJson">لیست فیلدها در قالب JSON: string</param>
        /// <returns>List&lt;ContentField&gt;</returns>
        private static List<ContentField> GetFields(string fieldsJson)
        {
            var fields = new List<ContentField>();
            var fieldContainer = Newtonsoft.Json.JsonConvert.DeserializeObject(fieldsJson);
            var container = fieldContainer as JContainer;
            if (container != null && container.HasValues)
            {
                var field = new ContentField();
                foreach (var property in (from node in container.Children().Where(node => node != null && node.HasValues) from child in node.Children() select child as JProperty).Where(property => property != null))
                {
                    var value = property.Value as JValue;
                    // تنظیم لیست اعتبارسنجی های هر فیلد
                    if (property.Name.Equals("FieldValidations"))
                    {
                        if (property.Count > 0)
                        {
                            field.FieldValidations = new List<ContentFieldValidation>();
                            foreach (var prop in (from p in property.Children().Where(node => node != null && node.HasValues) select p).Children())
                            {
                                var validation = new ContentFieldValidation();
                                foreach (var v in prop.Children())
                                {
                                    var jprop = v as JProperty;
                                    if (jprop != null)
                                    {
                                        var val = jprop.Value as JValue;
                                        if (val == null)
                                            continue;
                                        switch (jprop.Name)
                                        {
                                            case "ValidationName":
                                                validation.ValidationName = val.Value.ToString();
                                                break;
                                            case "FirstParam":
                                                validation.FirstParam = val.Value.ToString();
                                                break;
                                            case "SecondParam":
                                                validation.SecondParam = val.Value.ToString();
                                                break;
                                            case "ThirdParam":
                                                validation.ThirdParam = val.Value.ToString();
                                                break;
                                        }
                                    }
                                }
                                field.FieldValidations.Add(validation);
                            }
                        }
                    }

                    // تنظیم اطلاعات فیلد
                    if (value != null)
                    {
                        switch (property.Name)
                        {
                            case "FieldName":
                                field.FieldName = value.Value.ToString();
                                break;
                            case "Label":
                                field.Label = value.Value.ToString();
                                break;
                            case "DefaultValue":
                                field.DefaultValue = value.Value.ToString();
                                break;
                            case "ShowInList":
                                field.ShowInList = Convert.ToBoolean(value.Value).GetToggle();
                                break;
                            case "Tooltip":
                                field.Tooltip = value.Value.ToString();
                                break;
                            case "Length":
                                int length;
                                int.TryParse(Convert.ToString(value.Value), out length);
                                field.Length = length;
                                break;
                            case "AllowNull":
                                field.AllowNull = Convert.ToBoolean(value.Value).GetToggle();
                                break;
                            case "CustomSetting":
                                field.CustomSetting = value.Value.ToString();
                                break;
                            case "ControlType":
                                field.ControlType = value.Value.ToString();
                                break;
                            case "DataType":
                                field.DataType = value.Value.ToString();
                                break;
                        }
                    }
                    if (value == null && property.Name.Equals("FieldValidations"))
                    {
                        fields.Add(field.Clone());
                        field = new ContentField();
                    }
                }
            }
            return fields;
        }

        #endregion

    }
}