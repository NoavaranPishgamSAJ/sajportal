﻿namespace SAJPortal.Areas.Contents
{

    #region Assembly

    using System.Web.Mvc;

    #endregion

    /// <summary>
    /// ثبت ناحیه محتوا ها
    /// </summary>
    public class ContentsAreaRegistration : AreaRegistration
    {

        #region Attribute

        /// <summary>
        /// نام ناحیه
        /// </summary>
        public override string AreaName => "Contents";

        #endregion

        #region Method

        /// <summary>
        /// متد ثبت ناحیه
        /// </summary>
        /// <param name="context">AreaRegistrationContext</param>
        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                name: "Contents",
                url: "admin/contents/{action}/{id}",
                defaults: new { controller = "home", action = "index", id = UrlParameter.Optional },
                namespaces: new[] { "SAJPortal.Areas.Contents.Controllers" }
            );
        }

        #endregion

    }
}