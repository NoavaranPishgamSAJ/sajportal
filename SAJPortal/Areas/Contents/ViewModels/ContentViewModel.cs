﻿namespace SAJPortal.Areas.Contents.ViewModels
{

    #region Assembly

    using System.ComponentModel.DataAnnotations;
    using Resources;
    using System.Collections.Generic;
    using ORM.DescriptiveModel;

    #endregion

    /// <summary>
    /// مدل محتوا ها
    /// </summary>
    public class ContentViewModel
    {

        /// <summary>
        /// شناسه
        /// </summary>
        public int ItemId { get; set; }

        /// <summary>
        /// نام محتوا
        /// </summary>
        [Required(ErrorMessageResourceType = typeof(Global), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(Global), Name = "ContentName")]
        public string ContentName { get; set; }

        /// <summary>
        /// برچسب
        /// </summary>
        [Display(ResourceType = typeof(Global), Name = "Label")]
        public string Label { get; set; }

        /// <summary>
        /// مخفی بودن
        /// </summary>
        [Required(ErrorMessageResourceType = typeof(Global), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(Global), Name = "Hidden")]
        public bool Hidden { get; set; }

        /// <summary>
        /// قابل مرتب سازی بودن
        /// </summary>
        [Required(ErrorMessageResourceType = typeof(Global), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(Global), Name = "Sortable")]
        public bool Sortable { get; set; }

        /// <summary>
        /// قابل صفحه بندی شدن
        /// </summary>
        [Required(ErrorMessageResourceType = typeof(Global), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(Global), Name = "Paging")]
        public bool Paging { get; set; }

        /// <summary>
        /// تعداد رکورد در هر صفحه
        /// </summary>
        [Required(ErrorMessageResourceType = typeof(Global), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(Global), Name = "PageSize")]
        public int PageSize { get; set; }

        /// <summary>
        /// شناسه مخزن
        /// </summary>
        [Required(ErrorMessageResourceType = typeof(Global), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(Global), Name = "RepositoryId")]
        public int RepositoryId { get; set; }

        /// <summary>
        /// لیست فیلدها در قالب JSON
        /// </summary>
        [Required(ErrorMessageResourceType = typeof(Global), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(Global), Name = "Fields")]
        public string Fields { get; set; }

        /// <summary>
        /// لیست فیلدها
        /// </summary>
        public List<ContentField> FieldsObject => Newtonsoft.Json.JsonConvert.DeserializeObject<List<ContentField>>(Fields);

        /// <summary>
        /// نام محتواهای خارجی در قالب JSON
        /// </summary>
        [Display(ResourceType = typeof(Global), Name = "Categories")]
        public string Categories { get; set; }

        /// <summary>
        /// لیست محتوا های خارجی
        /// </summary>
        [Display(ResourceType = typeof(Global), Name = "Categories")]
        public string[] CategoriesList { get; set; }

        /// <summary>
        /// نام فیلدهای محتواهای خارجی در قالب JSON
        /// </summary>
        [Display(ResourceType = typeof(Global), Name = "CategoryFields")]
        public string CategoryFields { get; set; }

        /// <summary>
        /// لیست نام فیلدهای محتواهای خارجی
        /// </summary>
        [Display(ResourceType = typeof(Global), Name = "CategoryFields")]
        public string[] CategoryFieldsList { get; set; }

        /// <summary>
        /// مقدار فیلدهای محتواهای خارجی در قالب JSON
        /// </summary>
        [Display(ResourceType = typeof(Global), Name = "CategoryFields")]
        public string CategoryFieldsValue { get; set; }

        /// <summary>
        /// لیست مقدار فیلدهای محتواهای خارجی
        /// </summary>
        [Display(ResourceType = typeof(Global), Name = "CategoryFields")]
        public string[] CategoryFieldsValueList { get; set; }

        /// <summary>
        /// مجوز مورد نیاز صفحه در هنگام بازدید
        /// </summary>
        [Display(ResourceType = typeof(Global), Name = "RoleName")]
        public string RoleName { get; set; }

    }
}