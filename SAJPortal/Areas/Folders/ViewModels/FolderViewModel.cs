﻿namespace SAJPortal.Areas.Folders.ViewModels
{

    #region Assembly

    using System.ComponentModel.DataAnnotations;
    using Resources;

    #endregion

    /// <summary>
    /// مدل پوشه ها
    /// </summary>
    public class FolderViewModel
    {
        
        /// <summary>
        /// شناسه
        /// </summary>
        public int ItemId { get; set; }

        /// <summary>
        /// نام پوشه
        /// </summary>
        [Required(ErrorMessageResourceType = typeof(Global), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(Global), Name = "FolderName")]
        public string FolderName { get; set; }

        /// <summary>
        /// مسیر نسبی
        /// </summary>
        [Required(ErrorMessageResourceType = typeof(Global), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(Global), Name = "VirtualPath")]
        public string VirtualPath { get; set; }

        /// <summary>
        /// شناسه نوع پوشه
        /// </summary>
        [Required(ErrorMessageResourceType = typeof(Global), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(Global), Name = "FolderTypeId")]
        public int FolderTypeId { get; set; }

        /// <summary>
        /// شناسه محتوا
        /// </summary>
        [Display(ResourceType = typeof(Global), Name = "ContentId")]
        public int ContentId { get; set; }

        /// <summary>
        /// شناسه سایت
        /// </summary>
        [Required(ErrorMessageResourceType = typeof(Global), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(Global), Name = "SiteId")]
        public int SiteId { get; set; }

    }
}