﻿namespace SAJPortal.Areas.Folders.Controllers
{

    #region Assembly

    using CMS.Helper;
    using CMS.Attribute;
    using DataService;
    using ORM.Model;
    using System.Web.Mvc;
    using ViewModels;
    using System;
    using ORM.Condition;
    using AutoMapper;
    using System.Linq;
    using Portable.IO;
    using System.Collections.Generic;
    using Resources;

    #endregion

    /// <summary>
    /// کنترل کننده مدیریت پوشه ها
    /// </summary>
    [OutputCache(VaryByParam = "*", Duration = 0, NoStore = true)]
    [LoggedOrAuthorized]
    public class HomeController : Controller
    {

        #region Attribute

        private const int PageSize = 10;

        private int PageIndex
        {
            get
            {
                if (TempData["PageIndex"] == null)
                    TempData["PageIndex"] = 1;
                return Convert.ToInt32(TempData["PageIndex"]);
            }
            set { TempData["PageIndex"] = value; }
        }

        private long PageCount
        {
            set { TempData["PageCount"] = value; }
        }

        private FilterExpressionList Criterias
        {
            get
            {
                if (TempData["Criterias"] == null)
                    TempData["Criterias"] = new FilterExpressionList();
                return TempData["Criterias"] as FilterExpressionList;
            }
            set { TempData["Criterias"] = value; }
        }

        #endregion

        #region Method

        /// <summary>
        /// لیست پوشه ها
        /// </summary>
        /// <returns>ActionResult</returns>
        [DapperAuthorize(Roles = "FoldersList")]
        public ActionResult Index()
        {
            PageIndex = 1;
            Criterias = new FilterExpressionList();
            if (!User.IsInRole("IsAdmin") && !Criterias.ContainFilter("SiteId"))
            {
                Criterias.Add(new ORM.Condition.Filter("SiteId", SqlOperators.Equal, ModuleHelper.GetBaseSite().ItemId));
            }
            var list = DynamicService.Current.GetList<FolderView>(PageIndex - 1, PageSize, Criterias, string.Empty);
            PageCount = Convert.ToInt64(Math.Ceiling((double)DynamicService.Current.GetCount<FolderView>(Criterias, string.Empty) / PageSize));
            return View(list);
        }

        /// <summary>
        /// لیست پوشه ها
        /// </summary>
        /// <param name="search">فیلتر لیست: string</param>
        /// <param name="formType">نوع فیلتر: string</param>
        /// <returns>ActionResult</returns>
        [DapperAuthorize(Roles = "FoldersList")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index(string search, string formType)
        {
            if (formType.Equals("pager") && Request.Form.AllKeys.Contains("PageIndex"))
                PageIndex = Convert.ToInt32(Request.Form["PageIndex"]);
            else if (formType.Equals("search"))
            {
                PageIndex = 1;
                Criterias.Clear();
                Criterias.Add(new ORM.Condition.Filter("FolderName", SqlOperators.Like, search, SqlOperand.And, DbType.NVarChar));
            }
            if (!User.IsInRole("IsAdmin") && !Criterias.ContainFilter("SiteId"))
            {
                Criterias.Add(new ORM.Condition.Filter("SiteId", SqlOperators.Equal, ModuleHelper.GetBaseSite().ItemId));
            }
            var list = DynamicService.Current.GetList<FolderView>(PageIndex - 1, PageSize, Criterias, string.Empty);
            PageCount = Convert.ToInt64(Math.Ceiling((double)DynamicService.Current.GetCount<FolderView>(Criterias, string.Empty) / PageSize));
            return View(list);
        }

        /// <summary>
        /// فرم پوشه جدید
        /// </summary>
        /// <returns>ActionResult</returns>
        [DapperAuthorize(Roles = "NewFolder")]
        [HttpGet]
        public ActionResult New()
        {
            GetFolderTypes();
            GetSites();
            GetContents();
            return View();
        }

        /// <summary>
        /// ثبت پوشه جدید
        /// </summary>
        /// <param name="model">مدل پوشه: FolderViewModel</param>
        /// <returns>ActionResult</returns>
        [DapperAuthorize(Roles = "NewFolder")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult New(FolderViewModel model)
        {
            if (ModelState.IsValid)
            {
                DynamicService.Current.CreateItem(new Folder
                {
                    FolderName = model.FolderName,
                    VirtualPath = model.VirtualPath,
                    FolderTypeId = model.FolderTypeId,
                    ContentId = model.ContentId.Equals(-1) ? (int?)null : model.ContentId,
                    SiteId = model.SiteId,
                    AddedDate = DateTime.Today
                });

                // تعیین مسیر پوشه
                var path = HttpContext.Server.MapPath($"~/CmsData/PortalFolders/{model.VirtualPath}");
                if (!FileSystem.Current.ExistsDirectory(path))
                {
                    // ساخت پوشه در مسیر تعیین شده
                    FileSystem.Current.AppDirectory.CreateDirectory(path);
                }

                return RedirectToAction("Index", "Home");
            }
            GetFolderTypes();
            GetSites();
            GetContents();
            return View(model);
        }

        /// <summary>
        /// فرم ویرایش پوشه
        /// </summary>
        /// <param name="id">شناسه پوشه: int</param>
        /// <returns>ActionResult</returns>
        [DapperAuthorize(Roles = "EditFolder")]
        [HttpGet]
        public ActionResult Edit(int id)
        {
            var folder = DynamicService.Current.GetItem<Folder>(id);
            var folderVm = Mapper.Map<FolderViewModel>(folder);
            GetFolderTypes();
            GetSites();
            GetContents();
            return View(folderVm);
        }

        /// <summary>
        /// ویرایش پوشه
        /// </summary>
        /// <param name="id">شناسه پوشه: int</param>
        /// <param name="model">مدل پوشه: FolderViewModel</param>
        /// <returns>ActionResult</returns>
        [DapperAuthorize(Roles = "EditFolder")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, FolderViewModel model)
        {
            if (ModelState.IsValid)
            {
                var folder = DynamicService.Current.GetItem<Folder>(id);
                // تعیین مسیر پوشه قبلی
                var pathOld = HttpContext.Server.MapPath($"~/CmsData/PortalFolders/{folder.VirtualPath}");

                folder.FolderName = model.FolderName;
                folder.VirtualPath = model.VirtualPath;
                folder.FolderTypeId = model.FolderTypeId;
                folder.ContentId = model.ContentId.Equals(-1) ? (int?)null : model.ContentId;
                folder.SiteId = model.SiteId;
                folder.ModifiedDate = DateTime.Today;

                DynamicService.Current.UpdateItem(folder);

                // تععین مسیر پوشه
                var path = HttpContext.Server.MapPath($"~/CmsData/PortalFolders/{model.VirtualPath}");
                if (FileSystem.Current.ExistsDirectory(pathOld))
                {
                    // حذف پوشه قبلی
                    var directory = FileSystem.Current.GetDirectory(pathOld);
                    directory.Delete();

                    // ساخت پوشه در مسیر تعیین شده
                    FileSystem.Current.AppDirectory.CreateDirectory(path);
                }

                return RedirectToAction("Index", "Home");
            }
            GetFolderTypes();
            GetSites();
            GetContents();
            return View(model);
        }

        /// <summary>
        /// حذف پوشه
        /// </summary>
        /// <param name="id">شناسه پوشه: int</param>
        /// <returns>ActionResult</returns>
        [DapperAuthorize(Roles = "DeleteFolder")]
        [HttpGet]
        public ActionResult Delete(int id)
        {
            var folder = DynamicService.Current.GetItem<Folder>(id);
            DynamicService.Current.DeleteItem(folder);

            // تعیین مسیر پوشه
            var path = HttpContext.Server.MapPath($"~/CmsData/PortalFolders/{folder.VirtualPath}");
            if (FileSystem.Current.ExistsDirectory(path))
            {
                // حذف پوشه
                var directory = FileSystem.Current.GetDirectory(path);
                directory.Delete();
            }

            return RedirectToAction("Index", "Home", new { area = "Folders" });
        }

        /// <summary>
        /// لیست نوع پوشه ها
        /// </summary>
        public void GetFolderTypes()
        {
            var folderTypes = DynamicService.Current.GetList<FolderType>();
            ViewBag.FolderTypes = folderTypes.Select(s => new SelectListItem { Text = s.TypeName, Value = s.ItemId.ToString() }).ToList();
        }

        /// <summary>
        /// لیست سایت ها
        /// </summary>
        public void GetSites()
        {
            var criterias = new FilterExpressionList();
            if (!User.IsInRole("IsAdmin"))
            {
                criterias.Add(new ORM.Condition.Filter("ItemId", SqlOperators.Equal, ModuleHelper.GetBaseSite().ItemId));
            }
            var sites = DynamicService.Current.GetList<Site>(criterias);
            ViewBag.Sites = sites.Select(s => new SelectListItem { Text = s.Label, Value = s.ItemId.ToString() }).ToList();
        }

        /// <summary>
        /// لیست محتواها
        /// </summary>
        public void GetContents()
        {
            var criterias = new FilterExpressionList();
            if (!User.IsInRole("IsAdmin"))
            {
                criterias.Add(new ORM.Condition.Filter("RepositoryId", SqlOperators.Equal, ModuleHelper.GetBaseSite().RepositoryId));
            }
            var contents = DynamicService.Current.GetList<Content>(criterias);
            var none = new List<SelectListItem>
            {
                new SelectListItem {Text = CultureHelper.Localize("ComboNone"), Value = "-1"}
            };
            none.AddRange(contents.Select(s => new SelectListItem { Text = s.ContentName, Value = s.ItemId.ToString() }).ToList());
            ViewBag.Contents = none;
        }

        #endregion 

    }
}