﻿namespace SAJPortal.Areas.Dashboard.Controllers
{

    #region Assembly

    using System.Web.Mvc;
    using CMS.Attribute;

    #endregion

    /// <summary>
    /// کنترل کننده داشبورد کاربر
    /// </summary>
    [OutputCache(VaryByParam = "*", Duration = 0, NoStore = true)]
    [LoggedOrAuthorized]
    public class HomeController : Controller
    {

        #region Method

        /// <summary>
        /// رویداد اصلی داشبورد
        /// </summary>
        /// <returns>ActionResult</returns>
        [DapperAuthorize(Roles = "Dashboard")]
        public ActionResult Index()
        {
            return View();
        }

        #endregion

    }
}