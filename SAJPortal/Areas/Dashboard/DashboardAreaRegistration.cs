﻿namespace SAJPortal.Areas.Dashboard
{

    #region Assembly

    using System.Web.Mvc;

    #endregion

    /// <summary>
    /// ثبت ناحیه داشبورد کاربر
    /// </summary>
    public class DashboardAreaRegistration : AreaRegistration
    {

        #region Attribute

        /// <summary>
        /// نام ناحیه
        /// </summary>
        public override string AreaName => "Dashboard";

        #endregion

        #region Method

        /// <summary>
        /// متد ثبت ناحیه
        /// </summary>
        /// <param name="context">AreaRegistrationContext</param>
        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                name: "Dashboard",
                url: "admin/dashboard/{action}/{id}",
                defaults: new { controller = "home", action = "index", id = UrlParameter.Optional },
                namespaces: new[] { "SAJPortal.Areas.Dashboard.Controllers" }
            );
        }

        #endregion

    }
}