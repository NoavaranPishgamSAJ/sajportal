﻿namespace SAJPortal.Areas.Users.Controllers
{

    #region Assembly

    using Utility;
    using System.Web.Security;
    using System.Linq;
    using ORM.Enums;
    using System.Web.Mvc;
    using ViewModels;
    using CMS.Extension;
    using DataService;
    using System.Configuration;
    using Resources;
    using ORM.Model;
    using System;
    using CMS.Helper;
    using System.Security.Principal;
    using CMS.Attribute;

    #endregion

    /// <summary>
    /// کنترل کننده عملیات کاربران
    /// </summary>
    [OutputCache(VaryByParam = "*", Duration = 0, NoStore = true)]
    [AllowAnonymous]
    public class HomeController : Controller
    {

        #region Method

        /// <summary>
        /// فرم ورود به سیستم
        /// </summary>
        /// <returns>ActionResult</returns>
        [HttpGet]
        public ActionResult Login()
        {
            return View();
        }

        /// <summary>
        /// ورود به سیستم
        /// </summary>
        /// <param name="model">مدل ورود به سیستم: LoginViewModel</param>
        /// <param name="returnUrl">آدرس بازگشتی: string</param>
        /// <returns>ActionResult</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(LoginViewModel model, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                // اگر کاربر معتبر بود
                var isLoggedIn = MembershipService.Current.Login(model.Username, model.Password);
                if (isLoggedIn)
                {
                    var baseSite = ModuleHelper.GetBaseSite();
                    var user = LoggedInUser(model.Username, false);
                    
                    // اگر کاربر جزء سایت فعال بود یا مدیر بود اجازه ورود به پرتال را دارد
                    if (user != null && baseSite != null && (user.MembershipId.Equals(baseSite.MembershipId) || user.IsAdmin == Toggle.True))
                    {
                        // اگر کاربر راهبر بود به صفحه مدیریت پرتال ارسال می شود
                        if (user.IsAdmin == Toggle.True || HttpContext.User.IsInRole("Dashboard"))
                            return RedirectToAction("Index", "Home", new { area = "Dashboard" });

                        // ارسال کاربر به صفحه اصلی سایت
                        return Redirect(Url.Content("~/"));
                    }
                }
            }
            return View();
        }

        private Customer LoggedInUser(string username, bool isPersistent)
        {
            var user = MembershipService.Current.GetUser(username);
            var permissions = MembershipService.Current.UserExistPermissions(user.ItemId).Select(p => p.DevName).ToList();
            if (user.IsAdmin == Toggle.True)
            {
                permissions.Add("IsAdmin");
            }
            var userRoles = string.Join(",", permissions);

            // ساخت بلیط کاربر
            var authTicket = new FormsAuthenticationTicket(1, username, DateTime.Now, DateTime.Now.AddMinutes(30), isPersistent, userRoles, FormsAuthentication.FormsCookiePath);
            try
            {
                var identity = new GenericIdentity(authTicket.Name, "Forms");
                var principal = new GenericPrincipal(identity, permissions.ToArray());
                CmsVariables.FormsAuthentication.Set(new GenericPrincipal(identity, permissions.ToArray()));
                HttpContext.User = principal;
            }
            catch (Exception ex)
            {
                Logger.Log(ex);
            }
            return user;
        }

        /// <summary>
        /// فرم فراموشی رمز عبور
        /// </summary>
        /// <returns>ActionResult</returns>
        [HttpGet]
        public ActionResult ForgetPassword()
        {
            return View();
        }

        /// <summary>
        /// بازیابی رمز عبور کاربر
        /// </summary>
        /// <param name="model">مدل فراموشی رمز عبور: ForgetPasswordViewModel</param>
        /// <returns>ActionResult</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ForgetPassword(ForgetPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = MembershipService.Current.GetUser(model.Username);
                if (user != null && user.PasswordAnswer.Equals(model.PasswordAnswer))
                {
                    // ایمیل کردن رمز عبور کاربر به ایمیل خودش
                    var mailHelper = new MailHelper
                    {
                        Sender = ConfigurationManager.AppSettings["EmailFromAddress"],
                        Recipient = user.Email,
                        RecipientCc = null,
                        Subject = Global.ForgetPassword,
                        Body = Global.YourPasswordIs + user.Password
                    };
                    if (mailHelper.Send())
                        return RedirectToAction("ConfirmForgetPassword", "Home");
                }
            }
            return View(model);
        }

        /// <summary>
        /// تاییدیه دریافت رمز عبور
        /// </summary>
        /// <returns>ActionResult</returns>
        [HttpGet]
        public ActionResult ConfirmForgetPassword()
        {
            return View();
        }

        /// <summary>
        /// فرم تنظیم مجدد رمز عبور
        /// </summary>
        /// <returns>ActionResult</returns>
        [HttpGet]
        public ActionResult ResetPassword()
        {
            return View();
        }

        /// <summary>
        /// تنظیم مجدد رمز عبور
        /// </summary>
        /// <param name="model">مدل تنظیم مجدد رمز عبور: ResetPasswordViewModel</param>
        /// <returns>ActionResult</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ResetPassword(ResetPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = MembershipService.Current.GetUser(model.Username);
                if (user != null && user.PasswordQuestion.Equals(model.PasswordQuestion) && user.PasswordAnswer.Equals(model.PasswordAnswer))
                {
                    // تنظیم مجدد رمز عبور
                    user.Password = System.Web.Security.Membership.GeneratePassword(10, 3);
                    if (DynamicService.Current.UpdateItem(user))
                    {
                        // ارسال رمز عبور جدید به ایمیل کاربر
                        var mailHelper = new MailHelper
                        {
                            Sender = ConfigurationManager.AppSettings["EmailFromAddress"],
                            Recipient = user.Email,
                            RecipientCc = null,
                            Subject = Global.ResetPassword,
                            Body = Global.YourNewPasswordIs + user.Password
                        };
                        if (mailHelper.Send())
                            return RedirectToAction("ConfirmResetPassword", "Home");
                    }
                }
            }
            return View(model);
        }

        /// <summary>
        /// تاییدیه تنظیم رمز عبور
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult ConfirmResetPassword()
        {
            return View();
        }

        /// <summary>
        /// فرم ثبت نام کاربر
        /// </summary>
        /// <returns>ActionResult</returns>
        [HttpGet]
        public ActionResult Register()
        {
            return View();
        }

        /// <summary>
        /// ثبت نام کاربر
        /// </summary>
        /// <param name="model">مدل ثبت نام کاربر: RegisterViewModel</param>
        /// <returns>ActionResult</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Register(RegisterViewModel model)
        {
            if (ModelState.IsValid)
            {
                // ایجاد کاربر در سایت فعال
                var site = Session.GetItem<Site>("ActiveSite");
                DynamicService.Current.CreateItem(new Customer
                {
                    Username = model.Username,
                    Password = model.Password,
                    FirstName = model.FirstName,
                    LastName = model.LastName,
                    IsAdmin = Toggle.False,
                    BoycottUser = Toggle.False,
                    UseOperationDate = Toggle.False,
                    MembershipId = site.MembershipId,
                    LastLoginDate = null,
                    EmailConfirmed = Toggle.False,
                    PhoneNumberConfirmed = Toggle.False,
                    PasswordQuestion = model.PasswordQuestion,
                    PasswordAnswer = model.PasswordAnswer,
                    IsApproved = Toggle.False,
                    AccessFailedCount = 0,
                    AddedDate = DateTime.Today
                });
                return RedirectToAction("ConfirmRegister", "Home");
            }
            return View(model);
        }

        /// <summary>
        /// تاییدیه ثبت نام کاربر
        /// </summary>
        /// <returns>ActionResult</returns>
        [HttpGet]
        public ActionResult ConfirmRegister()
        {
            return View();
        }

        /// <summary>
        /// خروج از سیستم
        /// </summary>
        /// <returns>ActionResult</returns>
        [LoggedOrAuthorized]
        [HttpGet]
        public ActionResult SignOut()
        {
            // حذف بلیط کاربر
            FormsAuthentication.SignOut();
            if (CmsVariables.FormsAuthentication.GetOrDefault().Identity.Name.IsNotEmpty())
            {
                CmsVariables.FormsAuthentication.Reset();
                HttpContext.Request.Cookies.Remove(FormsAuthentication.FormsCookieName);
            }

            // نامعتبر کردن جلسه کاربر
            Session.Abandon();

            // حذف کوکی جلسه کاربر
            var sessionId = Response.Cookies["ASP.NET_SessionId"];
            if (sessionId != null)
                sessionId.Expires = DateTime.Now.AddDays(-30); //Delete the cookie

            // انتقال کاربر به صفحه ورود به سیستم
            return Redirect(Url.Content(@"~/users"));
        }

        #endregion

    }
}