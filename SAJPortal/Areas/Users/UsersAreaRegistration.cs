﻿namespace SAJPortal.Areas.Users
{

    #region Assembly

    using System.Web.Mvc;

    #endregion

    /// <summary>
    /// ثبت ناحیه کاربران
    /// </summary>
    public class UsersAreaRegistration : AreaRegistration
    {

        #region Attribute

        /// <summary>
        /// نام ناحیه
        /// </summary>
        public override string AreaName => "Users";

        #endregion

        #region Method

        /// <summary>
        /// متد ثبت ناحیه
        /// </summary>
        /// <param name="context">AreaRegistrationContext</param>
        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                name: "Users",
                url: "users/{action}",
                defaults: new { controller = "home", action = "login" },
                namespaces: new[] { "SAJPortal.Areas.Users.Controllers" }
            );
        }

        #endregion

    }
}