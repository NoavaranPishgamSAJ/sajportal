﻿namespace SAJPortal.Areas.Users.ViewModels
{

    #region Assembly

    using System.ComponentModel.DataAnnotations;
    using Resources;

    #endregion

    /// <summary>
    /// مدل ورود به سیستم
    /// </summary>
    public class LoginViewModel
    {

        /// <summary>
        /// نام کاربری
        /// </summary>
        [Required(ErrorMessageResourceType = typeof(Global), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(Global), Name = "Username")]
        public string Username { get; set; }

        /// <summary>
        /// رمز عبور
        /// </summary>
        [Required(ErrorMessageResourceType = typeof(Global), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(Global), Name = "Password")]
        public string Password { get; set; }

    }
}