﻿namespace SAJPortal.Areas.Users.ViewModels
{

    #region Assembly

    using System.ComponentModel.DataAnnotations;
    using Resources;

    #endregion

    /// <summary>
    /// مدل تنظیم مجدد رمز عبور
    /// </summary>
    public class ResetPasswordViewModel
    {

        /// <summary>
        /// نام کاربری
        /// </summary>
        [Required(ErrorMessageResourceType = typeof(Global), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(Global), Name = "Username")]
        public string Username { get; set; }

        /// <summary>
        /// سوال رمز عبور
        /// </summary>
        [Required(ErrorMessageResourceType = typeof(Global), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(Global), Name = "PasswordQuestion")]
        public string PasswordQuestion { get; set; }

        /// <summary>
        /// جواب رمز عبور
        /// </summary>
        [Required(ErrorMessageResourceType = typeof(Global), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(Global), Name = "PasswordAnswer")]
        public string PasswordAnswer { get; set; }

    }
}