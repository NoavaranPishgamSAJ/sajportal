﻿namespace SAJPortal.Areas.Users.ViewModels
{

    #region Assembly

    using System.ComponentModel.DataAnnotations;
    using Resources;

    #endregion

    /// <summary>
    /// مدل فراموشی رمز عبور
    /// </summary>
    public class ForgetPasswordViewModel
    {

        /// <summary>
        /// نام کاربری
        /// </summary>
        [Required(ErrorMessageResourceType = typeof(Global), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(Global), Name = "Username")]
        public string Username { get; set; }

        /// <summary>
        /// جواب رمز عبور
        /// </summary>
        [Required(ErrorMessageResourceType = typeof(Global), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(Global), Name = "PasswordAnswer")]
        public string PasswordAnswer { get; set; }

    }
}