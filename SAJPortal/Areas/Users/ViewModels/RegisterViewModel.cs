﻿namespace SAJPortal.Areas.Users.ViewModels
{

    #region Assembly

    using System.ComponentModel.DataAnnotations;
    using Resources;

    #endregion

    /// <summary>
    /// مدل ثبت نام کاربر
    /// </summary>
    public class RegisterViewModel
    {

        /// <summary>
        /// نام کاربری
        /// </summary>
        [Required(ErrorMessageResourceType = typeof(Global), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(Global), Name = "Username")]
        public string Username { get; set; }

        /// <summary>
        /// رمز عبور
        /// </summary>
        [Required(ErrorMessageResourceType = typeof(Global), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(Global), Name = "Password")]
        public string Password { get; set; }

        /// <summary>
        /// ایمیل
        /// </summary>
        [Required(ErrorMessageResourceType = typeof(Global), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(Global), Name = "Email")]
        public string Email { get; set; }

        /// <summary>
        /// سوال رمز عبور
        /// </summary>
        [Required(ErrorMessageResourceType = typeof(Global), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(Global), Name = "PasswordQuestion")]
        public string PasswordQuestion { get; set; }

        /// <summary>
        /// جواب رمز عبور
        /// </summary>
        [Required(ErrorMessageResourceType = typeof(Global), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(Global), Name = "PasswordAnswer")]
        public string PasswordAnswer { get; set; }

        /// <summary>
        /// شماره تلفن
        /// </summary>
        [Required(ErrorMessageResourceType = typeof(Global), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(Global), Name = "PhoneNumber")]
        public string PhoneNumber { get; set; }

        /// <summary>
        /// نام
        /// </summary>
        [Display(ResourceType = typeof(Global), Name = "FirstName")]
        public string FirstName { get; set; }

        /// <summary>
        /// نام خانوادگی
        /// </summary>
        [Display(ResourceType = typeof(Global), Name = "LastName")]
        public string LastName { get; set; }

    }
}