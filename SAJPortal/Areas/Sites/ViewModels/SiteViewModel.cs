﻿namespace SAJPortal.Areas.Sites.ViewModels
{

    #region Assembly

    using System.ComponentModel.DataAnnotations;
    using Resources;
    using ORM.Enums;

    #endregion

    /// <summary>
    /// مدل سایت
    /// </summary>
    public class SiteViewModel
    {

        /// <summary>
        /// شناسه
        /// </summary>
        public int ItemId { get; set; }

        /// <summary>
        /// نام سایت
        /// </summary>
        [Required(ErrorMessageResourceType = typeof(Global), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(Global), Name = "SiteName")]
        public string SiteName { get; set; }

        /// <summary>
        /// عنوان
        /// </summary>
        [Display(ResourceType = typeof(Global), Name = "Title")]
        public string Title { get; set; }

        /// <summary>
        /// برچسب
        /// </summary>
        [Display(ResourceType = typeof(Global), Name = "Label")]
        public string Label { get; set; }

        /// <summary>
        /// نوع نسخه سایت
        /// </summary>
        [Required(ErrorMessageResourceType = typeof(Global), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(Global), Name = "Mode")]
        public SiteMode Mode { get; set; }

        /// <summary>
        /// فرهنگ
        /// </summary>
        [Required(ErrorMessageResourceType = typeof(Global), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(Global), Name = "Cultures")]
        public string Culture { get; set; }

        /// <summary>
        /// دامنه ها در قالب JSON
        /// </summary>
        [Display(ResourceType = typeof(Global), Name = "Domains")]
        public string Domains { get; set; }

        /// <summary>
        /// لیست دامنه ها
        /// </summary>
        [Display(ResourceType = typeof(Global), Name = "Domains")]
        public string[] DomainsList { get; set; }

        /// <summary>
        /// مسیر اضافی سایت
        /// </summary>
        [Display(ResourceType = typeof(Global), Name = "SitePath")]
        public string SitePath { get; set; }
        /// <summary>
        /// نسخه
        /// </summary>
        [Display(ResourceType = typeof(Global), Name = "Version")]
        public string Version { get; set; }

        /// <summary>
        /// مرورگرهای کاربر در قالب JSON
        /// </summary>
        [Display(ResourceType = typeof(Global), Name = "UserAgent")]
        public string UserAgent { get; set; }

        /// <summary>
        /// لیست مرورگرهای کاربر
        /// </summary>
        [Display(ResourceType = typeof(Global), Name = "UserAgent")]
        public string[] UserAgentList { get; set; }

        /// <summary>
        /// آنلاین بودن سایت
        /// </summary>
        [Required(ErrorMessageResourceType = typeof(Global), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(Global), Name = "IsOnline")]
        public bool IsOnline { get; set; }

        /// <summary>
        /// سایت شروع
        /// </summary>
        [Required(ErrorMessageResourceType = typeof(Global), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(Global), Name = "IsBase")]
        public bool IsBase { get; set; }

        /// <summary>
        /// هاست ایمیل
        /// </summary>
        [Display(ResourceType = typeof(Global), Name = "SmtpHost")]
        public string SmtpHost { get; set; }

        /// <summary>
        /// نام کاربری ایمیل
        /// </summary>
        [Display(ResourceType = typeof(Global), Name = "SmtpUsername")]
        public string SmtpUsername { get; set; }

        /// <summary>
        /// رمز عبور ایمیل
        /// </summary>
        [Display(ResourceType = typeof(Global), Name = "SmtpPassword")]
        public string SmtpPassword { get; set; }

        /// <summary>
        /// درگاه ایمیل
        /// </summary>
        [Display(ResourceType = typeof(Global), Name = "SmtpPort")]
        public string SmtpPort { get; set; }

        /// <summary>
        /// ارسال کننده ایمیل
        /// </summary>
        [Display(ResourceType = typeof(Global), Name = "SmtpFrom")]
        public string SmtpFrom { get; set; }

        /// <summary>
        /// دریافت کننده های ایمیل در قالب JSON
        /// </summary>
        [Display(ResourceType = typeof(Global), Name = "SmtpTo")]
        public string SmtpTo { get; set; }

        /// <summary>
        /// لیست دریافت کننده های ایمیل
        /// </summary>
        [Display(ResourceType = typeof(Global), Name = "SmtpTo")]
        public string[] SmtpToList { get; set; }

        /// <summary>
        /// نویسنده
        /// </summary>
        [Display(ResourceType = typeof(Global), Name = "Author")]
        public string Author { get; set; }

        /// <summary>
        /// کلمات کلیدی
        /// </summary>
        [Display(ResourceType = typeof(Global), Name = "Keyword")]
        public string Keyword { get; set; }

        /// <summary>
        /// توضیحات
        /// </summary>
        [Display(ResourceType = typeof(Global), Name = "Description")]
        public string Description { get; set; }

        /// <summary>
        /// نام فیلدهای سفارشی در قالب JSON
        /// </summary>
        [Display(ResourceType = typeof(Global), Name = "CustomFields")]
        public string CustomFields { get; set; }

        /// <summary>
        /// مقدار فیلدهای سفارشی در قالب JSON 
        /// </summary>
        [Display(ResourceType = typeof(Global), Name = "CustomFields")]
        public string CustomFieldsValue { get; set; }

        /// <summary>
        /// لیست نام فیلدهای سفارشی
        /// </summary>
        [Display(ResourceType = typeof(Global), Name = "CustomFields")]
        public string[] CustomFieldsList { get; set; }

        /// <summary>
        /// لیست مقدار فیلدهای سفارشی
        /// </summary>
        [Display(ResourceType = typeof(Global), Name = "CustomFields")]
        public string[] CustomFieldsValueList { get; set; }

        /// <summary>
        /// استاندارد سازی ادرس های سایت
        /// </summary>
        [Display(ResourceType = typeof(Global), Name = "Canonical")]
        public string Canonical { get; set; }

        /// <summary>
        /// بلاک های متا در قالب JSON
        /// </summary>
        [Display(ResourceType = typeof(Global), Name = "HtmlMetaBlock")]
        public string HtmlMetaBlock { get; set; }

        /// <summary>
        /// لیست بلاک های متا
        /// </summary>
        [Display(ResourceType = typeof(Global), Name = "HtmlMetaBlock")]
        public string[] HtmlMetaBlockList { get; set; }

        /// <summary>
        /// منطقه زمانی
        /// </summary>
        [Display(ResourceType = typeof(Global), Name = "TimeZoneId")]
        public string TimeZoneId { get; set; }

        /// <summary>
        /// شناسه عضویت
        /// </summary>
        [Required(ErrorMessageResourceType = typeof(Global), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(Global), Name = "MembershipId")]
        public int MembershipId { get; set; }

        /// <summary>
        /// شناسه مخزن
        /// </summary>
        [Required(ErrorMessageResourceType = typeof(Global), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(Global), Name = "RepositoryId")]
        public int RepositoryId { get; set; }

        /// <summary>
        /// شناسه پدر
        /// </summary>
        [Display(ResourceType = typeof(Global), Name = "ParentId")]
        public int ParentId { get; set; }

    }
}