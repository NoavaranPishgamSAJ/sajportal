﻿namespace SAJPortal.Areas.Sites.Controllers
{

    #region Assembly

    using DataService;
    using ORM.Model;
    using System.Web.Mvc;
    using ViewModels;
    using System;
    using ORM.Condition;
    using AutoMapper;
    using System.Linq;
    using System.Collections.Generic;
    using System.Globalization;
    using Resources;
    using CMS.Attribute;
    using ORM.Enums;
    using CMS.Helper;

    #endregion

    /// <summary>
    /// کنترل کننده مدیریت سایت ها
    /// </summary>
    [OutputCache(VaryByParam = "*", Duration = 0, NoStore = true)]
    [LoggedOrAuthorized]
    public class HomeController : Controller
    {

        #region Attribute

        private const int PageSize = 10;

        private int PageIndex
        {
            get
            {
                if (TempData["PageIndex"] == null)
                    TempData["PageIndex"] = 1;
                return Convert.ToInt32(TempData["PageIndex"]);
            }
            set { TempData["PageIndex"] = value; }
        }

        private long PageCount
        {
            set { TempData["PageCount"] = value; }
        }

        private FilterExpressionList Criterias
        {
            get
            {
                if (TempData["Criterias"] == null)
                    TempData["Criterias"] = new FilterExpressionList();
                return TempData["Criterias"] as FilterExpressionList;
            }
            set { TempData["Criterias"] = value; }
        }

        #endregion

        #region Method

        /// <summary>
        /// لیست سایت ها
        /// </summary>
        /// <returns>ActionResult</returns>
        [DapperAuthorize(Roles = "SitesList")]
        public ActionResult Index()
        {
            PageIndex = 1;
            Criterias = new FilterExpressionList();
            if (!User.IsInRole("IsAdmin") && !Criterias.ContainFilter("ItemId"))
            {
                Criterias.Add(new ORM.Condition.Filter("ItemId", SqlOperators.Equal, ModuleHelper.GetBaseSite().ItemId));
            }
            else if (!Criterias.ContainFilter("ItemId"))
                Criterias.Add(new ORM.Condition.Filter("ItemId", SqlOperators.NotEqual, 1));
            var list = DynamicService.Current.GetList<Site>(PageIndex - 1, PageSize, Criterias, string.Empty);
            PageCount = Convert.ToInt64(Math.Ceiling((double)DynamicService.Current.GetCount<Site>(Criterias, string.Empty) / PageSize));
            return View(list);
        }

        /// <summary>
        /// لیست سایت ها
        /// </summary>
        /// <param name="search">فیلتر لیست: string</param>
        /// <param name="formType">نوع فیلتر: string</param>
        /// <returns>ActionResult</returns>
        [DapperAuthorize(Roles = "SitesList")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index(string search, string formType)
        {
            if (formType.Equals("pager") && Request.Form.AllKeys.Contains("PageIndex"))
                PageIndex = Convert.ToInt32(Request.Form["PageIndex"]);
            else if (formType.Equals("search"))
            {
                PageIndex = 1;
                Criterias.Clear();
                Criterias.Add(new ORM.Condition.Filter("Label", SqlOperators.Like, search, SqlOperand.And, DbType.NVarChar));
            }
            if (!User.IsInRole("IsAdmin") && !Criterias.ContainFilter("ItemId"))
            {
                Criterias.Add(new ORM.Condition.Filter("ItemId", SqlOperators.Equal, ModuleHelper.GetBaseSite().ItemId));
            }
            else if (!Criterias.ContainFilter("ItemId"))
                Criterias.Add(new ORM.Condition.Filter("ItemId", SqlOperators.NotEqual, 1));
            var list = DynamicService.Current.GetList<Site>(PageIndex - 1, PageSize, Criterias, string.Empty);
            PageCount = Convert.ToInt64(Math.Ceiling((double)DynamicService.Current.GetCount<Site>(Criterias, string.Empty) / PageSize));
            return View(list);
        }

        /// <summary>
        /// فرم سایت جدید
        /// </summary>
        /// <returns>ActionResult</returns>
        [DapperAuthorize(Roles = "NewSite")]
        [Admin]
        [HttpGet]
        public ActionResult New()
        {
            GetMemberships();
            GetRepositories();
            GetCultures();
            GetTimeZones();
            GetSites();
            return View();
        }

        /// <summary>
        /// ثبت سایت
        /// </summary>
        /// <param name="model">مدل سایت: SiteViewModel</param>
        /// <returns>ActionResult</returns>
        [DapperAuthorize(Roles = "NewSite")]
        [Admin]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult New(SiteViewModel model)
        {
            if (ModelState.IsValid)
            {
                DynamicService.Current.CreateItem(new Site
                {
                    SiteName = model.SiteName,
                    Title = model.Title,
                    Label = model.Label,
                    Mode = model.Mode,
                    Culture = model.Culture,
                    DomainsList = model.DomainsList,
                    SitePath = model.SitePath,
                    Version = model.Version,
                    UserAgentList = model.UserAgentList,
                    IsOnline = model.IsOnline.GetToggle(),
                    IsBase = model.IsBase.GetToggle(),
                    SmtpHost = model.SmtpHost,
                    SmtpUsername = model.SmtpUsername,
                    SmtpPassword = model.SmtpPassword,
                    SmtpPort = model.SmtpPort,
                    SmtpFrom = model.SmtpFrom,
                    SmtpToList = model.SmtpToList,
                    Author = model.Author,
                    Keyword = model.Keyword,
                    Description = model.Description,
                    CustomFieldsList = model.CustomFieldsList,
                    CustomFieldsValueList = model.CustomFieldsValueList,
                    Canonical = model.Canonical,
                    HtmlMetaBlockList = model.HtmlMetaBlockList,
                    TimeZoneId = model.TimeZoneId,
                    MembershipId = model.MembershipId,
                    RepositoryId = model.RepositoryId,
                    ParentId = model.ParentId.Equals(-1) ? (int?)null : model.ParentId,
                    AddedDate = DateTime.Today
                });

                return RedirectToAction("Index", "Home");
            }
            GetMemberships();
            GetRepositories();
            GetCultures();
            GetTimeZones();
            GetSites();
            return View(model);
        }

        /// <summary>
        /// فرم ویرایش سایت
        /// </summary>
        /// <param name="id">شناسه سایت: int</param>
        /// <returns>ActionResult</returns>
        [DapperAuthorize(Roles = "EditSite")]
        [HttpGet]
        public ActionResult Edit(int id)
        {
            var site = DynamicService.Current.GetItem<Site>(id);
            if (site == null)
                return HttpNotFound();
            var siteVm = Mapper.Map<SiteViewModel>(site);
            GetMemberships();
            GetRepositories();
            GetCultures();
            GetTimeZones();
            GetSites();
            return View(siteVm);
        }

        /// <summary>
        /// ویرایش سایت
        /// </summary>
        /// <param name="id">شناسه سایت: int</param>
        /// <param name="model">مدل سایت: SiteViewModel</param>
        /// <returns>ActionResult</returns>
        [DapperAuthorize(Roles = "EditSite")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, SiteViewModel model)
        {
            if (ModelState.IsValid)
            {
                var site = DynamicService.Current.GetItem<Site>(id);
                if (site == null)
                    return HttpNotFound();
                site.SiteName = model.SiteName;
                site.Title = model.Title;
                site.Label = model.Label;
                site.Mode = model.Mode;
                site.Culture = model.Culture;
                site.DomainsList = model.DomainsList;
                site.SitePath = model.SitePath;
                site.Version = model.Version;
                site.UserAgentList = model.UserAgentList;
                site.IsOnline = model.IsOnline.GetToggle();
                site.IsBase = model.IsBase.GetToggle();
                site.SmtpHost = model.SmtpHost;
                site.SmtpUsername = model.SmtpUsername;
                site.SmtpPassword = model.SmtpPassword;
                site.SmtpPort = model.SmtpPort;
                site.SmtpFrom = model.SmtpFrom;
                site.SmtpToList = model.SmtpToList;
                site.Author = model.Author;
                site.Keyword = model.Keyword;
                site.Description = model.Description;
                site.CustomFieldsList = model.CustomFieldsList;
                site.CustomFieldsValueList = model.CustomFieldsValueList;
                site.Canonical = model.Canonical;
                site.HtmlMetaBlockList = model.HtmlMetaBlockList;
                site.TimeZoneId = model.TimeZoneId;
                site.MembershipId = model.MembershipId;
                site.RepositoryId = model.RepositoryId;
                site.ParentId = model.ParentId.Equals(-1) ? (int?)null : model.ParentId;
                site.ModifiedDate = DateTime.Today;

                DynamicService.Current.UpdateItem(site);
                return RedirectToAction("Index", "Home");
            }
            GetMemberships();
            GetRepositories();
            GetCultures();
            GetTimeZones();
            GetSites();
            return View(model);
        }

        /// <summary>
        /// حذف سایت
        /// </summary>
        /// <param name="id">شناسه سایت: int</param>
        /// <returns>ActionResult</returns>
        [DapperAuthorize(Roles = "DeleteSite")]
        [Admin]
        [HttpGet]
        public ActionResult Delete(int id)
        {
            DynamicService.Current.DeleteItem<Site>(id);
            return RedirectToAction("Index", "Home", new { area = "Sites" });
        }

        /// <summary>
        /// لیست عضویت ها
        /// </summary>
        public void GetMemberships()
        {
            var memberships = DynamicService.Current.GetList<Membership>();
            ViewBag.Memberships = memberships.Select(m => new SelectListItem { Text = m.MembershipName, Value = m.ItemId.ToString() }).ToList();
        }

        /// <summary>
        /// لیست مخازن
        /// </summary>
        public void GetRepositories()
        {
            var repositories = DynamicService.Current.GetList<Repository>();
            ViewBag.Repositories = repositories.Select(r => new SelectListItem { Text = r.RepositoryName, Value = r.ItemId.ToString() }).ToList();
        }

        /// <summary>
        /// لیست فرهنگ ها
        /// </summary>
        public void GetCultures()
        {
            ViewBag.Cultures = GetCountryList().Select(c => new SelectListItem { Text = c.Value, Value = c.Value }).ToList();
        }

        /// <summary>
        /// لیست منطقه های زمانی
        /// </summary>
        public void GetTimeZones()
        {
            ViewBag.TimeZones = TimeZoneInfo.GetSystemTimeZones().Select(c => new SelectListItem() { Text = c.DisplayName, Value = c.Id }).ToList();
        }

        /// <summary>
        /// لیست سایت ها
        /// </summary>
        public void GetSites()
        {
            var sites = DynamicService.Current.GetList<Site>();
            var none = new List<SelectListItem>
            {
                new SelectListItem {Text = CultureHelper.Localize("ComboNone"), Value = "-1"}
            };
            none.AddRange(sites.Select(s => new SelectListItem { Text = s.Label, Value = s.ItemId.ToString() }).ToList());
            ViewBag.Sites = none;
        }

        /// <summary>
        /// لیست کشورها
        /// </summary>
        /// <returns>Dictionary&lt;string, string&gt;</returns>
        public static Dictionary<string, string> GetCountryList()
        {
            var cultures = CultureInfo.GetCultures(CultureTypes.AllCultures & ~CultureTypes.NeutralCultures);
            return cultures.ToDictionary(culture => culture.Name, culture => culture.TextInfo.CultureName);
        }

        #endregion 

    }
}