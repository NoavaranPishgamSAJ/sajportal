﻿namespace SAJPortal.Areas.Pages.ViewModels
{

    #region Assembly

    using System.ComponentModel.DataAnnotations;
    using Resources;
    using ORM.Enums;

    #endregion

    /// <summary>
    /// مدل صفحات
    /// </summary>
    public class PageViewModel
    {

        /// <summary>
        /// شناسه
        /// </summary>
        public int ItemId { get; set; }

        /// <summary>
        /// نام صفحه
        /// </summary>
        [Required(ErrorMessageResourceType = typeof(Global), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(Global), Name = "PageName")]
        public string PageName { get; set; }

        /// <summary>
        /// مسیر نسبی
        /// </summary>
        [Required(ErrorMessageResourceType = typeof(Global), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(Global), Name = "VirtualPath")]
        public string VirtualPath { get; set; }

        /// <summary>
        /// قالب صفحه
        /// </summary>
        [Required(ErrorMessageResourceType = typeof(Global), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(Global), Name = "Template")]
        public string Template { get; set; }

        /// <summary>
        /// عنوان صفحه
        /// </summary>
        [Display(ResourceType = typeof(Global), Name = "Title")]
        public string Title { get; set; }

        /// <summary>
        /// عنوان منو
        /// </summary>
        [Display(ResourceType = typeof(Global), Name = "MenuTitle")]
        public string MenuTitle { get; set; }

        /// <summary>
        /// نام پارامترهای مسیریابی درقالب JSON
        /// </summary>
        [Display(ResourceType = typeof(Global), Name = "RouteValues")]
        public string RouteFields { get; set; }

        /// <summary>
        /// مقدار پارامترهای مسیریابی درقالب JSON
        /// </summary>
        [Display(ResourceType = typeof(Global), Name = "RouteValues")]
        public string RouteFieldsValue { get; set; }

        /// <summary>
        /// لیست نام پارامترهای مسیریابی
        /// </summary>
        [Display(ResourceType = typeof(Global), Name = "RouteValues")]
        public string[] RouteFieldsList { get; set; }

        /// <summary>
        /// لیست مقدار پارامترهای مسیریابی
        /// </summary>
        [Display(ResourceType = typeof(Global), Name = "RouteValues")]
        public string[] RouteFieldsValueList { get; set; }

        /// <summary>
        /// نام پارامترهای صفحه در قالب JSON
        /// </summary>
        [Display(ResourceType = typeof(Global), Name = "Parameters")]
        public string Parameters { get; set; }

        /// <summary>
        /// مقدار پارامترهای صفحه در قالب JSON
        /// </summary>
        [Display(ResourceType = typeof(Global), Name = "Parameters")]
        public string ParametersValue { get; set; }

        /// <summary>
        /// لیست نام پارامترهای صفحه
        /// </summary>
        [Display(ResourceType = typeof(Global), Name = "Parameters")]
        public string[] ParametersList { get; set; }

        /// <summary>
        /// لیست مقدار پارامترهای صفحه
        /// </summary>
        [Display(ResourceType = typeof(Global), Name = "Parameters")]
        public string[] ParametersValueList { get; set; }

        /// <summary>
        /// نوع صفحه
        /// </summary>
        [Required(ErrorMessageResourceType = typeof(Global), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(Global), Name = "PageType")]
        public PageTypes PageType { get; set; }

        /// <summary>
        /// نمایش در منو
        /// </summary>
        [Required(ErrorMessageResourceType = typeof(Global), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(Global), Name = "ShowInMenu")]
        public bool ShowInMenu { get; set; }

        /// <summary>
        /// نمایش در ردپا
        /// </summary>
        [Required(ErrorMessageResourceType = typeof(Global), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(Global), Name = "ShowInBreadcrumb")]
        public bool ShowInBreadcrumb { get; set; }

        /// <summary>
        /// صفحه آغازین
        /// </summary>
        [Required(ErrorMessageResourceType = typeof(Global), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(Global), Name = "IsFirstPage")]
        public bool IsFirstPage { get; set; }

        /// <summary>
        /// نام مجوز مورد نیاز برای دیدن صفحه
        /// </summary>
        [Display(ResourceType = typeof(Global), Name = "RoleName")]
        public string RoleName { get; set; }

        /// <summary>
        /// شناسه والد
        /// </summary>
        [Required(ErrorMessageResourceType = typeof(Global), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(Global), Name = "ParentId")]
        public int ParentId { get; set; }

        /// <summary>
        /// شناسه سایت
        /// </summary>
        [Required(ErrorMessageResourceType = typeof(Global), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(Global), Name = "SiteId")]
        public int SiteId { get; set; }

    }
}