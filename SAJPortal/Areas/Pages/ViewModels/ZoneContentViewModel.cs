﻿namespace SAJPortal.Areas.Pages.ViewModels
{

    #region Assembly

    using System.ComponentModel.DataAnnotations;
    using Resources;

    #endregion

    /// <summary>
    /// مدل ناحیه های صفحات
    /// </summary>
    public class ZoneContentViewModel
    {

        /// <summary>
        /// شناسه
        /// </summary>
        public int ItemId { get; set; }

        /// <summary>
        /// نام ناحیه
        /// </summary>
        [Required(ErrorMessageResourceType = typeof(Global), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(Global), Name = "ZoneName")]
        public string ZoneName { get; set; }

        /// <summary>
        /// ترتیب
        /// </summary>
        [Required(ErrorMessageResourceType = typeof(Global), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(Global), Name = "OrderId")]
        public int OrderId { get; set; }

        /// <summary>
        /// مخفی بودن
        /// </summary>
        [Required(ErrorMessageResourceType = typeof(Global), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(Global), Name = "Hidden")]
        public bool Hidden { get; set; }

        /// <summary>
        /// نام پارامترهای آدرس در قالب JSON
        /// </summary>
        [Display(ResourceType = typeof(Global), Name = "RouteValues")]
        public string RouteFields { get; set; }

        /// <summary>
        /// مقدار پارامترهای آدرس در قالب JSON
        /// </summary>
        [Display(ResourceType = typeof(Global), Name = "RouteValues")]
        public string RouteFieldsValue { get; set; }

        /// <summary>
        /// لیست نام پارامترهای آدرس
        /// </summary>
        [Display(ResourceType = typeof(Global), Name = "RouteValues")]
        public string[] RouteFieldsList { get; set; }

        /// <summary>
        /// لیست مقدار پارامترهای آدرس
        /// </summary>
        [Display(ResourceType = typeof(Global), Name = "RouteValues")]
        public string[] RouteFieldsValueList { get; set; }

        /// <summary>
        /// محتوای HTML
        /// </summary>
        [Display(ResourceType = typeof(Global), Name = "HtmlString")]
        public string HtmlString { get; set; }

        /// <summary>
        /// شناسه نما
        /// </summary>
        [Display(ResourceType = typeof(Global), Name = "ViewId")]
        public int ViewId { get; set; }

    }
}