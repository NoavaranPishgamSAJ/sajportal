﻿namespace SAJPortal.Areas.Pages.Controllers
{

    #region Assembly

    using DataService;
    using ORM.Model;
    using System.Web.Mvc;
    using ViewModels;
    using System;
    using ORM.Condition;
    using AutoMapper;
    using System.Linq;
    using CMS.Attribute;
    using ORM.Enums;
    using System.Collections.Generic;
    using Resources;
    using CMS.Helper;

    #endregion

    /// <summary>
    /// کنترل کننده مدیریت ناحیه های صفحات
    /// </summary>
    [OutputCache(VaryByParam = "*", Duration = 0, NoStore = true)]
    [LoggedOrAuthorized]
    public class ZonesController : Controller
    {

        #region Attribute

        private const int PageSize = 10;

        private int PageIndex
        {
            get
            {
                if (TempData["PageIndex"] == null)
                    TempData["PageIndex"] = 1;
                return Convert.ToInt32(TempData["PageIndex"]);
            }
            set { TempData["PageIndex"] = value; }
        }

        private long PageCount
        {
            set { TempData["PageCount"] = value; }
        }

        private FilterExpressionList Criterias
        {
            get
            {
                if (TempData["Criterias"] == null)
                    TempData["Criterias"] = new FilterExpressionList();
                return TempData["Criterias"] as FilterExpressionList;
            }
            set { TempData["Criterias"] = value; }
        }

        #endregion

        #region Method

        /// <summary>
        /// لیست ناحیه ها
        /// </summary>
        /// <param name="pid">شناسه صفحه: int</param>
        /// <returns>ActionResult</returns>
        [DapperAuthorize(Roles = "ZonesList")]
        public ActionResult Index(int pid)
        {
            PageIndex = 1;
            Criterias = new FilterExpressionList();
            if (!Criterias.ContainFilter("PageId"))
                Criterias.Add(new ORM.Condition.Filter("PageId", SqlOperators.Equal, pid));
            var list = DynamicService.Current.GetList<ZoneContent>(PageIndex - 1, PageSize, Criterias, string.Empty);
            PageCount = Convert.ToInt64(Math.Ceiling((double)DynamicService.Current.GetCount<ZoneContent>(Criterias, string.Empty) / PageSize));
            return View(list);
        }

        /// <summary>
        /// لیست ناحیه ها
        /// </summary>
        /// <param name="pid">شناسه صفحه: int</param>
        /// <param name="search">فیلتر لیست: string</param>
        /// <param name="formType">نوع فیلتر: string</param>
        /// <returns>ActionResult</returns>
        [DapperAuthorize(Roles = "ZonesList")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index(int pid, string search, string formType)
        {
            if (formType.Equals("pager") && Request.Form.AllKeys.Contains("PageIndex"))
                PageIndex = Convert.ToInt32(Request.Form["PageIndex"]);
            else if (formType.Equals("search"))
            {
                PageIndex = 1;
                Criterias.Clear();
                Criterias.Add(new ORM.Condition.Filter("ZoneName", SqlOperators.Like, search, SqlOperand.And, DbType.NVarChar));
            }
            if (!Criterias.ContainFilter("PageId"))
                Criterias.Add(new ORM.Condition.Filter("PageId", SqlOperators.Equal, pid));
            var list = DynamicService.Current.GetList<ZoneContent>(PageIndex - 1, PageSize, Criterias, string.Empty);
            PageCount = Convert.ToInt64(Math.Ceiling((double)DynamicService.Current.GetCount<ZoneContent>(Criterias, string.Empty) / PageSize));
            return View(list);
        }

        /// <summary>
        /// فرم ناحیه جدید
        /// </summary>
        /// <param name="pid">شناسه صفحه: int</param>
        /// <returns>ActionResult</returns>
        [DapperAuthorize(Roles = "NewZone")]
        [HttpGet]
        public ActionResult New(int pid)
        {
            GetViews();
            return View();
        }

        /// <summary>
        /// ثب ناحیه
        /// </summary>
        /// <param name="pid">شناسه صفحه: int</param>
        /// <param name="model">مدل ناحیه: ZoneContentViewModel</param>
        /// <returns>ActionResult</returns>
        [DapperAuthorize(Roles = "NewZone")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult New(int pid, ZoneContentViewModel model)
        {
            if (ModelState.IsValid)
            {
                DynamicService.Current.CreateItem(new ZoneContent
                {
                    ZoneName = model.ZoneName,
                    OrderId = model.OrderId,
                    Hidden = model.Hidden.GetToggle(),
                    RouteFieldsList = model.RouteFieldsList,
                    RouteFieldsValueList = model.RouteFieldsValueList,
                    HtmlString = model.HtmlString,
                    ViewId = model.ViewId != -1 ? (int?)model.ViewId : null,
                    PageId = pid,
                    AddedDate = DateTime.Today
                });
                return RedirectToAction("Index", "Zones", new { pid });
            }
            GetViews();
            return View(model);
        }

        /// <summary>
        /// فرم ویرایش ناحیه
        /// </summary>
        /// <param name="pid">شناسه صفحه: int</param>
        /// <param name="id">شناسه ناحیه: int</param>
        /// <returns>ActionResult</returns>
        [DapperAuthorize(Roles = "EditZone")]
        [HttpGet]
        public ActionResult Edit(int pid, int id)
        {
            var zoneContent = DynamicService.Current.GetItem<ZoneContent>(id);
            var zoneContentVm = Mapper.Map<ZoneContentViewModel>(zoneContent);
            zoneContentVm.HtmlString = System.Web.HttpUtility.UrlDecode(zoneContent.HtmlString);
            GetViews();
            return View(zoneContentVm);
        }

        /// <summary>
        /// ویرایش ناحیه
        /// </summary>
        /// <param name="pid">شناسه صفحه: int</param>
        /// <param name="id">شناسه ناحیه: int</param>
        /// <param name="model">مدل ناحیه: ZoneContentViewModel</param>
        /// <returns>ActionResult</returns>
        [DapperAuthorize(Roles = "EditZone")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int pid, int id, ZoneContentViewModel model)
        {
            if (ModelState.IsValid)
            {
                var zoneContent = DynamicService.Current.GetItem<ZoneContent>(id);
                zoneContent.ZoneName = model.ZoneName;
                zoneContent.OrderId = model.OrderId;
                zoneContent.Hidden = model.Hidden.GetToggle();
                zoneContent.RouteFieldsList = model.RouteFieldsList;
                zoneContent.RouteFieldsValueList = model.RouteFieldsValueList;
                zoneContent.HtmlString = model.HtmlString;
                zoneContent.ViewId = model.ViewId != -1 ? (int?)model.ViewId : null;
                zoneContent.PageId = pid;
                zoneContent.ModifiedDate = DateTime.Today;

                DynamicService.Current.UpdateItem(zoneContent);
                return RedirectToAction("Index", "Zones", new { pid });
            }
            GetViews();
            return View(model);
        }

        /// <summary>
        /// حذف ناحیه
        /// </summary>
        /// <param name="pid">شناسه صفحه: int</param>
        /// <param name="id">شناسه ناحیه: int</param>
        /// <returns>ActionResult</returns>
        [DapperAuthorize(Roles = "DeleteZone")]
        [HttpGet]
        public ActionResult Delete(int pid, int id)
        {
            DynamicService.Current.DeleteItem<ZoneContent>(id);

            return RedirectToAction("Index", "Zones", new { pid });
        }

        /// <summary>
        /// لیست نماها
        /// </summary>
        public void GetViews()
        {
            var criterias = new FilterExpressionList();
            if (!User.IsInRole("IsAdmin"))
            {
                criterias.Add(new ORM.Condition.Filter("SiteId", SqlOperators.Equal, ModuleHelper.GetBaseSite().ItemId));
            }
            var views = DynamicService.Current.GetList<View>(criterias);
            var none = new List<SelectListItem>
            {
                new SelectListItem {Text = CultureHelper.Localize("ComboNone"), Value = "-1" }
            };
            none.AddRange(views.Select(s => new SelectListItem { Text = s.ViewName, Value = s.ItemId.ToString() }).ToList());
            ViewBag.Views = none;
        }

        #endregion 

    }
}