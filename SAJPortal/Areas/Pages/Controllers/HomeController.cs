﻿namespace SAJPortal.Areas.Pages.Controllers
{

    #region Assembly

    using DataService;
    using ORM.Model;
    using System.Web.Mvc;
    using ViewModels;
    using System;
    using ORM.Condition;
    using ORM.Enums;
    using CMS.Helper;
    using AutoMapper;
    using System.Collections.Generic;
    using System.Linq;
    using Resources;
    using CMS.Attribute;

    #endregion

    /// <summary>
    /// کنترل کننده صفحات
    /// </summary>
    [OutputCache(VaryByParam = "*", Duration = 0, NoStore = true)]
    [LoggedOrAuthorized]
    public class HomeController : Controller
    {

        #region Attribute

        private const int PageSize = 10;

        private int PageIndex
        {
            get
            {
                if (TempData["PageIndex"] == null)
                    TempData["PageIndex"] = 1;
                return Convert.ToInt32(TempData["PageIndex"]);
            }
            set { TempData["PageIndex"] = value; }
        }

        private long PageCount
        {
            set { TempData["PageCount"] = value; }
        }

        private FilterExpressionList Criterias
        {
            get
            {
                if (TempData["Criterias"] == null)
                    TempData["Criterias"] = new FilterExpressionList();
                return TempData["Criterias"] as FilterExpressionList;
            }
            set { TempData["Criterias"] = value; }
        }

        #endregion

        #region Method

        /// <summary>
        /// لیست صفحات
        /// </summary>
        /// <returns>ActionResult</returns>
        [DapperAuthorize(Roles = "PagesList")]
        public ActionResult Index()
        {
            PageIndex = 1;
            Criterias = new FilterExpressionList();
            if (!Criterias.ContainFilter("PageType"))
                Criterias.Add(new ORM.Condition.Filter("PageType", SqlOperators.NotEqual, 1));
            if (!User.IsInRole("IsAdmin") && !Criterias.ContainFilter("SiteId"))
            {
                Criterias.Add(new ORM.Condition.Filter("SiteId", SqlOperators.Equal, ModuleHelper.GetBaseSite().ItemId));
            }
            var list = DynamicService.Current.GetList<Page>(PageIndex - 1, PageSize, Criterias, string.Empty);
            PageCount = Convert.ToInt64(Math.Ceiling((double)DynamicService.Current.GetCount<Page>(Criterias, string.Empty) / PageSize));
            return View(list);
        }

        /// <summary>
        /// لیست صفحات
        /// </summary>
        /// <param name="search">فیلتر لیست: string</param>
        /// <param name="formType">نوع فیلتر: string</param>
        /// <returns>ActionResult</returns>
        [DapperAuthorize(Roles = "PagesList")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index(string search, string formType)
        {
            if (formType.Equals("pager") && Request.Form.AllKeys.Contains("PageIndex"))
                PageIndex = Convert.ToInt32(Request.Form["PageIndex"]);
            else if (formType.Equals("search"))
            {
                PageIndex = 1;
                Criterias.Clear();
                Criterias.Add(new ORM.Condition.Filter("PageName", SqlOperators.Like, search, SqlOperand.And, DbType.NVarChar));
            }
            if (!Criterias.ContainFilter("PageType"))
                Criterias.Add(new ORM.Condition.Filter("PageType", SqlOperators.NotEqual, 1));
            if (!User.IsInRole("IsAdmin") && !Criterias.ContainFilter("SiteId"))
            {
                Criterias.Add(new ORM.Condition.Filter("SiteId", SqlOperators.Equal, ModuleHelper.GetBaseSite().ItemId));
            }
            var list = DynamicService.Current.GetList<Page>(PageIndex - 1, PageSize, Criterias, string.Empty);
            PageCount = Convert.ToInt64(Math.Ceiling((double)DynamicService.Current.GetCount<Page>(Criterias, string.Empty) / PageSize));
            return View(list);
        }

        /// <summary>
        /// فرم صفحه جدید
        /// </summary>
        /// <returns>ActionResult</returns>
        [DapperAuthorize(Roles = "NewPage")]
        [HttpGet]
        public ActionResult New()
        {
            GetPageTypes();
            GetSites();
            GetPages();
            return View();
        }

        /// <summary>
        /// ثبت صفحه
        /// </summary>
        /// <param name="model">مدل صفحه: PageViewModel</param>
        /// <returns>ActionResult</returns>
        [DapperAuthorize(Roles = "NewPage")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult New(PageViewModel model)
        {
            if (ModelState.IsValid)
            {
                DynamicService.Current.CreateItem(new Page
                {
                    PageName = model.PageName,
                    Template = model.Template,
                    VirtualPath = model.VirtualPath,
                    Title = model.Title,
                    MenuTitle = model.MenuTitle,
                    RouteFieldsList = model.RouteFieldsList,
                    RouteFieldsValueList = model.RouteFieldsValueList,
                    ParametersList = model.ParametersList,
                    ParametersValueList = model.ParametersValueList,
                    PageType = model.PageType,
                    ShowInMenu = model.ShowInMenu.GetToggle(),
                    ShowInBreadcrumb = model.ShowInBreadcrumb.GetToggle(),
                    IsFirstPage = model.IsFirstPage.GetToggle(),
                    RoleName = model.RoleName,
                    AddedDate = DateTime.Today,
                    ParentId = model.ParentId.Equals(-1) ? (int?)null : model.ParentId,
                    SiteId = model.SiteId
                });

                return RedirectToAction("Index", "Home");
            }
            GetPageTypes();
            GetSites();
            GetPages();
            return View(model);
        }

        /// <summary>
        /// فرم ویرایش صفحه
        /// </summary>
        /// <param name="id">شناسه صفحه</param>
        /// <returns>ActionResult</returns>
        [DapperAuthorize(Roles = "EditPage")]
        [HttpGet]
        public ActionResult Edit(int id)
        {
            var page = DynamicService.Current.GetItem<Page>(id);
            var pageVm = Mapper.Map<PageViewModel>(page);
            GetPageTypes();
            GetSites();
            GetPages();
            return View(pageVm);
        }

        /// <summary>
        /// ویرایش صفحه
        /// </summary>
        /// <param name="id">شناسه صفحه</param>
        /// <param name="model">مدل صفحه: PageViewModel</param>
        /// <returns>ActionResult</returns>
        [DapperAuthorize(Roles = "EditPage")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, PageViewModel model)
        {
            if (ModelState.IsValid)
            {
                var page = DynamicService.Current.GetItem<Page>(id);
                page.PageName = model.PageName;
                page.Template = model.Template;
                page.VirtualPath = model.VirtualPath;
                page.Title = model.Title;
                page.MenuTitle = model.MenuTitle;
                page.RouteFieldsList = model.RouteFieldsList;
                page.RouteFieldsValueList = model.RouteFieldsValueList;
                page.ParametersList = model.ParametersList;
                page.ParametersValueList = model.ParametersValueList;
                page.PageType = model.PageType;
                page.ShowInMenu = model.ShowInMenu.GetToggle();
                page.ShowInBreadcrumb = model.ShowInBreadcrumb.GetToggle();
                page.IsFirstPage = model.IsFirstPage.GetToggle();
                page.RoleName = model.RoleName;
                page.ModifiedDate = DateTime.Today;
                page.ParentId = model.ParentId.Equals(-1) ? (int?)null : model.ParentId;
                page.SiteId = model.SiteId;

                DynamicService.Current.UpdateItem(page);
                return RedirectToAction("Index", "Home");
            }
            GetPageTypes();
            GetSites();
            GetPages();
            return View(model);
        }

        /// <summary>
        /// حذف صفحه
        /// </summary>
        /// <param name="id">شناسه صفحه</param>
        /// <returns>ActionResult</returns>
        [DapperAuthorize(Roles = "DeletePage")]
        [HttpGet]
        public ActionResult Delete(int id)
        {
            DynamicService.Current.DeleteItem<Page>(id);
            return RedirectToAction("Index", "Home", new { area = "Pages" });
        }

        /// <summary>
        /// لیست نوع صفحات
        /// </summary>
        public void GetPageTypes()
        {
            ViewBag.PageTypes = ModelHelper.EnumToSelectList<PageTypes>();
        }

        /// <summary>
        /// لیست سایت ها
        /// </summary>
        public void GetSites()
        {
            var criterias = new FilterExpressionList();
            if (!User.IsInRole("IsAdmin"))
            {
                criterias.Add(new ORM.Condition.Filter("ItemId", SqlOperators.Equal, ModuleHelper.GetBaseSite().ItemId));
            }
            var sites = DynamicService.Current.GetList<Site>(criterias);
            ViewBag.Sites = sites.Select(s => new SelectListItem { Text = s.Label, Value = s.ItemId.ToString() }).ToList();
        }

        /// <summary>
        /// لیست صفحات
        /// </summary>
        public void GetPages()
        {
            var criterias = new FilterExpressionList();
            if (!User.IsInRole("IsAdmin"))
            {
                criterias.Add(new ORM.Condition.Filter("SiteId", SqlOperators.Equal, ModuleHelper.GetBaseSite().ItemId));
            }
            var pages = DynamicService.Current.GetList<Page>(criterias);
            var none = new List<SelectListItem>
            {
                new SelectListItem {Text = CultureHelper.Localize("ComboNone"), Value = "-1"}
            };
            none.AddRange(pages.Select(s => new SelectListItem { Text = s.Title, Value = s.ItemId.ToString() }).ToList());
            ViewBag.Pages = none;
        }

        #endregion

    }
}