﻿namespace SAJPortal.Areas.Pages
{

    #region Assembly

    using System.Web.Mvc;

    #endregion

    /// <summary>
    /// ثبت ناحیه صفحات
    /// </summary>
    public class PagesAreaRegistration : AreaRegistration
    {

        #region Attribute

        /// <summary>
        /// نام ناحیه
        /// </summary>
        public override string AreaName => "Pages";

        #endregion

        #region Method

        /// <summary>
        /// متد ثبت ناحیه
        /// </summary>
        /// <param name="context">AreaRegistrationContext</param>
        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                name: "Pages",
                url: "admin/pages/{action}/{id}",
                defaults: new { controller = "home", action = "index", id = UrlParameter.Optional },
                namespaces: new[] { "SAJPortal.Areas.Pages.Controllers" }
            );

            context.MapRoute(
               name: "Zones",
               url: "admin/pages/zones/{action}/{pid}/{id}",
               defaults: new { controller = "zones", action = "index", pid = UrlParameter.Optional, id = UrlParameter.Optional },
               namespaces: new[] { "SAJPortal.Areas.Pages.Controllers" }
           );
        }

        #endregion

    }
}