﻿namespace SAJPortal.Areas.RepositoryFolders.Controllers
{

    #region Assembly

    using CMS.Attribute;
    using Utility;
    using DataService;
    using System.Web.Mvc;
    using ViewModels;
    using System;
    using ORM.Condition;
    using System.Linq;
    using ORM.Enums;
    using Resources;
    using CMS.Helper;
    using Portable.IO;
    using System.Collections.Generic;
    using Dapper.Contrib.Extensions;
    using ORM.Model;
    using ORM.DescriptiveModel;

    #endregion

    /// <summary>
    /// کنترل کننده پوشه های محتوا
    /// </summary>
    [OutputCache(VaryByParam = "*", Duration = 0, NoStore = true)]
    [LoggedOrAuthorized]
    public class HomeController : Controller
    {

        #region Attribute

        private int _pageSize = 10;

        private int PageIndex
        {
            get
            {
                if (TempData["PageIndex"] == null)
                    TempData["PageIndex"] = 1;
                return Convert.ToInt32(TempData["PageIndex"]);
            }
            set { TempData["PageIndex"] = value; }
        }

        private long PageCount
        {
            set { TempData["PageCount"] = value; }
        }

        private FilterExpressionList Criterias
        {
            get
            {
                if (TempData["Criterias"] == null)
                    TempData["Criterias"] = new FilterExpressionList();
                return TempData["Criterias"] as FilterExpressionList;
            }
            set { TempData["Criterias"] = value; }
        }

        #endregion

        #region Method

        /// <summary>
        /// لیست اطلاعات پوشه محتوا
        /// </summary>
        /// <param name="cid">شناسه محتوا: int</param>
        /// <returns>ActionResult</returns>
        [DapperAuthorize(Roles = "RepositoryFoldersList")]
        public ActionResult Index(int cid)
        {
            PageIndex = 1;
            var content = ModelHelper.GetContent(cid);
            if (content != null)
            {
                if (content.Paging == Toggle.True)
                {
                    _pageSize = content.PageSize;
                }

                // بررسی دارا بودن مجوز کار بر روی محتوا
                if (content.RoleName.IsNotEmpty() && !User.IsInRole(content.RoleName))
                    return Redirect(Uri.EscapeUriString("~/error/1001"));

                // واکشی لیست فیلدهای محتوا
                ViewBag.Fields = content.FieldsObject.Where(p => p.ShowInList.Equal(Toggle.True)).ToList();
                var repository = ModelHelper.GetRepository(content.RepositoryId);
                if (repository != null)
                {
                    Criterias = new FilterExpressionList();

                    // تعیین نام جدول محتوا
                    var tableName = ModelHelper.GetTableName(repository.RepositoryName, content.ContentName);
                    var list = DynamicService.Current.GetList(tableName, PageIndex - 1, _pageSize, Criterias, string.Empty);
                    PageCount = Convert.ToInt64(Math.Ceiling((double)DynamicService.Current.GetCount(tableName, Criterias, string.Empty) / _pageSize));
                    return View(list);
                }
            }
            return View();
        }

        /// <summary>
        /// لیست اطلاعات پوشه محتوا
        /// </summary>
        /// <param name="cid">شناسه محتوا: int</param>
        /// <param name="search">فیلتر لیست: string</param>
        /// <param name="formType">نوع فیلتر: string</param>
        /// <returns>ActionResult</returns>
        [DapperAuthorize(Roles = "RepositoryFoldersList")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index(int cid, string search, string formType)
        {
            var content = ModelHelper.GetContent(cid);
            if (content != null)
            {
                if (content.Paging == Toggle.True)
                {
                    _pageSize = content.PageSize;
                }

                // بررسی دارا بودن مجوز کار بر روی محتوا
                if (content.RoleName.IsNotEmpty() && !User.IsInRole(content.RoleName))
                    return Redirect(Uri.EscapeUriString("~/error/1001"));

                // واکشی لیست فیلدهای محتوا
                ViewBag.Fields = content.FieldsObject.Where(p => p.ShowInList.Equal(Toggle.True)).ToList();
                var repository = ModelHelper.GetRepository(content.RepositoryId);
                if (repository != null)
                {
                    if (formType.Equals("pager") && Request.Form.AllKeys.Contains("PageIndex"))
                        PageIndex = Convert.ToInt32(Request.Form["PageIndex"]);
                    else if (formType.Equals("search"))
                    {
                        PageIndex = 1;
                        Criterias.Clear();
                        Criterias.Add(new ORM.Condition.Filter(ViewBag.Fields[0].FieldName, SqlOperators.Like, search, SqlOperand.And, DbType.NVarChar));
                    }

                    // تعیین نام جدول محتوا
                    var tableName = ModelHelper.GetTableName(repository.RepositoryName, content.ContentName);
                    var list = DynamicService.Current.GetList(tableName, PageIndex - 1, _pageSize, Criterias, string.Empty);
                    PageCount = Convert.ToInt64(Math.Ceiling((double)DynamicService.Current.GetCount(tableName, Criterias, string.Empty) / _pageSize));
                    return View(list);
                }
            }
            return View();
        }

        /// <summary>
        /// فرم ثبت در پوشه محتوا
        /// </summary>
        /// <param name="cid">شناسه محتوا: int</param>
        /// <returns>ActionResult</returns>
        [DapperAuthorize(Roles = "NewRepositoryFolder")]
        [HttpGet]
        public ActionResult New(int cid)
        {
            var content = ModelHelper.GetContent(cid);
            if (content != null)
            {
                // بررسی دارا بودن مجوز کار بر روی محتوا
                if (content.RoleName.IsNotEmpty() && !User.IsInRole(content.RoleName))
                    return Redirect(Uri.EscapeUriString("~/error/1001"));

                // واکشی لیست فیلدهای محتوا
                ViewBag.Fields = content.FieldsObject;

                // واکشی لیست فیلدهای خارجی محتوا
                ViewBag.Categories = content.CategoriesList;

                // دریافت لیست اطلاعات فیلدهای خارجی از جداول آن ها
                foreach (var category in content.CategoriesList)
                {
                    if (category.IsNotEmpty())
                    {
                        var list = DynamicService.Current.GetList(category)?.ToArray();
                        if (list != null && list.Any())
                        {
                            // در هنگام تخصیص دادن لیست فیلدهای خارجی به لیست افتادنی، اگر جدول فیلد خارجی
                            // شامل فیلد DisplayName بود در لیست افتادنی مقدار فیلد نمایشی را برابر DisplayName
                            // می گذارد در غیر اینصورت اگر شامل فیلد Title بود برابر Title می گذارد
                            if (DynamicClass.GetProperty(list[0], "DisplayName") != null)
                                ViewData[category] = list.Select(r => new SelectListItem { Text = r.DisplayName, Value = r.ItemId.ToString() }).ToList();
                            else if (DynamicClass.GetProperty(list[0], "Title") != null)
                                ViewData[category] = list.Select(r => new SelectListItem { Text = r.Title, Value = r.ItemId.ToString() }).ToList();
                        }
                    }
                }
            }
            return View();
        }

        /// <summary>
        /// ثبت در پوشه محتوا
        /// </summary>
        /// <param name="cid">شناسه محتوا: int</param>
        /// <param name="model">مدل محتوا پوشه: RepositoryFolderViewModel</param>
        /// <returns>ActionResult</returns>
        [DapperAuthorize(Roles = "NewRepositoryFolder")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult New(int cid, RepositoryFolderViewModel model)
        {
            var content = ModelHelper.GetContent(cid);
            if (content != null)
            {
                // بررسی دارا بودن مجوز کار بر روی محتوا
                if (content.RoleName.IsNotEmpty() && !User.IsInRole(content.RoleName))
                    return Redirect(Uri.EscapeUriString("~/error/1001"));
                var repository = ModelHelper.GetRepository(content.RepositoryId);
                if (repository != null)
                {
                    // واکشی لیست فیلدهای محتوا
                    ViewBag.Fields = content.FieldsObject;

                    // واکشی لیست فیلدهای خارجی محتوا
                    ViewBag.Categories = content.CategoriesList;
                    // دریافت لیست اطلاعات فیلدهای خارجی از جداول آن ها
                    foreach (var category in content.CategoriesList)
                    {
                        if (category.IsNotEmpty())
                        {
                            var list = DynamicService.Current.GetList(category)?.ToArray();
                            if (list != null && list.Any())
                            {
                                // در هنگام تخصیص دادن لیست فیلدهای خارجی به لیست افتادنی، اگر جدول فیلد خارجی
                                // شامل فیلد DisplayName بود در لیست افتادنی مقدار فیلد نمایشی را برابر DisplayName
                                // می گذارد در غیر اینصورت اگر شامل فیلد Title بود برابر Title می گذارد
                                if (DynamicClass.GetProperty(list[0], "DisplayName") != null)
                                    ViewData[category] = list.Select(r => new SelectListItem { Text = r.DisplayName, Value = r.ItemId.ToString() }).ToList();
                                else if (DynamicClass.GetProperty(list[0], "Title") != null)
                                    ViewData[category] = list.Select(r => new SelectListItem { Text = r.Title, Value = r.ItemId.ToString() }).ToList();
                            }
                        }
                    }
                    var isError = false;
                    // بررسی اعتبارسنجی فیلد
                    foreach (var field in content.FieldsObject)
                    {
                        foreach (var validation in field.FieldValidations)
                        {
                            switch (validation.ValidationName)
                            {
                                case "Required":// الزامی بودن فیلد
                                    if (field.ControlType.Equals("Image"))
                                    {
                                        if (!Request.Files.AllKeys.Contains(field.FieldName) || Request.Files[field.FieldName] == null)
                                        {
                                            ModelState.AddModelError(field.FieldName, string.Format(CultureHelper.Localize("Required"), field.FieldName));
                                            isError = true;
                                        }
                                    }
                                    else if (!Request.Form.AllKeys.Contains(field.FieldName) || Request.Form[field.FieldName].Trim().IsEmpty())
                                    {
                                        ModelState.AddModelError(field.FieldName, string.Format(CultureHelper.Localize("Required"), field.FieldName));
                                        isError = true;
                                    }
                                    break;
                                case "Length":// بررسی طول فیلد
                                    var min = 0;
                                    var max = Convert.ToInt32(validation.SecondParam);
                                    if (validation.ThirdParam.IsNotEmpty())
                                    {
                                        min = Convert.ToInt32(validation.SecondParam);
                                        max = Convert.ToInt32(validation.ThirdParam);
                                    }
                                    if (!Request.Form.AllKeys.Contains(field.FieldName) ||
                                        Request.Form[field.FieldName].Trim().IsEmpty() ||
                                        (Request.Form[field.FieldName].Trim().Length < min && Request.Form[field.FieldName].Trim().Length > max))
                                    {
                                        ModelState.AddModelError(field.FieldName, string.Format(CultureHelper.Localize("Length"), field.FieldName, min, max));
                                        isError = true;
                                    }
                                    break;
                                case "Regex":// اعتبارسنج سفارشی توسط کاربر
                                    break;
                            }
                        }
                    }
                    if (!isError)
                    {
                        var fields = content.FieldsObject;
                        fields.AddRange(content.CategoriesList.Select(category => new ContentField {FieldName = category, DataType = "int", AllowNull = Toggle.False}));

                        // دریافت یک نسخه خالی از موجودیت محتوا
                        var entity = Create(ModelHelper.GetTableName(repository.RepositoryName, content.ContentName), fields);

                        // مقداردهی فیلدهای موجودیت محتوا
                        foreach (var field in content.FieldsObject)
                        {
                            if (Request.Form.AllKeys.Contains(field.FieldName))
                                // اگر نوع فیلد عکس باشد
                                if (field.ControlType.Equals("Image"))
                                {
                                    if (Request.Files[field.FieldName] != null && Request.Files[field.FieldName].ContentLength > 0)
                                    {
                                        var file = Request.Files[field.FieldName];
                                        if (file != null)
                                        {
                                            var path = HttpContext.Server.MapPath($"~/CmsData/PortalFolders/{content.ContentName}/{file.FileName}");
                                            if (!FileSystem.Current.ExistsFile(path))
                                            {
                                                using (var reader = new System.IO.BinaryReader(file.InputStream))
                                                {
                                                    System.IO.File.WriteAllBytes(path, reader.ReadBytes(file.ContentLength));
                                                }
                                                // مقداردهی فیلد موجودیت
                                                DynamicClass.SetProperty(entity, field.FieldName, $"/CmsData/PortalFolders/{content.ContentName}/{file.FileName}");
                                            }
                                        }
                                    }
                                }
                                // اگر نوع فیلد متن باشد
                                else
                                {
                                    // مقداردهی فیلد موجودیت
                                    DynamicClass.SetProperty(entity, field.FieldName, DynamicClass.GetFieldValue(field.DataType, Request.Form[field.FieldName], field.AllowNull.GetValue()));
                                }
                        }

                        // مقداردهی فیلدهای موجودیت محتوا از نوع لیست افتادنی
                        foreach (var category in content.CategoriesList)
                        {
                            if (category.IsNotEmpty())
                            {
                                var categoryName = $"{category}Id";
                                if (Request.Form.AllKeys.Contains(categoryName))
                                    // مقداردهی فیلد موجودیت
                                    DynamicClass.SetProperty(entity, categoryName, Convert.ToInt32(Request.Form[categoryName]));
                            }
                        }
                        DynamicClass.SetProperty(entity, "AddedDate", DateTime.Today);
                        DynamicService.Current.CreateItem(entity);
                        return RedirectToAction("Index", "Home", new { cid });
                    }
                }
            }
            return View();
        }

        /// <summary>
        /// فرم ویرایش از پوشه محتوا
        /// </summary>
        /// <param name="cid">شناسه محتوا: int</param>
        /// <param name="id">شناسه پوشه محتوا: int</param>
        /// <returns>ActionResult</returns>
        [DapperAuthorize(Roles = "EditRepositoryFolder")]
        [HttpGet]
        public ActionResult Edit(int cid, int id)
        {
            var content = ModelHelper.GetContent(cid);
            if (content != null)
            {
                // بررسی دارا بودن مجوز کار بر روی محتوا
                if (content.RoleName.IsNotEmpty() && !User.IsInRole(content.RoleName))
                    return Redirect(Uri.EscapeUriString("~/error/1001"));

                // واکشی لیست فیلدهای محتوا
                ViewBag.Fields = content.FieldsObject;

                // واکشی لیست فیلدهای خارجی محتوا
                ViewBag.Categories = content.CategoriesList;
                var repository = ModelHelper.GetRepository(content.RepositoryId);
                if (repository != null)
                {
                    Criterias = new FilterExpressionList();
                    var tableName = ModelHelper.GetTableName(repository.RepositoryName, content.ContentName);
                    var repositoryFolder = DynamicService.Current.GetItem(tableName, id);
                    // دریافت لیست اطلاعات فیلدهای خارجی از جداول آن ها
                    foreach (var category in content.CategoriesList)
                    {
                        if (category.IsNotEmpty())
                        {
                            var value = DynamicClass.GetProperty(repositoryFolder, $"{category}Id");
                            var list = DynamicService.Current.GetList(category)?.ToArray();
                            if (list != null && list.Any())
                            {
                                // در هنگام تخصیص دادن لیست فیلدهای خارجی به لیست افتادنی، اگر جدول فیلد خارجی
                                // شامل فیلد DisplayName بود در لیست افتادنی مقدار فیلد نمایشی را برابر DisplayName
                                // می گذارد در غیر اینصورت اگر شامل فیلد Title بود برابر Title می گذارد
                                if (DynamicClass.GetProperty(list[0], "DisplayName") != null)
                                    ViewData[category] = list.Select(r => new SelectListItem { Text = r.DisplayName, Value = r.ItemId.ToString(), Selected = r.ItemId.ToString() == value.ToString() }).ToList();
                                else if (DynamicClass.GetProperty(list[0], "Title") != null)
                                    ViewData[category] = list.Select(r => new SelectListItem { Text = r.Title, Value = r.ItemId.ToString(), Selected = r.ItemId.ToString() == value.ToString() }).ToList();
                            }
                        }
                    }
                    return View(repositoryFolder);
                }
            }
            return View();
        }

        /// <summary>
        /// ویرایش از پوشه محتوا
        /// </summary>
        /// <param name="cid">شناسه محتوا: int</param>
        /// <param name="id">شناسه پوشه محتوا: int</param>
        /// <param name="model">مدل پوشه محتوا: RepositoryFolderViewModel</param>
        /// <returns>ActionResult</returns>
        [DapperAuthorize(Roles = "EditRepositoryFolder")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int cid, int id, RepositoryFolderViewModel model)
        {
            var content = ModelHelper.GetContent(cid);
            if (content != null)
            {
                // بررسی دارا بودن مجوز کار بر روی محتوا
                if (content.RoleName.IsNotEmpty() && !User.IsInRole(content.RoleName))
                    return Redirect(Uri.EscapeUriString("~/error/1001"));
                var repository = ModelHelper.GetRepository(content.RepositoryId);
                if (repository != null)
                {
                    // واکشی لیست فیلدهای محتوا
                    ViewBag.Fields = content.FieldsObject;

                    // واکشی لیست فیلدهای خارجی محتوا
                    ViewBag.Categories = content.CategoriesList;
                    Criterias = new FilterExpressionList();
                    var tableName = ModelHelper.GetTableName(repository.RepositoryName, content.ContentName);
                    var repositoryFolder = DynamicService.Current.GetItem(tableName, id);
                    // دریافت لیست اطلاعات فیلدهای خارجی از جداول آن ها
                    foreach (var category in content.CategoriesList)
                    {
                        if (category.IsNotEmpty())
                        {
                            var value = DynamicClass.GetProperty(repositoryFolder, $"{category}Id");
                            var list = DynamicService.Current.GetList(category)?.ToArray();
                            if (list != null && list.Any())
                            {
                                // در هنگام تخصیص دادن لیست فیلدهای خارجی به لیست افتادنی، اگر جدول فیلد خارجی
                                // شامل فیلد DisplayName بود در لیست افتادنی مقدار فیلد نمایشی را برابر DisplayName
                                // می گذارد در غیر اینصورت اگر شامل فیلد Title بود برابر Title می گذارد
                                if (DynamicClass.GetProperty(list[0], "DisplayName") != null)
                                    ViewData[category] = list.Select(r => new SelectListItem { Text = r.DisplayName, Value = r.ItemId.ToString(), Selected = r.ItemId.ToString() == value.ToString() }).ToList();
                                else if (DynamicClass.GetProperty(list[0], "Title") != null)
                                    ViewData[category] = list.Select(r => new SelectListItem { Text = r.Title, Value = r.ItemId.ToString(), Selected = r.ItemId.ToString() == value.ToString() }).ToList();
                            }
                        }
                    }
                    var isError = false;
                    // بررسی اعتبارسنجی فیلد
                    foreach (var field in content.FieldsObject)
                    {
                        foreach (var validation in field.FieldValidations)
                        {
                            switch (validation.ValidationName)
                            {
                                case "Required":// الزامی بودن فیلد
                                    if (field.ControlType.Equals("Image"))
                                    {
                                        if (!Request.Files.AllKeys.Contains(field.FieldName) || Request.Files[field.FieldName] == null)
                                        {
                                            ModelState.AddModelError(field.FieldName, string.Format(CultureHelper.Localize("Required"), field.FieldName));
                                            isError = true;
                                        }
                                    }
                                    else if (!Request.Form.AllKeys.Contains(field.FieldName) || Request.Form[field.FieldName].Trim().IsEmpty())
                                    {
                                        ModelState.AddModelError(field.FieldName, string.Format(CultureHelper.Localize("Required"), field.FieldName));
                                        isError = true;
                                    }
                                    break;
                                case "Length":// بررسی طول فیلد
                                    var min = 0;
                                    var max = Convert.ToInt32(validation.SecondParam);
                                    if (validation.ThirdParam.IsNotEmpty())
                                    {
                                        min = Convert.ToInt32(validation.SecondParam);
                                        max = Convert.ToInt32(validation.ThirdParam);
                                    }
                                    if (!Request.Form.AllKeys.Contains(field.FieldName) ||
                                        Request.Form[field.FieldName].Trim().IsEmpty() ||
                                        (Request.Form[field.FieldName].Trim().Length < min && Request.Form[field.FieldName].Trim().Length > max))
                                    {
                                        ModelState.AddModelError(field.FieldName, string.Format(CultureHelper.Localize("Length"), field.FieldName, min, max));
                                        isError = true;
                                    }
                                    break;
                                case "Regex":// اعتبارسنج سفارشی توسط کاربر
                                    break;
                            }
                        }
                    }
                    if (!isError)
                    {
                        var fields = content.FieldsObject;
                        fields.AddRange(content.CategoriesList.Select(category => new ContentField { FieldName = category, DataType = "int", AllowNull = Toggle.False }));

                        // دریافت یک نسخه خالی از موجودیت محتوا
                        var entity = Create(ModelHelper.GetTableName(repository.RepositoryName, content.ContentName), fields);

                        // مقداردهی کلید اصلی
                        DynamicClass.SetProperty(entity, "ItemId", id);
                        // مقداردهی فیلدهای موجودیت محتوا
                        foreach (var field in content.FieldsObject)
                        {
                            if (Request.Form.AllKeys.Contains(field.FieldName))
                                // اگر نوع فیلد عکس باشد
                                if (field.ControlType.Equals("Image"))
                                {
                                    if (Request.Files[field.FieldName] != null && Request.Files[field.FieldName].ContentLength > 0)
                                    {
                                        var file = Request.Files[field.FieldName];
                                        if (file != null)
                                        {
                                            var path = HttpContext.Server.MapPath($"~/CmsData/PortalFolders/{content.ContentName}/{file.FileName}");
                                            if (!FileSystem.Current.ExistsFile(path))
                                            {
                                                var pathOld = DynamicClass.GetProperty(repositoryFolder, field.FieldName);
                                                if (pathOld != null && ((string)pathOld).IsNotEmpty() && FileSystem.Current.ExistsFile(pathOld))
                                                {
                                                    IFile fileOld = FileSystem.Current.GetFile(pathOld);
                                                    fileOld.Delete();
                                                }

                                                using (var reader = new System.IO.BinaryReader(file.InputStream))
                                                {
                                                    System.IO.File.WriteAllBytes(path, reader.ReadBytes(file.ContentLength));
                                                }
                                                // مقداردهی فیلد موجودیت
                                                DynamicClass.SetProperty(entity, field.FieldName, $"/CmsData/PortalFolders/{content.ContentName}/{file.FileName}");
                                            }
                                        }
                                    }
                                }
                                // اگر نوع فیلد متن باشد
                                else
                                {
                                    // مقداردهی فیلد موجودیت
                                    DynamicClass.SetProperty(entity, field.FieldName, DynamicClass.GetFieldValue(field.DataType, Request.Form[field.FieldName], field.AllowNull.GetValue()));
                                }
                        }
                        // مقداردهی فیلدهای موجودیت محتوا از نوع لیست افتادنی
                        foreach (var category in content.CategoriesList)
                        {
                            if (category.IsNotEmpty())
                            {
                                var categoryName = $"{category}Id";
                                if (Request.Form.AllKeys.Contains(categoryName))
                                    // مقداردهی فیلد موجودیت
                                    DynamicClass.SetProperty(entity, categoryName, Convert.ToInt32(Request.Form[categoryName]));
                            }
                        }
                        var value = DynamicClass.GetProperty(repositoryFolder, "AddedDate");
                        DynamicClass.SetProperty(entity, "AddedDate", value);
                        DynamicClass.SetProperty(entity, "ModifiedDate", DateTime.Today);
                        DynamicService.Current.UpdateItem(entity);
                        return RedirectToAction("Index", "Home", new { cid });
                    }
                    return View(repositoryFolder);
                }
            }
            return View();
        }

        /// <summary>
        /// حذف از پوشه محتوا
        /// </summary>
        /// <param name="cid">شناسه محتوا: int</param>
        /// <param name="id">شناسه پوشه محتوا: int</param>
        /// <returns>ActionResult</returns>
        [DapperAuthorize(Roles = "DeleteRepositoryFolder")]
        [HttpGet]
        public ActionResult Delete(int cid, int id)
        {
            var content = ModelHelper.GetContent(cid);
            if (content != null)
            {
                // بررسی دارا بودن مجوز کار بر روی محتوا
                if (content.RoleName.IsNotEmpty() && !User.IsInRole(content.RoleName))
                    return Redirect(Uri.EscapeUriString("~/error/1001"));
                var repository = ModelHelper.GetRepository(content.RepositoryId);
                if (repository != null)
                {
                    Criterias = new FilterExpressionList();
                    var tableName = ModelHelper.GetTableName(repository.RepositoryName, content.ContentName);
                    var repositoryFolder = DynamicService.Current.GetItem(tableName, id);
                    foreach (var field in content.FieldsObject)
                    {
                        // در صورتی که دارای فیلد از نوع عکس باشد باید فایل آن حذف شود
                        if (field.ControlType.Equals("Image") && repositoryFolder != null)
                        {
                            var path = DynamicClass.GetProperty(repositoryFolder, field.FieldName);
                            if (path != null && ((string)path).IsNotEmpty() && FileSystem.Current.ExistsFile(path))
                            {
                                IFile fileOld = FileSystem.Current.GetFile(path);
                                fileOld.Delete();
                            }
                        }
                    }

                    // دریافت یک نسخه خالی از موجودیت محتوا
                    var fields = content.FieldsObject;
                    fields.AddRange(content.CategoriesList.Select(category => new ContentField { FieldName = category, DataType = "int", AllowNull = Toggle.False }));

                    // دریافت یک نسخه خالی از موجودیت محتوا
                    var entity = Create(ModelHelper.GetTableName(repository.RepositoryName, content.ContentName), fields);

                    // مقداردهی کلید اصلی
                    DynamicClass.SetProperty(entity, "ItemId", id);
                    DynamicService.Current.DeleteItem(entity);
                }
            }
            return RedirectToAction("Index", "Home", new { cid });
        }

        /// <summary>
        /// از این متد برای ایجاد کلاس داینامیک استفاده می شود
        /// </summary>
        /// <param name="name">نام کلاس: string</param>
        /// <param name="fields">فیلدهای کلاس: List&lt;ContentField&gt;</param>
        /// <returns>dynamic</returns>
        public dynamic Create(string name, List<ContentField> fields)
        {
            var fieldsStr = fields.Aggregate(string.Empty, (current, field) => current + $@"public {DynamicClass.GetFieldTypeStr(field.DataType, field.AllowNull.GetValue())} {field.FieldName} {{ get; set; }}");
            var definition = $@"
            using Utility;
            using Dapper.Contrib.Extensions;
            using Newtonsoft.Json;
            using System.Runtime.Serialization;
            using System.ComponentModel;
            using System.Linq;

            namespace ORM.Model
            {{
                [Table(""{name}"")]
                public class {name} : GenericBaseModel
                {{
                    {fieldsStr}
                }}
            }}";
            var referencedAssemblies = new List<string>
            {
                typeof(DynamicClass).Assembly.Location,
                typeof(TableAttribute).Assembly.Location,
                typeof(GenericBaseModel).Assembly.Location,
                typeof(Newtonsoft.Json.JsonException).Assembly.Location,
                typeof(System.Runtime.Serialization.Formatter).Assembly.Location,
                typeof(System.ComponentModel.ArrayConverter).Assembly.Location,
                typeof(Enumerable).Assembly.Location
            };
            var entity = DynamicClass.Create<dynamic>(definition, $"ORM.Model.{name}", referencedAssemblies);
            return entity;
        }

        #endregion

    }
}