﻿namespace SAJPortal.Areas.RepositoryFolders
{

    #region Assembly

    using System.Web.Mvc;

    #endregion

    /// <summary>
    /// ثبت ناحیه پوشه محتوا
    /// </summary>
    public class RepositoryFoldersAreaRegistration : AreaRegistration
    {

        #region Attribute

        /// <summary>
        /// نام ناحیه
        /// </summary>
        public override string AreaName => "RepositoryFolders";

        #endregion

        #region Method

        /// <summary>
        /// ثبت متد ناحیه
        /// </summary>
        /// <param name="context"></param>
        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                name: "RepositoryFolders",
                url: "admin/repositoryfolders/{action}/{cid}/{id}",
                defaults: new { controller = "home", action = "index", cid = UrlParameter.Optional, id = UrlParameter.Optional },
                namespaces: new[] { "SAJPortal.Areas.RepositoryFolders.Controllers" }
            );
        }

        #endregion

    }
}