﻿namespace SAJPortal.Areas.RepositoryFolders.ViewModels
{

    #region Assembly

    using System.ComponentModel.DataAnnotations;
    using Resources;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Utility;
    using ORM.DescriptiveModel;

    #endregion

    /// <summary>
    /// مدل پوشه مخزن
    /// </summary>
    public class RepositoryFolderViewModel
    {

        /// <summary>
        /// شناسه
        /// </summary>
        public int ItemId { get; set; }

        /// <summary>
        /// نام محتوا
        /// </summary>
        [Required(ErrorMessageResourceType = typeof(Global), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(Global), Name = "ContentName")]
        public string ContentName { get; set; }

        /// <summary>
        /// برچسب
        /// </summary>
        [Display(ResourceType = typeof(Global), Name = "Label")]
        public string Label { get; set; }

        /// <summary>
        /// مخفی بودن
        /// </summary>
        [Required(ErrorMessageResourceType = typeof(Global), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(Global), Name = "Hidden")]
        public bool Hidden { get; set; }

        /// <summary>
        /// قابل مرتب سازی
        /// </summary>
        [Required(ErrorMessageResourceType = typeof(Global), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(Global), Name = "Sortable")]
        public bool Sortable { get; set; }

        /// <summary>
        /// قابل صفحه بندی
        /// </summary>
        [Required(ErrorMessageResourceType = typeof(Global), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(Global), Name = "Paging")]
        public bool Paging { get; set; }

        /// <summary>
        /// تعداد رکورد در هر صفحه
        /// </summary>
        [Required(ErrorMessageResourceType = typeof(Global), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(Global), Name = "PageSize")]
        public int PageSize { get; set; }

        /// <summary>
        /// شناسه مخزن
        /// </summary>
        [Required(ErrorMessageResourceType = typeof(Global), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(Global), Name = "RepositoryId")]
        public int RepositoryId { get; set; }

        /// <summary>
        /// لیست فیلدها در قالب JSON
        /// </summary>
        [Required(ErrorMessageResourceType = typeof(Global), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(Global), Name = "Fields")]
        public string Fields { get; set; }

        /// <summary>
        /// لیست فیلدها
        /// </summary>
        public List<ContentField> FieldsObject => Newtonsoft.Json.JsonConvert.DeserializeObject<List<ContentField>>(Fields);

        /// <summary>
        /// موجودیت های خارجی در قالب JSON
        /// </summary>
        [Display(ResourceType = typeof(Global), Name = "Categories")]
        public string Categories { get; set; }

        /// <summary>
        /// لیست موجودیت های خارجی
        /// </summary>
        public List<string> CategoriesObject => Categories.IsNotEmpty() ? Categories.Split(new[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries).ToList() : new List<string>();

        /// <summary>
        /// فیلدهای موجودیت های خارجی در قالب JSON
        /// </summary>
        [Display(ResourceType = typeof(Global), Name = "CategoryFields")]
        public string CategoryFields { get; set; }

        /// <summary>
        /// لیست فیلدهای موجودیت های خارجی
        /// </summary>
        public Dictionary<string, string> CategoryFieldsObject
        {
            get
            {
                return CategoryFields.IsNotEmpty()
                    ? CategoryFields.
                        Split(new[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries)
                        .Select(field => new
                        {
                            FieldName = field.Split(new[] { "=" }, StringSplitOptions.RemoveEmptyEntries).FirstOrDefault(),
                            FieldValue = field.Split(new[] { "=" }, StringSplitOptions.RemoveEmptyEntries).LastOrDefault()
                        })
                        .ToDictionary(field => field.FieldName, field => field.FieldValue)
                    : new Dictionary<string, string>();
            }
        }

    }
}