﻿namespace SAJPortal.Areas.Views.Controllers
{

    #region Assembly

    using CMS.Attribute;
    using DataService;
    using ORM.Model;
    using System.Web.Mvc;
    using ViewModels;
    using System;
    using ORM.Condition;
    using AutoMapper;
    using System.Linq;
    using Portable.IO;
    using CMS.Helper;

    #endregion

    /// <summary>
    /// کنترل کننده مدیریت نماها
    /// </summary>
    [OutputCache(VaryByParam = "*", Duration = 0, NoStore = true)]
    [LoggedOrAuthorized]
    public class HomeController : Controller
    {

        #region Attribute

        private const int PageSize = 10;

        private int PageIndex
        {
            get
            {
                if (TempData["PageIndex"] == null)
                    TempData["PageIndex"] = 1;
                return Convert.ToInt32(TempData["PageIndex"]);
            }
            set { TempData["PageIndex"] = value; }
        }

        private long PageCount
        {
            set { TempData["PageCount"] = value; }
        }

        private FilterExpressionList Criterias
        {
            get
            {
                if (TempData["Criterias"] == null)
                    TempData["Criterias"] = new FilterExpressionList();
                return TempData["Criterias"] as FilterExpressionList;
            }
            set { TempData["Criterias"] = value; }
        }

        #endregion

        #region Method

        /// <summary>
        /// لیست نماها
        /// </summary>
        /// <returns>ActionResult</returns>
        [DapperAuthorize(Roles = "ViewsList")]
        public ActionResult Index()
        {
            PageIndex = 1;
            Criterias = new FilterExpressionList();
            if (!User.IsInRole("IsAdmin") && !Criterias.ContainFilter("SiteId"))
            {
                Criterias.Add(new ORM.Condition.Filter("SiteId", SqlOperators.Equal, ModuleHelper.GetBaseSite().ItemId));
            }
            var list = DynamicService.Current.GetList<View>(PageIndex - 1, PageSize, Criterias, string.Empty);
            PageCount = Convert.ToInt64(Math.Ceiling((double)DynamicService.Current.GetCount<View>(Criterias, string.Empty) / PageSize));
            return View(list);
        }

        /// <summary>
        /// لیست نماها
        /// </summary>
        /// <param name="search">فیلتر لیست: string</param>
        /// <param name="formType">نوع فیلتر: string</param>
        /// <returns>ActionResult</returns>
        [DapperAuthorize(Roles = "ViewsList")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index(string search, string formType)
        {
            if (formType.Equals("pager") && Request.Form.AllKeys.Contains("PageIndex"))
                PageIndex = Convert.ToInt32(Request.Form["PageIndex"]);
            else if (formType.Equals("search"))
            {
                PageIndex = 1;
                Criterias.Clear();
                Criterias.Add(new ORM.Condition.Filter("ViewName", SqlOperators.Like, search, SqlOperand.And, DbType.NVarChar));
            }
            if (!User.IsInRole("IsAdmin") && !Criterias.ContainFilter("SiteId"))
            {
                Criterias.Add(new ORM.Condition.Filter("SiteId", SqlOperators.Equal, ModuleHelper.GetBaseSite().ItemId));
            }
            var list = DynamicService.Current.GetList<View>(PageIndex - 1, PageSize, Criterias, string.Empty);
            PageCount = Convert.ToInt64(Math.Ceiling((double)DynamicService.Current.GetCount<View>(Criterias, string.Empty) / PageSize));
            return View(list);
        }

        /// <summary>
        /// فرم نمای جدید
        /// </summary>
        /// <returns>ActionResult</returns>
        [DapperAuthorize(Roles = "NewView")]
        [HttpGet]
        public ActionResult New()
        {
            GetSites();
            return View();
        }

        /// <summary>
        /// ثبت نمای جدید
        /// </summary>
        /// <param name="model">مدل نما: ViewViewModel</param>
        /// <returns>ActionResult</returns>
        [DapperAuthorize(Roles = "NewView")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult New(ViewViewModel model)
        {
            if (ModelState.IsValid)
            {
                DynamicService.Current.CreateItem(new View
                {
                    ViewName = model.ViewName,
                    VirtualPath = model.VirtualPath,
                    Template = model.Template,
                    RouteFieldsList = model.RouteFieldsList,
                    RouteFieldsValueList = model.RouteFieldsValueList,
                    ParametersList = model.ParametersList,
                    ParametersValueList = model.ParametersValueList,
                    SiteId = model.SiteId,
                    AddedDate = DateTime.Today
                });

                // تعیین مسیر نما
                var path = HttpContext.Server.MapPath($"~/CmsData/Views/{model.VirtualPath}");
                if (!FileSystem.Current.ExistsFile(path))
                {
                    // ایجاد نما
                    FileSystem.Current.AppDirectory.CreateFile(path);
                    if (FileSystem.Current.ExistsFile(path))
                    {
                        // نوشتن محتوای نما
                        System.IO.File.WriteAllText(path, System.Web.HttpUtility.UrlDecode(model.Content));
                    }
                }

                return RedirectToAction("Index", "Home");
            }
            GetSites();
            return View(model);
        }

        /// <summary>
        /// فرم ویرایش نما
        /// </summary>
        /// <param name="id">شناسه نما: int</param>
        /// <returns>ActionResult</returns>
        [DapperAuthorize(Roles = "EditView")]
        [HttpGet]
        public ActionResult Edit(int id)
        {
            var view = DynamicService.Current.GetItem<View>(id);
            var viewVm = Mapper.Map<ViewViewModel>(view);
            GetSites();

            // تعیین مسیر نما
            var path = HttpContext.Server.MapPath($"~/CmsData/Views/{view.VirtualPath}");
            if (FileSystem.Current.ExistsFile(path))
            {
                // خواندن محتوای نما
                viewVm.Content = System.IO.File.ReadAllText(path);
            }
            return View(viewVm);
        }

        /// <summary>
        /// ویرایش نما
        /// </summary>
        /// <param name="id">شناسه نما: int</param>
        /// <param name="model">مدل نما: ViewViewModel</param>
        /// <returns>ActionResult</returns>
        [DapperAuthorize(Roles = "EditView")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, ViewViewModel model)
        {
            if (ModelState.IsValid)
            {
                var view = DynamicService.Current.GetItem<View>(id);

                // تعیین مسیر قبلی نما
                var pathOld = HttpContext.Server.MapPath($"~/CmsData/Views/{view.VirtualPath}");

                view.ViewName = model.ViewName;
                view.VirtualPath = model.VirtualPath;
                view.Template = model.Template;
                view.RouteFieldsList = model.RouteFieldsList;
                view.RouteFieldsValueList = model.RouteFieldsValueList;
                view.ParametersList = model.ParametersList;
                view.ParametersValueList = model.ParametersValueList;
                view.SiteId = model.SiteId;
                view.ModifiedDate = DateTime.Today;

                DynamicService.Current.UpdateItem(view);

                // تعیین مسیر نما
                var path = HttpContext.Server.MapPath($"~/CmsData/Views/{model.VirtualPath}");
                if (FileSystem.Current.ExistsFile(pathOld))
                {
                    // حذف فایل نمای قبلی
                    var file = FileSystem.Current.GetFile(pathOld);
                    file.Delete();

                    // ایجاد نما
                    FileSystem.Current.AppDirectory.CreateFile(path);

                    if (FileSystem.Current.ExistsFile(path))
                    {
                        // نوشتن محتوای نما
                        System.IO.File.WriteAllText(path, System.Web.HttpUtility.UrlDecode(model.Content));
                    }
                }

                return RedirectToAction("Index", "Home");
            }
            GetSites();
            return View(model);
        }

        /// <summary>
        /// حذف نما
        /// </summary>
        /// <param name="id">شناسه نما: int</param>
        /// <returns>ActionResult</returns>
        [DapperAuthorize(Roles = "DeleteView")]
        [HttpGet]
        public ActionResult Delete(int id)
        {
            var view = DynamicService.Current.GetItem<View>(id);
            DynamicService.Current.DeleteItem(view);

            // تعیین مسیر نما
            var path = HttpContext.Server.MapPath($"~/CmsData/Views/{view.VirtualPath}");
            if (FileSystem.Current.ExistsFile(path))
            {
                // حذف فایل نما
                var file = FileSystem.Current.GetFile(path);
                file.Delete();
            }

            return RedirectToAction("Index", "Home", new { area = "Views" });
        }

        /// <summary>
        /// لیست سایت ها
        /// </summary>
        public void GetSites()
        {
            var criterias = new FilterExpressionList();
            if (!User.IsInRole("IsAdmin"))
            {
                criterias.Add(new ORM.Condition.Filter("ItemId", SqlOperators.Equal, ModuleHelper.GetBaseSite().ItemId));
            }
            var sites = DynamicService.Current.GetList<Site>(criterias);
            ViewBag.Sites = sites.Select(s => new SelectListItem { Text = s.Label, Value = s.ItemId.ToString() }).ToList();
        }

        #endregion 

    }
}