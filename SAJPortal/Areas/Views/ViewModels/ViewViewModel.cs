﻿namespace SAJPortal.Areas.Views.ViewModels
{

    #region Assembly

    using System.ComponentModel.DataAnnotations;
    using Resources;

    #endregion

    /// <summary>
    /// مدل نما
    /// </summary>
    public class ViewViewModel
    {

        /// <summary>
        /// شناسه
        /// </summary>
        public int ItemId { get; set; }

        /// <summary>
        /// نام نما
        /// </summary>
        [Required(ErrorMessageResourceType = typeof(Global), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(Global), Name = "ViewName")]
        public string ViewName { get; set; }

        /// <summary>
        /// مسیر نسبی
        /// </summary>
        [Required(ErrorMessageResourceType = typeof(Global), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(Global), Name = "VirtualPath")]
        public string VirtualPath { get; set; }

        /// <summary>
        /// قالب
        /// </summary>
        [Display(ResourceType = typeof(Global), Name = "Template")]
        public string Template { get; set; }

        /// <summary>
        /// نام پارامترهای مسیریابی در قالب JSON
        /// </summary>
        [Display(ResourceType = typeof(Global), Name = "RouteValues")]
        public string RouteFields { get; set; }

        /// <summary>
        /// مقدار پارامترهای مسیریابی در قالب JSON
        /// </summary>
        [Display(ResourceType = typeof(Global), Name = "RouteValues")]
        public string RouteFieldsValue { get; set; }

        /// <summary>
        /// نام پارامترهای مسیریابی
        /// </summary>
        [Display(ResourceType = typeof(Global), Name = "RouteValues")]
        public string[] RouteFieldsList { get; set; }

        /// <summary>
        /// مقدار پارامترهای مسیریابی
        /// </summary>
        [Display(ResourceType = typeof(Global), Name = "RouteValues")]
        public string[] RouteFieldsValueList { get; set; }

        /// <summary>
        /// نام پارامترهای نما در قالب JSON
        /// </summary>
        [Display(ResourceType = typeof(Global), Name = "Parameters")]
        public string Parameters { get; set; }

        /// <summary>
        /// مقدار پارامترها نما در قالب JSON
        /// </summary>
        [Display(ResourceType = typeof(Global), Name = "Parameters")]
        public string ParametersValue { get; set; }

        /// <summary>
        /// نام پارامترهای نما
        /// </summary>
        [Display(ResourceType = typeof(Global), Name = "Parameters")]
        public string[] ParametersList { get; set; }

        /// <summary>
        /// مقدار پارامترها نما
        /// </summary>
        [Display(ResourceType = typeof(Global), Name = "Parameters")]
        public string[] ParametersValueList { get; set; }

        /// <summary>
        /// شناسه سایت
        /// </summary>
        [Required(ErrorMessageResourceType = typeof(Global), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(Global), Name = "SiteId")]
        public int SiteId { get; set; }

        /// <summary>
        /// محتوای نما
        /// </summary>
        public string Content { get; set; }

    }
}