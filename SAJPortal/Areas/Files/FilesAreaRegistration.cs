﻿namespace SAJPortal.Areas.Files
{

    #region Assembly

    using System.Web.Mvc;

    #endregion

    /// <summary>
    /// ثبت ناحیه فایل ها
    /// </summary>
    public class FilesAreaRegistration : AreaRegistration 
    {

        #region Attribute

        /// <summary>
        /// نام ناحیه
        /// </summary>
        public override string AreaName => "Files";

        #endregion

        #region Method

        /// <summary>
        /// متد ثبت ناحیه
        /// </summary>
        /// <param name="context">AreaRegistrationContext</param>
        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                name: "Files",
                url: "admin/files/{action}/{fid}/{id}",
                defaults: new { controller = "home", action = "index", fid = UrlParameter.Optional, id = UrlParameter.Optional },
                namespaces: new[] { "SAJPortal.Areas.Files.Controllers" }
            );
        }

        #endregion

    }
}