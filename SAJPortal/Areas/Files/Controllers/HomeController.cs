﻿namespace SAJPortal.Areas.Files.Controllers
{

    #region Assembly

    using DataService;
    using ORM.Model;
    using System.Web.Mvc;
    using ViewModels;
    using System;
    using ORM.Condition;
    using AutoMapper;
    using System.Linq;
    using System.Web;
    using CMS.Helper;
    using Portable.IO;
    using CMS.Attribute;

    #endregion

    /// <summary>
    /// کنترل کننده مدیریت رسانه
    /// </summary>
    [OutputCache(VaryByParam = "*", Duration = 0, NoStore = true)]
    [LoggedOrAuthorized]
    public class HomeController : Controller
    {

        #region Attribute

        private const int PageSize = 10;

        private int PageIndex
        {
            get
            {
                if (TempData["PageIndex"] == null)
                    TempData["PageIndex"] = 1;
                return Convert.ToInt32(TempData["PageIndex"]);
            }
            set { TempData["PageIndex"] = value; }
        }

        private long PageCount
        {
            set { TempData["PageCount"] = value; }
        }

        private FilterExpressionList Criterias
        {
            get
            {
                if (TempData["Criterias"] == null)
                    TempData["Criterias"] = new FilterExpressionList();
                return TempData["Criterias"] as FilterExpressionList;
            }
            set { TempData["Criterias"] = value; }
        }

        #endregion

        #region Method

        /// <summary>
        /// لیست فایل ها
        /// </summary>
        /// <param name="fid">شناسه پوشه: int</param>
        /// <returns>ActionResult</returns>
        [DapperAuthorize(Roles = "FilesList")]
        public ActionResult Index(int fid)
        {
            PageIndex = 1;
            Criterias = new FilterExpressionList();
            if(!Criterias.ContainFilter("FolderId"))
                Criterias.Add(new ORM.Condition.Filter("FolderId", SqlOperators.Equal, fid));
            var list = DynamicService.Current.GetList<File>(PageIndex - 1, PageSize, Criterias, string.Empty);
            PageCount = Convert.ToInt64(Math.Ceiling((double)DynamicService.Current.GetCount<File>(Criterias, string.Empty) / PageSize));
            return View(list);
        }

        /// <summary>
        /// لیست فایل ها
        /// </summary>
        /// <param name="fid">شناسه پوشه: int</param>
        /// <param name="search">فیلتر لیست: string</param>
        /// <param name="formType">نوع فیلتر: string</param>
        /// <returns>ActionResult</returns>
        [DapperAuthorize(Roles = "FilesList")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index(int fid, string search, string formType)
        {
            if (formType.Equals("pager") && Request.Form.AllKeys.Contains("PageIndex"))
                PageIndex = Convert.ToInt32(Request.Form["PageIndex"]);
            else if (formType.Equals("search"))
            {
                PageIndex = 1;
                Criterias.Clear();
                Criterias.Add(new ORM.Condition.Filter("FileName", SqlOperators.Like, search, SqlOperand.And, DbType.NVarChar));
            }
            if (!Criterias.ContainFilter("FolderId"))
                Criterias.Add(new ORM.Condition.Filter("FolderId", SqlOperators.Equal, fid));
            var list = DynamicService.Current.GetList<File>(PageIndex - 1, PageSize, Criterias, string.Empty);
            PageCount = Convert.ToInt64(Math.Ceiling((double)DynamicService.Current.GetCount<File>(Criterias, string.Empty) / PageSize));
            return View(list);
        }

        /// <summary>
        /// فرم فایل جدید
        /// </summary>
        /// <param name="fid">شناسه پوشه: int</param>
        /// <returns>ActionResult</returns>
        [DapperAuthorize(Roles = "NewFile")]
        [HttpGet]
        public ActionResult New(int fid)
        {
            return View();
        }

        /// <summary>
        /// ثبت فایل جدید
        /// </summary>
        /// <param name="fid">شناسه پوشه: int</param>
        /// <param name="fileUpload">فایل بارگذاری شده: HttpPostedFileBase</param>
        /// <returns>ActionResult</returns>
        [DapperAuthorize(Roles = "NewFile")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult New(int fid, HttpPostedFileBase fileUpload)
        {
            var folder = ModelHelper.GetFolder(fid);
            if (folder != null)
            {
                if (fileUpload != null && fileUpload.ContentLength > 0)
                {
                    // تعیین مسیر فایل برای ذخیره
                    var path = HttpContext.Server.MapPath($"~/CmsData/PortalFolders/{folder.VirtualPath}/{fileUpload.FileName}");
                    if (!FileSystem.Current.ExistsFile(path))
                    {
                        // نوشتن فایل در مسیر تعیین شده
                        using (var reader = new System.IO.BinaryReader(fileUpload.InputStream))
                        {
                            System.IO.File.WriteAllBytes(path, reader.ReadBytes(fileUpload.ContentLength));
                        }
                        DynamicService.Current.CreateItem(new File
                        {
                            FileName = fileUpload.FileName,
                            FileExtension = System.IO.Path.GetExtension(fileUpload.FileName),
                            VirtualPath = path,
                            FolderId = fid,
                            SiteId = folder.SiteId,
                            AddedDate = DateTime.Today
                        });
                        return RedirectToAction("Index", "Home", new { fid });
                    }
                }
            }
            return View();
        }

        /// <summary>
        /// فرم ویرایش فایل
        /// </summary>
        /// <param name="fid">شناسه پوشه: int</param>
        /// <param name="id">شناسه فایل مورد نظر: int</param>
        /// <returns>ActionResult</returns>
        [DapperAuthorize(Roles = "EditFile")]
        [HttpGet]
        public ActionResult Edit(int fid, int id)
        {
            var file = DynamicService.Current.GetItem<File>(id);
            var fileVm = Mapper.Map<FileViewModel>(file);
            return View(fileVm);
        }

        /// <summary>
        /// ویرایش فایل
        /// </summary>
        /// <param name="fid">شناسه پوشه: int</param>
        /// <param name="id">شناسه فایل: int</param>
        /// <param name="fileUpload">فایل بارگذاری شده: HttpPostedFileBase</param>
        /// <returns>ActionResult</returns>
        [DapperAuthorize(Roles = "EditFile")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int fid, int id, HttpPostedFileBase fileUpload)
        {
            var folder = ModelHelper.GetFolder(fid);
            if (folder != null)
            {
                if (fileUpload != null && fileUpload.ContentLength > 0)
                {
                    // تعیین مسیر فایل برای ذخیره
                    var path = HttpContext.Server.MapPath($"~/CmsData/PortalFolders/{folder.VirtualPath}/{fileUpload.FileName}");
                    if (!FileSystem.Current.ExistsFile(path))
                    {
                        // نوشتن فایل در مسیر تعیین شده
                        using (var reader = new System.IO.BinaryReader(fileUpload.InputStream))
                        {
                            System.IO.File.WriteAllBytes(path, reader.ReadBytes(fileUpload.ContentLength));
                        }

                        var file = DynamicService.Current.GetItem<File>(id);
                        // تعیین مسیر فایل قبلی برای حذف
                        var pathOld = HttpContext.Server.MapPath($"~/CmsData/PortalFolders/{folder.VirtualPath}/{file.FileName}");
                        if (FileSystem.Current.ExistsFile(pathOld))
                        {
                            // حذف فایل قبلی
                            var fileOld = FileSystem.Current.GetFile(pathOld);
                            fileOld.Delete();
                        }

                        file.FileName = fileUpload.FileName;
                        file.FileExtension = System.IO.Path.GetExtension(fileUpload.FileName);
                        file.VirtualPath = path;
                        file.ModifiedDate = DateTime.Today;
                        DynamicService.Current.UpdateItem(file);
                        return RedirectToAction("Index", "Home", new { fid });
                    }
                }
            }
            return View();
        }

        /// <summary>
        /// حذف فایل
        /// </summary>
        /// <param name="fid">شناسه پوشه: int</param>
        /// <param name="id">شناسه فایل: int</param>
        /// <returns>ActionResult</returns>
        [DapperAuthorize(Roles = "DeleteFile")]
        [HttpGet]
        public ActionResult Delete(int fid, int id)
        {
            var folder = ModelHelper.GetFolder(fid);
            if (folder != null)
            {
                var file = DynamicService.Current.GetItem<File>(id);
                // تعیین مسیر فایل برای حذف
                var path = HttpContext.Server.MapPath($"~/CmsData/PortalFolders/{folder.VirtualPath}/{file.FileName}");
                if (FileSystem.Current.ExistsFile(path))
                {
                    // حذف فایل
                    var fileOld = FileSystem.Current.GetFile(path);
                    fileOld.Delete();
                }
                DynamicService.Current.DeleteItem(file);
                return RedirectToAction("Index", "Home", new { fid });
            }
            return RedirectToAction("Index", "Home", new { fid });
        }

        #endregion 

    }
}