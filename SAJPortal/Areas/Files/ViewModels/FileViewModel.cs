﻿namespace SAJPortal.Areas.Files.ViewModels
{

    #region Assembly

    using System.ComponentModel.DataAnnotations;
    using Resources;

    #endregion

    /// <summary>
    /// مدل فایل ها
    /// </summary>
    public class FileViewModel
    {

        /// <summary>
        /// شناسه
        /// </summary>
        public int ItemId { get; set; }

        /// <summary>
        /// مسیر نسبی
        /// </summary>
        [Required(ErrorMessageResourceType = typeof(Global), ErrorMessageResourceName = "Required")]
        [Display(ResourceType = typeof(Global), Name = "VirtualPath")]
        public string VirtualPath { get; set; }

    }
}