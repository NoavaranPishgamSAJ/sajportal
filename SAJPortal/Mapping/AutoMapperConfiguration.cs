﻿namespace SAJPortal.Mapping
{

    #region Assembly

    using AutoMapper;

    #endregion

    /// <summary>
    /// از این کلاس برای نگاشت مدل ها به مدل های نما استفاده می شود
    /// </summary>
    public class AutoMapperConfiguration
    {

        #region Method

        /// <summary>
        /// تنظیم نگاشت ها
        /// </summary>
        public static void Configure()
        {
            Mapper.Initialize(cfg =>
            {
                cfg.AddProfile<MappingProfile>();
            });
        }

        #endregion

    }
}
