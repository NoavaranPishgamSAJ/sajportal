﻿namespace SAJPortal.Mapping
{

    #region Assembly

    using Areas.Pages.ViewModels;
    using Areas.Sites.ViewModels;
    using Areas.Repositories.ViewModels;
    using Areas.Memberships.ViewModels;
    using Areas.Folders.ViewModels;
    using Areas.Views.ViewModels;
    using Areas.Layouts.ViewModels;
    using Areas.Contents.ViewModels;
    using AutoMapper;
    using ORM.Model;
    using ORM.Enums;
    using Areas.Files.ViewModels;

    #endregion

    /// <summary>
    /// این کلاس لیست نگاشت های مدل به مدل نما را در خود نگه می دارد
    /// </summary>
    public class MappingProfile : Profile
    {

        #region Method

        /// <summary>
        /// سازنده کلاس
        /// </summary>
        public MappingProfile()
        {
            CreateMap<Page, PageViewModel>()
                .ForMember(dto => dto.ShowInBreadcrumb, conf => conf.MapFrom(ol => ol.ShowInBreadcrumb.GetValue()))
                .ForMember(dto => dto.ShowInMenu, conf => conf.MapFrom(ol => ol.ShowInMenu.GetValue()))
                .ForMember(dto => dto.IsFirstPage, conf => conf.MapFrom(ol => ol.IsFirstPage.GetValue()));

            CreateMap<Site, SiteViewModel>()
                .ForMember(dto => dto.IsOnline, conf => conf.MapFrom(ol => ol.IsOnline.GetValue()))
                .ForMember(dto => dto.IsBase, conf => conf.MapFrom(ol => ol.IsBase.GetValue()));

            CreateMap<Repository, RepositoryViewModel>()
                .ForMember(dto => dto.ShowHiddenFolder, conf => conf.MapFrom(ol => ol.ShowHiddenFolder.GetValue()))
                .ForMember(dto => dto.EnableWorkFlow, conf => conf.MapFrom(ol => ol.EnableWorkFlow.GetValue()));

            CreateMap<Membership, MembershipViewModel>();

            CreateMap<Folder, FolderViewModel>();

            CreateMap<View, ViewViewModel>();

            CreateMap<Layout, LayoutViewModel>();

            CreateMap<Content, ContentViewModel>()
                .ForMember(dto => dto.Hidden, conf => conf.MapFrom(ol => ol.Hidden.GetValue()))
                .ForMember(dto => dto.Sortable, conf => conf.MapFrom(ol => ol.Sortable.GetValue()))
                .ForMember(dto => dto.Paging, conf => conf.MapFrom(ol => ol.Paging.GetValue()));

            CreateMap<File, FileViewModel>();

            CreateMap<Role, RoleViewModel>();

            CreateMap<Permission, PermissionViewModel>();

            CreateMap<Position, PositionViewModel>();

            CreateMap<Customer, CustomerViewModel>()
                .ForMember(dto => dto.IsAdmin, conf => conf.MapFrom(ol => ol.IsAdmin.GetValue()))
                .ForMember(dto => dto.BoycottUser, conf => conf.MapFrom(ol => ol.BoycottUser.GetValue()))
                .ForMember(dto => dto.UseOperationDate, conf => conf.MapFrom(ol => ol.UseOperationDate.GetValue()));

            CreateMap<ZoneContent, ZoneContentViewModel>()
                .ForMember(dto => dto.Hidden, conf => conf.MapFrom(ol => ol.Hidden.GetValue()));
        }

        #endregion

    }

}