function getCoords(elem) { // crossbrowser version
    var box = elem.getBoundingClientRect();

    var body = document.body;
    var docEl = document.documentElement;

    var scrollTop = window.pageYOffset || docEl.scrollTop || body.scrollTop;
    var scrollLeft = window.pageXOffset || docEl.scrollLeft || body.scrollLeft;

    var clientTop = docEl.clientTop || body.clientTop || 0;
    var clientLeft = docEl.clientLeft || body.clientLeft || 0;

    var top = box.top + scrollTop - clientTop;
    var left = box.left + scrollLeft - clientLeft;

    return { top: Math.round(top), left: Math.round(left) };
}

function disable_scroll() {
    document.onmousewheel = function () { stopWheel(); } /* IE7, IE8 */
    if (document.addEventListener) { /* Chrome, Safari, Firefox */
        document.addEventListener('DOMMouseScroll', stopWheel, false);
    }

    function stopWheel(e) {
        if (!e) { e = window.event; } /* IE7, IE8, Chrome, Safari */
        if (e.preventDefault) { e.preventDefault(); } /* Chrome, Safari, Firefox */
        e.returnValue = false; /* IE7, IE8 */
    }
}
function enable_scroll() {
    document.onmousewheel = null;  /* IE7, IE8 */
    if (document.addEventListener) { /* Chrome, Safari, Firefox */
        document.removeEventListener('DOMMouseScroll', null, false);
    }
}
/**
* jQuery DateSelect
* @author biohzrdmx <github.com/biohzrdmx>
* @version 1.0
* @requires jQuery 1.8+
* @license MIT
*/
(function ($) {
    // Default options
    $.dateSelect = {
        templates: {
            isRTL: false,
            selector:
				'<div class="date-select">' +
					'<div class="date-select popup">' +
						'<span class="tip"></span>' +
						'<div class="select day">' +
							'<a href="#" class="btn-arrow btn-up"><i class="icon icon-up"></i></a>' +
							'<div>' +
								'<span class="num">01</span>' +
								'<span class="text">Thursday</span>' +
							'</div>' +
							'<a href="#" class="btn-arrow btn-down"><i class="icon icon-down"></i></a>' +
						'</div>' +
						'<div class="select month">' +
							'<a href="#" class="btn-arrow btn-up"><i class="icon icon-up"></i></a>' +
							'<div>' +
								'<span class="num">01</span>' +
								'<span class="text">September</span>' +
							'</div>' +
							'<a href="#" class="btn-arrow btn-down"><i class="icon icon-down"></i></a>' +
						'</div>' +
						'<div class="select year">' +
							'<a href="#" class="btn-arrow btn-up"><i class="icon icon-up"></i></a>' +
							'<div>' +
								'<span class="num">2014</span>' +
							'</div>' +
							'<a href="#" class="btn-arrow btn-down"><i class="icon icon-down"></i></a>' +
						'</div>' +
						'<div class="buttons">' +
							'<a href="#" class="btn-cancel"><i class="fa fa-times"></i></a>' +
							'<a href="#" class="btn-ok"><i class="fa fa-check"></i></a>' +
						'</div>' +
					'</div>' +
				'</div>'
        },
        defaults: {
            formatDate: function (date, isRTL) {
                if (isRTL) {
                    var jdate = toJalaali(date.getFullYear(), date.getMonth() + 1, date.getDate());
                    var formatted = jdate.jy + '/' + $.dateSelect.pad(jdate.jm, 2) + '/' + $.dateSelect.pad(jdate.jd, 2);
                    return formatted;
                }
                else {
                    var formatted = date.getFullYear() + '/' + $.dateSelect.pad(date.getMonth() + 1, 2) + '/' + $.dateSelect.pad(date.getDate(), 2);
                    return formatted;
                }
            },
            parseDate: function (string, isRTL) {
                if (isRTL) {
                    var parts = string.match(/(\d{4})\/(\d{1,2})\/(\d{1,2})/);
                    if (parts && parts.length == 4) {
                        var gdate = toGregorian(parseInt(parts[1]), parseInt(parts[2]), parseInt(parts[3]));
                        return new Date(gdate.gy, gdate.gm - 1, gdate.gd);
                    }
                    return new Date();
                }
                else {
                    var date = new Date();
                    var parts = string.match(/(\d{4})\/(\d{1,2})\/(\d{1,2})/);
                    if (parts && parts.length == 4) {
                        date = new Date(parts[1], parts[2] - 1, parts[3]);
                    }
                    return date;
                }
            },
            container: '.page-wrapper',
            element: null,
            date: new Date().toDateString(),
            strings: {
                days: ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'],
                months: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']
            }
        },
        pad: function (num, size) {
            var s = num + "";
            while (s.length < size) s = "0" + s;
            return s;
        },
        update: function (el, date, opts, isRTL) {
            var day = el.find('.day'),
				month = el.find('.month'),
				year = el.find('.year'),
				dayNum = day.find('.num'),
				monthNum = month.find('.num'),
				yearNum = year.find('.num'),
				dayText = day.find('.text'),
				monthText = month.find('.text'),
				curDay = date.getDate();
            curWeekday = date.getDay();
            curMonth = date.getMonth();
            curYear = date.getFullYear();
            //
            if (isRTL) {
                //alert(["update", curYear, curMonth + 1, curDay]);
                var jdate = toJalaali(curYear, curMonth + 1, curDay);
                //alert(curWeekday);

                dayNum.text(jdate.jd);
                monthNum.text(jdate.jm);
                yearNum.text(jdate.jy);
                dayText.text(opts.strings.days[curWeekday]);
                monthText.text(opts.strings.months[curMonth]);
            }
            else {
                dayNum.text(curDay);
                monthNum.text(curMonth + 1);
                yearNum.text(curYear);
                dayText.text(opts.strings.days[curWeekday]);
                monthText.text(opts.strings.months[curMonth]);
            }
        },
        show: function (options) {
            var obj = this,
				opts = $.extend(true, {}, $.dateSelect.defaults, options),
				markup = $(obj.templates.selector),
				date = new Date(opts.date);
            //alert(["show", date.getFullYear(), date.getMonth(), date.getDate()]);
            // Get rid of another popups
            obj.hide(true);
            // Initialize value
            if (opts.isRTL) {
                opts.strings = {
                    days: ['یکشنبه', 'دوشنبه', 'سه شنبه', 'چهارشنبه', 'پنج شنبه', 'جمعه', 'شنبه'],
                    months: ['دی', 'بهمن', 'اسفند', 'فروردین', 'اردیبهشت', 'خرداد', 'تیر', 'مرداد', 'شهریور', 'مهر', 'آبان', 'آذر']
                };
            }
            if (opts.element) {
                if (typeof opts.element == 'string') {
                    opts.element = $(opts.element);
                }
                date = opts.parseDate(opts.element.val(), opts.isRTL);
                //alert(["initialize", date.getFullYear(), date.getMonth(), date.getDate()]);
            }
            // Update current selection
            obj.update(markup, date, opts, opts.isRTL);
            // Bind events
            markup.on('click', '.day .btn-up', function (e) {
                date.setDate(date.getDate() + 1);
                obj.update(markup, date, opts, opts.isRTL);
                e.preventDefault();
            });
            markup.on('click', '.day .btn-down', function (e) {
                date.setDate(date.getDate() - 1);
                obj.update(markup, date, opts, opts.isRTL);
                e.preventDefault();
            });
            markup.on('click', '.month .btn-up', function (e) {
                date.setMonth(date.getMonth() + 1);
                obj.update(markup, date, opts, opts.isRTL);
                e.preventDefault();
            });
            markup.on('click', '.month .btn-down', function (e) {
                date.setMonth(date.getMonth() - 1);
                obj.update(markup, date, opts, opts.isRTL);
                e.preventDefault();
            });
            markup.on('click', '.year .btn-up', function (e) {
                date.setFullYear(date.getFullYear() + 1);
                obj.update(markup, date, opts, opts.isRTL);
                e.preventDefault();
            });
            markup.on('click', '.year .btn-down', function (e) {
                date.setFullYear(date.getFullYear() - 1);
                obj.update(markup, date, opts, opts.isRTL);
                e.preventDefault();
            });
            markup.on('click', '.btn-ok', function (e) {
                e.preventDefault();
                var formatted = opts.formatDate(date, opts.isRTL);
                $(opts.element).val(formatted);
                $(opts.element).trigger("change");
                obj.hide();
                enable_scroll();
            });
            markup.on('click', '.btn-cancel', function (e) {
                e.preventDefault();
                obj.hide();
                enable_scroll();
            });
            markup.on('mousewheel', '.day', function (e) {
                disable_scroll();
                if (e.deltaY > 0) {
                    $('.day .btn-up').trigger('click');
                } else if (e.deltaY < 0) {
                    $('.day .btn-down').trigger('click');
                }
                setTimeout(enable_scroll, 500);
            });
            markup.on('mousewheel', '.month', function (e) {
                disable_scroll();
                if (e.deltaY > 0) {
                    $('.month .btn-up').trigger('click');
                } else if (e.deltaY < 0) {
                    $('.month .btn-down').trigger('click');
                }
                setTimeout(enable_scroll, 500);
            });
            markup.on('mousewheel', '.year', function (e) {
                disable_scroll();
                if (e.deltaY > 0) {
                    $('.year .btn-up').trigger('click');
                } else if (e.deltaY < 0) {
                    $('.year .btn-down').trigger('click');
                }
                setTimeout(enable_scroll, 500);
            });
            // Add element to DOM
            markup.hide();
            $(opts.container).append(markup);
            // Position
            if (opts.element) {
                var offset = getCoords(opts.element.get(0));//opts.element.offset();
                //alert([getCoords(opts.element.get(0)).left, getCoords(opts.element.get(0)).top]);
                //alert([offset.left, offset.top]);
                //
                markup.css({
                    left: offset.left + 'px',
                    top: offset.top + -50 + 'px'
                });
            }
            // Show
            markup.fadeIn(150);
        },
        hide: function (force) {
            var force = force || false,
				el = $('.date-select');
            if (force) {
                el.remove();
            } else {
                el.fadeOut(150, el.remove);
            }
        }
    };
    // Manual binding
    $.fn.dateSelect = function (options) {
        if (!this.length) { return this; }
        var opts = $.extend(true, {}, $.dateSelect.defaults, options);
        this.each(function () {
            var el = $(this),
				parent = el.parent();
            // Bind to the element itself
            el.on('click', function () {
                $.dateSelect.show({
                    element: el
                });
            });
            // Does it have a button?
            parent.find('[data-toggle=select]').on('click', function (e) {
                e.preventDefault();
                if ($('.date-select:visible').length) {
                    $.dateSelect.hide();
                } else {
                    $.dateSelect.show({
                        element: el
                    });
                }
            });
        });
        return this;
    };
    // Data support
    $('[data-select=date]').each(function () {
        var el = $(this);
        el.dateSelect();
    });
})(jQuery);
