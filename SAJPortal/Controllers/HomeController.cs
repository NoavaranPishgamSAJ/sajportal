﻿namespace SAJPortal.Controllers
{

    #region Assembly

    using System.Collections.Generic;
    using System.Web.Mvc;
    using System.Linq;
    using ORM.Model;
    using CMS.Helper;
    using DataService;
    using ORM.Condition;
    using ORM.Enums;
    using Utility;

    #endregion

    /// <summary>
    /// کنترل کننده اصلی پرتال برای اجرای اولین درخواست کاربر و صدا زدن صفحه اصلی سایت
    /// </summary>
    public class HomeController : Controller
    {

        #region Method

        /// <summary>
        /// رویداد اصلی هنگام ورود اولین درخواست کاربر
        /// </summary>
        /// <returns>ActionResult</returns>
        public ActionResult Index()
        {
            // پیدا کردن سایت پایه پرتال
            var baseSite = CmsVariables.ActiveSite.FirstOrDefault(p => p.IsBase == Toggle.True);
            if (baseSite != null)
            {
                // بدست آوردن صفحه شروع سایت
                var criterias = new FilterExpressionList();
                criterias.Add(new ORM.Condition.Filter("SiteId", SqlOperators.Equal, baseSite.ItemId));
                criterias.Add(new ORM.Condition.Filter("IsFirstPage", SqlOperators.Equal, Toggle.True));
                var pageList = DynamicService.Current.GetList<Page>(criterias);
                if (pageList != null)
                {
                    var pages = pageList as IList<Page> ?? pageList.ToList();
                    if (pages.Count > 0)
                    {
                        var page = pages.FirstOrDefault();
                        if (page != null)
                        {
                            // بدست آوردن آدرس صفحه
                            var path = "~/" + (baseSite.SitePath.IsNotEmpty() ? baseSite.SitePath + "/" : string.Empty) + page.VirtualPath.ToLower();

                            // انتقال به صفحه مورد نظر
                            return Redirect(path);
                        }
                    }
                }
            }
            return View();
        }

        #endregion

    }
}