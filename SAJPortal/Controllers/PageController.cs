﻿namespace SAJPortal.Controllers
{

    #region Assembly

    using Utility;
    using CMS.Helper;
    using System.Web.Mvc;
    using CMS.Sitemap;
    using System.Linq;
    using System;

    #endregion

    /// <summary>
    /// کنترل کننده صفحات پرتال
    /// </summary>
    public class PageController : Controller
    {

        #region Method

        /// <summary>
        /// رویداد اصلی هنگام درخواست نمایش صفحه
        /// </summary>
        /// <param name="id">شناسه صفحه</param>
        /// <returns>ActionResult</returns>
        public ActionResult Index(int? id)
        {
            // پبدا کردن منو صفحه
            var node = SitemapProvider.GetNodeByPath(Convert.ToInt32(RouteData.DataTokens.Single(p => p.Key.Equals("SiteId")).Value), RouteData.DataTokens.Single(p => p.Key.Equals("Url")).Value.ToString());
            
            // پیدا کردن سایت پایه پرتال
            var baseSite = ModuleHelper.GetBaseSite();

            // تنظیم عنوان صفحه
            ViewBag.Title = $"{baseSite?.Title} > {(node != null ? node.Title : string.Empty)}";
            if (node != null)
            {
                // تنظیم پارامترهای مسیریابی
                if (node.RouteFieldsList != null && node.RouteFieldsList.Count > 0)
                {
                    for (var i = 0; i < node.RouteFieldsList.Count; i++)
                    {
                        if (RouteData.Values.ContainsKey(node.RouteFieldsList[i]))
                            RouteData.Values[node.RouteFieldsList[i]] = node.RouteFieldsValueList[i];
                        else
                            RouteData.Values.Add(node.RouteFieldsList[i], node.RouteFieldsValueList[i]);
                    }
                }

                // تنظیم پارامترهای صفحه
                if (node.ParametersList != null && node.ParametersList.Count > 0)
                {
                    for (var i = 0; i < node.ParametersList.Count; i++)
                    {
                        if (ViewData.ContainsKey(node.ParametersList[i]))
                            ViewData[node.ParametersList[i]] = node.ParametersValueList[i];
                        else
                            ViewData.Add(node.ParametersList[i], node.ParametersValueList[i]);
                    }
                }

                // بررسی نوع مرورگر کاربر در صورت محدود شدن سایت به نمایش در مرورگر خاص
                if (baseSite?.UserAgentList != null && baseSite.UserAgentList.Count(u => u.IsNotEmpty()) > 0)
                {
                    var supported = false;
                    foreach (var userAgent in baseSite.UserAgentList)
                    {
                        // بررسی پشتیبانی از مرورگر
                        supported = Request.Browser.Browser.ToLower().Equals(userAgent.ToLower());
                    }
                    if (!supported)
                        return null;
                }

                // بررسی مجوز دسترسی به صفحه
                if (node.RoleName.IsNotEmpty() && !User.IsInRole(node.RoleName))
                {
                    // ارسال کاربر به صفحه خطا با کد عدم دسترسی
                    return Redirect(Uri.EscapeUriString("~/error/1001"));
                }
            }
            return View();
        }

        #endregion

    }
}