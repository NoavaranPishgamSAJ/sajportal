﻿namespace SAJPortal
{

    #region Assembly

    using System.Web.Mvc;
    using System.Web.Routing;
    using CMS.Routing;

    #endregion

    /// <summary>
    /// این کلاس تنظیمات مسیریابی پرتال را انجام می دهد
    /// </summary>
    public class RouteConfig
    {

        #region Method

        /// <summary>
        /// تنظیم مسیریابی پرتال
        /// </summary>
        /// <param name="routes">لیست مسیرها: RouteCollection</param>
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            //CMS.ModuleSetup.SetupRoutes(routes);

            /*routes.MapRoute(
                name: "CmsRoute",
                url: "Portal/{*permalink}",
                defaults: new { controller = "Page", action = "Index" },
                constraints: new { permalink = new BaseRoutingConstraint() },
                namespaces: new[] { "SAJPortal.Controllers" }
            );*/

            // تنظیم مسیر پردازشگر خطاها ELMAH
            routes.MapRoute(
                name: "admin_elmah",
                url: "admin/elmah/{type}",
                defaults: new { action = "Index", controller = "Elmah", type = UrlParameter.Optional },
                namespaces: new[] { "SAJPortal.Controllers" }
            );

            // تنظیم مسیر صفحات پرتال
            routes.MapRoute(
                name: "CmsRoute",
                url: "{*permalink}",
                defaults: new { controller = "Page", action = "Index" },
                constraints: new { permalink = new BaseRoutingConstraint() },
                namespaces: new[] { "SAJPortal.Controllers" }
            );

            // تنظیم مسیر پیشفرض MVC برای اجرای اولین درخواست کاربر
            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional },
                namespaces: new[] { "SAJPortal.Controllers" }
            );
        }

        #endregion

    }
}
