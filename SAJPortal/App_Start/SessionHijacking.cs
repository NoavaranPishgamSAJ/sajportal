﻿namespace SAJPortal
{

    #region Assembly

    using System;
    using System.Configuration;
    using System.Globalization;
    using System.Runtime.Serialization;
    using System.Security.Cryptography;
    using System.Text;
    using System.Web;
    using Utility;

    #endregion

    /// <summary>
    /// از این کلاس برای جلوگیری از هک شدن جلسه کاربر استفاده می شود
    /// </summary>
    public class SessionHijacking : IHttpModule
    {

        #region Attribute

        private static string _secretKey;

        /// <summary>
        /// نام ماژول
        /// </summary>
        public string ModuleName => "SessionHijackingModule";

        #endregion

        #region Method

        /// <summary>
        /// این متد برای مقدار دهی اولیه این کلاس در زمان اجرای صفحه است
        /// </summary>
        /// <param name="application">دستگیره برنامه: HttpApplication</param>
        public void Init(HttpApplication application)
        {
            // بدست آوردن کلید معتبر یا تنظیم اولیه آن
            if (_secretKey == null)
                _secretKey = GetKey();

            // ثبت دستگیره رویدادهای BeginRequest و EndRequest
            application.BeginRequest += Application_BeginRequest;
            application.EndRequest += Application_EndRequest;
        }

        /// <summary>
        /// از بین برنده کلاس
        /// </summary>
        public void Dispose()
        {
        }

        /// <summary>
        /// این متد برای رسیدگی به رویداد HttpApplication.BeginRequest می باشد
        /// </summary>
        /// <param name="sender">ارسال کننده: object</param>
        /// <param name="e">دستگیره رویداد: EventArgs</param>
        public void Application_BeginRequest(object sender, EventArgs e)
        {
            // دریافت درخواست فعلی
            var currentRequest = ((HttpApplication)sender).Request;

            // دریافت شناسه جلسه دات نت از موجودیت درخواست
            var requestCookie = RetrieveRequestCookie(currentRequest, "ASP.NET_SessionId");

            // بررسی موجود بودن جلسه
            if (requestCookie != null)
            {
                // اگر طول کوکی کمتر از 24 بود یعنی ما آدرس MAC را نداریم پس خطایی رخ داده است
                if (requestCookie.Value.Length <= 24)
                    return;
                    //throw new SessionerrorException("Invalid Session");

                // دریافت شناسه جلسه
                var sessionId = requestCookie.Value.Substring(0, 24);

                // دریافت آدرس MAC
                var mac = requestCookie.Value.Substring(24);

                // ایجاد آدرس MAC براساس شناسه جلسه و یکسری اطلاعات دیگر کاربر(user agent, etc)
                var macCompare = CreateMac(currentRequest, _secretKey);

                // بررسی برابر بودن آدرس های MAC
                if (string.CompareOrdinal(mac, macCompare) != 0)
                    return;
                    //throw new SessionerrorException("Invalid Session");

                // تنظیم مقدار کوکی با شناسه جلسه
                requestCookie.Value = sessionId;
            }
        }

        /// <summary>
        /// این متد برای رسیدگی به رویداد HttpApplication.EndRequest می باشد
        /// </summary>
        /// <param name="sender">ارسال کننده: object</param>
        /// <param name="e">دستگیره رویداد: EventArgs</param>
        public void Application_EndRequest(object sender, EventArgs e)
        {
            // گرفتن درخواست فعلی
            var currentRequest = ((HttpApplication)sender).Request;

            // دریافت کوکی جلسه
            var sessionCookie = RetrieveResponseCookie(((HttpApplication)sender).Response, "ASP.NET_SessionId");

            // مطمئن شدن از تهی نبودن کوکی
            if (sessionCookie != null)
            {
                // اضافه کردن آدرس MAC جدید تولید شده توسط کلاس به کوکی در انتهای درخواست
                sessionCookie.Value += CreateMac(currentRequest, _secretKey);
            }
        }

        /// <summary>
        /// این متد برای دریافت کلید معتبر از فایل web.config می باشد
        /// </summary>
        /// <returns></returns>
        private static string GetKey()
        {
            // دریافت کلید
            var validationKey = ConfigurationManager.AppSettings["SessionKey"];

            // بررسی خالی نبودن کلید
            if (validationKey.IsEmpty())
                throw new SessionerrorException("SessionKey not found. Application ending");

            return validationKey;
        }

        /// <summary>
        /// از این متد برای دریافت کوکی های درخواست استفاده می شود
        /// </summary>
        /// <param name="currentRequest">درخواست فعلی: HttpRequest</param>
        /// <param name="cookieName">نام کوکی: string</param>
        /// <returns>HttpCookie</returns>
        private static HttpCookie RetrieveRequestCookie(HttpRequest currentRequest, string cookieName)
        {
            var cookieCollection = currentRequest.Cookies;
            return FindTheCookie(cookieCollection, cookieName);
        }

        /// <summary>
        /// از این متد برای دریافت کوکی های پاسخ استفاده می شود
        /// </summary>
        /// <param name="currentResponse">پاسخ فعلی: HttpResponse</param>
        /// <param name="cookieName">نام کوکی: string</param>
        /// <returns>HttpCookie</returns>
        private static HttpCookie RetrieveResponseCookie(HttpResponse currentResponse, string cookieName)
        {
            var cookies = currentResponse.Cookies;
            return FindTheCookie(cookies, cookieName);
        }

        /// <summary>
        /// از این متد برای دریافت کوکی بر اساس نام آن استفاده می شود
        /// </summary>
        /// <param name="cookieCollection">مجموعه کوکی ها برای جستجو در آن: HttpCookieCollection</param>
        /// <param name="cookieName">کوکی مورد نظر ما: string</param>
        /// <returns>HttpCookie</returns>
        private static HttpCookie FindTheCookie(HttpCookieCollection cookieCollection, string cookieName)
        {
            for (var i = 0; i < cookieCollection.Count; i++)
            {
                if (string.Compare(cookieCollection[i]?.Name, cookieName, true, CultureInfo.InvariantCulture) == 0)
                    return cookieCollection[i];
            }

            return null;
        }

        /// <summary>
        /// تولید یک MAC جدید برای کوکی جلسه
        /// </summary>
        /// <param name="currentRequest">درخواست فعلی: HttpRequest</param>
        /// <param name="validationKey">کلید معتبر در فایل web.config: string</param>
        /// <returns>string</returns>
        private static string CreateMac(HttpRequest currentRequest, string validationKey)
        {
            var sb = new StringBuilder();
            sb.Append(currentRequest.Browser.Browser);
            sb.Append(currentRequest.Browser.Platform);
            sb.Append(currentRequest.Browser.MajorVersion);
            sb.Append(currentRequest.Browser.MinorVersion);
            using (var hmac = new HMACSHA1(Encoding.UTF8.GetBytes(validationKey)))
            {
                return Convert.ToBase64String(hmac.ComputeHash(Encoding.UTF8.GetBytes(sb.ToString())));
            }
        }

        #endregion

    }

    /// <summary>
    /// خطای جلسه نامعتبر
    /// </summary>
    [Serializable]
    internal class SessionerrorException : Exception
    {

        #region Method

        public SessionerrorException() : base("Invalid Session") { }

        public SessionerrorException(string message) : base(message) { }

        public SessionerrorException(string message, Exception inner) : base(message, inner) { }

        protected SessionerrorException(SerializationInfo info, StreamingContext context) : base(info, context) { }

        #endregion

    }

}
