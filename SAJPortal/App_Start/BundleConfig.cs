﻿namespace SAJPortal
{

    #region Assembly

    using System.Web.Optimization;

    #endregion

    /// <summary>
    /// این کلاس کار تعریف منابع پرتال را به عهده دارد
    /// </summary>
    public class BundleConfig
    {

        #region Method

        /// <summary>
        /// تعریف منابه پرتال
        /// </summary>
        /// <param name="bundles">لیست منابع: BundleCollection</param>
        public static void RegisterBundles(BundleCollection bundles)
        {

            #region Script

            bundles.Add(new ScriptBundle("~/Jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/Jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/Modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/BootstrapScript").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new ScriptBundle("~/BootstrapRTLScript").Include(
                      "~/Scripts/bootstrap-rtl.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new ScriptBundle("~/AdminTheme/MenuScript").Include(
                      "~/Scripts/AdminTheme/sidebar-menu.js"));

            bundles.Add(new ScriptBundle("~/AdminTheme/ScrollbarScript").Include(
                      "~/Scripts/AdminTheme/jquery.scrollbar.js"));

            bundles.Add(new ScriptBundle("~/AdminTheme/TabMenuScript").Include(
                      "~/Scripts/AdminTheme/tabs-to-dropdown.js"));

            bundles.Add(new ScriptBundle("~/TagsInputScript").Include(
                      "~/Scripts/bootstrap-tagsinput.min.js"));

            bundles.Add(new ScriptBundle("~/AdminTheme/MultifieldScript").Include(
                      "~/Scripts/AdminTheme/jquery.multifield.min.js"));

            bundles.Add(new ScriptBundle("~/ImageUploadScript").Include(
                      "~/Scripts/bootstrap-imageupload.min.js"));

            bundles.Add(new ScriptBundle("~/SummerNoteScript").Include(
                      "~/Content/SummerNote/summernote.min.js"));

            bundles.Add(new ScriptBundle("~/ModalScript").Include(
                      "~/Scripts/baze.modal.js"));

            bundles.Add(new ScriptBundle("~/BootstrapValidateScript").Include(
                      "~/Scripts/validate-bootstrap.jquery.min.js"));

            bundles.Add(new ScriptBundle("~/DatePickerScript").Include(
                      "~/Scripts/jquery.dateselect.js"));

            bundles.Add(new ScriptBundle("~/JalaaliScript").Include(
                      "~/Scripts/jalaali.js"));

            bundles.Add(new ScriptBundle("~/DropTabsScript").Include(
                      "~/Scripts/jquery.droptabs.js"));

            #endregion

            #region Style

            bundles.Add(new StyleBundle("~/BootstrapStyle").Include(
                      "~/Content/bootstrap.css"));

            bundles.Add(new StyleBundle("~/BootstrapRTLStyle").Include(
                      "~/Content/bootstrap-rtl.css"));

            bundles.Add(new StyleBundle("~/BootstrapThemeStyle").Include(
                      "~/Content/bootstrap-theme.css"));

            bundles.Add(new StyleBundle("~/BootstrapThemeRTLStyle").Include(
                      "~/Content/bootstrap-theme-rtl.css"));

            bundles.Add(new StyleBundle("~/AdminThemeStyle").Include(
                      "~/Content/AdminTheme/site.css"));

            bundles.Add(new StyleBundle("~/AdminTheme/MenuStyle").Include(
                      "~/Content/AdminTheme/sidebar-menu.css"));

            bundles.Add(new StyleBundle("~/AdminTheme/ScrollbarStyle").Include(
                      "~/Content/AdminTheme/jquery.scrollbar.css"));

            bundles.Add(new StyleBundle("~/AdminTheme/TabMenuStyle").Include(
                      "~/Content/AdminTheme/tabs-to-dropdown.css"));

            bundles.Add(new StyleBundle("~/AdminTheme/ResponsiveTableStyle").Include(
                      "~/Content/AdminTheme/table.css"));

            bundles.Add(new StyleBundle("~/TagsInputStyle").Include(
                      "~/Content/bootstrap-tagsinput.css"));

            bundles.Add(new StyleBundle("~/ImageUploadStyle").Include(
                      "~/Content/bootstrap-imageupload.min.css"));

            bundles.Add(new StyleBundle("~/SummerNoteStyle").Include(
                      "~/Content/SummerNote/summernote.css"));

            bundles.Add(new StyleBundle("~/ModalStyle").Include(
                      "~/Content/baze.modal.css"));

            bundles.Add(new StyleBundle("~/DatePickerStyle").Include(
                      "~/Content/jquery.dateselect.css"));

            bundles.Add(new StyleBundle("~/DropTabsStyle").Include(
                      "~/Content/jquery.droptabs.css"));

            #endregion

            #region Module Script

            bundles.Add(new ScriptBundle("~/Module/TabContent/Script").Include(
                    "~/Scripts/Module/TabContent/jquery.responsiveTabs.js"));

            bundles.Add(new ScriptBundle("~/Module/Gallery/Script").Include(
                    "~/Scripts/Module/Gallery/jquery.swipebox.js"));

            bundles.Add(new ScriptBundle("~/Module/GridGallery/Script").Include(
                    "~/Scripts/Module/GridGallery/wookmark.js"));

            bundles.Add(new ScriptBundle("~/Module/Accordion/Script").Include(
                    "~/Scripts/Module/Accordion/jquery.coolfieldset.js"));

            bundles.Add(new ScriptBundle("~/Module/Faq/Script").Include(
                    "~/Scripts/Module/Faq/jquery.faq.js"));

            bundles.Add(new ScriptBundle("~/Module/Calculator/Script").Include(
                    "~/Scripts/Module/Calculator/SimpleCalculadorajQuery.js"));

            bundles.Add(new ScriptBundle("~/Module/TabNews/Script").Include(
                    "~/Scripts/Module/News/responsivetabs.js"));

            bundles.Add(new ScriptBundle("~/Module/TickerNews/Script").Include(
                    "~/Scripts/Module/News/BreakingNews.js"));

            bundles.Add(new ScriptBundle("~/Module/BoxNews/Script").Include(
                    "~/Scripts/Module/News/jquery.bootstrap.newsbox.js"));

            #endregion

            #region Module Style

            bundles.Add(new StyleBundle("~/Module/TabContent/Style").Include(
                    "~/Content/Module/TabContent/responsive-tabs.css"));

            bundles.Add(new StyleBundle("~/Module/Gallery/Style").Include(
                    "~/Content/Module/Gallery/swipebox.min.css"));

            bundles.Add(new StyleBundle("~/Module/Accordion/Style").Include(
                    "~/Content/Module/Accordion/jquery.coolfieldset.css"));

            bundles.Add(new StyleBundle("~/Module/Faq/Style").Include(
                      "~/Content/Module/Faq/jquery.faq.min.css",
                      "~/Content/Module/Faq/jquery.selectlist.min.css"));

            bundles.Add(new StyleBundle("~/Module/Calculator/Style").Include(
                    "~/Content/Module/Calculator/SimpleCalculadorajQuery.css"));

            bundles.Add(new StyleBundle("~/Module/TabNews/Style").Include(
                    "~/Content/Module/News/responsivetabs.css"));

            bundles.Add(new StyleBundle("~/Module/TickerNews/Style").Include(
                    "~/Content/Module/News/BreakingNews.css"));

            #endregion

            BundleTable.EnableOptimizations = true;
        }

        #endregion

    }
}
