﻿namespace SAJPortal
{

    #region Assembly

    using System.Web.Mvc;

    #endregion

    /// <summary>
    /// این کلاس شامل فیلترهایی هست که در زمان اجرای هر درخواست فعال می شود
    /// </summary>
    public class FilterConfig
    {

        #region Method

        /// <summary>
        /// ثبت فیلترها
        /// </summary>
        /// <param name="filters">لیست فیلترها: GlobalFilterCollection</param>
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }

        #endregion

    }
}
