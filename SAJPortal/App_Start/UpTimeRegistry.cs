﻿namespace SAJPortal
{

    #region Assembly

    using System.Configuration;
    using System;
    using FluentScheduler;
    using Utility;
    using System.Net;
    using CMS.Configuration;

    #endregion

    /// <summary>
    /// کلاس اجرایی بیدار نگه دار پرتال
    /// </summary>
    /// <remarks>
    /// سایت های دات نت در حالت عادی بعد از پایان مهلت جلسه اگر کاربری با آن کار نکند به خواب می روند و وقتی کاربر
    /// جدید درخواست کار با سایت را داشته باشد، برنامه دوباره شروع به راه اندازی مجدد خود می کند. این کار سبب
    /// کندی در اجرای سایت در هر بار راه اندازی اولیه به مدت 5 دقیقه می شود. برای جلوگیری از این موضوع این کلاس
    /// هر چند دقیقه یکبار یکسری آدرس هایی که به آن داده شده را فراخوانی می کند تا نگذارد سایت به خواب برود
    /// </remarks>
    public class UpTimeRegistry : Registry
    {

        #region Method

        /// <summary>
        /// سازنده کلاس
        /// </summary>
        public UpTimeRegistry()
        {
            // تنظیم زمانبند برای هر 15 دقیقه یکبار
            Schedule(async () =>
            {
                try
                {
                    // خواندن تنظیمات از فایل web.config
                    var upTime = ConfigurationManager.GetSection("UpTimeSection") as UpTimeConfigurationSection;
                    if (upTime != null)
                    {
                        // دریافت لیست آدرس ها
                        foreach (UrlConfig url in upTime.Urls)
                        {
                            // فراخوانی آدرس مورد نظر
                            var request = (HttpWebRequest)WebRequest.Create(url.Path);
                            //request.UseDefaultCredentials = true;
                            //request.Proxy.Credentials = CredentialCache.DefaultCredentials;
                            request.UserAgent = "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.2; .NET CLR 1.0.3705;)";
                            using (var response = await request.GetResponseAsync())
                            using (var content = response.GetResponseStream())
                            {
                                content?.Close();
                            }
                        }
                    }
                    //Logger.Log(new Exception(@"Task is run:" + DateTime.Now.ToLongDateString()));
                }
                catch (Exception ex)
                {
                    Logger.Log(ex);
                }
            }).ToRunNow().AndEvery(15).Minutes();
        }

        #endregion

    }
}
