﻿namespace ORM.Enums
{

    #region Assembly

    using Resources;
    using System.ComponentModel.DataAnnotations;

    #endregion

    /// <summary>
    /// نوع نسخه سایت
    /// </summary>
    public enum SiteMode : byte
    {
        /// <summary>
        /// حالت اشکالزدایی
        /// </summary>
        [Display(ResourceType = typeof(Global), Name = "Debug")]
        Debug = 0,
        /// <summary>
        /// حالت انتشار
        /// </summary>
        [Display(ResourceType = typeof(Global), Name = "Release")]
        Release = 1

    }
}
