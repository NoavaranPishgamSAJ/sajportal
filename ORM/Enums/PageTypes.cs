﻿namespace ORM.Enums
{

    #region Assembly

    using Resources;
    using System.ComponentModel.DataAnnotations;

    #endregion

    /// <summary>
    /// نوع صفحات پرتال
    /// </summary>
    public enum PageTypes : byte
    {

        /// <summary>
        /// صفحه مدیریت
        /// </summary>
        [Display(ResourceType = typeof(Global), Name = "AdminPage")]
        AdminPage = 1,

        /// <summary>
        /// صفحه کاربر
        /// </summary>
        [Display(ResourceType = typeof(Global), Name = "SitePage")]
        SitePage = 2

    }
}
