﻿namespace ORM.Enums
{

    #region Assembly

    using Resources;
    using System;
    using System.ComponentModel.DataAnnotations;

    #endregion

    /// <summary>
    /// از این نوع داده به جای مقدار bool استفاده می شود
    /// </summary>
    public enum Toggle : byte
    {

        /// <summary>
        /// نادرست
        /// </summary>
        [Display(ResourceType = typeof(Global), Name = "No")]
        False = 0,
        /// <summary>
        /// درست
        /// </summary>
        [Display(ResourceType = typeof(Global), Name = "Yes")]
        True = 1

    }

    /// <summary>
    /// این کلاس برای توسعه کارایی نوع داده Toggle می باشد
    /// </summary>
    public static class ToggleExtension
    {

        /// <summary>
        /// از این متد برای بدست آوردن مقدار نوشتاری موجودیت Toggle استفاده می شود.
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// string toggleStr = Toggle.True.Localize();
        /// </code>
        /// </example>
        /// <returns>string</returns>
        public static string Localize(this Toggle o)
        {
            return o.Equals(Toggle.True) ? CultureHelper.Localize("Yes") : CultureHelper.Localize("No");
        }

        /// <summary>
        /// این متد برای تبدیل نوع bool به Toggle استفاده می شود
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// Toggle isTrue = false.GetToggle();
        /// </code>
        /// </example>
        /// <returns>Toggle</returns>
        public static Toggle GetToggle(this bool o)
        {
            return o ? Toggle.True : Toggle.False;
        }

        /// <summary>
        /// این متد برای تبدیل نوع Toggle به bool استفاده می شود
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// bool isTrue = Toggle.True.GetValue();
        /// </code>
        /// </example>
        /// <returns>Toggle</returns>
        public static bool GetValue(this Toggle o)
        {
            return o.Equals(Toggle.True);
        }

        /// <summary>
        /// این متد برای تبدیل نوع Toggle به byte استفاده می شود
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// byte isTrue = Toggle.True.GetNumber();
        /// </code>
        /// </example>
        /// <returns>byte</returns>
        public static byte GetNumber(this Toggle o)
        {
            return Convert.ToByte(o.Equals(Toggle.True));
        }

    }
}
