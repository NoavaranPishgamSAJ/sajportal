﻿namespace ORM.Model
{

    #region Assembly

    using Dapper.Contrib.Extensions;
    using Enums;
    using System.Runtime.Serialization;

    #endregion

    /// <summary>
    /// جدول مجوزهای کاربر
    /// </summary>
    [Table("CustomerPermission")]
    [DataContract]
    public class CustomerPermission : BaseModel<CustomerPermission>
    {

        #region Property

        /// <summary>
        /// شناسه کاربر
        /// </summary>
        [DataMember]
        public int CustomerId { get; set; }

        /// <summary>
        /// شناسه مجوز
        /// </summary>
        [DataMember]
        public int PermissionId { get; set; }

        /// <summary>
        /// دارا بودن یا نبودن
        /// </summary>
        [DataMember]
        public Toggle IsHave { get; set; }

        #endregion

    }
}
