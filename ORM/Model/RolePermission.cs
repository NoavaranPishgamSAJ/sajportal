﻿namespace ORM.Model
{

    #region Assembly

    using Dapper.Contrib.Extensions;
    using System.Runtime.Serialization;

    #endregion

    /// <summary>
    /// جدول مجوزهای نقش
    /// </summary>
    [Table("RolePermission")]
    [DataContract]
    public class RolePermission : BaseModel<RolePermission>
    {

        #region Property

        /// <summary>
        /// شناسه نقش
        /// </summary>
        [DataMember]
        public int RoleId { get; set; }

        /// <summary>
        /// شناسه مجوز
        /// </summary>
        [DataMember]
        public int PermissionId { get; set; }

        #endregion

    }
}
