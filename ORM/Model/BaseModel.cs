﻿namespace ORM.Model
{

    #region Assembly

    using System.Linq;
    using Dapper.Contrib.Extensions;
    using System;
    using Utility.DateUtil;
    using System.Runtime.Serialization;
    using System.ComponentModel;
    using Utility;
    using System.Collections.Generic;

    #endregion

    /// <summary>
    /// این کلاس موجودیت پدر برای تمامی موجودیت های دیتابیس می باشد
    /// </summary>
    /// <typeparam name="T"></typeparam>
    [DataContract]
    public class BaseModel<T> : INotifyPropertyChanged where T : class
    {

        #region Attribute

        /// <summary>
        /// دستگیره رویداد تغییر مقدار فیلد
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// شناسه
        /// </summary>
        [Key]
        [DataMember]
        public int ItemId { get; set; }

        /// <summary>
        /// تاریخ ثبت
        /// </summary>
        [DataMember]
        public DateTime AddedDate { get; set; }

        /// <summary>
        /// تاریخ ثبت به فارسی
        /// </summary>
        [Computed]
        [DataMember]
        public string AddedDateFa => DateConvertor.DateLocalization(AddedDate, false, false, '/');

        /// <summary>
        /// تاریخ ویرایش
        /// </summary>
        [DataMember]
        public DateTime? ModifiedDate { get; set; }

        /// <summary>
        /// تاریخ ویرایش به فارسی
        /// </summary>
        [Computed]
        [DataMember]
        public string ModifiedDateFa => ModifiedDate.HasValue ? DateConvertor.DateLocalization(ModifiedDate.Value, false, false, '/') : string.Empty;

        #endregion

        #region Method

        /// <summary>
        /// سازنده کلاس
        /// </summary>
        public BaseModel()
        {
            ModifiedDate = null;
        }

        /// <summary>
        /// این متد نام جدول مربوط به این موجودیت در دیتابیس را باز می گرداند
        /// </summary>
        /// <returns>string</returns>
        public string GetTableName()
        {
            var attrs = Attribute.GetCustomAttributes(typeof(T));
            foreach (var attribute in attrs.OfType<TableAttribute>())
            {
                return attribute.Name;
            }
            return string.Empty;
        }

        /// <summary>
        /// رویداد تغییر مقدار فیلد
        /// </summary>
        protected void OnPropertyChanged(PropertyChangedEventArgs e)
        {
            PropertyChanged?.Invoke(this, e);
        }

        #endregion

    }

    /// <summary>
    /// این کلاس موجودیت پدر برای تمامی موجودیت های دیتابیس می باشد
    /// </summary>
    [DataContract]
    public class GenericBaseModel : DynamicClass, INotifyPropertyChanged
    {

        #region Attribute

        /// <summary>
        /// دستگیره رویداد تغییر مقدار فیلد
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// نام جدول مربوط به موجودیت در دیتابیس
        /// </summary>
        [Computed]
        [DataMember]
        public string TableName { get; set; }

        /// <summary>
        /// شناسه
        /// </summary>
        [Key]
        [DataMember]
        public int ItemId { get; set; }

        /// <summary>
        /// تاریخ ثبت
        /// </summary>
        [DataMember]
        public DateTime AddedDate { get; set; }

        /// <summary>
        /// تاریخ ثبت به فارسی
        /// </summary>
        [Computed]
        [DataMember]
        public string AddedDateFa => DateConvertor.DateLocalization(AddedDate, false, false, '/');

        /// <summary>
        /// تاریخ ویرایش
        /// </summary>
        [DataMember]
        public DateTime? ModifiedDate { get; set; }

        /// <summary>
        /// تاریخ ویرایش به فارسی
        /// </summary>
        [Computed]
        [DataMember]
        public string ModifiedDateFa => ModifiedDate.HasValue ? DateConvertor.DateLocalization(ModifiedDate.Value, false, false, '/') : string.Empty;

        #endregion

        #region Method

        /// <summary>
        /// سازنده کلاس
        /// </summary>
        public GenericBaseModel()
        {
            ModifiedDate = null;
        }

        /// <summary>
        /// سازنده کلاس
        /// </summary>
        public GenericBaseModel(List<Field> fields) : base(fields)
        {
            ModifiedDate = null;
        }

        /// <summary>
        /// این متد نام جدول مربوط به این موجودیت در دیتابیس را باز می گرداند
        /// </summary>
        /// <returns>string</returns>
        public string GetTableName()
        {
            return TableName;
        }

        /// <summary>
        /// رویداد تغییر مقدار فیلد
        /// </summary>
        protected void OnPropertyChanged(PropertyChangedEventArgs e)
        {
            PropertyChanged?.Invoke(this, e);
        }

        #endregion

    }

}
