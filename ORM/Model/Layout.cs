﻿namespace ORM.Model
{

    #region Assembly

    using Dapper.Contrib.Extensions;
    using System.Runtime.Serialization;

    #endregion

    /// <summary>
    /// جدول قالب
    /// </summary>
    [Table("Layout")]
    [DataContract]
    public class Layout : BaseModel<Layout>
    {

        #region Property

        /// <summary>
        /// نام قالب
        /// </summary>
        [DataMember]
        public string LayoutName { get; set; }

        /// <summary>
        /// مسیر نسبی قالب
        /// </summary>
        [DataMember]
        public string VirtualPath { get; set; }

        /// <summary>
        /// شناسه سایت
        /// </summary>
        [DataMember]
        public int SiteId { get; set; }

        #endregion

    }
}
