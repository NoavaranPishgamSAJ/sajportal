﻿namespace ORM.Model
{

    #region Assembly

    using Dapper.Contrib.Extensions;
    using System.Runtime.Serialization;

    #endregion

    /// <summary>
    /// نمای لیست تمامی مجوزهای فعال کاربر
    /// </summary>
    [Table("CustomerPermissionsView")]
    [DataContract]
    public class CustomerPermissions : BaseModel<CustomerPermissions>
    {

        #region Property

        /// <summary>
        /// شناسه کاربر
        /// </summary>
        [DataMember]
        public int CustomerId { get; set; }

        /// <summary>
        /// شناسه مجوز
        /// </summary>
        [DataMember]
        public int PermissionId { get; set; }

        /// <summary>
        /// نام مجوز
        /// </summary>
        [DataMember]
        public string DisplayName { get; set; }

        /// <summary>
        /// نام مجوز در برنامه
        /// </summary>
        [DataMember]
        public string DevName { get; set; }

        #endregion

    }
}
