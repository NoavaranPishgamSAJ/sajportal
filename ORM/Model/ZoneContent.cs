﻿namespace ORM.Model
{

    #region Assembly

    using Dapper.Contrib.Extensions;
    using Enums;
    using System.Runtime.Serialization;
    using System.ComponentModel;
    using Newtonsoft.Json;
    using System.Linq;
    using Utility;

    #endregion

    /// <summary>
    /// جدول محتوای ناحیه های صفحات
    /// </summary>
    [Table("ZoneContent")]
    [DataContract]
    public class ZoneContent : BaseModel<ZoneContent>
    {

        #region Attribute

        private string[] _routeFieldsList;

        private string[] _routeFieldsValueList;

        #endregion

        #region Property

        /// <summary>
        /// نام ناحیه
        /// </summary>
        [DataMember]
        public string ZoneName { get; set; }

        /// <summary>
        /// ترتیب نمایش
        /// </summary>
        [DataMember]
        public int OrderId { get; set; }

        /// <summary>
        /// مخفی بودن
        /// </summary>
        [DataMember]
        public Toggle Hidden { get; set; }

        /// <summary>
        /// نام پارامترهای مسیریابی در قالب JSON
        /// </summary>
        [DataMember]
        public string RouteFields { get; set; }

        /// <summary>
        /// مقدار پارامترهای مسیریابی در قالب JSON
        /// </summary>
        [DataMember]
        public string RouteFieldsValue { get; set; }

        /// <summary>
        /// نام پارامترهای مسیریابی
        /// </summary>
        [Computed]
        [DataMember]
        public string[] RouteFieldsList
        {
            get { return RouteFields.IsNotEmpty() && RouteFields.Length > 4 ? JsonConvert.DeserializeObject<string[]>(RouteFields) : new string[] { }; }
            set { _routeFieldsList = value; OnPropertyChanged("RouteFieldsList"); }
        }

        /// <summary>
        /// مقدار پارامترهای مسیریابی
        /// </summary>
        [Computed]
        [DataMember]
        public string[] RouteFieldsValueList
        {
            get { return RouteFieldsValue.IsNotEmpty() && RouteFieldsValue.Length > 4 ? JsonConvert.DeserializeObject<string[]>(RouteFieldsValue) : new string[] { }; }
            set { _routeFieldsValueList = value; OnPropertyChanged("RouteFieldsValueList"); }
        }

        /// <summary>
        /// متن ثابت HTML
        /// </summary>
        [DataMember]
        public string HtmlString { get; set; }

        /// <summary>
        /// شناسه نما
        /// </summary>
        [DataMember]
        public int? ViewId { get; set; }

        /// <summary>
        /// شناسه ماژول
        /// </summary>
        [DataMember]
        public int? ModuleId { get; set; }

        /// <summary>
        /// شناسه صفحه
        /// </summary>
        [DataMember]
        public int PageId { get; set; }

        #endregion

        #region Method

        /// <summary>
        /// سازنده کلاس
        /// </summary>
        public ZoneContent()
        {
            RouteFields = null;
            RouteFieldsValue = null;
            HtmlString = null;
            ViewId = null;
            ModuleId = null;
        }

        /// <summary>
        /// رویداد تغییر مقدار فیلد
        /// </summary>
        protected void OnPropertyChanged(string propertyName)
        {
            switch (propertyName)
            {
                case "RouteFieldsList":
                    if (_routeFieldsList != null)
                        RouteFields = JsonConvert.SerializeObject(_routeFieldsList.Where(p => p.IsNotEmpty()).ToArray());
                    break;
                case "RouteFieldsValueList":
                    if (_routeFieldsValueList != null)
                        RouteFieldsValue = JsonConvert.SerializeObject(_routeFieldsValueList.Where(p => p.IsNotEmpty()).ToArray());
                    break;
            }

            OnPropertyChanged(new PropertyChangedEventArgs(propertyName));
        }

        #endregion

    }
}
