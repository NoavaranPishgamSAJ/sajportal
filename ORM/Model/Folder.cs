﻿namespace ORM.Model
{

    #region Assembly

    using Dapper.Contrib.Extensions;
    using System.Runtime.Serialization;

    #endregion

    /// <summary>
    /// جدول پوشه
    /// </summary>
    [Table("Folder")]
    [DataContract]
    public class Folder : BaseModel<Folder>
    {

        #region Property

        /// <summary>
        /// نام پوشه
        /// </summary>
        [DataMember]
        public string FolderName { get; set; }

        /// <summary>
        /// مسیر نسبی پوشه
        /// </summary>
        [DataMember]
        public string VirtualPath { get; set; }

        /// <summary>
        /// شناسه نوع پوشه
        /// </summary>
        [DataMember]
        public int FolderTypeId { get; set; }

        /// <summary>
        /// شناسه محتوا
        /// </summary>
        [DataMember]
        public int? ContentId { get; set; }

        /// <summary>
        /// شناسه سایت
        /// </summary>
        [DataMember]
        public int SiteId { get; set; }

        #endregion

        #region Method

        /// <summary>
        /// سازنده
        /// </summary>
        public Folder()
        {
            ContentId = null;
        }

        #endregion

    }
}
