﻿namespace ORM.Model
{

    #region Assembly

    using Dapper.Contrib.Extensions;
    using System.Runtime.Serialization;

    #endregion

    /// <summary>
    /// جدول عضویت
    /// </summary>
    [Table("Membership")]
    [DataContract]
    public class Membership : BaseModel<Membership>
    {

        #region Property

        /// <summary>
        /// نام عضویت
        /// </summary>
        [DataMember]
        public string MembershipName { get; set; }

        #endregion

    }
}
