﻿namespace ORM.Model
{

    #region Assembly

    using Dapper.Contrib.Extensions;
    using System.Runtime.Serialization;

    #endregion

    /// <summary>
    /// جدول نقش های کاربر
    /// </summary>
    [Table("CustomerRole")]
    [DataContract]
    public class CustomerRole : BaseModel<CustomerRole>
    {

        #region Property

        /// <summary>
        /// شناسه کاربر
        /// </summary>
        [DataMember]
        public int CustomerId { get; set; }

        /// <summary>
        /// شناسه نقش
        /// </summary>
        [DataMember]
        public int RoleId { get; set; }

        #endregion

    }
}
