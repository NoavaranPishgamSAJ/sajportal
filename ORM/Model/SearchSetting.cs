﻿namespace ORM.Model
{

    #region Assembly

    using Dapper.Contrib.Extensions;
    using Newtonsoft.Json;
    using System.Collections.Generic;
    using System.Runtime.Serialization;

    #endregion

    /// <summary>
    /// جدول تنظیمات جستجو
    /// </summary>
    [Table("SearchSetting")]
    [DataContract]
    public class SearchSetting : BaseModel<SearchSetting>
    {

        #region Property

        /// <summary>
        /// آدرس صفحه
        /// </summary>
        [DataMember]
        public string LinkPage { get; set; }

        /// <summary>
        /// پارامترهای مسیریابی در قالب JSON
        /// </summary>
        [DataMember]
        public string RouteField { get; set; }

        /// <summary>
        /// پارامترهای مسیریابی
        /// </summary>
        [Computed]
        [DataMember]
        public Dictionary<string, object> RouteFieldObject
        {
            get { return JsonConvert.DeserializeObject<Dictionary<string, object>>(RouteField); }
            set { RouteField = JsonConvert.SerializeObject(value); }
        }

        /// <summary>
        /// شناسه محتوا
        /// </summary>
        [DataMember]
        public int ContentId { get; set; }

        /// <summary>
        /// شناسه مخزن
        /// </summary>
        [DataMember]
        public int RepositoryId { get; set; }

        #endregion

    }
}
