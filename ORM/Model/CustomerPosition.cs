﻿namespace ORM.Model
{

    #region Assembly

    using Dapper.Contrib.Extensions;
    using System.Runtime.Serialization;

    #endregion

    /// <summary>
    /// جدول سمت های کاربر
    /// </summary>
    [Table("CustomerPosition")]
    [DataContract]
    public class CustomerPosition : BaseModel<CustomerPosition>
    {

        #region Property

        /// <summary>
        /// شناسه کاربر
        /// </summary>
        [DataMember]
        public int CustomerId { get; set; }

        /// <summary>
        /// شناسه سمت
        /// </summary>
        [DataMember]
        public int PositionId { get; set; }

        #endregion

    }
}
