﻿namespace ORM.Model
{

    #region Assembly

    using Dapper.Contrib.Extensions;
    using System.Runtime.Serialization;

    #endregion

    /// <summary>
    /// جدول نقش
    /// </summary>
    [Table("Role")]
    [DataContract]
    public class Role : BaseModel<Role>
    {

        #region Property

        /// <summary>
        /// نام نقش
        /// </summary>
        [DataMember]
        public string DisplayName { get; set; }

        /// <summary>
        /// شناسه عضویت
        /// </summary>
        [DataMember]
        public int MembershipId { get; set; }

        #endregion

    }
}
