﻿namespace ORM.Model
{

    #region Assembly

    using Dapper.Contrib.Extensions;
    using Enums;
    using Newtonsoft.Json;
    using System.Runtime.Serialization;
    using System.Linq;
    using Utility;
    using System.ComponentModel;

    #endregion

    /// <summary>
    /// جدول سایت ها
    /// </summary>
    [Table("Site")]
    [DataContract]
    public class Site : BaseModel<Site>
    {

        #region Attribute

        private string[] _domainsList;

        private string[] _userAgentList;

        private string[] _smtpToList;

        private string[] _customFieldsList;

        private string[] _customFieldsValueList;

        private string[] _htmlMetaBlockList;

        #endregion

        #region Property

        /// <summary>
        /// نام سایت
        /// </summary>
        [DataMember]
        public string SiteName { get; set; }

        /// <summary>
        /// عنوان سایت
        /// </summary>
        [DataMember]
        public string Title { get; set; }

        /// <summary>
        /// برچسب سایت
        /// </summary>
        [DataMember]
        public string Label { get; set; }

        /// <summary>
        /// نوع نسخه سایت
        /// </summary>
        [DataMember]
        public SiteMode Mode { get; set; }

        /// <summary>
        /// فرهنگ
        /// </summary>
        [DataMember]
        public string Culture { get; set; }

        /// <summary>
        /// لیست دامنه های قابل قبول در قالب JSON
        /// </summary>
        [DataMember]
        public string Domains { get; set; }

        /// <summary>
        /// لیست دامنه های قابل قبول
        /// </summary>
        [Computed]
        [DataMember]
        public string[] DomainsList
        {
            get { return Domains.IsNotEmpty() && Domains.Length > 4 ? JsonConvert.DeserializeObject<string[]>(Domains) : new string[] { }; }
            set { _domainsList = value; OnPropertyChanged("DomainsList"); }
        }

        /// <summary>
        /// مسیر الحاقی سایت
        /// </summary>
        [DataMember]
        public string SitePath { get; set; }

        /// <summary>
        /// نسخه سایت
        /// </summary>
        [DataMember]
        public string Version { get; set; }

        /// <summary>
        /// لیست مرورگرهای قابل پشتیبانی در قالب JSON
        /// </summary>
        [DataMember]
        public string UserAgent { get; set; }

        /// <summary>
        /// لیست مرورگرهای قابل پشتیبانی
        /// </summary>
        [Computed]
        [DataMember]
        public string[] UserAgentList
        {
            get { return UserAgent.IsNotEmpty() && UserAgent.Length > 4 ? JsonConvert.DeserializeObject<string[]>(UserAgent) : new string[] { }; }
            set { _userAgentList = value; OnPropertyChanged("UserAgentList"); }
        }

        /// <summary>
        /// در دسترس بودن
        /// </summary>
        [DataMember]
        public Toggle IsOnline { get; set; }

        /// <summary>
        /// سایت ابتدایی در هنگام دسترسی به پرتال از طریق مرورگر
        /// </summary>
        [DataMember]
        public Toggle IsBase { get; set; }

        /// <summary>
        /// هاست ایمیل
        /// </summary>
        [DataMember]
        public string SmtpHost { get; set; }

        /// <summary>
        /// نام کاربری ایمیل
        /// </summary>
        [DataMember]
        public string SmtpUsername { get; set; }

        /// <summary>
        /// رمز عبور ایمیل
        /// </summary>
        [DataMember]
        public string SmtpPassword { get; set; }

        /// <summary>
        /// درگاه ایمیل
        /// </summary>
        [DataMember]
        public string SmtpPort { get; set; }

        /// <summary>
        /// آدرس ارسال کننده ایمیل
        /// </summary>
        [DataMember]
        public string SmtpFrom { get; set; }

        /// <summary>
        /// لیست آدرس های دریافت کننده ایمیل های ارسالی از طریق پرتال در قالب JSON
        /// </summary>
        [DataMember]
        public string SmtpTo { get; set; }

        /// <summary>
        /// لیست آدرس های دریافت کننده ایمیل های ارسالی از طریق پرتال
        /// </summary>
        [Computed]
        [DataMember]
        public string[] SmtpToList
        {
            get { return SmtpTo.IsNotEmpty() && SmtpTo.Length > 4 ? JsonConvert.DeserializeObject<string[]>(SmtpTo) : new string[] { }; }
            set { _smtpToList = value; OnPropertyChanged("SmtpToList"); }
        }

        /// <summary>
        /// مدیر سایت
        /// </summary>
        [DataMember]
        public string Author { get; set; }

        /// <summary>
        /// کلمات کلیدی
        /// </summary>
        [DataMember]
        public string Keyword { get; set; }

        /// <summary>
        /// توضیحات سایت
        /// </summary>
        [DataMember]
        public string Description { get; set; }

        /// <summary>
        /// نام فیلدهای سفارشی در قالب JSON
        /// </summary>
        [DataMember]
        public string CustomFields { get; set; }

        /// <summary>
        /// مقدار فیلدهای سفارشی در قالب JSON
        /// </summary>
        [DataMember]
        public string CustomFieldsValue { get; set; }

        /// <summary>
        /// نام فیلدهای سفارشی
        /// </summary>
        [Computed]
        [DataMember]
        public string[] CustomFieldsList
        {
            get { return CustomFields.IsNotEmpty() && CustomFields.Length > 4 ? JsonConvert.DeserializeObject<string[]>(CustomFields) : new string[] { }; }
            set { _customFieldsList = value; OnPropertyChanged("CustomFieldsList"); }
        }

        /// <summary>
        /// مقدار فیلدهای سفارشی
        /// </summary>
        [Computed]
        [DataMember]
        public string[] CustomFieldsValueList
        {
            get { return CustomFieldsValue.IsNotEmpty() && CustomFieldsValue.Length > 4 ? JsonConvert.DeserializeObject<string[]>(CustomFieldsValue) : new string[] { }; }
            set { _customFieldsValueList = value; OnPropertyChanged("CustomFieldsValueList"); }
        }

        /// <summary>
        /// استاندارد سازی آدرس ها
        /// </summary>
        [DataMember]
        public string Canonical { get; set; }

        /// <summary>
        /// لیست بلاک های متا در قالب JSON
        /// </summary>
        [DataMember]
        public string HtmlMetaBlock { get; set; }

        /// <summary>
        /// لیست بلاک های متا
        /// </summary>
        [Computed]
        [DataMember]
        public string[] HtmlMetaBlockList
        {
            get { return HtmlMetaBlock.IsNotEmpty() && HtmlMetaBlock.Length > 4 ? JsonConvert.DeserializeObject<string[]>(HtmlMetaBlock) : new string[] { }; }
            set { _htmlMetaBlockList = value; OnPropertyChanged("HtmlMetaBlockList"); }
        }

        /// <summary>
        /// منطقه زمانی
        /// </summary>
        [DataMember]
        public string TimeZoneId { get; set; }

        /// <summary>
        /// شناسه عضویت
        /// </summary>
        [DataMember]
        public int MembershipId { get; set; }

        /// <summary>
        /// شناسه مخزن
        /// </summary>
        [DataMember]
        public int RepositoryId { get; set; }

        /// <summary>
        /// شناسه والد
        /// </summary>
        [DataMember]
        public int? ParentId { get; set; }

        #endregion

        #region Method

        /// <summary>
        /// سازنده کلاس
        /// </summary>
        public Site()
        {
            Title = null;
            Label = null;
            Domains = null;
            SitePath = null;
            Version = null;
            UserAgent = null;
            SmtpHost = null;
            SmtpUsername = null;
            SmtpPassword = null;
            SmtpPort = null;
            SmtpFrom = null;
            SmtpTo = null;
            Author = null;
            Keyword = null;
            Description = null;
            CustomFields = null;
            CustomFieldsValue = null;
            Canonical = null;
            HtmlMetaBlock = null;
            TimeZoneId = null;
            ParentId = null;
        }

        /// <summary>
        /// رویداد تغییر مقدار فیلد
        /// </summary>
        protected void OnPropertyChanged(string propertyName)
        {
            switch (propertyName)
            {
                case "DomainsList":
                    if (_domainsList != null)
                        Domains = JsonConvert.SerializeObject(_domainsList.Where(p => p.IsNotEmpty()).ToArray());
                    break;
                case "UserAgentList":
                    if (_userAgentList != null)
                        UserAgent = JsonConvert.SerializeObject(_userAgentList.Where(p => p.IsNotEmpty()).ToArray());
                    break;
                case "SmtpToList":
                    if (_smtpToList != null)
                        SmtpTo = JsonConvert.SerializeObject(_smtpToList.Where(p => p.IsNotEmpty()).ToArray());
                    break;
                case "CustomFieldsList":
                    if (_customFieldsList != null)
                        CustomFields = JsonConvert.SerializeObject(_customFieldsList.Where(p => p.IsNotEmpty()).ToArray());
                    break;
                case "CustomFieldsValueList":
                    if (_customFieldsValueList != null)
                        CustomFieldsValue = JsonConvert.SerializeObject(_customFieldsValueList.Where(p => p.IsNotEmpty()).ToArray());
                    break;
                case "HtmlMetaBlockList":
                    if (_htmlMetaBlockList != null)
                        HtmlMetaBlock = JsonConvert.SerializeObject(_htmlMetaBlockList.Where(p => p.IsNotEmpty()).ToArray());
                    break;
            }

            OnPropertyChanged(new PropertyChangedEventArgs(propertyName));
        }

        #endregion

    }
}
