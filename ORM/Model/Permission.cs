﻿namespace ORM.Model
{

    #region Assembly

    using Dapper.Contrib.Extensions;
    using System.Runtime.Serialization;

    #endregion

    /// <summary>
    /// جدول مجوز
    /// </summary>
    [Table("Permission")]
    [DataContract]
    public class Permission : BaseModel<Permission>
    {

        #region Property

        /// <summary>
        /// نام مجوز
        /// </summary>
        [DataMember]
        public string DisplayName { get; set; }

        /// <summary>
        /// نام مجوز در برنامه
        /// </summary>
        [DataMember]
        public string DevName { get; set; }

        /// <summary>
        /// شناسه عضویت
        /// </summary>
        [DataMember]
        public int MembershipId { get; set; }

        #endregion

    }
}
