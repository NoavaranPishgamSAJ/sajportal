﻿namespace ORM.Model
{

    #region Assembly

    using System;
    using Dapper.Contrib.Extensions;
    using Enums;
    using Utility.DateUtil;
    using System.Runtime.Serialization;

    #endregion

    /// <summary>
    /// جدول کاربران سیستم
    /// </summary>
    [Table("Customer")]
    [DataContract]
    public class Customer : BaseModel<Customer>
    {

        #region Property

        /// <summary>
        /// نام کاربری
        /// </summary>
        [DataMember]
        public string Username { get; set; }

        /// <summary>
        /// رمز عبور
        /// </summary>
        [DataMember]
        public string Password { get; set; }

        /// <summary>
        /// نام
        /// </summary>
        [DataMember]
        public string FirstName { get; set; }

        /// <summary>
        /// نام خانوادگی
        /// </summary>
        [DataMember]
        public string LastName { get; set; }

        /// <summary>
        /// نام پدر
        /// </summary>
        [DataMember]
        public string FatherName { get; set; }

        /// <summary>
        /// شناسه ملی
        /// </summary>
        [DataMember]
        public string NationalCode { get; set; }

        /// <summary>
        /// متن بازیابی رمز عبور
        /// </summary>
        [DataMember]
        public string RetrievalText { get; set; }

        /// <summary>
        /// مدیر بودن
        /// </summary>
        [DataMember]
        public Toggle IsAdmin { get; set; }

        /// <summary>
        /// مسدود بودن
        /// </summary>
        [DataMember]
        public Toggle BoycottUser { get; set; }

        /// <summary>
        /// آخرین تاریخ ورود
        /// </summary>
        [DataMember]
        public DateTime? LastLoginDate { get; set; }

        /// <summary>
        /// آخرین تاریخ ورود به فارسی
        /// </summary>
        [Computed]
        [DataMember]
        public string LastLoginDateFa => LastLoginDate.HasValue ? DateConvertor.DateLocalization(LastLoginDate.Value, false, false, '/') : string.Empty;

        /// <summary>
        /// آخرین شناسه ورود
        /// </summary>
        [DataMember]
        public string LastLoginIdentification { get; set; }

        /// <summary>
        /// استفاده از تاریخ موثر
        /// </summary>
        [DataMember]
        public Toggle UseOperationDate { get; set; }

        /// <summary>
        /// تاریخ موثر
        /// </summary>
        [DataMember]
        public DateTime? OperationDate { get; set; }

        /// <summary>
        /// تاریخ موثر به فارسی
        /// </summary>
        [Computed]
        [DataMember]
        public string OperationDateStr => OperationDate.HasValue ? DateConvertor.DateLocalization(OperationDate.Value, false, false, '/') : string.Empty;

        /// <summary>
        /// اعلان عمومی که از طرف مدیر سایت به کاربران ابلاغ می شود
        /// </summary>
        [DataMember]
        public string PublicMassage { get; set; }

        /// <summary>
        /// شماره شناسایی
        /// </summary>
        [DataMember]
        public string IdentificationNumber { get; set; }

        /// <summary>
        /// ایمیل
        /// </summary>
        [DataMember]
        public string Email { get; set; }

        /// <summary>
        /// تاییدیه ایمیل
        /// </summary>
        [DataMember]
        public Toggle EmailConfirmed { get; set; }

        /// <summary>
        /// شماره تلفن
        /// </summary>
        [DataMember]
        public string PhoneNumber { get; set; }

        /// <summary>
        /// تاییدیه شماره تلفن
        /// </summary>
        [DataMember]
        public Toggle PhoneNumberConfirmed { get; set; }

        /// <summary>
        /// سوال امنیتی رمز عبور
        /// </summary>
        [DataMember]
        public string PasswordQuestion { get; set; }

        /// <summary>
        /// جواب امنیتی رمز عبور
        /// </summary>
        [DataMember]
        public string PasswordAnswer { get; set; }

        /// <summary>
        /// تایید شده
        /// </summary>
        [DataMember]
        public Toggle IsApproved { get; set; }

        /// <summary>
        /// آخرین تاریخ فعالیت
        /// </summary>
        [DataMember]
        public DateTime? LastActivityDate { get; set; }

        /// <summary>
        /// آخرین تاریخ فعالیت به فارسی
        /// </summary>
        [Computed]
        [DataMember]
        public string LastActivityDateFa => LastActivityDate.HasValue ? DateConvertor.DateLocalization(LastActivityDate.Value, false, false, '/') : string.Empty;

        /// <summary>
        /// تعداد سعی نامعتبر برای ورود
        /// </summary>
        [DataMember]
        public int AccessFailedCount { get; set; }

        /// <summary>
        /// شناسه عضویت
        /// </summary>
        [DataMember]
        public int MembershipId { get; set; }

        #endregion

        #region Method

        /// <summary>
        /// سازنده کلاس
        /// </summary>
        public Customer()
        {
            LastLoginDate = null;
            LastLoginIdentification = null;
            OperationDate = null;
            PublicMassage = null;
            IdentificationNumber = null;
            Email = null;
            PhoneNumber = null;
            LastActivityDate = null;
            IdentificationNumber = null;
        }

        #endregion

    }
}
