﻿namespace ORM.Model
{

    #region Assembly

    using Dapper.Contrib.Extensions;
    using System.Runtime.Serialization;

    #endregion

    /// <summary>
    /// جدول سمت
    /// </summary>
    [Table("Position")]
    [DataContract]
    public class Position : BaseModel<Position>
    {

        #region Property

        /// <summary>
        /// نام سمت
        /// </summary>
        [DataMember]
        public string DisplayName { get; set; }

        /// <summary>
        /// شناسه عضویت
        /// </summary>
        [DataMember]
        public int MembershipId { get; set; }

        #endregion

    }
}
