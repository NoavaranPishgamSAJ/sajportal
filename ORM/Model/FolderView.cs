﻿namespace ORM.Model
{

    #region Assembly

    using Dapper.Contrib.Extensions;
    using System.Runtime.Serialization;

    #endregion

    /// <summary>
    /// نمای پوشه
    /// </summary>
    [Table("FolderView")]
    [DataContract]
    public class FolderView : BaseModel<FolderView>
    {

        #region Property

        /// <summary>
        /// نام پوشه
        /// </summary>
        [DataMember]
        public string FolderName { get; set; }

        /// <summary>
        /// مسیر نسبی پوشه
        /// </summary>
        [DataMember]
        public string VirtualPath { get; set; }

        /// <summary>
        /// شناسه نوع پوشه
        /// </summary>
        [DataMember]
        public int FolderTypeId { get; set; }

        /// <summary>
        /// شناسه محتوا
        /// </summary>
        [DataMember]
        public int? ContentId { get; set; }

        /// <summary>
        /// شناسه والد
        /// </summary>
        [DataMember]
        public int ParentId { get; set; }

        /// <summary>
        /// شناسه سایت
        /// </summary>
        [DataMember]
        public int SiteId { get; set; }

        /// <summary>
        /// نام نوع پوشه
        /// </summary>
        [DataMember]
        public string FolderType { get; set; }

        /// <summary>
        /// نام سایت
        /// </summary>
        [DataMember]
        public string Site { get; set; }

        /// <summary>
        /// نام محتوا
        /// </summary>
        [DataMember]
        public string ContentName { get; set; }

        /// <summary>
        /// شناسه مخزن
        /// </summary>
        [DataMember]
        public int? RepositoryId { get; set; }

        #endregion

        #region Method

        /// <summary>
        /// سازنده
        /// </summary>
        public FolderView()
        {
            ContentId = null;
            ContentName = null;
            RepositoryId = null;
        }

        #endregion

    }
}
