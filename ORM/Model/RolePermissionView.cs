﻿namespace ORM.Model
{

    #region Assembly

    using Dapper.Contrib.Extensions;
    using System.Runtime.Serialization;

    #endregion

    /// <summary>
    /// نمای مجوزهای نقش
    /// </summary>
    [Table("RolePermissionView")]
    [DataContract]
    public class RolePermissionView : BaseModel<RolePermissionView>
    {

        #region Property

        /// <summary>
        /// شناسه نقش
        /// </summary>
        [DataMember]
        public int RoleId { get; set; }

        /// <summary>
        /// شناسه مجوز
        /// </summary>
        [DataMember]
        public int PermissionId { get; set; }

        /// <summary>
        /// نام مجوز
        /// </summary>
        [DataMember]
        public string PermissionDisplayName { get; set; }

        /// <summary>
        /// نام مجوز در برنامه
        /// </summary>
        [DataMember]
        public string PermissionDevName { get; set; }

        #endregion

    }
}
