﻿namespace ORM.Model
{

    #region Assembly

    using Dapper.Contrib.Extensions;
    using Enums;
    using System.Runtime.Serialization;

    #endregion

    /// <summary>
    /// جدول مخزن
    /// </summary>
    [Table("Repository")]
    [DataContract]
    public class Repository : BaseModel<Repository>
    {

        #region Property

        /// <summary>
        /// نام مخزن
        /// </summary>
        [DataMember]
        public string RepositoryName { get; set; }

        /// <summary>
        /// نمایش پوشه های مخفی
        /// </summary>
        [DataMember]
        public Toggle ShowHiddenFolder { get; set; }

        /// <summary>
        /// فعال سازی گردش کار
        /// </summary>
        [DataMember]
        public Toggle EnableWorkFlow { get; set; }

        #endregion

    }
}
