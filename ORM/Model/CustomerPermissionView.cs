﻿namespace ORM.Model
{

    #region Assembly

    using Dapper.Contrib.Extensions;
    using Enums;
    using System.Runtime.Serialization;

    #endregion

    /// <summary>
    /// نمای مجوزهای کاربر
    /// </summary>
    [Table("CustomerPermissionView")]
    [DataContract]
    public class CustomerPermissionView : BaseModel<CustomerPermissionView>
    {

        #region Property

        /// <summary>
        /// شناسه کاربر
        /// </summary>
        [DataMember]
        public int CustomerId { get; set; }

        /// <summary>
        /// شناسه مجوز
        /// </summary>
        [DataMember]
        public int PermissionId { get; set; }

        /// <summary>
        /// دارا بودن یا نبودن
        /// </summary>
        [DataMember]
        public Toggle IsHave { get; set; }

        /// <summary>
        /// نام مجوز
        /// </summary>
        [DataMember]
        public string PermissionDisplayName { get; set; }

        /// <summary>
        /// نام مجوز در برنامه
        /// </summary>
        [DataMember]
        public string PermissionDevName { get; set; }

        #endregion

    }
}
