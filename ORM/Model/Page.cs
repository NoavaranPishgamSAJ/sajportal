﻿namespace ORM.Model
{

    #region Assembly

    using Utility;
    using Dapper.Contrib.Extensions;
    using Enums;
    using Newtonsoft.Json;
    using System.Runtime.Serialization;
    using System.ComponentModel;
    using System.Linq;

    #endregion

    /// <summary>
    /// جدول صفحات پرتال
    /// </summary>
    [Table("Page")]
    [DataContract]
    public class Page : BaseModel<Page>
    {

        #region Attribute

        private string[] _routeFieldsList;

        private string[] _routeFieldsValueList;

        private string[] _parametersList;

        private string[] _parametersValueList;

        #endregion

        #region Property

        /// <summary>
        /// نام صفحه
        /// </summary>
        [DataMember]
        public string PageName { get; set; }

        /// <summary>
        /// مسیر نسبی صفحه
        /// </summary>
        [DataMember]
        public string VirtualPath { get; set; }

        /// <summary>
        /// قالب صفحه
        /// </summary>
        [DataMember]
        public string Template { get; set; }

        /// <summary>
        /// عنوان
        /// </summary>
        [DataMember]
        public string Title { get; set; }

        /// <summary>
        /// عنوان منو
        /// </summary>
        [DataMember]
        public string MenuTitle { get; set; }

        /// <summary>
        /// نام پارامترهای مسیریابی در قالب JSON
        /// </summary>
        [DataMember]
        public string RouteFields { get; set; }

        /// <summary>
        /// مقدار پارامترهای مسیریابی در قالب JSON
        /// </summary>
        [DataMember]
        public string RouteFieldsValue { get; set; }

        /// <summary>
        /// نام پارامترهای مسیریابی
        /// </summary>
        [Computed]
        [DataMember]
        public string[] RouteFieldsList
        {
            get { return RouteFields.IsNotEmpty() && RouteFields.Length > 4 ? JsonConvert.DeserializeObject<string[]>(RouteFields) : new string[] { }; }
            set { _routeFieldsList = value; OnPropertyChanged("RouteFieldsList"); }
        }

        /// <summary>
        /// مقدار پارامترهای مسیریابی
        /// </summary>
        [Computed]
        [DataMember]
        public string[] RouteFieldsValueList
        {
            get { return RouteFieldsValue.IsNotEmpty() && RouteFieldsValue.Length > 4 ? JsonConvert.DeserializeObject<string[]>(RouteFieldsValue) : new string[] { }; }
            set { _routeFieldsValueList = value; OnPropertyChanged("RouteFieldsValueList"); }
        }

        /// <summary>
        /// نام پارامترهای صفحه در قالب JSON
        /// </summary>
        [DataMember]
        public string Parameters { get; set; }

        /// <summary>
        /// مقدار پارامترهای صفحه در قالب JSON
        /// </summary>
        [DataMember]
        public string ParametersValue { get; set; }

        /// <summary>
        /// نام پارامترهای صفحه
        /// </summary>
        [Computed]
        [DataMember]
        public string[] ParametersList
        {
            get { return Parameters.IsNotEmpty() && Parameters.Length > 4 ? JsonConvert.DeserializeObject<string[]>(Parameters) : new string[] { }; }
            set { _parametersList = value; OnPropertyChanged("ParametersList"); }
        }

        /// <summary>
        /// مقدار پارامترهای صفحه
        /// </summary>
        [Computed]
        [DataMember]
        public string[] ParametersValueList
        {
            get { return ParametersValue.IsNotEmpty() && ParametersValue.Length > 4 ? JsonConvert.DeserializeObject<string[]>(ParametersValue) : new string[] { }; }
            set { _parametersValueList = value; OnPropertyChanged("ParametersValueList"); }
        }

        /// <summary>
        /// نوع صفحه
        /// </summary>
        [DataMember]
        public PageTypes PageType { get; set; }

        /// <summary>
        /// نمایش در منو
        /// </summary>
        [DataMember]
        public Toggle ShowInMenu { get; set; }

        /// <summary>
        /// نمایش در رده پا
        /// </summary>
        [DataMember]
        public Toggle ShowInBreadcrumb { get; set; }

        /// <summary>
        /// تعیین به عنوان صفحه اصلی
        /// </summary>
        [DataMember]
        public Toggle IsFirstPage { get; set; }

        /// <summary>
        /// نام مجوز جهت دسترسی به صفحه
        /// </summary>
        [DataMember]
        public string RoleName { get; set; }

        /// <summary>
        /// شناسه والد
        /// </summary>
        [DataMember]
        public int? ParentId { get; set; }

        /// <summary>
        /// شناسه سایت
        /// </summary>
        [DataMember]
        public int SiteId { get; set; }

        #endregion

        #region Method

        /// <summary>
        /// سازنده
        /// </summary>
        public Page()
        {
            Title = null;
            MenuTitle = null;
            RouteFields = null;
            RouteFieldsValue = null;
            Parameters = null;
            ParametersValue = null;
            RoleName = null;
        }

        /// <summary>
        /// رویداد تغییر مقدار فیلد
        /// </summary>
        protected void OnPropertyChanged(string propertyName)
        {
            switch (propertyName)
            {
                case "RouteFieldsList":
                    if (_routeFieldsList != null)
                        RouteFields = JsonConvert.SerializeObject(_routeFieldsList.Where(p => p.IsNotEmpty()).ToArray());
                    break;
                case "RouteFieldsValueList":
                    if (_routeFieldsValueList != null)
                        RouteFieldsValue = JsonConvert.SerializeObject(_routeFieldsValueList.Where(p => p.IsNotEmpty()).ToArray());
                    break;
                case "ParametersList":
                    if (_parametersList != null)
                        Parameters = JsonConvert.SerializeObject(_parametersList.Where(p => p.IsNotEmpty()).ToArray());
                    break;
                case "ParametersValueList":
                    if (_parametersValueList != null)
                        ParametersValue = JsonConvert.SerializeObject(_parametersValueList.Where(p => p.IsNotEmpty()).ToArray());
                    break;
            }

            OnPropertyChanged(new PropertyChangedEventArgs(propertyName));
        }

        #endregion

    }
}
