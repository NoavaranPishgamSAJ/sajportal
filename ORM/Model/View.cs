﻿namespace ORM.Model
{

    #region Assembly

    using Dapper.Contrib.Extensions;
    using Newtonsoft.Json;
    using System.ComponentModel;
    using System.Runtime.Serialization;
    using Utility;
    using System.Linq;

    #endregion

    /// <summary>
    /// جدول نماها
    /// </summary>
    [Table("Views")]
    [DataContract]
    public class View : BaseModel<View>
    {

        #region Attribute

        private string[] _routeFieldsList;

        private string[] _routeFieldsValueList;

        private string[] _parametersList;

        private string[] _parametersValueList;

        #endregion

        #region Property

        /// <summary>
        /// نام نما
        /// </summary>
        [DataMember]
        public string ViewName { get; set; }

        /// <summary>
        /// مسیر نسبی
        /// </summary>
        [DataMember]
        public string VirtualPath { get; set; }

        /// <summary>
        /// قالب
        /// </summary>
        [DataMember]
        public string Template { get; set; }

        /// <summary>
        /// نام پارامترهای مسیریابی در قالب JSON
        /// </summary>
        [DataMember]
        public string RouteFields { get; set; }

        /// <summary>
        /// مقدار پارامترهای مسیریابی در قالب JSON
        /// </summary>
        [DataMember]
        public string RouteFieldsValue { get; set; }

        /// <summary>
        /// نام پارامترهای مسیریابی
        /// </summary>
        [Computed]
        [DataMember]
        public string[] RouteFieldsList
        {
            get { return RouteFields.IsNotEmpty() && RouteFields.Length > 4 ? JsonConvert.DeserializeObject<string[]>(RouteFields) : new string[] { }; }
            set { _routeFieldsList = value; OnPropertyChanged("RouteFieldsList"); }
        }

        /// <summary>
        /// مقدار پارامترهای مسیریابی
        /// </summary>
        [Computed]
        [DataMember]
        public string[] RouteFieldsValueList
        {
            get { return RouteFieldsValue.IsNotEmpty() && RouteFieldsValue.Length > 4 ? JsonConvert.DeserializeObject<string[]>(RouteFieldsValue) : new string[] { }; }
            set { _routeFieldsValueList = value; OnPropertyChanged("RouteFieldsValueList"); }
        }

        /// <summary>
        /// نام پارامترهای صفحه در قالب JSON
        /// </summary>
        [DataMember]
        public string Parameters { get; set; }

        /// <summary>
        /// مقدار پارامترهای صفحه در قالب JSON
        /// </summary>
        [DataMember]
        public string ParametersValue { get; set; }

        /// <summary>
        /// نام پارامترهای صفحه
        /// </summary>
        [Computed]
        [DataMember]
        public string[] ParametersList
        {
            get { return Parameters.IsNotEmpty() && Parameters.Length > 4 ? JsonConvert.DeserializeObject<string[]>(Parameters) : new string[] { }; }
            set { _parametersList = value; OnPropertyChanged("ParametersList"); }
        }

        /// <summary>
        /// مقدار پارامترهای صفحه
        /// </summary>
        [Computed]
        [DataMember]
        public string[] ParametersValueList
        {
            get { return ParametersValue.IsNotEmpty() && ParametersValue.Length > 4 ? JsonConvert.DeserializeObject<string[]>(ParametersValue) : new string[] { }; }
            set { _parametersValueList = value; OnPropertyChanged("ParametersValueList"); }
        }

        /// <summary>
        /// شناسه سایت
        /// </summary>
        [DataMember]
        public int SiteId { get; set; }

        #endregion

        #region Method

        /// <summary>
        /// سازنده کلاس
        /// </summary>
        public View()
        {
            Template = null;
            RouteFields = null;
            RouteFieldsValue = null;
            Parameters = null;
        }

        /// <summary>
        /// رویداد تغییر مقدار فیلد
        /// </summary>
        protected void OnPropertyChanged(string propertyName)
        {
            switch (propertyName)
            {
                case "RouteFieldsList":
                    if (_routeFieldsList != null)
                        RouteFields = JsonConvert.SerializeObject(_routeFieldsList.Where(p => p.IsNotEmpty()).ToArray());
                    break;
                case "RouteFieldsValueList":
                    if (_routeFieldsValueList != null)
                        RouteFieldsValue = JsonConvert.SerializeObject(_routeFieldsValueList.Where(p => p.IsNotEmpty()).ToArray());
                    break;
                case "ParametersList":
                    if (_parametersList != null)
                        Parameters = JsonConvert.SerializeObject(_parametersList.Where(p => p.IsNotEmpty()).ToArray());
                    break;
                case "ParametersValueList":
                    if (_parametersValueList != null)
                        ParametersValue = JsonConvert.SerializeObject(_parametersValueList.Where(p => p.IsNotEmpty()).ToArray());
                    break;
            }

            OnPropertyChanged(new PropertyChangedEventArgs(propertyName));
        }

        #endregion

    }
}
