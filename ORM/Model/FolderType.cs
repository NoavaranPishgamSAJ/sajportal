﻿namespace ORM.Model
{

    #region Assembly

    using Dapper.Contrib.Extensions;
    using System.Runtime.Serialization;

    #endregion

    /// <summary>
    /// جدول نوع پوشه
    /// </summary>
    [Table("FolderType")]
    [DataContract]
    public class FolderType : BaseModel<FolderType>
    {

        #region Property

        /// <summary>
        /// نام نوع
        /// </summary>
        [DataMember]
        public string TypeName { get; set; }

        #endregion

    }
}
