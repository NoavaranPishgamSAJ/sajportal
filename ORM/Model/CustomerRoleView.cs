﻿namespace ORM.Model
{

    #region Assembly

    using Dapper.Contrib.Extensions;
    using System.Runtime.Serialization;

    #endregion

    /// <summary>
    /// نمای نقش های کاربر
    /// </summary>
    [Table("CustomerRoleView")]
    [DataContract]
    public class CustomerRoleView : BaseModel<CustomerRoleView>
    {

        #region Property

        /// <summary>
        /// شناسه کاربر
        /// </summary>
        [DataMember]
        public int CustomerId { get; set; }

        /// <summary>
        /// شناسه نقش
        /// </summary>
        [DataMember]
        public int RoleId { get; set; }

        /// <summary>
        /// نام نقش
        /// </summary>
        [DataMember]
        public string RoleDisplayName { get; set; }

        #endregion

    }
}
