﻿namespace ORM.Model
{

    #region Assembly

    using Dapper.Contrib.Extensions;
    using System.Runtime.Serialization;

    #endregion

    /// <summary>
    /// جدول فایل های رسانه
    /// </summary>
    [Table("Files")]
    [DataContract]
    public class File : BaseModel<File>
    {

        #region Property

        /// <summary>
        /// نام فایل
        /// </summary>
        [DataMember]
        public string FileName { get; set; }

        /// <summary>
        /// پسوند فایل
        /// </summary>
        [DataMember]
        public string FileExtension { get; set; }

        /// <summary>
        /// مسیر نسبی فایل
        /// </summary>
        [DataMember]
        public string VirtualPath { get; set; }

        /// <summary>
        /// شناسه پوشه
        /// </summary>
        [DataMember]
        public int FolderId { get; set; }

        /// <summary>
        /// شناسه سایت
        /// </summary>
        [DataMember]
        public int SiteId { get; set; }

        #endregion

    }
}
