﻿namespace ORM.Model
{

    #region Assembly

    using System.Linq;
    using Dapper.Contrib.Extensions;
    using Enums;
    using System.Collections.Generic;
    using System.Runtime.Serialization;
    using Newtonsoft.Json;
    using Utility;
    using DescriptiveModel;
    using System.ComponentModel;

    #endregion

    /// <summary>
    /// جدول محتوای پویا
    /// </summary>
    [Table("Content")]
    [DataContract]
    public class Content : BaseModel<Content>
    {

        #region Attribute

        private string[] _categoriesList;

        private string[] _categoryFieldsList;

        private string[] _categoryFieldsValueList;

        #endregion

        #region Property

        /// <summary>
        /// نام محتوا
        /// </summary>
        [DataMember]
        public string ContentName { get; set; }

        /// <summary>
        /// عنوان محتوا
        /// </summary>
        [DataMember]
        public string Label { get; set; }

        /// <summary>
        /// مخفی بودن
        /// </summary>
        [DataMember]
        public Toggle Hidden { get; set; }

        /// <summary>
        /// قابلیت مرتب سازی داشتن
        /// </summary>
        [DataMember]
        public Toggle Sortable { get; set; }

        /// <summary>
        /// قابلیت صفحه بندی داشتن
        /// </summary>
        [DataMember]
        public Toggle Paging { get; set; }

        /// <summary>
        /// تعداد موارد در هر صفحه
        /// </summary>
        [DataMember]
        public int PageSize { get; set; }

        /// <summary>
        /// لیست فیلدها در قالب JSON
        /// </summary>
        [DataMember]
        public string Fields { get; set; }

        /// <summary>
        /// لیست فیلدها
        /// </summary>
        [Computed]
        [DataMember]
        public List<ContentField> FieldsObject
        {
            get { return Fields.IsNotEmpty() && Fields.Length > 4 ? JsonConvert.DeserializeObject<List<ContentField>>(Fields) : new List<ContentField>(); }
            set { Fields = JsonConvert.SerializeObject(value); }
        }

        /// <summary>
        /// لیست دسته بندی ها در قالب JSON
        /// </summary>
        [DataMember]
        public string Categories { get; set; }

        /// <summary>
        /// لیست دسته بندی ها
        /// </summary>
        [Computed]
        [DataMember]
        public string[] CategoriesList
        {
            get { return Categories.IsNotEmpty() && Categories.Length > 4 ? JsonConvert.DeserializeObject<string[]>(Categories) : new string[] { }; }
            set { _categoriesList = value; OnPropertyChanged("CategoriesList"); }
        }

        /// <summary>
        /// لیست فیلدهای دسته بندی در قالب JSON
        /// </summary>
        [DataMember]
        public string CategoryFields { get; set; }

        /// <summary>
        /// لیست فیلدهای دسته بندی
        /// </summary>
        [Computed]
        [DataMember]
        public string[] CategoryFieldsList
        {
            get { return CategoryFields.IsNotEmpty() && CategoryFields.Length > 4 ? JsonConvert.DeserializeObject<string[]>(CategoryFields) : new string[] { }; }
            set { _categoryFieldsList = value; OnPropertyChanged("CategoryFieldsList"); }
        }

        /// <summary>
        /// لیست فیلدهای دسته بندی در قالب JSON
        /// </summary>
        [DataMember]
        public string CategoryFieldsValue { get; set; }

        /// <summary>
        /// لیست فیلدهای دسته بندی
        /// </summary>
        [Computed]
        [DataMember]
        public string[] CategoryFieldsValueList
        {
            get { return CategoryFieldsValue.IsNotEmpty() && CategoryFieldsValue.Length > 4 ? JsonConvert.DeserializeObject<string[]>(CategoryFieldsValue) : new string[] { }; }
            set { _categoryFieldsValueList = value; OnPropertyChanged("CategoryFieldsValueList"); }
        }

        /// <summary>
        /// نام مجوز جهت دسترسی به محتوا
        /// </summary>
        [DataMember]
        public string RoleName { get; set; }

        /// <summary>
        /// شناسه مخزن
        /// </summary>
        [DataMember]
        public int RepositoryId { get; set; }

        #endregion

        #region Method

        /// <summary>
        /// سازنده کلاس
        /// </summary>
        public Content()
        {
            Label = null;
            Categories = null;
            CategoryFields = null;
            CategoryFieldsValue = null;
            RoleName = null;
        }

        /// <summary>
        /// رویداد تغییر مقدار فیلد
        /// </summary>
        protected void OnPropertyChanged(string propertyName)
        {
            switch (propertyName)
            {
                case "CategoriesList":
                    if (_categoriesList != null)
                        Categories = JsonConvert.SerializeObject(_categoriesList.Where(p => p.IsNotEmpty()).ToArray());
                    break;
                case "CategoryFieldsList":
                    if (_categoryFieldsList != null)
                        CategoryFields = JsonConvert.SerializeObject(_categoryFieldsList.Where(p => p.IsNotEmpty()).ToArray());
                    break;
                case "CategoryFieldsValueList":
                    if (_categoryFieldsValueList != null)
                        CategoryFieldsValue = JsonConvert.SerializeObject(_categoryFieldsValueList.Where(p => p.IsNotEmpty()).ToArray());
                    break;
            }

            OnPropertyChanged(new PropertyChangedEventArgs(propertyName));
        }

        #endregion

    }
}
