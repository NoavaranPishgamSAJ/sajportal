﻿namespace ORM.Model
{

    #region Assembly

    using Dapper.Contrib.Extensions;
    using System.Runtime.Serialization;

    #endregion

    /// <summary>
    /// نمای سمت های کاربر
    /// </summary>
    [Table("CustomerPositionView")]
    [DataContract]
    public class CustomerPositionView : BaseModel<CustomerPositionView>
    {

        #region Property

        /// <summary>
        /// شناسه کاربر
        /// </summary>
        [DataMember]
        public int CustomerId { get; set; }

        /// <summary>
        /// شناسه سمت
        /// </summary>
        [DataMember]
        public int PositionId { get; set; }

        /// <summary>
        /// نام سمت
        /// </summary>
        [DataMember]
        public string PositionDisplayName { get; set; }

        #endregion

    }
}
