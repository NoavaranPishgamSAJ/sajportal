﻿namespace ORM.Condition
{

    #region Assembly

    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Drawing;
    using Dapper;
    using Utility;
    using static System.String;

    #endregion

    #region Enum

    /// <summary>
    /// عملوندهای مورد استفاده در رشته پرسجو دیتابیس
    /// </summary>
    public enum SqlOperand
    {
        /// <summary>
        /// و
        /// </summary>
        And,
        /// <summary>
        /// یا
        /// </summary>
        Or
    }

    /// <summary>
    /// عملگرهای مورد استفاده در رشته پرسجو دیتابیس
    /// </summary>
    public enum SqlOperators
    {
        /// <summary>
        /// بزرگتراز
        /// </summary>
        Greater,
        /// <summary>
        /// کوچکتراز
        /// </summary>
        Less,
        /// <summary>
        /// برابر با
        /// </summary>
        Equal,
        /// <summary>
        /// شروع شده با
        /// </summary>
        StartsLike,
        /// <summary>
        /// خاتمه یافته با
        /// </summary>
        EndsLike,
        /// <summary>
        /// شامل شدن
        /// </summary>
        Like,
        /// <summary>
        /// شامل نشده
        /// </summary>
        NotLike,
        /// <summary>
        /// کوچکتر یا مساوی
        /// </summary>
        LessOrEqual,
        /// <summary>
        /// بزرگتر یا مساوی
        /// </summary>
        GreaterOrEqual,
        /// <summary>
        /// نابرابر با
        /// </summary>
        NotEqual,
        /// <summary>
        /// تهی بودن
        /// </summary>
        Null,
        /// <summary>
        /// پر بودن
        /// </summary>
        NotNull
    }

    /// <summary>
    /// نوع فیلدها برای استفاده در رشته پرسجو دیتابیس
    /// </summary>
    public enum DbType
    {
        /// <summary>
        /// long
        /// </summary>
        BigInt,
        /// <summary>
        /// byte[]
        /// </summary>
        Binary,
        /// <summary>
        /// bool
        /// </summary>
        Bit,
        /// <summary>
        /// char
        /// </summary>
        Char,
        /// <summary>
        /// DateTime
        /// </summary>
        DateTime,
        /// <summary>
        /// decimal
        /// </summary>
        Decimal,
        /// <summary>
        /// float
        /// </summary>
        Float,
        /// <summary>
        /// byte[]
        /// </summary>
        Image,
        /// <summary>
        /// byte[]
        /// </summary>
        Blob,
        /// <summary>
        /// int
        /// </summary>
        Int,
        /// <summary>
        /// long
        /// </summary>
        Money,
        /// <summary>
        /// char
        /// </summary>
        NChar,
        /// <summary>
        /// string
        /// </summary>
        NText,
        /// <summary>
        /// char
        /// </summary>
        NVarChar,
        /// <summary>
        /// double
        /// </summary>
        Real,
        /// <summary>
        /// Guid
        /// </summary>
        UniqueIdentifier,
        /// <summary>
        /// DateTime
        /// </summary>
        SmallDateTime,
        /// <summary>
        /// short
        /// </summary>
        SmallInt,
        /// <summary>
        /// int
        /// </summary>
        SmallMoney,
        /// <summary>
        /// string
        /// </summary>
        Text,
        /// <summary>
        /// string
        /// </summary>
        Timestamp,
        /// <summary>
        /// byte
        /// </summary>
        TinyInt,
        /// <summary>
        /// byte[]
        /// </summary>
        VarBinary,
        /// <summary>
        /// char
        /// </summary>
        VarChar,
        /// <summary>
        /// dynamic
        /// </summary>
        Variant,
        /// <summary>
        /// string
        /// </summary>
        Xml,
        /// <summary>
        /// struct
        /// </summary>
        Udt,
        /// <summary>
        /// struct
        /// </summary>
        Structured,
        /// <summary>
        /// DateTime
        /// </summary>
        Date,
        /// <summary>
        /// TimeSpan
        /// </summary>
        Time,
        /// <summary>
        /// DateTime
        /// </summary>
        DateTime2,
        /// <summary>
        /// DateTimeOffset
        /// </summary>
        DateTimeOffse,
        /// <summary>
        /// string
        /// </summary>
        NumberString
    }

    #endregion

    #region Interface

    /// <summary>
    /// از این رابط برای اعمال فیلتر بر روی رشته پرسجو دیتابیس استفاده می شود
    /// </summary>
    public interface IFilter
    {

        #region Property

        /// <summary>
        /// عملوند
        /// </summary>
        string Operand
        {
            get;
        }

        /// <summary>
        /// رشته پارامتریک شرط
        /// </summary>
        string FilterString
        {
            get;
        }

        /// <summary>
        /// رشته مقداری شرط
        /// </summary>
        string FilterValueString
        {
            get;
        }

        /// <summary>
        /// لیسا پارامترها به همراه مقدار و نوع شان
        /// </summary>
        DynamicParameters FilterValueParameter
        {
            get;
        }

        #endregion

    }

    #endregion

    /// <summary>
    /// از این کلاس برای اعمال فیلتر بر روی رشته پرسجو دیتابیس استفاده می شود
    /// </summary>
    [Serializable]
    public class Filter : IFilter
    {

        #region Attribute

        private readonly string _filterName;

        private readonly string _filterParameterName;

        private readonly SqlOperators _filterOperator;

        private readonly dynamic _filterValue;

        private readonly DbType _filterType;

        private readonly SqlOperand _filterOperand;

        #endregion

        #region Property

        /// <summary>
        /// عملوند مابین شرط ها
        /// </summary>
        public string Operand => _filterOperand == SqlOperand.And ? " AND " : " OR ";

        /// <summary>
        /// رشته پارامتریک شرط
        /// </summary>
        public string FilterString
        {
            get
            {
                var strFilter = "";
                if (!IsNullOrEmpty(_filterName))
                {
                    switch (_filterOperator)
                    {
                        case SqlOperators.Greater:
                            strFilter = _filterName + " > @" + _filterParameterName;
                            break;
                        case SqlOperators.Less:
                            strFilter = _filterName + " < @" + _filterParameterName;
                            break;
                        case SqlOperators.Equal:
                            strFilter = _filterName + " = @" + _filterParameterName;
                            break;
                        case SqlOperators.LessOrEqual:
                            strFilter = _filterName + " <= @" + _filterParameterName;
                            break;
                        case SqlOperators.GreaterOrEqual:
                            strFilter = _filterName + " >= @" + _filterParameterName;
                            break;
                        case SqlOperators.NotEqual:
                            strFilter = _filterName + " <> @" + _filterParameterName;
                            break;
                        case SqlOperators.StartsLike:
                            strFilter = _filterName + " LIKE @" + _filterParameterName;
                            break;
                        case SqlOperators.EndsLike:
                            strFilter = _filterName + " LIKE @" + _filterParameterName;
                            break;
                        case SqlOperators.Like:
                            strFilter = _filterName + " LIKE @" + _filterParameterName;
                            break;
                        case SqlOperators.NotLike:
                            strFilter = _filterName + " NOT LIKE @" + _filterParameterName;
                            break;
                        case SqlOperators.Null:
                            strFilter = _filterName + " IS NULL";
                            break;
                        case SqlOperators.NotNull:
                            strFilter = _filterName + " IS NOT NULL";
                            break;
                            //default:
                            //throw new Exception("This operator type is not supported");
                    }
                }
                return strFilter;
            }
        }

        /// <summary>
        /// رشته مقداری شرط
        /// </summary>
        public string FilterValueString
        {
            get
            {
                var strFilter = "";
                if (!IsNullOrEmpty(_filterName))
                {
                    dynamic cval = GetValue(_filterValue, _filterType, _filterOperator);
                    switch (_filterOperator)
                    {
                        case SqlOperators.Greater:
                            strFilter = _filterName + " > " + cval;
                            break;
                        case SqlOperators.Less:
                            strFilter = _filterName + " < " + cval;
                            break;
                        case SqlOperators.Equal:
                            strFilter = _filterName + " = " + cval;
                            break;
                        case SqlOperators.LessOrEqual:
                            strFilter = _filterName + " <= " + cval;
                            break;
                        case SqlOperators.GreaterOrEqual:
                            strFilter = _filterName + " >= " + cval;
                            break;
                        case SqlOperators.NotEqual:
                            strFilter = _filterName + " <> " + cval;
                            break;
                        case SqlOperators.StartsLike:
                            strFilter = _filterName + " LIKE " + cval;
                            break;
                        case SqlOperators.EndsLike:
                            strFilter = _filterName + " LIKE " + cval;
                            break;
                        case SqlOperators.Like:
                            strFilter = _filterName + " LIKE " + cval;
                            break;
                        case SqlOperators.NotLike:
                            strFilter = _filterName + " NOT LIKE " + cval;
                            break;
                            //default:
                            //throw new Exception("This operator type is not supported");
                    }
                }
                return strFilter;
            }
        }

        /// <summary>
        /// لیست پارامترها به همراه مقدار و نوع شان
        /// </summary>
        public DynamicParameters FilterValueParameter
        {
            get
            {
                var dp = new DynamicParameters();
                if (!IsNullOrEmpty(_filterParameterName))
                {
                    if (_filterOperator == SqlOperators.Like || _filterOperator == SqlOperators.StartsLike || _filterOperator == SqlOperators.EndsLike)
                        dp.Add("@" + _filterParameterName, (_filterOperator.Equal(SqlOperators.Like) || _filterOperator.Equal(SqlOperators.StartsLike) ? "%" : Empty) + CommonUtil.PersianFix(_filterValue.ToString()) + (_filterOperator.Equal(SqlOperators.Like) || _filterOperator.Equal(SqlOperators.EndsLike) ? "%" : Empty));
                    else
                        dp.Add("@" + _filterParameterName, _filterValue);
                }
                return dp;
            }
        }

        #endregion

        #region Method

        /// <summary>
        /// سازنده کلاس شرط
        /// </summary>
        /// <remarks>
        /// به صورت پیش فرض مقادیر فیلدهای این کلاس به صورت زیر است:
        /// filterName = Empty;
        /// filterParameterName = Empty;
        /// filterOperator = SqlOperators.Equal;
        /// filterValue = null;
        /// filterType = DbType.Int;
        /// filterOperand = SqlOperand.And;
        /// </remarks>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// Filter filter = new Filter();
        /// </code>
        /// </example>
        public Filter()
        {
            _filterName = Empty;
            _filterParameterName = Empty;
            _filterOperator = SqlOperators.Equal;
            _filterValue = null;
            _filterType = DbType.Int;
            _filterOperand = SqlOperand.And;
        }

        /// <summary>
        /// سازنده کلاس شرط
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// Filter filter = new Filter("ID", SqlOperators.Equal, 123, [SqlOperand.And], [DbType.Int], ["Id"]);
        /// </code>
        /// </example>
        /// <param name="filterName">نام فیلد: string</param>
        /// <param name="filterOperator">عملگر: SqlOperators</param>
        /// <param name="filterValue">مقدار فیلد: dynamic</param>
        /// <param name="filterOperand">عملوند: SqlOperand</param>
        /// <param name="filterType">نوع فیلد: DbType</param>
        /// <param name="filterParameterName">نام پارامتر: string</param>
        public Filter(string filterName, SqlOperators filterOperator, dynamic filterValue, SqlOperand filterOperand = SqlOperand.And, DbType filterType = DbType.Int, string filterParameterName = "")
        {
            _filterName = filterName;
            _filterParameterName = filterParameterName.Equals("") ? filterName : filterParameterName;
            _filterOperator = filterOperator;
            _filterValue = filterValue;
            _filterType = filterType;
            _filterOperand = filterOperand;
        }

        /// <summary>
        /// این متد مقدار پارامتر را به صورتی که قابل استفاده در رشته پرسجو دیتابیس باشد برمی گرداند
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// string strValue = GetValue("false", DbType.Bit, SqlOperators.Equal);
        /// </code>
        /// </example>
        /// <param name="value">مقدار پارامتر: dynamic</param>
        /// <param name="ctype">نوغ پارامتر: DbType</param>
        /// <param name="operators">عملگر: SqlOperators</param>
        /// <returns>string</returns>
        private static string GetValue(dynamic value, DbType ctype, SqlOperators operators)
        {
            try
            {
                if (value != null && !value.ToString().Equals(Empty))
                {
                    switch (ctype)
                    {
                        case DbType.Bit:
                            if (value.ToString().Equals("0"))
                                return "0";
                            if (value.ToString().Equals("1"))
                                return "1";
                            if (value.ToString().ToLower().Equals("false"))
                                return "0";
                            return value.ToString().ToLower().Equals("true") ? "1" : "0";
                        case DbType.Binary:
                        case DbType.VarBinary:
                            return value.ToString();
                        case DbType.TinyInt:
                        case DbType.SmallInt:
                            return value.ToString();
                        case DbType.Int:
                            return value.ToString();
                        case DbType.BigInt:
                        case DbType.Money:
                        case DbType.SmallMoney:
                            return value.ToString();
                        case DbType.Float:
                            return value.ToString();
                        case DbType.Real:
                        case DbType.Decimal:
                            return value.ToString();
                        case DbType.Char:
                        case DbType.NChar:
                        case DbType.VarChar:
                        case DbType.NVarChar:
                        case DbType.Text:
                        case DbType.NText:
                        case DbType.UniqueIdentifier:
                        case DbType.Xml:
                            return "N'" + (operators.Equal(SqlOperators.Like) || operators.Equal(SqlOperators.StartsLike) ? "%" : Empty) + CommonUtil.PersianFix(value.ToString()) + (operators.Equal(SqlOperators.Like) || operators.Equal(SqlOperators.EndsLike) ? "%" : Empty) + "'";
                        case DbType.Image:
                            return Images.ImageToArray(value as Image).ToString();
                        case DbType.Variant:
                        case DbType.Udt:
                        case DbType.Structured:
                            return value.ToString();
                        case DbType.Date:
                        case DbType.DateTime:
                        case DbType.DateTime2:
                        case DbType.SmallDateTime:
                            return "N'" + ((DateTime)value).ToString("yyyy-MM-dd") + "'";
                        case DbType.Time:
                            return "N'" + ((DateTime)value).ToString("yyyy-MM-dd") + "'";
                        case DbType.Timestamp:
                            return "N'" + ((DateTime)value).ToString("yyyy-MM-dd") + "'";
                        case DbType.Blob:
                            return value.ToString();
                        case DbType.DateTimeOffse:
                            break;
                        case DbType.NumberString:
                            break;
                        default:
                            return value.ToString();
                    }
                }
                else if (value == null)
                    return "NULL";
                else if (value.ToString().Equals(Empty))
                {
                    if (ctype == DbType.Char || ctype == DbType.NChar || ctype == DbType.VarChar ||
                        ctype == DbType.NVarChar || ctype == DbType.Text || ctype == DbType.NText ||
                        ctype == DbType.UniqueIdentifier || ctype == DbType.Xml)
                    {
                        return "N'" + CommonUtil.PersianFix(value.ToString()) + "'";
                    }
                    return value.ToString();
                }
            }
            catch (Exception ex) { Logger.Log(ex); }
            return "NULL";
        }

        #endregion

    }

    /// <summary>
    /// از این کلاس برای اعمال فیلتر بر روی رشته پرسجو دیتابیس استفاده می شود
    /// </summary>
    /// <remarks>
    /// از این فیلتر فقط در زمانی استفاده می شود که بخواهیم بررسی کنیم که آیا از مابین لیستی از مقادیر حداقل یک از آن ها در فیلد مورد نظر موجود باشد یا نه
    /// </remarks>
    [Serializable]
    public class InFilter : IFilter
    {

        #region Attribute

        private readonly string _filterName;

        private readonly StringCollection _filterValues;

        #endregion

        #region Property

        /// <summary>
        /// عملوند مابین شرط ها
        /// </summary>
        public string Operand => Empty;

        /// <summary>
        /// رشته پارامتریک شرط
        /// </summary>
        public string FilterString
        {
            get
            {
                var strFilter = _filterName + " IN(@" + _filterName + ")";
                return strFilter;
            }
        }

        /// <summary>
        /// رشته مقداری شرط
        /// </summary>
        public string FilterValueString
        {
            get
            {
                var strFilter = "";
                if (_filterValues.Count > 0)
                {
                    for (var i = 0; i < _filterValues.Count - 1; i++)
                    {
                        strFilter += _filterValues[i] + ",";
                    }
                    strFilter += _filterValues[_filterValues.Count - 1];
                    strFilter = _filterName + " IN(" + strFilter + ")";
                }
                return strFilter;
            }
        }

        /// <summary>
        /// لیست پارامترها به همراه مقدار و نوع شان
        /// </summary>
        public DynamicParameters FilterValueParameter
        {
            get
            {
                var dp = new DynamicParameters();
                var strValue = "";
                if (_filterValues.Count > 0)
                {
                    for (var i = 0; i < _filterValues.Count - 1; i++)
                    {
                        strValue += _filterValues[i] + ",";
                    }
                    strValue += _filterValues[_filterValues.Count - 1];
                }
                dp.Add("@" + _filterName, strValue);
                return dp;
            }
        }

        #endregion

        #region Method

        /// <summary>
        /// سازنده کلاس شرط
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// StringCollection myCol = new StringCollection();
        /// String[] myArr = new String[] { "1", "25", "65", "70" };
        /// myCol.AddRange(myArr);
        /// InFilter filter = new InFilter("ID", myCol);
        /// </code>
        /// </example>
        /// <param name="filterName">نام فیلد: string</param>
        /// <param name="filterValues">مقادیر مورد نظر: StringCollection</param>
        public InFilter(string filterName, StringCollection filterValues)
        {
            _filterName = filterName;
            _filterValues = filterValues;
        }

        #endregion

    }

    /// <summary>
    /// از این کلاس برای اعمال فیلتر بر روی رشته پرسجو دیتابیس استفاده می شود
    /// </summary>
    /// <remarks>
    /// از این کلاس برای ترکیب کردن چند فیلتر در یک پرانتز با عملوند و استفاده می شود
    /// </remarks>
    [Serializable]
    public class AndFilter : IFilter
    {

        #region Attribute

        private readonly FilterExpressionList _filterList = new FilterExpressionList();

        #endregion

        #region Property

        /// <summary>
        /// عملوند مابین شرط ها
        /// </summary>
        public string Operand => Empty;

        /// <summary>
        /// رشته پارامتریک شرط
        /// </summary>
        public string FilterString => _filterList.AndFilterString;

        /// <summary>
        /// رشته مقداری شرط
        /// </summary>
        public string FilterValueString => _filterList.AndFilterValueString;

        /// <summary>
        /// لیست پارامترها به همراه مقدار و نوع شان
        /// </summary>
        public DynamicParameters FilterValueParameter => _filterList.FilterValueParameter;

        #endregion

        #region Method

        /// <summary>
        /// سازنده کلاس شرط
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// AndFilter filter = new AndFilter(new Filter("ID", SqlOperators.Equal, 123), new Filter("ID", SqlOperators.NotNull, null));
        /// </code>
        /// </example>
        /// <param name="filterLeft">فیلتر سمت چپ: IFilter</param>
        /// <param name="filterRight">فیلتر سمت راست: IFilter</param>
        public AndFilter(IFilter filterLeft, IFilter filterRight)
        {
            _filterList.Add(filterLeft);
            _filterList.Add(filterRight);
        }

        /// <summary>
        /// سازنده کلاس شرط
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// AndFilter filter = new AndFilter(new FilterExpressionList(new Filter[] { new Filter("ID", SqlOperators.Equal, 123), new Filter("ID", SqlOperators.NotNull, null) }));
        /// </code>
        /// </example>
        /// <param name="filterList">لیست فیلترها: FilterExpressionList</param>
        public AndFilter(FilterExpressionList filterList)
        {
            _filterList = filterList;
        }

        /// <summary>
        /// این متد بررسی می کند که آیا شرط مورد نظر در رشته پرسجو موجود است یا خیر
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// bool isExist = ContainFilter("ID");
        /// </code>
        /// </example>
        /// <param name="name">نام فیلد: string</param>
        /// <returns>bool</returns>
        public bool ContainFilter(string name)
        {
            return FilterString.Contains(name);
        }

        #endregion

    }

    /// <summary>
    /// از این کلاس برای اعمال فیلتر بر روی رشته پرسجو دیتابیس استفاده می شود
    /// </summary>
    /// <remarks>
    /// از این کلاس برای ترکیب کردن چند فیلتر در یک پرانتز با عملوند یا استفاده می شود
    /// </remarks>
    [Serializable]
    public class OrFilter : IFilter
    {

        #region Attribute

        private readonly FilterExpressionList _filterList = new FilterExpressionList();

        #endregion

        #region Property

        /// <summary>
        /// عملوند مابین شرط ها
        /// </summary>
        public string Operand => Empty;

        /// <summary>
        /// رشته پارامتریک شرط
        /// </summary>
        public string FilterString => _filterList.OrFilterString;

        /// <summary>
        /// رشته مقداری شرط
        /// </summary>
        public string FilterValueString => _filterList.OrFilterValueString;

        /// <summary>
        /// لیست پارامترها به همراه مقدار و نوع شان
        /// </summary>
        public DynamicParameters FilterValueParameter => _filterList.FilterValueParameter;

        #endregion

        #region Method

        /// <summary>
        /// سازنده کلاس شرط
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// OrFilter filter = new OrFilter(new Filter("ID", SqlOperators.Equal, 123), new Filter("ID", SqlOperators.NotNull, null));
        /// </code>
        /// </example>
        /// <param name="filterLeft">فیلتر سمت چپ: IFilter</param>
        /// <param name="filterRight">فیلتر سمت راست: IFilter</param>
        public OrFilter(IFilter filterLeft, IFilter filterRight)
        {
            _filterList.Add(filterLeft);
            _filterList.Add(filterRight);
        }

        /// <summary>
        /// سازنده کلاس شرط
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// OrFilter filter = new OrFilter(new FilterExpressionList(new Filter[] { new Filter("ID", SqlOperators.Equal, 123), new Filter("ID", SqlOperators.NotNull, null) }));
        /// </code>
        /// </example>
        /// <param name="filterList">لیست فیلترها: FilterExpressionList</param>
        public OrFilter(FilterExpressionList filterList)
        {
            _filterList = filterList;
        }

        /// <summary>
        /// این متد بررسی می کند که آیا شرط مورد نظر در رشته پرسجو موجود است یا خیر
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// bool isExist = ContainFilter("ID");
        /// </code>
        /// </example>
        /// <param name="name">نام فیلد: string</param>
        /// <returns>bool</returns>
        public bool ContainFilter(string name)
        {
            return FilterString.Contains(name);
        }

        #endregion

    }

    /// <summary>
    /// از این کلاس برای اعمال مجموعه ای از فیلترها استفاده می شود
    /// </summary>
    /// <remarks>
    /// فیلترهای این کلاس می تواند شامل فیلتر های AndFilter, OrFilter, InFilter, Filter و نیز خود FilterExpressionList باشد
    /// </remarks>
    [Serializable]
    public class FilterExpressionList
    {

        #region Attribute

        private readonly List<IFilter> _filterItems;

        #endregion

        #region Property

        /// <summary>
        /// دسترسی به فیلترهای موجود در لیست بر اساس اندیس عددی آن ها
        /// </summary>
        public IFilter this[int index]
        {
            get
            {
                return _filterItems[index];
            }
            set
            {
                _filterItems[index] = value;
            }
        }

        /// <summary>
        /// تعداد فیلترهای موجود در لیست
        /// </summary>
        public int Count => _filterItems.Count;

        /// <summary>
        /// رشته پارامتریک شرط
        /// </summary>
        public string FilterString
        {
            get
            {
                var strFilter = "";
                if (_filterItems.Count > 0)
                {
                    for (var i = 0; i < _filterItems.Count - 1; i++)
                    {
                        if (!IsNullOrEmpty(_filterItems[i].FilterString))
                        {
                            strFilter += _filterItems[i].FilterString + " AND ";// _filterItems[i].Operand;
                        }
                    }
                    strFilter += _filterItems[_filterItems.Count - 1].FilterString;
                    if (!IsNullOrEmpty(strFilter))
                    {
                        strFilter = "(" + strFilter + ")";
                    }
                }
                return strFilter;
            }
        }

        /// <summary>
        /// رشته مقداری شرط
        /// </summary>
        public string FilterValueString
        {
            get
            {
                var strFilter = "";
                if (_filterItems.Count > 0)
                {
                    for (var i = 0; i < _filterItems.Count - 1; i++)
                    {
                        if (!IsNullOrEmpty(_filterItems[i].FilterValueString))
                        {
                            strFilter += _filterItems[i].FilterValueString + " AND ";// _filterItems[i].Operand;
                        }
                    }
                    strFilter += _filterItems[_filterItems.Count - 1].FilterValueString;
                    if (!IsNullOrEmpty(strFilter))
                    {
                        strFilter = "(" + strFilter + ")";
                    }
                }
                return strFilter;
            }
        }

        /// <summary>
        /// رشته پارامتریک شرط با عملوند و
        /// </summary>
        public string AndFilterString
        {
            get
            {
                var strFilter = "";
                if (_filterItems.Count > 0)
                {
                    for (var i = 0; i < _filterItems.Count - 1; i++)
                    {
                        if (!IsNullOrEmpty(_filterItems[i].FilterString))
                        {
                            strFilter += _filterItems[i].FilterString + " AND ";
                        }
                    }
                    strFilter += _filterItems[_filterItems.Count - 1].FilterString;
                    if (!IsNullOrEmpty(strFilter))
                    {
                        strFilter = "(" + strFilter + ")";
                    }
                }
                return strFilter;
            }
        }

        /// <summary>
        /// رشته مقداری شرط با عملوند و
        /// </summary>
        public string AndFilterValueString
        {
            get
            {
                var strFilter = "";
                if (_filterItems.Count > 0)
                {
                    for (var i = 0; i < _filterItems.Count - 1; i++)
                    {
                        if (!IsNullOrEmpty(_filterItems[i].FilterValueString))
                        {
                            strFilter += _filterItems[i].FilterValueString + " AND ";
                        }
                    }
                    strFilter += _filterItems[_filterItems.Count - 1].FilterValueString;
                    if (!IsNullOrEmpty(strFilter))
                    {
                        strFilter = "(" + strFilter + ")";
                    }
                }
                return strFilter;
            }
        }

        /// <summary>
        /// رشته پارامتریک شرط با عملوند یا
        /// </summary>
        public string OrFilterString
        {
            get
            {
                var strFilter = "";
                if (_filterItems.Count > 0)
                {
                    for (var i = 0; i < _filterItems.Count - 1; i++)
                    {
                        if (!IsNullOrEmpty(_filterItems[i].FilterString))
                        {
                            strFilter += _filterItems[i].FilterString + " OR ";
                        }
                    }
                    strFilter += _filterItems[_filterItems.Count - 1].FilterString;
                    if (!IsNullOrEmpty(strFilter))
                    {
                        strFilter = "(" + strFilter + ")";
                    }
                }
                return strFilter;
            }
        }

        /// <summary>
        /// رشته مقداری شرط با عملوند یا
        /// </summary>
        public string OrFilterValueString
        {
            get
            {
                var strFilter = "";
                if (_filterItems.Count > 0)
                {
                    for (var i = 0; i < _filterItems.Count - 1; i++)
                    {
                        if (!IsNullOrEmpty(_filterItems[i].FilterValueString))
                        {
                            strFilter += _filterItems[i].FilterValueString + " OR ";
                        }
                    }
                    strFilter += _filterItems[_filterItems.Count - 1].FilterValueString;
                    if (!IsNullOrEmpty(strFilter))
                    {
                        strFilter = "(" + strFilter + ")";
                    }
                }
                return strFilter;
            }
        }

        /// <summary>
        /// لیست پارامترها به همراه مقدار و نوع شان
        /// </summary>
        public DynamicParameters FilterValueParameter
        {
            get
            {
                var dp = new DynamicParameters();
                if (_filterItems.Count > 0)
                {
                    foreach (var filter in _filterItems)
                    {
                        dp.AddDynamicParams(filter.FilterValueParameter);
                    }
                }
                return dp;
            }
        }

        #endregion

        #region Method

        /// <summary>
        /// سازنده کلاس شرط
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// FilterExpressionList filters = new FilterExpressionList();
        /// </code>
        /// </example>
        public FilterExpressionList()
        {
            _filterItems = new List<IFilter>();
        }

        /// <summary>
        /// سازنده کلاس شرط
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// FilterExpressionList filters = new FilterExpressionList(new Filter[] { new Filter("ID", SqlOperators.Equal, 123), new Filter("ID", SqlOperators.NotNull, null) });
        /// </code>
        /// </example>
        /// <param name="filterItems">لیست فیلترها: IEnumerable&lt;Filter&gt;</param>
        public FilterExpressionList(IEnumerable<Filter> filterItems)
        {
            _filterItems = new List<IFilter>();
            _filterItems.AddRange(filterItems);
        }

        /// <summary>
        /// این متد برای اضافه کردن یک فیلتر به لیست فیلترها می باشد
        /// </summary>
        /// <remarks>
        /// نکته: می توان به جای یک فیلتر یک FilterExpressionList دیگر را به لیست فیلترها اضافه کرد
        /// </remarks>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// FilterExpressionList filters = new FilterExpressionList(new Filter[] { new Filter("ID", SqlOperators.Equal, 123), new Filter("ID", SqlOperators.NotNull, null) });
        /// filters.Add(new Filter("Name", SqlOperators.Equal, "John"));
        /// </code>
        /// </example>
        /// <param name="filterExpresion">فیلتر مورد نظر: IFilter</param>
        public void Add(IFilter filterExpresion)
        {
            _filterItems.Add(filterExpresion);
        }

        /// <summary>
        /// این متد بررسی می کند که آیا شرط مورد نظر در رشته پرسجو موجود است یا خیر
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// bool isExist = ContainFilter("ID");
        /// </code>
        /// </example>
        /// <param name="name">نام فیلد: string</param>
        /// <returns>bool</returns>
        public bool ContainFilter(string name)
        {
            return AndFilterString.Contains(name);
        }

        /// <summary>
        /// خالی کردن لیست فیلترها
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// FilterExpressionList filters = new FilterExpressionList(new Filter[] { new Filter("ID", SqlOperators.Equal, 123), new Filter("ID", SqlOperators.NotNull, null) });
        /// filters.Clear();
        /// </code>
        /// </example>
        public void Clear()
        {
            _filterItems.Clear();
        }

        #endregion

    }
}
