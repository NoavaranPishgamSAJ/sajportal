﻿namespace ORM.DescriptiveModel
{

    #region Assembly

    using Enums;
    using System.Collections.Generic;

    #endregion

    /// <summary>
    /// از این کلاس در ساخت موجودیت های پویا در زمان اجرا استفاده می شود
    /// </summary>
    /// <remarks>
    /// این کلاس در ساخت فیلدهای موجودیت استفاده می شود
    /// </remarks>
    public class ContentField
    {

        #region Property

        /// <summary>
        /// نام فیلد
        /// </summary>
        public string FieldName { get; set; }

        /// <summary>
        /// عنوان فیلد
        /// </summary>
        public string Label { get; set; }

        /// <summary>
        /// مقدار پیش فرض
        /// </summary>
        public string DefaultValue { get; set; }

        /// <summary>
        /// نمایش در لیست گرید
        /// </summary>
        public Toggle ShowInList { get; set; }

        /// <summary>
        /// پاورقی
        /// </summary>
        public string Tooltip { get; set; }

        /// <summary>
        /// طول فیلد
        /// </summary>
        public int? Length { get; set; }

        /// <summary>
        /// تهی پذیر بودن
        /// </summary>
        public Toggle AllowNull { get; set; }

        /// <summary>
        /// تنظیمات اضافی
        /// </summary>
        public string CustomSetting { get; set; }

        /// <summary>
        /// نوع کنترل HTML در فرم
        /// </summary>
        public string ControlType { get; set; }

        /// <summary>
        /// نوع داده فیلد
        /// </summary>
        public string DataType { get; set; }

        /// <summary>
        /// لیست اعتبارسنجی های فیلد
        /// </summary>
        public List<ContentFieldValidation> FieldValidations { get; set; }

        #endregion

        #region Method

        /// <summary>
        /// سازنده کلاس
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// ContentField contentField = new ContentField();
        /// </code>
        /// </example>
        public ContentField()
        {
            Label = string.Empty;
            DefaultValue = string.Empty;
            Tooltip = string.Empty;
            Length = 0;
            CustomSetting = string.Empty;
            FieldValidations = new List<ContentFieldValidation>();
        }

        /// <summary>
        /// این متد خصوصیات کلاس را در قالب json بر می گرداند
        /// </summary>
        /// <returns>string</returns>
        public override string ToString()
        {
            var result = string.Empty;
            result += $@"{{""FieldName"":""{FieldName}"",""Label"":""{Label}"",""DefaultValue"":""{DefaultValue}"",";
            result += $@"""ShowInList"":{ShowInList.GetNumber()},""Tooltip"":""{Tooltip}"",""Length"":{Length},""AllowNull"":{AllowNull.GetNumber()},";
            result += $@"""CustomSetting"":""{CustomSetting}"",""ControlType"":""{ControlType}"",""DataType"":""{DataType}"",";
            if (FieldValidations?.Count > 0)
            {
                result += @"""FieldValidations"":[";
                result += $@"{{""FirstParam"":""{FieldValidations[0].FirstParam}"",""SecondParam"":""{FieldValidations[0].SecondParam}"",""ThirdParam"":""{FieldValidations[0].ThirdParam}"",""ValidationName"":""{FieldValidations[0].ValidationName}""}}";
                for (int i = 1; i < FieldValidations.Count; i++)
                {
                    result += $@",{{""FirstParam"":""{FieldValidations[i].FirstParam}"",""SecondParam"":""{FieldValidations[i].SecondParam}"",""ThirdParam"":""{FieldValidations[i].ThirdParam}"",""ValidationName"":""{FieldValidations[i].ValidationName}""}}";
                }
                result += @"]";
            }
            result += @"}";
            return result;
        }

        #endregion

    }
}
