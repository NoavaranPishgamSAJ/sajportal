﻿namespace ORM.DescriptiveModel
{

    #region Assembly

    #endregion

    /// <summary>
    /// از این کلاس در ساخت موجودیت های پویا در زمان اجرا استفاده می شود
    /// </summary>
    /// <remarks>
    /// این کلاس در ساخت اعتبارسنجی های فیلدهای موجودیت استفاده می شود
    /// </remarks>
    public class ContentFieldValidation
    {

        #region Property

        /// <summary>
        /// نام اعتبارسنج
        /// </summary>
        public string ValidationName { get; set; }

        /// <summary>
        /// پارامتر اول
        /// </summary>
        public string FirstParam { get; set; }

        /// <summary>
        /// پارامتر دوم
        /// </summary>
        public string SecondParam { get; set; }

        /// <summary>
        /// پارامتر سوم
        /// </summary>
        public string ThirdParam { get; set; }

        #endregion

        #region Method

        /// <summary>
        /// سازنده کلاس
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// ContentFieldValidation validation = new ContentFieldValidation();
        /// </code>
        /// </example>
        public ContentFieldValidation()
        {
            FirstParam = string.Empty;
            SecondParam = string.Empty;
            ThirdParam = string.Empty;
        }

        #endregion

    }
}
