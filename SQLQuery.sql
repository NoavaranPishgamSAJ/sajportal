USE [SAJPortal]
GO
/****** Object:  Table [dbo].[Content]    Script Date: 11/27/2016 01:43:57 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Content](
	[ItemId] [int] IDENTITY(1,1) NOT NULL,
	[ContentName] [nvarchar](100) NOT NULL,
	[Label] [nvarchar](100) NULL,
	[Hidden] [bit] NOT NULL,
	[Sortable] [bit] NOT NULL,
	[Paging] [bit] NOT NULL,
	[PageSize] [int] NOT NULL,
	[Fields] [ntext] NOT NULL,
	[Categories] [ntext] NULL,
	[CategoryFields] [ntext] NULL,
	[CategoryFieldsValue] [ntext] NULL,
	[RoleName] [nvarchar](50) NULL,
	[RepositoryId] [int] NOT NULL,
	[AddedDate] [date] NOT NULL,
	[ModifiedDate] [date] NULL,
 CONSTRAINT [PK_Content] PRIMARY KEY CLUSTERED 
(
	[ItemId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Customer]    Script Date: 11/27/2016 01:43:57 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Customer](
	[ItemId] [int] IDENTITY(1,1) NOT NULL,
	[Username] [nvarchar](150) NOT NULL,
	[Password] [nvarchar](50) NOT NULL,
	[FirstName] [nvarchar](150) NULL,
	[LastName] [nvarchar](150) NULL,
	[FatherName] [nvarchar](150) NULL,
	[NationalCode] [nvarchar](20) NULL,
	[RetrievalText] [nvarchar](150) NULL,
	[IsAdmin] [bit] NOT NULL,
	[BoycottUser] [bit] NOT NULL,
	[LastLoginDate] [date] NULL,
	[LastLoginIdentification] [nvarchar](150) NULL,
	[UseOperationDate] [bit] NOT NULL,
	[OperationDate] [date] NULL,
	[PublicMassage] [ntext] NULL,
	[IdentificationNumber] [nvarchar](50) NULL,
	[Email] [nvarchar](150) NULL,
	[EmailConfirmed] [bit] NOT NULL,
	[PhoneNumber] [nvarchar](150) NULL,
	[PhoneNumberConfirmed] [bit] NOT NULL,
	[PasswordQuestion] [nvarchar](250) NOT NULL,
	[PasswordAnswer] [nvarchar](250) NOT NULL,
	[IsApproved] [bit] NOT NULL,
	[LastActivityDate] [datetime] NULL,
	[AccessFailedCount] [int] NOT NULL,
	[MembershipId] [int] NOT NULL,
	[AddedDate] [date] NOT NULL,
	[ModifiedDate] [date] NULL,
 CONSTRAINT [PK_Customer] PRIMARY KEY CLUSTERED 
(
	[ItemId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[CustomerPermission]    Script Date: 11/27/2016 01:43:57 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CustomerPermission](
	[ItemId] [int] IDENTITY(1,1) NOT NULL,
	[CustomerId] [int] NOT NULL,
	[PermissionId] [int] NOT NULL,
	[IsHave] [bit] NOT NULL,
	[AddedDate] [date] NOT NULL,
	[ModifiedDate] [date] NULL,
 CONSTRAINT [PK_CustomerPermission] PRIMARY KEY CLUSTERED 
(
	[ItemId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[CustomerPosition]    Script Date: 11/27/2016 01:43:57 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CustomerPosition](
	[ItemId] [int] IDENTITY(1,1) NOT NULL,
	[CustomerId] [int] NOT NULL,
	[PositionId] [int] NOT NULL,
	[AddedDate] [date] NOT NULL,
	[ModifiedDate] [date] NULL,
 CONSTRAINT [PK_CustomerPosition] PRIMARY KEY CLUSTERED 
(
	[ItemId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[CustomerRole]    Script Date: 11/27/2016 01:43:57 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CustomerRole](
	[ItemId] [int] IDENTITY(1,1) NOT NULL,
	[CustomerId] [int] NOT NULL,
	[RoleId] [int] NOT NULL,
	[AddedDate] [date] NOT NULL,
	[ModifiedDate] [date] NULL,
 CONSTRAINT [PK_CustomerRole] PRIMARY KEY CLUSTERED 
(
	[ItemId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Files]    Script Date: 11/27/2016 01:43:57 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Files](
	[ItemId] [int] IDENTITY(1,1) NOT NULL,
	[FileName] [nvarchar](50) NOT NULL,
	[FileExtension] [nvarchar](5) NOT NULL,
	[VirtualPath] [nvarchar](max) NOT NULL,
	[FolderId] [int] NOT NULL,
	[SiteId] [int] NOT NULL,
	[AddedDate] [date] NOT NULL,
	[ModifiedDate] [date] NULL,
 CONSTRAINT [PK_File] PRIMARY KEY CLUSTERED 
(
	[ItemId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Folder]    Script Date: 11/27/2016 01:43:57 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Folder](
	[ItemId] [int] IDENTITY(1,1) NOT NULL,
	[FolderName] [nvarchar](50) NOT NULL,
	[VirtualPath] [nvarchar](max) NOT NULL,
	[FolderTypeId] [int] NOT NULL,
	[ContentId] [int] NULL,
	[SiteId] [int] NOT NULL,
	[AddedDate] [date] NOT NULL,
	[ModifiedDate] [date] NULL,
 CONSTRAINT [PK_Folder] PRIMARY KEY CLUSTERED 
(
	[ItemId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[FolderType]    Script Date: 11/27/2016 01:43:57 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FolderType](
	[ItemId] [int] IDENTITY(1,1) NOT NULL,
	[TypeName] [nvarchar](50) NOT NULL,
	[AddedDate] [date] NOT NULL,
	[ModifiedDate] [date] NULL,
 CONSTRAINT [PK_FolderType] PRIMARY KEY CLUSTERED 
(
	[ItemId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Layout]    Script Date: 11/27/2016 01:43:57 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Layout](
	[ItemId] [int] IDENTITY(1,1) NOT NULL,
	[LayoutName] [nvarchar](50) NOT NULL,
	[VirtualPath] [nvarchar](max) NOT NULL,
	[SiteId] [int] NOT NULL,
	[AddedDate] [date] NOT NULL,
	[ModifiedDate] [date] NULL,
 CONSTRAINT [PK_Layout] PRIMARY KEY CLUSTERED 
(
	[ItemId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Membership]    Script Date: 11/27/2016 01:43:57 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Membership](
	[ItemId] [int] IDENTITY(1,1) NOT NULL,
	[MembershipName] [nvarchar](50) NOT NULL,
	[AddedDate] [date] NOT NULL,
	[ModifiedDate] [date] NULL,
 CONSTRAINT [PK_Membership] PRIMARY KEY CLUSTERED 
(
	[ItemId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Page]    Script Date: 11/27/2016 01:43:57 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Page](
	[ItemId] [int] IDENTITY(1,1) NOT NULL,
	[PageName] [nvarchar](50) NOT NULL,
	[VirtualPath] [nvarchar](max) NOT NULL,
	[Template] [nvarchar](max) NOT NULL,
	[Title] [nvarchar](max) NULL,
	[MenuTitle] [nvarchar](50) NULL,
	[RouteFields] [ntext] NULL,
	[RouteFieldsValue] [ntext] NULL,
	[Parameters] [ntext] NULL,
	[ParametersValue] [ntext] NULL,
	[PageType] [tinyint] NOT NULL,
	[ShowInMenu] [bit] NOT NULL,
	[ShowInBreadcrumb] [bit] NOT NULL,
	[IsFirstPage] [bit] NOT NULL,
	[RoleName] [nvarchar](50) NULL,
	[ParentId] [int] NULL,
	[SiteId] [int] NOT NULL,
	[AddedDate] [date] NOT NULL,
	[ModifiedDate] [date] NULL,
 CONSTRAINT [PK_Page] PRIMARY KEY CLUSTERED 
(
	[ItemId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Permission]    Script Date: 11/27/2016 01:43:57 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Permission](
	[ItemId] [int] IDENTITY(1,1) NOT NULL,
	[DisplayName] [nvarchar](150) NOT NULL,
	[DevName] [nvarchar](150) NOT NULL,
	[MembershipId] [int] NOT NULL,
	[AddedDate] [date] NOT NULL,
	[ModifiedDate] [date] NULL,
 CONSTRAINT [PK_Permission] PRIMARY KEY CLUSTERED 
(
	[ItemId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PersianSite_Accordion]    Script Date: 11/27/2016 01:43:57 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PersianSite_Accordion](
	[ItemId] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](50) NOT NULL,
	[Context] [ntext] NOT NULL,
	[ViewOrder] [tinyint] NULL,
	[GroupName] [nvarchar](100) NOT NULL,
	[AddedDate] [date] NOT NULL,
	[ModifiedDate] [date] NULL,
 CONSTRAINT [PK_PersianSite_Accordion] PRIMARY KEY CLUSTERED 
(
	[ItemId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PersianSite_BootstrapSlider]    Script Date: 11/27/2016 01:43:57 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PersianSite_BootstrapSlider](
	[ItemId] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](100) NULL,
	[ViewOrder] [tinyint] NULL,
	[Image] [nvarchar](255) NOT NULL,
	[GroupName] [nvarchar](100) NOT NULL,
	[AddedDate] [date] NOT NULL,
	[ModifiedDate] [date] NULL,
 CONSTRAINT [PK_PersianSite_BootstrapSlider] PRIMARY KEY CLUSTERED 
(
	[ItemId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PersianSite_BoxNews]    Script Date: 11/27/2016 01:43:57 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PersianSite_BoxNews](
	[ItemId] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](250) NOT NULL,
	[Abstract] [ntext] NOT NULL,
	[Description] [ntext] NULL,
	[Source] [nvarchar](255) NULL,
	[PositiveOpinion] [smallint] NULL,
	[NegativeOpinion] [smallint] NULL,
	[VisitCount] [smallint] NULL,
	[Keywords] [nvarchar](255) NULL,
	[StartShowDate] [date] NOT NULL,
	[EndShowDate] [date] NULL,
	[HaveComment] [bit] NULL,
	[Image] [nvarchar](255) NULL,
	[CreateDate] [date] NOT NULL,
	[GroupName] [nvarchar](100) NOT NULL,
	[AddedDate] [date] NOT NULL,
	[ModifiedDate] [date] NULL,
 CONSTRAINT [PK_PersianSite_BoxNews] PRIMARY KEY CLUSTERED 
(
	[ItemId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PersianSite_BoxNewsComment]    Script Date: 11/27/2016 01:43:57 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PersianSite_BoxNewsComment](
	[ItemId] [int] IDENTITY(1,1) NOT NULL,
	[Email] [nvarchar](150) NOT NULL,
	[FullName] [nvarchar](150) NOT NULL,
	[Context] [ntext] NOT NULL,
	[CreateDate] [date] NOT NULL,
	[PositiveOpinion] [smallint] NULL,
	[NegativeOpinion] [smallint] NULL,
	[PersianSite_BoxNewsId] [int] NOT NULL,
	[AddedDate] [date] NOT NULL,
	[ModifiedDate] [date] NULL,
 CONSTRAINT [PK_PersianSite_BoxNewsComment] PRIMARY KEY CLUSTERED 
(
	[ItemId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PersianSite_FaqCategory]    Script Date: 11/27/2016 01:43:57 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PersianSite_FaqCategory](
	[ItemId] [int] IDENTITY(1,1) NOT NULL,
	[DisplayName] [nvarchar](50) NOT NULL,
	[ViewOrder] [tinyint] NULL,
	[GroupName] [nvarchar](100) NOT NULL,
	[AddedDate] [date] NOT NULL,
	[ModifiedDate] [date] NULL,
 CONSTRAINT [PK_PersianSite_FaqCategory] PRIMARY KEY CLUSTERED 
(
	[ItemId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PersianSite_FaqQuestion]    Script Date: 11/27/2016 01:43:57 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PersianSite_FaqQuestion](
	[ItemId] [int] IDENTITY(1,1) NOT NULL,
	[Question] [nvarchar](255) NOT NULL,
	[Answer] [ntext] NOT NULL,
	[ViewOrder] [tinyint] NULL,
	[GroupName] [nvarchar](100) NOT NULL,
	[PersianSite_FaqCategoryId] [int] NOT NULL,
	[AddedDate] [date] NOT NULL,
	[ModifiedDate] [date] NULL,
 CONSTRAINT [PK_PersianSite_FaqQuestion] PRIMARY KEY CLUSTERED 
(
	[ItemId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PersianSite_Gallery]    Script Date: 11/27/2016 01:43:57 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PersianSite_Gallery](
	[ItemId] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](100) NULL,
	[ViewOrder] [tinyint] NULL,
	[Image] [nvarchar](255) NOT NULL,
	[GroupName] [nvarchar](100) NOT NULL,
	[AddedDate] [date] NOT NULL,
	[ModifiedDate] [date] NULL,
 CONSTRAINT [PK_PersianSite_Gallery] PRIMARY KEY CLUSTERED 
(
	[ItemId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PersianSite_GridGallery]    Script Date: 11/27/2016 01:43:57 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PersianSite_GridGallery](
	[ItemId] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](100) NULL,
	[ViewOrder] [tinyint] NULL,
	[Image] [nvarchar](255) NOT NULL,
	[GroupName] [nvarchar](100) NOT NULL,
	[AddedDate] [date] NOT NULL,
	[ModifiedDate] [date] NULL,
 CONSTRAINT [PK_PersianSite_GridGallery] PRIMARY KEY CLUSTERED 
(
	[ItemId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PersianSite_News]    Script Date: 11/27/2016 01:43:57 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PersianSite_News](
	[ItemId] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](250) NOT NULL,
	[Abstract] [ntext] NOT NULL,
	[Description] [ntext] NULL,
	[Source] [nvarchar](255) NULL,
	[PositiveOpinion] [smallint] NULL,
	[NegativeOpinion] [smallint] NULL,
	[VisitCount] [smallint] NULL,
	[Keywords] [nvarchar](255) NULL,
	[StartShowDate] [date] NOT NULL,
	[EndShowDate] [date] NULL,
	[HaveComment] [bit] NULL,
	[Image] [nvarchar](255) NULL,
	[CreateDate] [date] NOT NULL,
	[PersianSite_NewsCategoryId] [int] NOT NULL,
	[AddedDate] [date] NOT NULL,
	[ModifiedDate] [date] NULL,
 CONSTRAINT [PK_PersianSite_News] PRIMARY KEY CLUSTERED 
(
	[ItemId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PersianSite_NewsCategory]    Script Date: 11/27/2016 01:43:57 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PersianSite_NewsCategory](
	[ItemId] [int] IDENTITY(1,1) NOT NULL,
	[DisplayName] [nvarchar](100) NOT NULL,
	[ViewOrder] [tinyint] NULL,
	[AddedDate] [date] NOT NULL,
	[ModifiedDate] [date] NULL,
 CONSTRAINT [PK_PersianSite_NewsCategory] PRIMARY KEY CLUSTERED 
(
	[ItemId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PersianSite_NewsComment]    Script Date: 11/27/2016 01:43:57 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PersianSite_NewsComment](
	[ItemId] [int] IDENTITY(1,1) NOT NULL,
	[Email] [nvarchar](150) NOT NULL,
	[FullName] [nvarchar](150) NOT NULL,
	[Context] [ntext] NOT NULL,
	[CreateDate] [date] NOT NULL,
	[PositiveOpinion] [smallint] NULL,
	[NegativeOpinion] [smallint] NULL,
	[PersianSite_NewsId] [int] NOT NULL,
	[AddedDate] [date] NOT NULL,
	[ModifiedDate] [date] NULL,
 CONSTRAINT [PK_PersianSite_NewsComment] PRIMARY KEY CLUSTERED 
(
	[ItemId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PersianSite_Phonebook]    Script Date: 11/27/2016 01:43:57 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PersianSite_Phonebook](
	[ItemId] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](100) NULL,
	[FirstName] [nvarchar](100) NULL,
	[LastName] [nvarchar](100) NULL,
	[Email] [nvarchar](150) NULL,
	[Site] [nvarchar](150) NULL,
	[Phone] [nvarchar](100) NULL,
	[HomePhone] [nvarchar](100) NULL,
	[WorkPhone] [nvarchar](100) NULL,
	[Mobile] [nvarchar](100) NULL,
	[SecondMobile] [nvarchar](100) NULL,
	[Description] [ntext] NULL,
	[Address] [nvarchar](255) NULL,
	[PostalCode] [nvarchar](100) NULL,
	[Job] [nvarchar](150) NULL,
	[Picture] [nvarchar](255) NULL,
	[AddedDate] [date] NOT NULL,
	[ModifiedDate] [date] NULL,
 CONSTRAINT [PK_PersianSite_Phonebook] PRIMARY KEY CLUSTERED 
(
	[ItemId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PersianSite_TabContent]    Script Date: 11/27/2016 01:43:57 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PersianSite_TabContent](
	[ItemId] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](50) NOT NULL,
	[Context] [ntext] NOT NULL,
	[ViewOrder] [tinyint] NULL,
	[GroupName] [nvarchar](100) NOT NULL,
	[AddedDate] [date] NOT NULL,
	[ModifiedDate] [date] NULL,
 CONSTRAINT [PK_PersianSite_TabContent] PRIMARY KEY CLUSTERED 
(
	[ItemId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PersianSite_TickerNews]    Script Date: 11/27/2016 01:43:57 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PersianSite_TickerNews](
	[ItemId] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](250) NOT NULL,
	[Abstract] [ntext] NOT NULL,
	[Description] [ntext] NULL,
	[Source] [nvarchar](255) NULL,
	[PositiveOpinion] [smallint] NULL,
	[NegativeOpinion] [smallint] NULL,
	[VisitCount] [smallint] NULL,
	[Keywords] [nvarchar](255) NULL,
	[StartShowDate] [date] NOT NULL,
	[EndShowDate] [date] NULL,
	[HaveComment] [bit] NULL,
	[Image] [nvarchar](255) NULL,
	[CreateDate] [date] NOT NULL,
	[GroupName] [nvarchar](100) NOT NULL,
	[AddedDate] [date] NOT NULL,
	[ModifiedDate] [date] NULL,
 CONSTRAINT [PK_PersianSite_TickerNews] PRIMARY KEY CLUSTERED 
(
	[ItemId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PersianSite_TickerNewsComment]    Script Date: 11/27/2016 01:43:57 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PersianSite_TickerNewsComment](
	[ItemId] [int] IDENTITY(1,1) NOT NULL,
	[Email] [nvarchar](150) NOT NULL,
	[FullName] [nvarchar](150) NOT NULL,
	[Context] [ntext] NOT NULL,
	[CreateDate] [date] NOT NULL,
	[PositiveOpinion] [smallint] NULL,
	[NegativeOpinion] [smallint] NULL,
	[PersianSite_TickerNewsId] [int] NOT NULL,
	[AddedDate] [date] NOT NULL,
	[ModifiedDate] [date] NULL,
 CONSTRAINT [PK_PersianSite_TickerNewsComment] PRIMARY KEY CLUSTERED 
(
	[ItemId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Position]    Script Date: 11/27/2016 01:43:57 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Position](
	[ItemId] [int] IDENTITY(1,1) NOT NULL,
	[DisplayName] [nvarchar](150) NOT NULL,
	[MembershipId] [int] NOT NULL,
	[AddedDate] [date] NOT NULL,
	[ModifiedDate] [date] NULL,
 CONSTRAINT [PK_Position] PRIMARY KEY CLUSTERED 
(
	[ItemId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Repository]    Script Date: 11/27/2016 01:43:57 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Repository](
	[ItemId] [int] IDENTITY(1,1) NOT NULL,
	[RepositoryName] [nvarchar](50) NOT NULL,
	[ShowHiddenFolder] [bit] NOT NULL,
	[EnableWorkFlow] [bit] NOT NULL,
	[AddedDate] [date] NOT NULL,
	[ModifiedDate] [date] NULL,
 CONSTRAINT [PK_Repository] PRIMARY KEY CLUSTERED 
(
	[ItemId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Role]    Script Date: 11/27/2016 01:43:57 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Role](
	[ItemId] [int] IDENTITY(1,1) NOT NULL,
	[DisplayName] [nvarchar](100) NOT NULL,
	[MembershipId] [int] NOT NULL,
	[AddedDate] [date] NOT NULL,
	[ModifiedDate] [date] NULL,
 CONSTRAINT [PK_Role] PRIMARY KEY CLUSTERED 
(
	[ItemId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[RolePermission]    Script Date: 11/27/2016 01:43:57 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RolePermission](
	[ItemId] [int] IDENTITY(1,1) NOT NULL,
	[RoleId] [int] NOT NULL,
	[PermissionId] [int] NOT NULL,
	[AddedDate] [date] NOT NULL,
	[ModifiedDate] [date] NULL,
 CONSTRAINT [PK_RolePermission] PRIMARY KEY CLUSTERED 
(
	[ItemId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[SearchSetting]    Script Date: 11/27/2016 01:43:57 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SearchSetting](
	[ItemId] [int] IDENTITY(1,1) NOT NULL,
	[LinkPage] [nvarchar](max) NOT NULL,
	[RouteField] [ntext] NOT NULL,
	[ContentId] [int] NOT NULL,
	[RepositoryId] [int] NOT NULL,
	[AddedDate] [date] NOT NULL,
	[ModifiedDate] [date] NULL,
 CONSTRAINT [PK_SearchSetting] PRIMARY KEY CLUSTERED 
(
	[ItemId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Site]    Script Date: 11/27/2016 01:43:57 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Site](
	[ItemId] [int] IDENTITY(1,1) NOT NULL,
	[SiteName] [nvarchar](50) NOT NULL,
	[Title] [nvarchar](50) NULL,
	[Label] [nvarchar](50) NULL,
	[Mode] [bit] NOT NULL,
	[Culture] [nvarchar](50) NOT NULL,
	[Domains] [nvarchar](max) NULL,
	[SitePath] [nvarchar](50) NULL,
	[Version] [nvarchar](50) NULL,
	[UserAgent] [nvarchar](max) NULL,
	[IsOnline] [bit] NOT NULL,
	[IsBase] [bit] NOT NULL,
	[SmtpHost] [nvarchar](50) NULL,
	[SmtpUsername] [nvarchar](50) NULL,
	[SmtpPassword] [nvarchar](50) NULL,
	[SmtpPort] [nvarchar](4) NULL,
	[SmtpFrom] [nvarchar](50) NULL,
	[SmtpTo] [nvarchar](max) NULL,
	[Author] [nvarchar](max) NULL,
	[Keyword] [ntext] NULL,
	[Description] [ntext] NULL,
	[CustomFields] [ntext] NULL,
	[CustomFieldsValue] [ntext] NULL,
	[Canonical] [nvarchar](max) NULL,
	[HtmlMetaBlock] [ntext] NULL,
	[TimeZoneId] [nvarchar](50) NULL,
	[MembershipId] [int] NOT NULL,
	[RepositoryId] [int] NOT NULL,
	[ParentId] [int] NULL,
	[AddedDate] [date] NOT NULL,
	[ModifiedDate] [date] NULL,
 CONSTRAINT [PK_Site] PRIMARY KEY CLUSTERED 
(
	[ItemId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Views]    Script Date: 11/27/2016 01:43:57 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Views](
	[ItemId] [int] IDENTITY(1,1) NOT NULL,
	[ViewName] [nvarchar](50) NOT NULL,
	[VirtualPath] [nvarchar](max) NOT NULL,
	[Template] [nvarchar](max) NULL,
	[RouteFields] [ntext] NULL,
	[RouteFieldsValue] [ntext] NULL,
	[Parameters] [ntext] NULL,
	[ParametersValue] [ntext] NULL,
	[SiteId] [int] NOT NULL,
	[AddedDate] [date] NOT NULL,
	[ModifiedDate] [date] NULL,
 CONSTRAINT [PK_View] PRIMARY KEY CLUSTERED 
(
	[ItemId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ZoneContent]    Script Date: 11/27/2016 01:43:57 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ZoneContent](
	[ItemId] [int] IDENTITY(1,1) NOT NULL,
	[ZoneName] [nvarchar](50) NOT NULL,
	[OrderId] [int] NOT NULL,
	[Hidden] [bit] NOT NULL,
	[RouteFields] [ntext] NULL,
	[RouteFieldsValue] [ntext] NULL,
	[HtmlString] [ntext] NULL,
	[ViewId] [int] NULL,
	[ModuleId] [int] NULL,
	[PageId] [int] NOT NULL,
	[AddedDate] [date] NOT NULL,
	[ModifiedDate] [date] NULL,
 CONSTRAINT [PK_ZoneContent] PRIMARY KEY CLUSTERED 
(
	[ItemId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  View [dbo].[CustomerPermissionsView]    Script Date: 11/27/2016 01:43:57 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[CustomerPermissionsView] AS 
SELECT
    [CustomerRole].[CustomerId], [RolePermission].PermissionId, Permission.DisplayName, Permission.DevName
FROM
    [CustomerRole]
CROSS JOIN
    [RolePermission]
INNER JOIN
    Permission
ON
    RolePermission.PermissionId = Permission.ItemId
WHERE
    [CustomerRole].RoleId = [RolePermission].RoleId AND
    [RolePermission].PermissionId NOT IN (
		SELECT
			[CustomerPermission].PermissionId
		FROM
			[CustomerPermission]
		WHERE
			[CustomerPermission].IsHave = 0
	)
UNION ALL
SELECT
    [CustomerPermission].[CustomerId], [CustomerPermission].PermissionId, Permission.DisplayName, Permission.DevName
FROM
    [CustomerPermission]
INNER JOIN
	Permission
ON
	CustomerPermission.PermissionId = Permission.ItemId
WHERE
    [CustomerPermission].IsHave = 1


GO
/****** Object:  View [dbo].[CustomerPermissionView]    Script Date: 11/27/2016 01:43:57 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[CustomerPermissionView]
AS
SELECT     dbo.CustomerPermission.ItemId, dbo.CustomerPermission.CustomerId, dbo.CustomerPermission.PermissionId, dbo.CustomerPermission.IsHave, 
                      CASE WHEN CustomerPermission.IsHave = 1 THEN 'بله' ELSE 'خیر' END AS IsHaveStr, dbo.Permission.DisplayName AS PermissionDisplayName, 
                      dbo.Permission.DevName AS PermissionDevName, dbo.CustomerPermission.AddedDate, dbo.CustomerPermission.ModifiedDate
FROM         dbo.CustomerPermission INNER JOIN
                      dbo.Permission ON dbo.CustomerPermission.PermissionId = dbo.Permission.ItemId


GO
/****** Object:  View [dbo].[CustomerPositionView]    Script Date: 11/27/2016 01:43:57 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[CustomerPositionView]
AS
SELECT     dbo.CustomerPosition.ItemId, dbo.CustomerPosition.CustomerId, dbo.CustomerPosition.PositionId, dbo.Position.DisplayName AS PositionDisplayName, 
                      dbo.CustomerPosition.AddedDate, dbo.CustomerPosition.ModifiedDate
FROM         dbo.CustomerPosition INNER JOIN
                      dbo.Position ON dbo.CustomerPosition.PositionId = dbo.Position.ItemId


GO
/****** Object:  View [dbo].[CustomerRoleView]    Script Date: 11/27/2016 01:43:57 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[CustomerRoleView]
AS
SELECT     dbo.CustomerRole.ItemId, dbo.CustomerRole.CustomerId, dbo.CustomerRole.RoleId, dbo.Role.DisplayName AS RoleDisplayName, dbo.CustomerRole.AddedDate, 
                      dbo.CustomerRole.ModifiedDate
FROM         dbo.CustomerRole INNER JOIN
                      dbo.Role ON dbo.CustomerRole.RoleId = dbo.Role.ItemId


GO
/****** Object:  View [dbo].[FolderView]    Script Date: 11/27/2016 01:43:57 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[FolderView]
AS
SELECT     dbo.Folder.ItemId, dbo.Folder.FolderName, dbo.Folder.VirtualPath, dbo.Folder.FolderTypeId, dbo.Folder.SiteId, dbo.Folder.AddedDate, dbo.Folder.ModifiedDate, 
                      dbo.FolderType.TypeName AS FolderType, dbo.Site.SiteName AS Site, dbo.Folder.ContentId, dbo.[Content].ContentName, dbo.[Content].RepositoryId
FROM         dbo.Folder INNER JOIN
                      dbo.FolderType ON dbo.Folder.FolderTypeId = dbo.FolderType.ItemId INNER JOIN
                      dbo.Site ON dbo.Folder.SiteId = dbo.Site.ItemId LEFT OUTER JOIN
                      dbo.[Content] ON dbo.Folder.ContentId = dbo.[Content].ItemId


GO
/****** Object:  View [dbo].[RolePermissionView]    Script Date: 11/27/2016 01:43:57 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[RolePermissionView]
AS
SELECT     dbo.RolePermission.ItemId, dbo.RolePermission.RoleId, dbo.RolePermission.PermissionId, dbo.Permission.DisplayName AS PermissionDisplayName, 
                      dbo.Permission.DevName AS PermissionDevName, dbo.RolePermission.AddedDate, dbo.RolePermission.ModifiedDate
FROM         dbo.RolePermission INNER JOIN
                      dbo.Permission ON dbo.RolePermission.PermissionId = dbo.Permission.ItemId


GO
SET IDENTITY_INSERT [dbo].[Content] ON 

GO
INSERT [dbo].[Content] ([ItemId], [ContentName], [Label], [Hidden], [Sortable], [Paging], [PageSize], [Fields], [Categories], [CategoryFields], [CategoryFieldsValue], [RoleName], [RepositoryId], [AddedDate], [ModifiedDate]) VALUES (7, N'TabContent', N'Tab content', 0, 0, 1, 20, N'[{"FieldName":"Title","Label":"Title","DefaultValue":"","ShowInList":1,"Tooltip":"","Length":50,"AllowNull":0,"CustomSetting":"","ControlType":"TextBox","DataType":"nvarchar","FieldValidations":[{"ValidationName":"Required","FirstParam":"","SecondParam":"","ThirdParam":""}]},{"FieldName":"Context","Label":"Context","DefaultValue":"","ShowInList":0,"Tooltip":"","Length":0,"AllowNull":0,"CustomSetting":"","ControlType":"TextEditor","DataType":"ntext","FieldValidations":[{"ValidationName":"Required","FirstParam":"","SecondParam":"","ThirdParam":""}]},{"FieldName":"ViewOrder","Label":"ViewOrder","DefaultValue":"","ShowInList":1,"Tooltip":"","Length":0,"AllowNull":1,"CustomSetting":"","ControlType":"TextBox","DataType":"tinyint","FieldValidations":[]},{"FieldName":"GroupName","Label":"GroupName","DefaultValue":"","ShowInList":1,"Tooltip":"","Length":100,"AllowNull":0,"CustomSetting":"","ControlType":"TextBox","DataType":"nvarchar","FieldValidations":[{"ValidationName":"Required","FirstParam":"","SecondParam":"","ThirdParam":""}]}]', N'[]', N'[]', N'[]', N'TabContent', 2, CAST(N'2016-10-29' AS Date), CAST(N'2016-11-20' AS Date))
GO
INSERT [dbo].[Content] ([ItemId], [ContentName], [Label], [Hidden], [Sortable], [Paging], [PageSize], [Fields], [Categories], [CategoryFields], [CategoryFieldsValue], [RoleName], [RepositoryId], [AddedDate], [ModifiedDate]) VALUES (8, N'BootstrapSlider', N'Bootstrap slider', 0, 0, 1, 20, N'[{"FieldName":"Title","Label":"Title","DefaultValue":"","ShowInList":1,"Tooltip":"","Length":100,"AllowNull":1,"CustomSetting":"","ControlType":"TextBox","DataType":"nvarchar","FieldValidations":[]},{"FieldName":"ViewOrder","Label":"ViewOrder","DefaultValue":"","ShowInList":1,"Tooltip":"","Length":0,"AllowNull":1,"CustomSetting":"","ControlType":"TextBox","DataType":"tinyint","FieldValidations":[]},{"FieldName":"Image","Label":"Image","DefaultValue":"","ShowInList":0,"Tooltip":"","Length":255,"AllowNull":0,"CustomSetting":"","ControlType":"Image","DataType":"nvarchar","FieldValidations":[{"ValidationName":"Required","FirstParam":"","SecondParam":"","ThirdParam":""}]},{"FieldName":"GroupName","Label":"GroupName","DefaultValue":"","ShowInList":1,"Tooltip":"","Length":100,"AllowNull":0,"CustomSetting":"","ControlType":"TextBox","DataType":"nvarchar","FieldValidations":[{"ValidationName":"Required","FirstParam":"","SecondParam":"","ThirdParam":""}]}]', N'[]', N'[]', N'[]', N'BootstrapSlider', 2, CAST(N'2016-10-30' AS Date), CAST(N'2016-11-20' AS Date))
GO
INSERT [dbo].[Content] ([ItemId], [ContentName], [Label], [Hidden], [Sortable], [Paging], [PageSize], [Fields], [Categories], [CategoryFields], [CategoryFieldsValue], [RoleName], [RepositoryId], [AddedDate], [ModifiedDate]) VALUES (9, N'Gallery', N'Gallery', 0, 0, 1, 20, N'[{"FieldName":"Title","Label":"Title","DefaultValue":"","ShowInList":1,"Tooltip":"","Length":100,"AllowNull":1,"CustomSetting":"","ControlType":"TextBox","DataType":"nvarchar","FieldValidations":[]},{"FieldName":"ViewOrder","Label":"ViewOrder","DefaultValue":"","ShowInList":1,"Tooltip":"","Length":0,"AllowNull":1,"CustomSetting":"","ControlType":"TextBox","DataType":"tinyint","FieldValidations":[]},{"FieldName":"Image","Label":"Image","DefaultValue":"","ShowInList":0,"Tooltip":"","Length":255,"AllowNull":0,"CustomSetting":"","ControlType":"Image","DataType":"nvarchar","FieldValidations":[{"ValidationName":"Required","FirstParam":"","SecondParam":"","ThirdParam":""}]},{"FieldName":"GroupName","Label":"GroupName","DefaultValue":"","ShowInList":1,"Tooltip":"","Length":100,"AllowNull":0,"CustomSetting":"","ControlType":"TextBox","DataType":"nvarchar","FieldValidations":[{"ValidationName":"Required","FirstParam":"","SecondParam":"","ThirdParam":""}]}]', N'[]', N'[]', N'[]', N'Gallery', 2, CAST(N'2016-10-31' AS Date), CAST(N'2016-11-20' AS Date))
GO
INSERT [dbo].[Content] ([ItemId], [ContentName], [Label], [Hidden], [Sortable], [Paging], [PageSize], [Fields], [Categories], [CategoryFields], [CategoryFieldsValue], [RoleName], [RepositoryId], [AddedDate], [ModifiedDate]) VALUES (10, N'GridGallery', N'Grid gallery', 0, 0, 1, 20, N'[{"FieldName":"Title","Label":"Title","DefaultValue":"","ShowInList":1,"Tooltip":"","Length":100,"AllowNull":1,"CustomSetting":"","ControlType":"TextBox","DataType":"nvarchar","FieldValidations":[]},{"FieldName":"ViewOrder","Label":"ViewOrder","DefaultValue":"","ShowInList":1,"Tooltip":"","Length":0,"AllowNull":1,"CustomSetting":"","ControlType":"TextBox","DataType":"tinyint","FieldValidations":[]},{"FieldName":"Image","Label":"Image","DefaultValue":"","ShowInList":0,"Tooltip":"","Length":255,"AllowNull":0,"CustomSetting":"","ControlType":"Image","DataType":"nvarchar","FieldValidations":[{"ValidationName":"Required","FirstParam":"","SecondParam":"","ThirdParam":""}]},{"FieldName":"GroupName","Label":"GroupName","DefaultValue":"","ShowInList":1,"Tooltip":"","Length":100,"AllowNull":0,"CustomSetting":"","ControlType":"TextBox","DataType":"nvarchar","FieldValidations":[{"ValidationName":"Required","FirstParam":"","SecondParam":"","ThirdParam":""}]}]', N'[]', N'[]', N'[]', N'GridGallery', 2, CAST(N'2016-10-31' AS Date), CAST(N'2016-11-20' AS Date))
GO
INSERT [dbo].[Content] ([ItemId], [ContentName], [Label], [Hidden], [Sortable], [Paging], [PageSize], [Fields], [Categories], [CategoryFields], [CategoryFieldsValue], [RoleName], [RepositoryId], [AddedDate], [ModifiedDate]) VALUES (11, N'Accordion', N'Accordion', 0, 0, 1, 20, N'[{"FieldName":"Title","Label":"Title","DefaultValue":"","ShowInList":1,"Tooltip":"","Length":50,"AllowNull":0,"CustomSetting":"","ControlType":"TextBox","DataType":"nvarchar","FieldValidations":[{"ValidationName":"Required","FirstParam":"","SecondParam":"","ThirdParam":""}]},{"FieldName":"Context","Label":"Context","DefaultValue":"","ShowInList":0,"Tooltip":"","Length":0,"AllowNull":0,"CustomSetting":"","ControlType":"TextEditor","DataType":"ntext","FieldValidations":[{"ValidationName":"Required","FirstParam":"","SecondParam":"","ThirdParam":""}]},{"FieldName":"ViewOrder","Label":"ViewOrder","DefaultValue":"","ShowInList":1,"Tooltip":"","Length":0,"AllowNull":1,"CustomSetting":"","ControlType":"TextBox","DataType":"tinyint","FieldValidations":[]},{"FieldName":"GroupName","Label":"GroupName","DefaultValue":"","ShowInList":1,"Tooltip":"","Length":100,"AllowNull":0,"CustomSetting":"","ControlType":"TextBox","DataType":"nvarchar","FieldValidations":[{"ValidationName":"Required","FirstParam":"","SecondParam":"","ThirdParam":""}]}]', N'[]', N'[]', N'[]', N'Accordion', 2, CAST(N'2016-10-31' AS Date), CAST(N'2016-11-20' AS Date))
GO
INSERT [dbo].[Content] ([ItemId], [ContentName], [Label], [Hidden], [Sortable], [Paging], [PageSize], [Fields], [Categories], [CategoryFields], [CategoryFieldsValue], [RoleName], [RepositoryId], [AddedDate], [ModifiedDate]) VALUES (12, N'FaqCategory', N'Faq category', 0, 0, 1, 20, N'[{"FieldName":"DisplayName","Label":"DisplayName","DefaultValue":"","ShowInList":1,"Tooltip":"","Length":50,"AllowNull":0,"CustomSetting":"","ControlType":"TextBox","DataType":"nvarchar","FieldValidations":[{"ValidationName":"Required","FirstParam":"","SecondParam":"","ThirdParam":""}]},{"FieldName":"ViewOrder","Label":"ViewOrder","DefaultValue":"","ShowInList":1,"Tooltip":"","Length":0,"AllowNull":1,"CustomSetting":"","ControlType":"TextBox","DataType":"tinyint","FieldValidations":[]},{"FieldName":"GroupName","Label":"GroupName","DefaultValue":"","ShowInList":1,"Tooltip":"","Length":100,"AllowNull":0,"CustomSetting":"","ControlType":"TextBox","DataType":"nvarchar","FieldValidations":[{"ValidationName":"Required","FirstParam":"","SecondParam":"","ThirdParam":""}]}]', N'[]', N'[]', N'[]', N'FaqCategory', 2, CAST(N'2016-10-31' AS Date), CAST(N'2016-11-20' AS Date))
GO
INSERT [dbo].[Content] ([ItemId], [ContentName], [Label], [Hidden], [Sortable], [Paging], [PageSize], [Fields], [Categories], [CategoryFields], [CategoryFieldsValue], [RoleName], [RepositoryId], [AddedDate], [ModifiedDate]) VALUES (14, N'FaqQuestion', N'Faq question', 0, 0, 1, 20, N'[{"FieldName":"Question","Label":"Question","DefaultValue":"","ShowInList":1,"Tooltip":"","Length":255,"AllowNull":0,"CustomSetting":"","ControlType":"TextBox","DataType":"nvarchar","FieldValidations":[{"ValidationName":"Required","FirstParam":"","SecondParam":"","ThirdParam":""}]},{"FieldName":"Answer","Label":"Answer","DefaultValue":"","ShowInList":0,"Tooltip":"","Length":0,"AllowNull":0,"CustomSetting":"","ControlType":"TextArea","DataType":"ntext","FieldValidations":[{"ValidationName":"Required","FirstParam":"","SecondParam":"","ThirdParam":""}]},{"FieldName":"ViewOrder","Label":"ViewOrder","DefaultValue":"","ShowInList":1,"Tooltip":"","Length":0,"AllowNull":1,"CustomSetting":"","ControlType":"TextBox","DataType":"tinyint","FieldValidations":[]},{"FieldName":"GroupName","Label":"GroupName","DefaultValue":"","ShowInList":1,"Tooltip":"","Length":100,"AllowNull":0,"CustomSetting":"","ControlType":"TextBox","DataType":"nvarchar","FieldValidations":[{"ValidationName":"Required","FirstParam":"","SecondParam":"","ThirdParam":""}]}]', N'["PersianSite_FaqCategory"]', N'["PersianSite_FaqCategory"]', N'["DisplayName"]', N'FaqQuestion', 2, CAST(N'2016-10-31' AS Date), CAST(N'2016-11-20' AS Date))
GO
INSERT [dbo].[Content] ([ItemId], [ContentName], [Label], [Hidden], [Sortable], [Paging], [PageSize], [Fields], [Categories], [CategoryFields], [CategoryFieldsValue], [RoleName], [RepositoryId], [AddedDate], [ModifiedDate]) VALUES (1013, N'Phonebook', N'Phonebook', 0, 0, 1, 20, N'[{"FieldName":"Title","Label":"Title","DefaultValue":"","ShowInList":1,"Tooltip":"","Length":100,"AllowNull":1,"CustomSetting":"","ControlType":"TextBox","DataType":"nvarchar","FieldValidations":[]},{"FieldName":"FirstName","Label":"First name","DefaultValue":"","ShowInList":1,"Tooltip":"","Length":100,"AllowNull":1,"CustomSetting":"","ControlType":"TextBox","DataType":"nvarchar","FieldValidations":[]},{"FieldName":"LastName","Label":"LastName","DefaultValue":"","ShowInList":1,"Tooltip":"","Length":100,"AllowNull":1,"CustomSetting":"","ControlType":"TextBox","DataType":"nvarchar","FieldValidations":[]},{"FieldName":"Email","Label":"Email","DefaultValue":"","ShowInList":1,"Tooltip":"","Length":150,"AllowNull":1,"CustomSetting":"","ControlType":"TextBox","DataType":"nvarchar","FieldValidations":[]},{"FieldName":"Site","Label":"Site","DefaultValue":"","ShowInList":0,"Tooltip":"","Length":150,"AllowNull":1,"CustomSetting":"","ControlType":"TextBox","DataType":"nvarchar","FieldValidations":[]},{"FieldName":"Phone","Label":"Phone","DefaultValue":"","ShowInList":1,"Tooltip":"","Length":100,"AllowNull":1,"CustomSetting":"","ControlType":"TextBox","DataType":"nvarchar","FieldValidations":[]},{"FieldName":"HomePhone","Label":"HomePhone","DefaultValue":"","ShowInList":1,"Tooltip":"","Length":100,"AllowNull":1,"CustomSetting":"","ControlType":"TextBox","DataType":"nvarchar","FieldValidations":[]},{"FieldName":"WorkPhone","Label":"WorkPhone","DefaultValue":"","ShowInList":1,"Tooltip":"","Length":100,"AllowNull":1,"CustomSetting":"","ControlType":"TextBox","DataType":"nvarchar","FieldValidations":[]},{"FieldName":"Mobile","Label":"Mobile","DefaultValue":"","ShowInList":1,"Tooltip":"","Length":100,"AllowNull":1,"CustomSetting":"","ControlType":"TextBox","DataType":"nvarchar","FieldValidations":[]},{"FieldName":"SecondMobile","Label":"SecondMobile","DefaultValue":"","ShowInList":1,"Tooltip":"","Length":100,"AllowNull":1,"CustomSetting":"","ControlType":"TextBox","DataType":"nvarchar","FieldValidations":[]},{"FieldName":"Description","Label":"Description","DefaultValue":"","ShowInList":0,"Tooltip":"","Length":0,"AllowNull":1,"CustomSetting":"","ControlType":"TextArea","DataType":"ntext","FieldValidations":[]},{"FieldName":"Address","Label":"Address","DefaultValue":"","ShowInList":0,"Tooltip":"","Length":255,"AllowNull":1,"CustomSetting":"","ControlType":"TextBox","DataType":"nvarchar","FieldValidations":[]},{"FieldName":"PostalCode","Label":"PostalCode","DefaultValue":"","ShowInList":0,"Tooltip":"","Length":100,"AllowNull":1,"CustomSetting":"","ControlType":"TextBox","DataType":"nvarchar","FieldValidations":[]},{"FieldName":"Job","Label":"Job","DefaultValue":"","ShowInList":0,"Tooltip":"","Length":150,"AllowNull":1,"CustomSetting":"","ControlType":"TextBox","DataType":"nvarchar","FieldValidations":[]},{"FieldName":"Picture","Label":"Picture","DefaultValue":"","ShowInList":0,"Tooltip":"","Length":255,"AllowNull":1,"CustomSetting":"","ControlType":"Image","DataType":"nvarchar","FieldValidations":[]}]', N'[]', N'[]', N'[]', N'Phonebook', 2, CAST(N'2016-10-31' AS Date), CAST(N'2016-11-20' AS Date))
GO
INSERT [dbo].[Content] ([ItemId], [ContentName], [Label], [Hidden], [Sortable], [Paging], [PageSize], [Fields], [Categories], [CategoryFields], [CategoryFieldsValue], [RoleName], [RepositoryId], [AddedDate], [ModifiedDate]) VALUES (1014, N'NewsCategory', N'News category', 0, 0, 1, 20, N'[{"FieldName":"DisplayName","Label":"DisplayName","DefaultValue":"","ShowInList":1,"Tooltip":"","Length":100,"AllowNull":0,"CustomSetting":"","ControlType":"TextBox","DataType":"nvarchar","FieldValidations":[{"ValidationName":"Required","FirstParam":"","SecondParam":"","ThirdParam":""}]},{"FieldName":"ViewOrder","Label":"ViewOrder","DefaultValue":"","ShowInList":1,"Tooltip":"","Length":0,"AllowNull":1,"CustomSetting":"","ControlType":"TextBox","DataType":"tinyint","FieldValidations":[]}]', N'[]', N'[]', N'[]', N'NewsCategory', 2, CAST(N'2016-11-01' AS Date), CAST(N'2016-11-27' AS Date))
GO
INSERT [dbo].[Content] ([ItemId], [ContentName], [Label], [Hidden], [Sortable], [Paging], [PageSize], [Fields], [Categories], [CategoryFields], [CategoryFieldsValue], [RoleName], [RepositoryId], [AddedDate], [ModifiedDate]) VALUES (1015, N'News', N'News', 0, 0, 1, 20, N'[{"FieldName":"Title","Label":"Title","DefaultValue":"","ShowInList":1,"Tooltip":"","Length":250,"AllowNull":0,"CustomSetting":"","ControlType":"TextBox","DataType":"nvarchar","FieldValidations":[{"ValidationName":"Required","FirstParam":"","SecondParam":"","ThirdParam":""}]},{"FieldName":"Abstract","Label":"Abstract","DefaultValue":"","ShowInList":0,"Tooltip":"","Length":0,"AllowNull":0,"CustomSetting":"","ControlType":"TextEditor","DataType":"ntext","FieldValidations":[{"ValidationName":"Required","FirstParam":"","SecondParam":"","ThirdParam":""}]},{"FieldName":"Description","Label":"Description","DefaultValue":"","ShowInList":0,"Tooltip":"","Length":0,"AllowNull":1,"CustomSetting":"","ControlType":"TextEditor","DataType":"ntext","FieldValidations":[]},{"FieldName":"Source","Label":"Source","DefaultValue":"","ShowInList":1,"Tooltip":"","Length":255,"AllowNull":1,"CustomSetting":"","ControlType":"TextBox","DataType":"nvarchar","FieldValidations":[]},{"FieldName":"PositiveOpinion","Label":"PositiveOpinion","DefaultValue":"","ShowInList":1,"Tooltip":"","Length":0,"AllowNull":1,"CustomSetting":"","ControlType":"TextBox","DataType":"smallint","FieldValidations":[]},{"FieldName":"NegativeOpinion","Label":"NegativeOpinion","DefaultValue":"","ShowInList":1,"Tooltip":"","Length":0,"AllowNull":1,"CustomSetting":"","ControlType":"TextBox","DataType":"smallint","FieldValidations":[]},{"FieldName":"VisitCount","Label":"VisitCount","DefaultValue":"","ShowInList":1,"Tooltip":"","Length":0,"AllowNull":1,"CustomSetting":"","ControlType":"TextBox","DataType":"smallint","FieldValidations":[]},{"FieldName":"Keywords","Label":"Keywords","DefaultValue":"","ShowInList":0,"Tooltip":"","Length":255,"AllowNull":1,"CustomSetting":"","ControlType":"TextBox","DataType":"nvarchar","FieldValidations":[]},{"FieldName":"StartShowDate","Label":"StartShowDate","DefaultValue":"","ShowInList":1,"Tooltip":"","Length":0,"AllowNull":0,"CustomSetting":"","ControlType":"Date","DataType":"date","FieldValidations":[{"ValidationName":"Required","FirstParam":"","SecondParam":"","ThirdParam":""}]},{"FieldName":"EndShowDate","Label":"EndShowDate","DefaultValue":"","ShowInList":1,"Tooltip":"","Length":0,"AllowNull":1,"CustomSetting":"","ControlType":"Date","DataType":"date","FieldValidations":[]},{"FieldName":"HaveComment","Label":"HaveComment","DefaultValue":"","ShowInList":1,"Tooltip":"","Length":0,"AllowNull":1,"CustomSetting":"","ControlType":"CheckBox","DataType":"bit","FieldValidations":[]},{"FieldName":"Image","Label":"Image","DefaultValue":"","ShowInList":0,"Tooltip":"","Length":255,"AllowNull":1,"CustomSetting":"","ControlType":"Image","DataType":"nvarchar","FieldValidations":[]},{"FieldName":"CreateDate","Label":"CreateDate","DefaultValue":"","ShowInList":1,"Tooltip":"","Length":0,"AllowNull":0,"CustomSetting":"","ControlType":"Date","DataType":"date","FieldValidations":[{"ValidationName":"Required","FirstParam":"","SecondParam":"","ThirdParam":""}]}]', N'["PersianSite_NewsCategory"]', N'["PersianSite_NewsCategory"]', N'["DisplayName"]', N'News', 2, CAST(N'2016-11-01' AS Date), CAST(N'2016-11-20' AS Date))
GO
INSERT [dbo].[Content] ([ItemId], [ContentName], [Label], [Hidden], [Sortable], [Paging], [PageSize], [Fields], [Categories], [CategoryFields], [CategoryFieldsValue], [RoleName], [RepositoryId], [AddedDate], [ModifiedDate]) VALUES (1016, N'NewsComment', N'NewsComment', 0, 0, 1, 20, N'[{"FieldName":"Email","Label":"Email","DefaultValue":"","ShowInList":1,"Tooltip":"","Length":150,"AllowNull":0,"CustomSetting":"","ControlType":"TextBox","DataType":"nvarchar","FieldValidations":[{"ValidationName":"Required","FirstParam":"","SecondParam":"","ThirdParam":""}]},{"FieldName":"FullName","Label":"FullName","DefaultValue":"","ShowInList":1,"Tooltip":"","Length":150,"AllowNull":0,"CustomSetting":"","ControlType":"TextBox","DataType":"nvarchar","FieldValidations":[{"ValidationName":"Required","FirstParam":"","SecondParam":"","ThirdParam":""}]},{"FieldName":"Context","Label":"Context","DefaultValue":"","ShowInList":0,"Tooltip":"","Length":0,"AllowNull":0,"CustomSetting":"","ControlType":"TextArea","DataType":"ntext","FieldValidations":[{"ValidationName":"Required","FirstParam":"","SecondParam":"","ThirdParam":""}]},{"FieldName":"CreateDate","Label":"CreateDate","DefaultValue":"","ShowInList":1,"Tooltip":"","Length":0,"AllowNull":0,"CustomSetting":"","ControlType":"Date","DataType":"date","FieldValidations":[{"ValidationName":"Required","FirstParam":"","SecondParam":"","ThirdParam":""}]},{"FieldName":"PositiveOpinion","Label":"PositiveOpinion","DefaultValue":"","ShowInList":1,"Tooltip":"","Length":0,"AllowNull":1,"CustomSetting":"","ControlType":"TextBox","DataType":"smallint","FieldValidations":[]},{"FieldName":"NegativeOpinion","Label":"NegativeOpinion","DefaultValue":"","ShowInList":1,"Tooltip":"","Length":0,"AllowNull":1,"CustomSetting":"","ControlType":"TextBox","DataType":"smallint","FieldValidations":[]}]', N'["PersianSite_News"]', N'["PersianSite_News"]', N'["Title"]', N'NewsComment', 2, CAST(N'2016-11-01' AS Date), CAST(N'2016-11-20' AS Date))
GO
INSERT [dbo].[Content] ([ItemId], [ContentName], [Label], [Hidden], [Sortable], [Paging], [PageSize], [Fields], [Categories], [CategoryFields], [CategoryFieldsValue], [RoleName], [RepositoryId], [AddedDate], [ModifiedDate]) VALUES (1017, N'TickerNews', N'TickerNews', 0, 0, 1, 20, N'[{"FieldName":"Title","Label":"Title","DefaultValue":"","ShowInList":1,"Tooltip":"","Length":250,"AllowNull":0,"CustomSetting":"","ControlType":"TextBox","DataType":"nvarchar","FieldValidations":[{"ValidationName":"Required","FirstParam":"","SecondParam":"","ThirdParam":""}]},{"FieldName":"Abstract","Label":"Abstract","DefaultValue":"","ShowInList":0,"Tooltip":"","Length":0,"AllowNull":0,"CustomSetting":"","ControlType":"TextEditor","DataType":"ntext","FieldValidations":[{"ValidationName":"Required","FirstParam":"","SecondParam":"","ThirdParam":""}]},{"FieldName":"Description","Label":"Description","DefaultValue":"","ShowInList":0,"Tooltip":"","Length":0,"AllowNull":1,"CustomSetting":"","ControlType":"TextEditor","DataType":"ntext","FieldValidations":[]},{"FieldName":"Source","Label":"Source","DefaultValue":"","ShowInList":1,"Tooltip":"","Length":255,"AllowNull":1,"CustomSetting":"","ControlType":"TextBox","DataType":"nvarchar","FieldValidations":[]},{"FieldName":"PositiveOpinion","Label":"PositiveOpinion","DefaultValue":"","ShowInList":1,"Tooltip":"","Length":0,"AllowNull":1,"CustomSetting":"","ControlType":"TextBox","DataType":"smallint","FieldValidations":[]},{"FieldName":"NegativeOpinion","Label":"NegativeOpinion","DefaultValue":"","ShowInList":1,"Tooltip":"","Length":0,"AllowNull":1,"CustomSetting":"","ControlType":"TextBox","DataType":"smallint","FieldValidations":[]},{"FieldName":"VisitCount","Label":"VisitCount","DefaultValue":"","ShowInList":1,"Tooltip":"","Length":0,"AllowNull":1,"CustomSetting":"","ControlType":"TextBox","DataType":"smallint","FieldValidations":[]},{"FieldName":"Keywords","Label":"Keywords","DefaultValue":"","ShowInList":0,"Tooltip":"","Length":255,"AllowNull":1,"CustomSetting":"","ControlType":"TextBox","DataType":"nvarchar","FieldValidations":[]},{"FieldName":"StartShowDate","Label":"StartShowDate","DefaultValue":"","ShowInList":1,"Tooltip":"","Length":0,"AllowNull":0,"CustomSetting":"","ControlType":"Date","DataType":"date","FieldValidations":[{"ValidationName":"Required","FirstParam":"","SecondParam":"","ThirdParam":""}]},{"FieldName":"EndShowDate","Label":"EndShowDate","DefaultValue":"","ShowInList":1,"Tooltip":"","Length":0,"AllowNull":1,"CustomSetting":"","ControlType":"Date","DataType":"date","FieldValidations":[]},{"FieldName":"HaveComment","Label":"HaveComment","DefaultValue":"","ShowInList":1,"Tooltip":"","Length":0,"AllowNull":1,"CustomSetting":"","ControlType":"CheckBox","DataType":"bit","FieldValidations":[]},{"FieldName":"Image","Label":"Image","DefaultValue":"","ShowInList":0,"Tooltip":"","Length":255,"AllowNull":1,"CustomSetting":"","ControlType":"Image","DataType":"nvarchar","FieldValidations":[]},{"FieldName":"CreateDate","Label":"CreateDate","DefaultValue":"","ShowInList":1,"Tooltip":"","Length":0,"AllowNull":0,"CustomSetting":"","ControlType":"Date","DataType":"date","FieldValidations":[{"ValidationName":"Required","FirstParam":"","SecondParam":"","ThirdParam":""}]},{"FieldName":"GroupName","Label":"GroupName","DefaultValue":"","ShowInList":1,"Tooltip":"","Length":100,"AllowNull":0,"CustomSetting":"","ControlType":"TextBox","DataType":"nvarchar","FieldValidations":[{"ValidationName":"Required","FirstParam":"","SecondParam":"","ThirdParam":""}]}]', N'[]', N'[]', N'[]', N'TickerNews', 2, CAST(N'2016-11-01' AS Date), CAST(N'2016-11-20' AS Date))
GO
INSERT [dbo].[Content] ([ItemId], [ContentName], [Label], [Hidden], [Sortable], [Paging], [PageSize], [Fields], [Categories], [CategoryFields], [CategoryFieldsValue], [RoleName], [RepositoryId], [AddedDate], [ModifiedDate]) VALUES (1018, N'TickerNewsComment', N'TickerNewsComment', 0, 0, 1, 20, N'[{"FieldName":"Email","Label":"Email","DefaultValue":"","ShowInList":1,"Tooltip":"","Length":150,"AllowNull":0,"CustomSetting":"","ControlType":"TextBox","DataType":"nvarchar","FieldValidations":[{"ValidationName":"Required","FirstParam":"","SecondParam":"","ThirdParam":""}]},{"FieldName":"FullName","Label":"FullName","DefaultValue":"","ShowInList":1,"Tooltip":"","Length":150,"AllowNull":0,"CustomSetting":"","ControlType":"TextBox","DataType":"nvarchar","FieldValidations":[{"ValidationName":"Required","FirstParam":"","SecondParam":"","ThirdParam":""}]},{"FieldName":"Context","Label":"Context","DefaultValue":"","ShowInList":0,"Tooltip":"","Length":0,"AllowNull":0,"CustomSetting":"","ControlType":"TextArea","DataType":"ntext","FieldValidations":[{"ValidationName":"Required","FirstParam":"","SecondParam":"","ThirdParam":""}]},{"FieldName":"CreateDate","Label":"CreateDate","DefaultValue":"","ShowInList":1,"Tooltip":"","Length":0,"AllowNull":0,"CustomSetting":"","ControlType":"Date","DataType":"date","FieldValidations":[{"ValidationName":"Required","FirstParam":"","SecondParam":"","ThirdParam":""}]},{"FieldName":"PositiveOpinion","Label":"PositiveOpinion","DefaultValue":"","ShowInList":1,"Tooltip":"","Length":0,"AllowNull":1,"CustomSetting":"","ControlType":"TextBox","DataType":"smallint","FieldValidations":[]},{"FieldName":"NegativeOpinion","Label":"NegativeOpinion","DefaultValue":"","ShowInList":1,"Tooltip":"","Length":0,"AllowNull":1,"CustomSetting":"","ControlType":"TextBox","DataType":"smallint","FieldValidations":[]}]', N'["PersianSite_TickerNews"]', N'["PersianSite_TickerNews"]', N'["Title"]', N'TickerNewsComment', 2, CAST(N'2016-11-01' AS Date), CAST(N'2016-11-20' AS Date))
GO
INSERT [dbo].[Content] ([ItemId], [ContentName], [Label], [Hidden], [Sortable], [Paging], [PageSize], [Fields], [Categories], [CategoryFields], [CategoryFieldsValue], [RoleName], [RepositoryId], [AddedDate], [ModifiedDate]) VALUES (1019, N'BoxNews', N'BoxNews', 0, 0, 1, 20, N'[{"FieldName":"Title","Label":"Title","DefaultValue":"","ShowInList":1,"Tooltip":"","Length":250,"AllowNull":0,"CustomSetting":"","ControlType":"TextBox","DataType":"nvarchar","FieldValidations":[{"ValidationName":"Required","FirstParam":"","SecondParam":"","ThirdParam":""}]},{"FieldName":"Abstract","Label":"Abstract","DefaultValue":"","ShowInList":0,"Tooltip":"","Length":0,"AllowNull":0,"CustomSetting":"","ControlType":"TextEditor","DataType":"ntext","FieldValidations":[{"ValidationName":"Required","FirstParam":"","SecondParam":"","ThirdParam":""}]},{"FieldName":"Description","Label":"Description","DefaultValue":"","ShowInList":0,"Tooltip":"","Length":0,"AllowNull":1,"CustomSetting":"","ControlType":"TextEditor","DataType":"ntext","FieldValidations":[]},{"FieldName":"Source","Label":"Source","DefaultValue":"","ShowInList":1,"Tooltip":"","Length":255,"AllowNull":1,"CustomSetting":"","ControlType":"TextBox","DataType":"nvarchar","FieldValidations":[]},{"FieldName":"PositiveOpinion","Label":"PositiveOpinion","DefaultValue":"","ShowInList":1,"Tooltip":"","Length":0,"AllowNull":1,"CustomSetting":"","ControlType":"TextBox","DataType":"smallint","FieldValidations":[]},{"FieldName":"NegativeOpinion","Label":"NegativeOpinion","DefaultValue":"","ShowInList":1,"Tooltip":"","Length":0,"AllowNull":1,"CustomSetting":"","ControlType":"TextBox","DataType":"smallint","FieldValidations":[]},{"FieldName":"VisitCount","Label":"VisitCount","DefaultValue":"","ShowInList":1,"Tooltip":"","Length":0,"AllowNull":1,"CustomSetting":"","ControlType":"TextBox","DataType":"smallint","FieldValidations":[]},{"FieldName":"Keywords","Label":"Keywords","DefaultValue":"","ShowInList":0,"Tooltip":"","Length":255,"AllowNull":1,"CustomSetting":"","ControlType":"TextBox","DataType":"nvarchar","FieldValidations":[]},{"FieldName":"StartShowDate","Label":"StartShowDate","DefaultValue":"","ShowInList":1,"Tooltip":"","Length":0,"AllowNull":0,"CustomSetting":"","ControlType":"Date","DataType":"date","FieldValidations":[{"ValidationName":"Required","FirstParam":"","SecondParam":"","ThirdParam":""}]},{"FieldName":"EndShowDate","Label":"EndShowDate","DefaultValue":"","ShowInList":1,"Tooltip":"","Length":0,"AllowNull":1,"CustomSetting":"","ControlType":"Date","DataType":"date","FieldValidations":[]},{"FieldName":"HaveComment","Label":"HaveComment","DefaultValue":"","ShowInList":1,"Tooltip":"","Length":0,"AllowNull":1,"CustomSetting":"","ControlType":"CheckBox","DataType":"bit","FieldValidations":[]},{"FieldName":"Image","Label":"Image","DefaultValue":"","ShowInList":0,"Tooltip":"","Length":255,"AllowNull":1,"CustomSetting":"","ControlType":"Image","DataType":"nvarchar","FieldValidations":[]},{"FieldName":"CreateDate","Label":"CreateDate","DefaultValue":"","ShowInList":1,"Tooltip":"","Length":0,"AllowNull":0,"CustomSetting":"","ControlType":"Date","DataType":"date","FieldValidations":[{"ValidationName":"Required","FirstParam":"","SecondParam":"","ThirdParam":""}]},{"FieldName":"GroupName","Label":"GroupName","DefaultValue":"","ShowInList":1,"Tooltip":"","Length":100,"AllowNull":0,"CustomSetting":"","ControlType":"TextBox","DataType":"nvarchar","FieldValidations":[{"ValidationName":"Required","FirstParam":"","SecondParam":"","ThirdParam":""}]}]', N'[]', N'[]', N'[]', N'BoxNews', 2, CAST(N'2016-11-01' AS Date), CAST(N'2016-11-20' AS Date))
GO
INSERT [dbo].[Content] ([ItemId], [ContentName], [Label], [Hidden], [Sortable], [Paging], [PageSize], [Fields], [Categories], [CategoryFields], [CategoryFieldsValue], [RoleName], [RepositoryId], [AddedDate], [ModifiedDate]) VALUES (1020, N'BoxNewsComment', N'BoxNewsComment', 0, 0, 1, 20, N'[{"FieldName":"Email","Label":"Email","DefaultValue":"","ShowInList":1,"Tooltip":"","Length":150,"AllowNull":0,"CustomSetting":"","ControlType":"TextBox","DataType":"nvarchar","FieldValidations":[{"ValidationName":"Required","FirstParam":"","SecondParam":"","ThirdParam":""}]},{"FieldName":"FullName","Label":"FullName","DefaultValue":"","ShowInList":1,"Tooltip":"","Length":150,"AllowNull":0,"CustomSetting":"","ControlType":"TextBox","DataType":"nvarchar","FieldValidations":[{"ValidationName":"Required","FirstParam":"","SecondParam":"","ThirdParam":""}]},{"FieldName":"Context","Label":"Context","DefaultValue":"","ShowInList":0,"Tooltip":"","Length":0,"AllowNull":0,"CustomSetting":"","ControlType":"TextArea","DataType":"ntext","FieldValidations":[{"ValidationName":"Required","FirstParam":"","SecondParam":"","ThirdParam":""}]},{"FieldName":"CreateDate","Label":"CreateDate","DefaultValue":"","ShowInList":1,"Tooltip":"","Length":0,"AllowNull":0,"CustomSetting":"","ControlType":"Date","DataType":"date","FieldValidations":[{"ValidationName":"Required","FirstParam":"","SecondParam":"","ThirdParam":""}]},{"FieldName":"PositiveOpinion","Label":"PositiveOpinion","DefaultValue":"","ShowInList":1,"Tooltip":"","Length":0,"AllowNull":1,"CustomSetting":"","ControlType":"TextBox","DataType":"smallint","FieldValidations":[]},{"FieldName":"NegativeOpinion","Label":"NegativeOpinion","DefaultValue":"","ShowInList":1,"Tooltip":"","Length":0,"AllowNull":1,"CustomSetting":"","ControlType":"TextBox","DataType":"smallint","FieldValidations":[]}]', N'["PersianSite_BoxNews"]', N'["PersianSite_BoxNews"]', N'["Title"]', N'BoxNewsComment', 2, CAST(N'2016-11-01' AS Date), CAST(N'2016-11-20' AS Date))
GO
SET IDENTITY_INSERT [dbo].[Content] OFF
GO
SET IDENTITY_INSERT [dbo].[Customer] ON 

GO
INSERT [dbo].[Customer] ([ItemId], [Username], [Password], [FirstName], [LastName], [FatherName], [NationalCode], [RetrievalText], [IsAdmin], [BoycottUser], [LastLoginDate], [LastLoginIdentification], [UseOperationDate], [OperationDate], [PublicMassage], [IdentificationNumber], [Email], [EmailConfirmed], [PhoneNumber], [PhoneNumberConfirmed], [PasswordQuestion], [PasswordAnswer], [IsApproved], [LastActivityDate], [AccessFailedCount], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (6, N'1', N'1', NULL, NULL, NULL, NULL, NULL, 1, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, 1, N'09120748028', 1, N'PasswordQuestion', N'PasswordAnswer', 1, NULL, 0, 1, CAST(N'2016-10-28' AS Date), NULL)
GO
INSERT [dbo].[Customer] ([ItemId], [Username], [Password], [FirstName], [LastName], [FatherName], [NationalCode], [RetrievalText], [IsAdmin], [BoycottUser], [LastLoginDate], [LastLoginIdentification], [UseOperationDate], [OperationDate], [PublicMassage], [IdentificationNumber], [Email], [EmailConfirmed], [PhoneNumber], [PhoneNumberConfirmed], [PasswordQuestion], [PasswordAnswer], [IsApproved], [LastActivityDate], [AccessFailedCount], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (7, N'2', N'2', NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, 0, NULL, 0, N'PasswordQuestion', N'PasswordAnswer', 0, NULL, 0, 2, CAST(N'2016-11-07' AS Date), NULL)
GO
INSERT [dbo].[Customer] ([ItemId], [Username], [Password], [FirstName], [LastName], [FatherName], [NationalCode], [RetrievalText], [IsAdmin], [BoycottUser], [LastLoginDate], [LastLoginIdentification], [UseOperationDate], [OperationDate], [PublicMassage], [IdentificationNumber], [Email], [EmailConfirmed], [PhoneNumber], [PhoneNumberConfirmed], [PasswordQuestion], [PasswordAnswer], [IsApproved], [LastActivityDate], [AccessFailedCount], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (8, N'3', N'3', NULL, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL, 0, NULL, 0, N'PasswordQuestion', N'PasswordAnswer', 0, NULL, 0, 3, CAST(N'2016-11-07' AS Date), NULL)
GO
SET IDENTITY_INSERT [dbo].[Customer] OFF
GO
SET IDENTITY_INSERT [dbo].[CustomerRole] ON 

GO
INSERT [dbo].[CustomerRole] ([ItemId], [CustomerId], [RoleId], [AddedDate], [ModifiedDate]) VALUES (2, 6, 1, CAST(N'2016-10-28' AS Date), NULL)
GO
INSERT [dbo].[CustomerRole] ([ItemId], [CustomerId], [RoleId], [AddedDate], [ModifiedDate]) VALUES (4, 7, 2, CAST(N'2016-11-20' AS Date), NULL)
GO
INSERT [dbo].[CustomerRole] ([ItemId], [CustomerId], [RoleId], [AddedDate], [ModifiedDate]) VALUES (5, 8, 3, CAST(N'2016-11-20' AS Date), NULL)
GO
SET IDENTITY_INSERT [dbo].[CustomerRole] OFF
GO
SET IDENTITY_INSERT [dbo].[Files] ON 

GO
INSERT [dbo].[Files] ([ItemId], [FileName], [FileExtension], [VirtualPath], [FolderId], [SiteId], [AddedDate], [ModifiedDate]) VALUES (4, N'Chrysanthemum.jpg', N'.jpg', N'C:\Users\doOb\Desktop\SAJPortal\SAJPortal\CmsData\PortalFolders\test\Chrysanthemum.jpg', 1014, 3, CAST(N'2016-11-07' AS Date), NULL)
GO
INSERT [dbo].[Files] ([ItemId], [FileName], [FileExtension], [VirtualPath], [FolderId], [SiteId], [AddedDate], [ModifiedDate]) VALUES (5, N'Lighthouse.jpg', N'.jpg', N'C:\Users\doOb\Desktop\SAJPortal\SAJPortal\CmsData\PortalFolders\ter\Lighthouse.jpg', 1015, 3, CAST(N'2016-11-07' AS Date), NULL)
GO
SET IDENTITY_INSERT [dbo].[Files] OFF
GO
SET IDENTITY_INSERT [dbo].[Folder] ON 

GO
INSERT [dbo].[Folder] ([ItemId], [FolderName], [VirtualPath], [FolderTypeId], [ContentId], [SiteId], [AddedDate], [ModifiedDate]) VALUES (1, N'test', N'test', 2, 7, 2, CAST(N'2016-11-23' AS Date), NULL)
GO
SET IDENTITY_INSERT [dbo].[Folder] OFF
GO
SET IDENTITY_INSERT [dbo].[FolderType] ON 

GO
INSERT [dbo].[FolderType] ([ItemId], [TypeName], [AddedDate], [ModifiedDate]) VALUES (1, N'Media', CAST(N'2010-01-01' AS Date), NULL)
GO
INSERT [dbo].[FolderType] ([ItemId], [TypeName], [AddedDate], [ModifiedDate]) VALUES (2, N'Repository', CAST(N'2010-01-01' AS Date), NULL)
GO
SET IDENTITY_INSERT [dbo].[FolderType] OFF
GO
SET IDENTITY_INSERT [dbo].[Layout] ON 

GO
INSERT [dbo].[Layout] ([ItemId], [LayoutName], [VirtualPath], [SiteId], [AddedDate], [ModifiedDate]) VALUES (2, N'_Layout', N'_Layout.cshtml', 2, CAST(N'2016-09-21' AS Date), CAST(N'2016-10-16' AS Date))
GO
SET IDENTITY_INSERT [dbo].[Layout] OFF
GO
SET IDENTITY_INSERT [dbo].[Membership] ON 

GO
INSERT [dbo].[Membership] ([ItemId], [MembershipName], [AddedDate], [ModifiedDate]) VALUES (1, N'Admin', CAST(N'2016-09-09' AS Date), CAST(N'2016-09-09' AS Date))
GO
INSERT [dbo].[Membership] ([ItemId], [MembershipName], [AddedDate], [ModifiedDate]) VALUES (2, N'PersianSite', CAST(N'2016-09-15' AS Date), CAST(N'2016-10-28' AS Date))
GO
INSERT [dbo].[Membership] ([ItemId], [MembershipName], [AddedDate], [ModifiedDate]) VALUES (3, N'EnglishSite', CAST(N'2016-11-07' AS Date), NULL)
GO
SET IDENTITY_INSERT [dbo].[Membership] OFF
GO
SET IDENTITY_INSERT [dbo].[Page] ON 

GO
INSERT [dbo].[Page] ([ItemId], [PageName], [VirtualPath], [Template], [Title], [MenuTitle], [RouteFields], [RouteFieldsValue], [Parameters], [ParametersValue], [PageType], [ShowInMenu], [ShowInBreadcrumb], [IsFirstPage], [RoleName], [ParentId], [SiteId], [AddedDate], [ModifiedDate]) VALUES (1, N'Dashboard', N'/Admin/Dashboard', N'~/Views/Shared/_AdminLayout.cshtml', N'Dashboard', N'Dashboard', N'[""]', N'[""]', N'[""]', N'[""]', 1, 1, 1, 0, N'Dashboard', NULL, 1, CAST(N'2001-01-01' AS Date), CAST(N'2016-09-24' AS Date))
GO
INSERT [dbo].[Page] ([ItemId], [PageName], [VirtualPath], [Template], [Title], [MenuTitle], [RouteFields], [RouteFieldsValue], [Parameters], [ParametersValue], [PageType], [ShowInMenu], [ShowInBreadcrumb], [IsFirstPage], [RoleName], [ParentId], [SiteId], [AddedDate], [ModifiedDate]) VALUES (2, N'Sites', N'/Admin/Sites', N'~/Views/Shared/_AdminLayout.cshtml', N'Sites', N'Pages', N'[""]', N'[""]', N'[""]', N'[""]', 1, 1, 1, 0, N'SitesList', 0, 1, CAST(N'2001-01-01' AS Date), NULL)
GO
INSERT [dbo].[Page] ([ItemId], [PageName], [VirtualPath], [Template], [Title], [MenuTitle], [RouteFields], [RouteFieldsValue], [Parameters], [ParametersValue], [PageType], [ShowInMenu], [ShowInBreadcrumb], [IsFirstPage], [RoleName], [ParentId], [SiteId], [AddedDate], [ModifiedDate]) VALUES (3, N'Pages', N'/Admin/Pages', N'~/Views/Shared/_AdminLayout.cshtml', N'Pages', N'Pages', N'[""]', N'[""]', N'[""]', N'[""]', 1, 1, 1, 0, N'PagesList', 0, 1, CAST(N'2001-01-01' AS Date), NULL)
GO
INSERT [dbo].[Page] ([ItemId], [PageName], [VirtualPath], [Template], [Title], [MenuTitle], [RouteFields], [RouteFieldsValue], [Parameters], [ParametersValue], [PageType], [ShowInMenu], [ShowInBreadcrumb], [IsFirstPage], [RoleName], [ParentId], [SiteId], [AddedDate], [ModifiedDate]) VALUES (4, N'Repositories', N'/Admin/Repositories', N'~/Views/Shared/_AdminLayout.cshtml', N'Repositories', N'Repositories', N'[""]', N'[""]', N'[""]', N'[""]', 1, 1, 1, 0, N'RepositoriesList', 0, 1, CAST(N'2001-01-01' AS Date), NULL)
GO
INSERT [dbo].[Page] ([ItemId], [PageName], [VirtualPath], [Template], [Title], [MenuTitle], [RouteFields], [RouteFieldsValue], [Parameters], [ParametersValue], [PageType], [ShowInMenu], [ShowInBreadcrumb], [IsFirstPage], [RoleName], [ParentId], [SiteId], [AddedDate], [ModifiedDate]) VALUES (5, N'Memberships', N'/Admin/Memberships', N'~/Views/Shared/_AdminLayout.cshtml', N'Memberships', N'Memberships', N'[""]', N'[""]', N'[""]', N'[""]', 1, 1, 1, 0, N'MembershipsList', 0, 1, CAST(N'2001-01-01' AS Date), NULL)
GO
INSERT [dbo].[Page] ([ItemId], [PageName], [VirtualPath], [Template], [Title], [MenuTitle], [RouteFields], [RouteFieldsValue], [Parameters], [ParametersValue], [PageType], [ShowInMenu], [ShowInBreadcrumb], [IsFirstPage], [RoleName], [ParentId], [SiteId], [AddedDate], [ModifiedDate]) VALUES (6, N'Home', N'/Home', N'~/CmsData/Layouts/_Layout.cshtml', N'Home', N'Home', N'[""]', N'[""]', N'[""]', N'[""]', 2, 1, 1, 1, NULL, NULL, 2, CAST(N'2016-09-15' AS Date), CAST(N'2016-10-01' AS Date))
GO
INSERT [dbo].[Page] ([ItemId], [PageName], [VirtualPath], [Template], [Title], [MenuTitle], [RouteFields], [RouteFieldsValue], [Parameters], [ParametersValue], [PageType], [ShowInMenu], [ShowInBreadcrumb], [IsFirstPage], [RoleName], [ParentId], [SiteId], [AddedDate], [ModifiedDate]) VALUES (7, N'Folders', N'/Admin/Folders', N'~/Views/Shared/_AdminLayout.cshtml', N'Folders', N'Folders', N'[""]', N'[""]', N'[""]', N'[""]', 1, 1, 1, 0, N'FoldersList', 0, 1, CAST(N'2010-01-01' AS Date), NULL)
GO
INSERT [dbo].[Page] ([ItemId], [PageName], [VirtualPath], [Template], [Title], [MenuTitle], [RouteFields], [RouteFieldsValue], [Parameters], [ParametersValue], [PageType], [ShowInMenu], [ShowInBreadcrumb], [IsFirstPage], [RoleName], [ParentId], [SiteId], [AddedDate], [ModifiedDate]) VALUES (8, N'Views', N'/Admin/Views', N'~/Views/Shared/_AdminLayout.cshtml', N'Views', N'Views', N'[""]', N'[""]', N'[""]', N'[""]', 1, 1, 1, 0, N'ViewsList', 0, 1, CAST(N'2010-01-01' AS Date), NULL)
GO
INSERT [dbo].[Page] ([ItemId], [PageName], [VirtualPath], [Template], [Title], [MenuTitle], [RouteFields], [RouteFieldsValue], [Parameters], [ParametersValue], [PageType], [ShowInMenu], [ShowInBreadcrumb], [IsFirstPage], [RoleName], [ParentId], [SiteId], [AddedDate], [ModifiedDate]) VALUES (9, N'Layouts', N'/Admin/Layouts', N'~/Views/Shared/_AdminLayout.cshtml', N'Layouts', N'Layouts', N'[""]', N'[""]', N'[""]', N'[""]', 1, 1, 1, 0, N'LayoutsList', 0, 1, CAST(N'2010-01-01' AS Date), NULL)
GO
INSERT [dbo].[Page] ([ItemId], [PageName], [VirtualPath], [Template], [Title], [MenuTitle], [RouteFields], [RouteFieldsValue], [Parameters], [ParametersValue], [PageType], [ShowInMenu], [ShowInBreadcrumb], [IsFirstPage], [RoleName], [ParentId], [SiteId], [AddedDate], [ModifiedDate]) VALUES (10, N'Contents', N'/Admin/Contents', N'~/Views/Shared/_AdminLayout.cshtml', N'Contents', N'Contents', N'[""]', N'[""]', N'[""]', N'[""]', 1, 1, 1, 0, N'ContentsList', 0, 1, CAST(N'2010-01-01' AS Date), NULL)
GO
INSERT [dbo].[Page] ([ItemId], [PageName], [VirtualPath], [Template], [Title], [MenuTitle], [RouteFields], [RouteFieldsValue], [Parameters], [ParametersValue], [PageType], [ShowInMenu], [ShowInBreadcrumb], [IsFirstPage], [RoleName], [ParentId], [SiteId], [AddedDate], [ModifiedDate]) VALUES (11, N'Settings', N'/Admin/Settings', N'~/Views/Shared/_AdminLayout.cshtml', N'Settings', N'Settings', N'[""]', N'[""]', N'[""]', N'[""]', 1, 1, 1, 0, N'Settings', 0, 1, CAST(N'2010-01-01' AS Date), NULL)
GO
INSERT [dbo].[Page] ([ItemId], [PageName], [VirtualPath], [Template], [Title], [MenuTitle], [RouteFields], [RouteFieldsValue], [Parameters], [ParametersValue], [PageType], [ShowInMenu], [ShowInBreadcrumb], [IsFirstPage], [RoleName], [ParentId], [SiteId], [AddedDate], [ModifiedDate]) VALUES (12, N'Home', N'/Home', N'~/CmsData/Layouts/_Layout.cshtml', N'Home', N'Home', N'[]', N'[]', N'[]', N'[]', 2, 1, 1, 1, NULL, NULL, 3, CAST(N'2016-11-05' AS Date), NULL)
GO
SET IDENTITY_INSERT [dbo].[Page] OFF
GO
SET IDENTITY_INSERT [dbo].[Permission] ON 

GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (1, N'Dashboard', N'Dashboard', 1, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (2, N'Sites list', N'SitesList', 1, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (4, N'New site', N'NewSite', 1, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (5, N'Edit site', N'EditSite', 1, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (6, N'Delete site', N'DeleteSite', 1, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (7, N'Pages list', N'PagesList', 1, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (8, N'New page', N'NewPage', 1, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (9, N'Edit page', N'EditPage', 1, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (10, N'Delete page', N'DeletePage', 1, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (11, N'Repositories list', N'RepositoriesList', 1, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (12, N'New repository', N'NewRepository', 1, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (13, N'Edit repository', N'EditRepository', 1, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (14, N'Delete repository', N'DeleteRepository', 1, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (15, N'Memberships list', N'MembershipsList', 1, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (16, N'New membership', N'NewMembership', 1, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (17, N'Edit membership', N'EditMembership', 1, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (18, N'Delete membership', N'DeleteMembership', 1, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (19, N'FoldersList', N'FoldersList', 1, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (20, N'New folder', N'NewFolder', 1, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (21, N'Edit folder', N'EditFolder', 1, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (22, N'Delete folder', N'DeleteFolder', 1, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (23, N'Views list', N'ViewsList', 1, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (24, N'New view', N'NewView', 1, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (25, N'Edit view', N'EditView', 1, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (26, N'Delete view', N'DeleteView', 1, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (27, N'Layouts list', N'LayoutsList', 1, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (28, N'New layout', N'NewLayout', 1, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (29, N'Edit layout', N'EditLayout', 1, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (30, N'Delete layout', N'DeleteLayout', 1, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (31, N'Contents list', N'ContentsList', 1, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (32, N'New content', N'NewContent', 1, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (33, N'Edit content', N'EditContent', 1, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (34, N'Delete content', N'DeleteContent', 1, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (35, N'Files list', N'FilesList', 1, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (36, N'New file', N'NewFile', 1, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (37, N'Edit file', N'EditFile', 1, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (38, N'Delete file', N'DeleteFile', 1, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (39, N'Repository folders list', N'RepositoryFoldersList', 1, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (40, N'New repository folder', N'NewRepositoryFolder', 1, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (41, N'Edit repository folder', N'EditRepositoryFolder', 1, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (42, N'Delete repository folder', N'DeleteRepositoryFolder', 1, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (43, N'Customers list', N'CustomersList', 1, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (44, N'New customer', N'NewCustomer', 1, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (45, N'Edit customer', N'EditCustomer', 1, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (46, N'Delete customer', N'DeleteCustomer', 1, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (47, N'Roles list', N'RolesList', 1, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (48, N'New role', N'NewRole', 1, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (49, N'Edit role', N'EditRole', 1, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (50, N'Delete role', N'DeleteRole', 1, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (51, N'PermissionsList', N'PermissionsList', 1, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (52, N'New permission', N'NewPermission', 1, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (53, N'Edit permission', N'EditPermission', 1, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (54, N'Delete permission', N'DeletePermission', 1, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (55, N'Positions list', N'PositionsList', 1, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (56, N'New position', N'NewPosition', 1, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (57, N'Edit position', N'EditPosition', 1, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (58, N'Delete position', N'DeletePosition', 1, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (59, N'Customer roles list', N'CustomerRolesList', 1, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (60, N'Edit customer role', N'EditCustomerRole', 1, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (61, N'Customer positions list', N'CustomerPositionsList', 1, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (62, N'Edit customer position', N'EditCustomerPosition', 1, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (63, N'Customer permissions list', N'CustomerPermissionsList', 1, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (64, N'Role permissions list', N'RolePermissionsList', 1, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (65, N'Edit role permission', N'EditRolePermission', 1, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (66, N'Zones list', N'ZonesList', 1, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (67, N'New zone', N'NewZone', 1, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (68, N'Edit zone', N'EditZone', 1, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (69, N'Delete zone', N'DeleteZone', 1, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (70, N'Settings', N'Settings', 1, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (71, N'Tab content', N'TabContent', 1, CAST(N'2016-10-29' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (72, N'Bootstrap slider', N'BootstrapSlider', 1, CAST(N'2016-10-30' AS Date), CAST(N'2016-10-30' AS Date))
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (73, N'Gallery', N'Gallery', 1, CAST(N'2016-10-31' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (74, N'Grid gallery', N'GridGallery', 1, CAST(N'2016-10-31' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (75, N'Accordion', N'Accordion', 1, CAST(N'2016-10-31' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (76, N'Faq category', N'FaqCategory', 1, CAST(N'2016-10-31' AS Date), CAST(N'2016-10-31' AS Date))
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (77, N'Faq question', N'FaqQuestion', 1, CAST(N'2016-10-31' AS Date), CAST(N'2016-10-31' AS Date))
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (1076, N'Phonebook', N'Phonebook', 1, CAST(N'2016-10-31' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (1077, N'News category', N'NewsCategory', 1, CAST(N'2016-11-01' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (1078, N'News', N'News', 1, CAST(N'2016-11-01' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (1079, N'News comment', N'NewsComment', 1, CAST(N'2016-11-01' AS Date), CAST(N'2016-11-02' AS Date))
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (1080, N'Ticker news', N'TickerNews', 1, CAST(N'2016-11-02' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (1081, N'Ticker news comment', N'TickerNewsComment', 1, CAST(N'2016-11-02' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (1082, N'Box news', N'BoxNews', 1, CAST(N'2016-11-02' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (1083, N'Box news comment', N'BoxNewsComment', 1, CAST(N'2016-11-02' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (1084, N'Error handling', N'ErrorHandling', 1, CAST(N'2016-11-02' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (1085, N'Dashboard', N'Dashboard', 2, CAST(N'2016-11-07' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (1087, N'Sites list', N'SitesList', 2, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (1088, N'New site', N'NewSite', 2, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (1089, N'Edit site', N'EditSite', 2, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (1090, N'Delete site', N'DeleteSite', 2, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (1091, N'Pages list', N'PagesList', 2, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (1092, N'New page', N'NewPage', 2, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (1093, N'Edit page', N'EditPage', 2, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (1094, N'Delete page', N'DeletePage', 2, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (1095, N'Repositories list', N'RepositoriesList', 2, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (1096, N'New repository', N'NewRepository', 2, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (1097, N'Edit repository', N'EditRepository', 2, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (1098, N'Delete repository', N'DeleteRepository', 2, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (1099, N'Memberships list', N'MembershipsList', 2, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (1100, N'New membership', N'NewMembership', 2, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (1101, N'Edit membership', N'EditMembership', 2, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (1102, N'Delete membership', N'DeleteMembership', 2, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (1103, N'FoldersList', N'FoldersList', 2, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (1104, N'New folder', N'NewFolder', 2, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (1105, N'Edit folder', N'EditFolder', 2, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (1106, N'Delete folder', N'DeleteFolder', 2, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (1107, N'Views list', N'ViewsList', 2, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (1108, N'New view', N'NewView', 2, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (1109, N'Edit view', N'EditView', 2, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (1110, N'Delete view', N'DeleteView', 2, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (1111, N'Layouts list', N'LayoutsList', 2, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (1112, N'New layout', N'NewLayout', 2, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (1113, N'Edit layout', N'EditLayout', 2, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (1114, N'Delete layout', N'DeleteLayout', 2, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (1115, N'Contents list', N'ContentsList', 2, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (1116, N'New content', N'NewContent', 2, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (1117, N'Edit content', N'EditContent', 2, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (1118, N'Delete content', N'DeleteContent', 2, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (1119, N'Files list', N'FilesList', 2, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (1120, N'New file', N'NewFile', 2, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (1121, N'Edit file', N'EditFile', 2, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (1122, N'Delete file', N'DeleteFile', 2, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (1123, N'Repository folders list', N'RepositoryFoldersList', 2, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (1124, N'New repository folder', N'NewRepositoryFolder', 2, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (1125, N'Edit repository folder', N'EditRepositoryFolder', 2, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (1126, N'Delete repository folder', N'DeleteRepositoryFolder', 2, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (1127, N'Customers list', N'CustomersList', 2, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (1128, N'New customer', N'NewCustomer', 2, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (1129, N'Edit customer', N'EditCustomer', 2, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (1130, N'Delete customer', N'DeleteCustomer', 2, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (1131, N'Roles list', N'RolesList', 2, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (1132, N'New role', N'NewRole', 2, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (1133, N'Edit role', N'EditRole', 2, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (1134, N'Delete role', N'DeleteRole', 2, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (1135, N'PermissionsList', N'PermissionsList', 2, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (1136, N'New permission', N'NewPermission', 2, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (1137, N'Edit permission', N'EditPermission', 2, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (1138, N'Delete permission', N'DeletePermission', 2, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (1139, N'Positions list', N'PositionsList', 2, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (1140, N'New position', N'NewPosition', 2, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (1141, N'Edit position', N'EditPosition', 2, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (1142, N'Delete position', N'DeletePosition', 2, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (1143, N'Customer roles list', N'CustomerRolesList', 2, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (1144, N'Edit customer role', N'EditCustomerRole', 2, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (1145, N'Customer positions list', N'CustomerPositionsList', 2, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (1146, N'Edit customer position', N'EditCustomerPosition', 2, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (1147, N'Customer permissions list', N'CustomerPermissionsList', 2, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (1148, N'Role permissions list', N'RolePermissionsList', 2, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (1149, N'Edit role permission', N'EditRolePermission', 2, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (1150, N'Zones list', N'ZonesList', 2, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (1151, N'New zone', N'NewZone', 2, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (1152, N'Edit zone', N'EditZone', 2, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (1153, N'Delete zone', N'DeleteZone', 2, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (1154, N'Settings', N'Settings', 2, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (1155, N'Tab content', N'TabContent', 2, CAST(N'2016-10-29' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (1156, N'Bootstrap slider', N'BootstrapSlider', 2, CAST(N'2016-10-30' AS Date), CAST(N'2016-10-30' AS Date))
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (1157, N'Gallery', N'Gallery', 2, CAST(N'2016-10-31' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (1158, N'Grid gallery', N'GridGallery', 2, CAST(N'2016-10-31' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (1159, N'Accordion', N'Accordion', 2, CAST(N'2016-10-31' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (1160, N'Faq category', N'FaqCategory', 2, CAST(N'2016-10-31' AS Date), CAST(N'2016-10-31' AS Date))
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (1161, N'Faq question', N'FaqQuestion', 2, CAST(N'2016-10-31' AS Date), CAST(N'2016-10-31' AS Date))
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (1162, N'Phonebook', N'Phonebook', 2, CAST(N'2016-10-31' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (1163, N'News category', N'NewsCategory', 2, CAST(N'2016-11-01' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (1164, N'News', N'News', 2, CAST(N'2016-11-01' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (1165, N'News comment', N'NewsComment', 2, CAST(N'2016-11-01' AS Date), CAST(N'2016-11-02' AS Date))
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (1166, N'Ticker news', N'TickerNews', 2, CAST(N'2016-11-02' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (1167, N'Ticker news comment', N'TickerNewsComment', 2, CAST(N'2016-11-02' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (1168, N'Box news', N'BoxNews', 2, CAST(N'2016-11-02' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (1169, N'Box news comment', N'BoxNewsComment', 2, CAST(N'2016-11-02' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (1170, N'Error handling', N'ErrorHandling', 2, CAST(N'2016-11-02' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (1171, N'Dashboard', N'Dashboard', 2, CAST(N'2016-11-07' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (1172, N'Dashboard', N'Dashboard', 3, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (1173, N'Sites list', N'SitesList', 3, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (1174, N'New site', N'NewSite', 3, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (1175, N'Edit site', N'EditSite', 3, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (1176, N'Delete site', N'DeleteSite', 3, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (1177, N'Pages list', N'PagesList', 3, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (1178, N'New page', N'NewPage', 3, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (1179, N'Edit page', N'EditPage', 3, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (1180, N'Delete page', N'DeletePage', 3, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (1181, N'Repositories list', N'RepositoriesList', 3, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (1182, N'New repository', N'NewRepository', 3, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (1183, N'Edit repository', N'EditRepository', 3, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (1184, N'Delete repository', N'DeleteRepository', 3, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (1185, N'Memberships list', N'MembershipsList', 3, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (1186, N'New membership', N'NewMembership', 3, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (1187, N'Edit membership', N'EditMembership', 3, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (1188, N'Delete membership', N'DeleteMembership', 3, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (1189, N'FoldersList', N'FoldersList', 3, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (1190, N'New folder', N'NewFolder', 3, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (1191, N'Edit folder', N'EditFolder', 3, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (1192, N'Delete folder', N'DeleteFolder', 3, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (1193, N'Views list', N'ViewsList', 3, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (1194, N'New view', N'NewView', 3, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (1195, N'Edit view', N'EditView', 3, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (1196, N'Delete view', N'DeleteView', 3, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (1197, N'Layouts list', N'LayoutsList', 3, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (1198, N'New layout', N'NewLayout', 3, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (1199, N'Edit layout', N'EditLayout', 3, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (1200, N'Delete layout', N'DeleteLayout', 3, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (1201, N'Contents list', N'ContentsList', 3, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (1202, N'New content', N'NewContent', 3, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (1203, N'Edit content', N'EditContent', 3, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (1204, N'Delete content', N'DeleteContent', 3, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (1205, N'Files list', N'FilesList', 3, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (1206, N'New file', N'NewFile', 3, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (1207, N'Edit file', N'EditFile', 3, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (1208, N'Delete file', N'DeleteFile', 3, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (1209, N'Repository folders list', N'RepositoryFoldersList', 3, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (1210, N'New repository folder', N'NewRepositoryFolder', 3, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (1211, N'Edit repository folder', N'EditRepositoryFolder', 3, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (1212, N'Delete repository folder', N'DeleteRepositoryFolder', 3, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (1213, N'Customers list', N'CustomersList', 3, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (1214, N'New customer', N'NewCustomer', 3, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (1215, N'Edit customer', N'EditCustomer', 3, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (1216, N'Delete customer', N'DeleteCustomer', 3, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (1217, N'Roles list', N'RolesList', 3, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (1218, N'New role', N'NewRole', 3, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (1219, N'Edit role', N'EditRole', 3, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (1220, N'Delete role', N'DeleteRole', 3, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (1221, N'PermissionsList', N'PermissionsList', 3, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (1222, N'New permission', N'NewPermission', 3, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (1223, N'Edit permission', N'EditPermission', 3, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (1224, N'Delete permission', N'DeletePermission', 3, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (1225, N'Positions list', N'PositionsList', 3, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (1226, N'New position', N'NewPosition', 3, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (1227, N'Edit position', N'EditPosition', 3, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (1228, N'Delete position', N'DeletePosition', 3, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (1229, N'Customer roles list', N'CustomerRolesList', 3, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (1230, N'Edit customer role', N'EditCustomerRole', 3, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (1231, N'Customer positions list', N'CustomerPositionsList', 3, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (1232, N'Edit customer position', N'EditCustomerPosition', 3, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (1233, N'Customer permissions list', N'CustomerPermissionsList', 3, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (1234, N'Role permissions list', N'RolePermissionsList', 3, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (1235, N'Edit role permission', N'EditRolePermission', 3, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (1236, N'Zones list', N'ZonesList', 3, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (1237, N'New zone', N'NewZone', 3, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (1238, N'Edit zone', N'EditZone', 3, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (1239, N'Delete zone', N'DeleteZone', 3, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (1240, N'Settings', N'Settings', 3, CAST(N'2016-10-16' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (1241, N'Tab content', N'TabContent', 3, CAST(N'2016-10-29' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (1242, N'Bootstrap slider', N'BootstrapSlider', 3, CAST(N'2016-10-30' AS Date), CAST(N'2016-10-30' AS Date))
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (1243, N'Gallery', N'Gallery', 3, CAST(N'2016-10-31' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (1244, N'Grid gallery', N'GridGallery', 3, CAST(N'2016-10-31' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (1245, N'Accordion', N'Accordion', 3, CAST(N'2016-10-31' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (1246, N'Faq category', N'FaqCategory', 3, CAST(N'2016-10-31' AS Date), CAST(N'2016-10-31' AS Date))
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (1247, N'Faq question', N'FaqQuestion', 3, CAST(N'2016-10-31' AS Date), CAST(N'2016-10-31' AS Date))
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (1248, N'Phonebook', N'Phonebook', 3, CAST(N'2016-10-31' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (1249, N'News category', N'NewsCategory', 3, CAST(N'2016-11-01' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (1250, N'News', N'News', 3, CAST(N'2016-11-01' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (1251, N'News comment', N'NewsComment', 3, CAST(N'2016-11-01' AS Date), CAST(N'2016-11-02' AS Date))
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (1252, N'Ticker news', N'TickerNews', 3, CAST(N'2016-11-02' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (1253, N'Ticker news comment', N'TickerNewsComment', 3, CAST(N'2016-11-02' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (1254, N'Box news', N'BoxNews', 3, CAST(N'2016-11-02' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (1255, N'Box news comment', N'BoxNewsComment', 3, CAST(N'2016-11-02' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (1256, N'Error handling', N'ErrorHandling', 3, CAST(N'2016-11-02' AS Date), NULL)
GO
INSERT [dbo].[Permission] ([ItemId], [DisplayName], [DevName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (1257, N'Dashboard', N'Dashboard', 3, CAST(N'2016-11-07' AS Date), NULL)
GO
SET IDENTITY_INSERT [dbo].[Permission] OFF
GO
SET IDENTITY_INSERT [dbo].[Repository] ON 

GO
INSERT [dbo].[Repository] ([ItemId], [RepositoryName], [ShowHiddenFolder], [EnableWorkFlow], [AddedDate], [ModifiedDate]) VALUES (1, N'Admin', 1, 1, CAST(N'2010-01-01' AS Date), CAST(N'2016-10-24' AS Date))
GO
INSERT [dbo].[Repository] ([ItemId], [RepositoryName], [ShowHiddenFolder], [EnableWorkFlow], [AddedDate], [ModifiedDate]) VALUES (2, N'PersianSite', 1, 1, CAST(N'2016-09-15' AS Date), CAST(N'2016-10-27' AS Date))
GO
INSERT [dbo].[Repository] ([ItemId], [RepositoryName], [ShowHiddenFolder], [EnableWorkFlow], [AddedDate], [ModifiedDate]) VALUES (3, N'EnglishSite', 1, 1, CAST(N'2016-11-07' AS Date), NULL)
GO
SET IDENTITY_INSERT [dbo].[Repository] OFF
GO
SET IDENTITY_INSERT [dbo].[Role] ON 

GO
INSERT [dbo].[Role] ([ItemId], [DisplayName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (1, N'Administrator', 1, CAST(N'2016-10-15' AS Date), NULL)
GO
INSERT [dbo].[Role] ([ItemId], [DisplayName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (2, N'Administrator', 2, CAST(N'2016-11-07' AS Date), CAST(N'2016-11-20' AS Date))
GO
INSERT [dbo].[Role] ([ItemId], [DisplayName], [MembershipId], [AddedDate], [ModifiedDate]) VALUES (3, N'Administrator', 3, CAST(N'2016-11-20' AS Date), CAST(N'2016-11-20' AS Date))
GO
SET IDENTITY_INSERT [dbo].[Role] OFF
GO
SET IDENTITY_INSERT [dbo].[RolePermission] ON 

GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (1837, 1, 1, CAST(N'2016-11-02' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (1838, 1, 2, CAST(N'2016-11-02' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (1839, 1, 4, CAST(N'2016-11-02' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (1840, 1, 5, CAST(N'2016-11-02' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (1841, 1, 6, CAST(N'2016-11-02' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (1842, 1, 7, CAST(N'2016-11-02' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (1843, 1, 8, CAST(N'2016-11-02' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (1844, 1, 9, CAST(N'2016-11-02' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (1845, 1, 10, CAST(N'2016-11-02' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (1846, 1, 11, CAST(N'2016-11-02' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (1847, 1, 12, CAST(N'2016-11-02' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (1848, 1, 13, CAST(N'2016-11-02' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (1849, 1, 14, CAST(N'2016-11-02' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (1850, 1, 15, CAST(N'2016-11-02' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (1851, 1, 16, CAST(N'2016-11-02' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (1852, 1, 17, CAST(N'2016-11-02' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (1853, 1, 18, CAST(N'2016-11-02' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (1854, 1, 19, CAST(N'2016-11-02' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (1855, 1, 20, CAST(N'2016-11-02' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (1856, 1, 21, CAST(N'2016-11-02' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (1857, 1, 22, CAST(N'2016-11-02' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (1858, 1, 23, CAST(N'2016-11-02' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (1859, 1, 24, CAST(N'2016-11-02' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (1860, 1, 25, CAST(N'2016-11-02' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (1861, 1, 26, CAST(N'2016-11-02' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (1862, 1, 27, CAST(N'2016-11-02' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (1863, 1, 28, CAST(N'2016-11-02' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (1864, 1, 29, CAST(N'2016-11-02' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (1865, 1, 30, CAST(N'2016-11-02' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (1866, 1, 31, CAST(N'2016-11-02' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (1867, 1, 32, CAST(N'2016-11-02' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (1868, 1, 33, CAST(N'2016-11-02' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (1869, 1, 34, CAST(N'2016-11-02' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (1870, 1, 35, CAST(N'2016-11-02' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (1871, 1, 36, CAST(N'2016-11-02' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (1872, 1, 37, CAST(N'2016-11-02' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (1873, 1, 38, CAST(N'2016-11-02' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (1874, 1, 39, CAST(N'2016-11-02' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (1875, 1, 40, CAST(N'2016-11-02' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (1876, 1, 41, CAST(N'2016-11-02' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (1877, 1, 42, CAST(N'2016-11-02' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (1878, 1, 43, CAST(N'2016-11-02' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (1879, 1, 44, CAST(N'2016-11-02' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (1880, 1, 45, CAST(N'2016-11-02' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (1881, 1, 46, CAST(N'2016-11-02' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (1882, 1, 47, CAST(N'2016-11-02' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (1883, 1, 48, CAST(N'2016-11-02' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (1884, 1, 49, CAST(N'2016-11-02' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (1885, 1, 50, CAST(N'2016-11-02' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (1886, 1, 51, CAST(N'2016-11-02' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (1887, 1, 52, CAST(N'2016-11-02' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (1888, 1, 53, CAST(N'2016-11-02' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (1889, 1, 54, CAST(N'2016-11-02' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (1890, 1, 55, CAST(N'2016-11-02' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (1891, 1, 56, CAST(N'2016-11-02' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (1892, 1, 57, CAST(N'2016-11-02' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (1893, 1, 58, CAST(N'2016-11-02' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (1894, 1, 59, CAST(N'2016-11-02' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (1895, 1, 60, CAST(N'2016-11-02' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (1896, 1, 61, CAST(N'2016-11-02' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (1897, 1, 62, CAST(N'2016-11-02' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (1898, 1, 63, CAST(N'2016-11-02' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (1899, 1, 64, CAST(N'2016-11-02' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (1900, 1, 65, CAST(N'2016-11-02' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (1901, 1, 66, CAST(N'2016-11-02' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (1902, 1, 67, CAST(N'2016-11-02' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (1903, 1, 68, CAST(N'2016-11-02' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (1904, 1, 69, CAST(N'2016-11-02' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (1905, 1, 70, CAST(N'2016-11-02' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (1906, 1, 71, CAST(N'2016-11-02' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (1907, 1, 72, CAST(N'2016-11-02' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (1908, 1, 73, CAST(N'2016-11-02' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (1909, 1, 74, CAST(N'2016-11-02' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (1910, 1, 75, CAST(N'2016-11-02' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (1911, 1, 76, CAST(N'2016-11-02' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (1912, 1, 77, CAST(N'2016-11-02' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (1913, 1, 1076, CAST(N'2016-11-02' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (1914, 1, 1077, CAST(N'2016-11-02' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (1915, 1, 1078, CAST(N'2016-11-02' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (1916, 1, 1079, CAST(N'2016-11-02' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (1917, 1, 1080, CAST(N'2016-11-02' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (1918, 1, 1081, CAST(N'2016-11-02' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (1919, 1, 1082, CAST(N'2016-11-02' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (1920, 1, 1083, CAST(N'2016-11-02' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (1921, 1, 1084, CAST(N'2016-11-02' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (1987, 2, 1085, CAST(N'2016-11-20' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (1988, 2, 1087, CAST(N'2016-11-20' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (1989, 2, 1089, CAST(N'2016-11-20' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (1990, 2, 1091, CAST(N'2016-11-20' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (1991, 2, 1092, CAST(N'2016-11-20' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (1992, 2, 1093, CAST(N'2016-11-20' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (1993, 2, 1094, CAST(N'2016-11-20' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (1994, 2, 1095, CAST(N'2016-11-20' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (1995, 2, 1097, CAST(N'2016-11-20' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (1996, 2, 1099, CAST(N'2016-11-20' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (1997, 2, 1101, CAST(N'2016-11-20' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (1998, 2, 1103, CAST(N'2016-11-20' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (1999, 2, 1104, CAST(N'2016-11-20' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (2000, 2, 1105, CAST(N'2016-11-20' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (2001, 2, 1106, CAST(N'2016-11-20' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (2002, 2, 1107, CAST(N'2016-11-20' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (2003, 2, 1108, CAST(N'2016-11-20' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (2004, 2, 1109, CAST(N'2016-11-20' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (2005, 2, 1110, CAST(N'2016-11-20' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (2006, 2, 1111, CAST(N'2016-11-20' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (2007, 2, 1112, CAST(N'2016-11-20' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (2008, 2, 1113, CAST(N'2016-11-20' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (2009, 2, 1114, CAST(N'2016-11-20' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (2010, 2, 1115, CAST(N'2016-11-20' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (2011, 2, 1116, CAST(N'2016-11-20' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (2012, 2, 1117, CAST(N'2016-11-20' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (2013, 2, 1118, CAST(N'2016-11-20' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (2014, 2, 1119, CAST(N'2016-11-20' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (2015, 2, 1120, CAST(N'2016-11-20' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (2016, 2, 1121, CAST(N'2016-11-20' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (2017, 2, 1122, CAST(N'2016-11-20' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (2018, 2, 1123, CAST(N'2016-11-20' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (2019, 2, 1124, CAST(N'2016-11-20' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (2020, 2, 1125, CAST(N'2016-11-20' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (2021, 2, 1126, CAST(N'2016-11-20' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (2022, 2, 1127, CAST(N'2016-11-20' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (2023, 2, 1128, CAST(N'2016-11-20' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (2024, 2, 1129, CAST(N'2016-11-20' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (2025, 2, 1130, CAST(N'2016-11-20' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (2026, 2, 1131, CAST(N'2016-11-20' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (2027, 2, 1132, CAST(N'2016-11-20' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (2028, 2, 1133, CAST(N'2016-11-20' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (2029, 2, 1134, CAST(N'2016-11-20' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (2030, 2, 1135, CAST(N'2016-11-20' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (2031, 2, 1136, CAST(N'2016-11-20' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (2032, 2, 1137, CAST(N'2016-11-20' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (2033, 2, 1138, CAST(N'2016-11-20' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (2034, 2, 1139, CAST(N'2016-11-20' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (2035, 2, 1140, CAST(N'2016-11-20' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (2036, 2, 1141, CAST(N'2016-11-20' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (2037, 2, 1142, CAST(N'2016-11-20' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (2038, 2, 1143, CAST(N'2016-11-20' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (2039, 2, 1144, CAST(N'2016-11-20' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (2040, 2, 1145, CAST(N'2016-11-20' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (2041, 2, 1146, CAST(N'2016-11-20' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (2042, 2, 1147, CAST(N'2016-11-20' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (2043, 2, 1148, CAST(N'2016-11-20' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (2044, 2, 1149, CAST(N'2016-11-20' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (2045, 2, 1150, CAST(N'2016-11-20' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (2046, 2, 1151, CAST(N'2016-11-20' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (2047, 2, 1152, CAST(N'2016-11-20' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (2048, 2, 1153, CAST(N'2016-11-20' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (2049, 2, 1154, CAST(N'2016-11-20' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (2050, 2, 1155, CAST(N'2016-11-20' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (2051, 2, 1156, CAST(N'2016-11-20' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (2052, 2, 1157, CAST(N'2016-11-20' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (2053, 2, 1158, CAST(N'2016-11-20' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (2054, 2, 1159, CAST(N'2016-11-20' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (2055, 2, 1160, CAST(N'2016-11-20' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (2056, 2, 1161, CAST(N'2016-11-20' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (2057, 2, 1162, CAST(N'2016-11-20' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (2058, 2, 1163, CAST(N'2016-11-20' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (2059, 2, 1164, CAST(N'2016-11-20' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (2060, 2, 1165, CAST(N'2016-11-20' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (2061, 2, 1166, CAST(N'2016-11-20' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (2062, 2, 1167, CAST(N'2016-11-20' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (2063, 2, 1168, CAST(N'2016-11-20' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (2064, 2, 1169, CAST(N'2016-11-20' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (2065, 2, 1170, CAST(N'2016-11-20' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (2066, 2, 1171, CAST(N'2016-11-20' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (2067, 3, 1172, CAST(N'2016-11-20' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (2068, 3, 1173, CAST(N'2016-11-20' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (2069, 3, 1175, CAST(N'2016-11-20' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (2070, 3, 1177, CAST(N'2016-11-20' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (2071, 3, 1178, CAST(N'2016-11-20' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (2072, 3, 1179, CAST(N'2016-11-20' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (2073, 3, 1180, CAST(N'2016-11-20' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (2074, 3, 1181, CAST(N'2016-11-20' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (2075, 3, 1183, CAST(N'2016-11-20' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (2076, 3, 1185, CAST(N'2016-11-20' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (2077, 3, 1187, CAST(N'2016-11-20' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (2078, 3, 1189, CAST(N'2016-11-20' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (2079, 3, 1190, CAST(N'2016-11-20' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (2080, 3, 1191, CAST(N'2016-11-20' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (2081, 3, 1192, CAST(N'2016-11-20' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (2082, 3, 1193, CAST(N'2016-11-20' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (2083, 3, 1194, CAST(N'2016-11-20' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (2084, 3, 1195, CAST(N'2016-11-20' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (2085, 3, 1196, CAST(N'2016-11-20' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (2086, 3, 1197, CAST(N'2016-11-20' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (2087, 3, 1198, CAST(N'2016-11-20' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (2088, 3, 1199, CAST(N'2016-11-20' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (2089, 3, 1200, CAST(N'2016-11-20' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (2090, 3, 1201, CAST(N'2016-11-20' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (2091, 3, 1202, CAST(N'2016-11-20' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (2092, 3, 1203, CAST(N'2016-11-20' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (2093, 3, 1204, CAST(N'2016-11-20' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (2094, 3, 1205, CAST(N'2016-11-20' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (2095, 3, 1206, CAST(N'2016-11-20' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (2096, 3, 1207, CAST(N'2016-11-20' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (2097, 3, 1208, CAST(N'2016-11-20' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (2098, 3, 1209, CAST(N'2016-11-20' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (2099, 3, 1210, CAST(N'2016-11-20' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (2100, 3, 1211, CAST(N'2016-11-20' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (2101, 3, 1212, CAST(N'2016-11-20' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (2102, 3, 1213, CAST(N'2016-11-20' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (2103, 3, 1214, CAST(N'2016-11-20' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (2104, 3, 1215, CAST(N'2016-11-20' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (2105, 3, 1216, CAST(N'2016-11-20' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (2106, 3, 1217, CAST(N'2016-11-20' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (2107, 3, 1218, CAST(N'2016-11-20' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (2108, 3, 1219, CAST(N'2016-11-20' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (2109, 3, 1220, CAST(N'2016-11-20' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (2110, 3, 1221, CAST(N'2016-11-20' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (2111, 3, 1222, CAST(N'2016-11-20' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (2112, 3, 1223, CAST(N'2016-11-20' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (2113, 3, 1224, CAST(N'2016-11-20' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (2114, 3, 1225, CAST(N'2016-11-20' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (2115, 3, 1226, CAST(N'2016-11-20' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (2116, 3, 1227, CAST(N'2016-11-20' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (2117, 3, 1228, CAST(N'2016-11-20' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (2118, 3, 1229, CAST(N'2016-11-20' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (2119, 3, 1230, CAST(N'2016-11-20' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (2120, 3, 1231, CAST(N'2016-11-20' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (2121, 3, 1232, CAST(N'2016-11-20' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (2122, 3, 1233, CAST(N'2016-11-20' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (2123, 3, 1234, CAST(N'2016-11-20' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (2124, 3, 1235, CAST(N'2016-11-20' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (2125, 3, 1236, CAST(N'2016-11-20' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (2126, 3, 1237, CAST(N'2016-11-20' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (2127, 3, 1238, CAST(N'2016-11-20' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (2128, 3, 1239, CAST(N'2016-11-20' AS Date), NULL)
GO
INSERT [dbo].[RolePermission] ([ItemId], [RoleId], [PermissionId], [AddedDate], [ModifiedDate]) VALUES (2129, 3, 1240, CAST(N'2016-11-20' AS Date), NULL)
GO
SET IDENTITY_INSERT [dbo].[RolePermission] OFF
GO
SET IDENTITY_INSERT [dbo].[Site] ON 

GO
INSERT [dbo].[Site] ([ItemId], [SiteName], [Title], [Label], [Mode], [Culture], [Domains], [SitePath], [Version], [UserAgent], [IsOnline], [IsBase], [SmtpHost], [SmtpUsername], [SmtpPassword], [SmtpPort], [SmtpFrom], [SmtpTo], [Author], [Keyword], [Description], [CustomFields], [CustomFieldsValue], [Canonical], [HtmlMetaBlock], [TimeZoneId], [MembershipId], [RepositoryId], [ParentId], [AddedDate], [ModifiedDate]) VALUES (1, N'Admin', N'Admin', N'Admin', 1, N'en', N'[]', NULL, N'1.0.0.0', N'[]', 1, 0, NULL, NULL, NULL, NULL, NULL, N'[]', N'Hasan Jodat Shandi', NULL, NULL, N'[]', N'[]', NULL, N'[]', N'UTC', 1, 1, NULL, CAST(N'2016-09-04' AS Date), CAST(N'2016-09-24' AS Date))
GO
INSERT [dbo].[Site] ([ItemId], [SiteName], [Title], [Label], [Mode], [Culture], [Domains], [SitePath], [Version], [UserAgent], [IsOnline], [IsBase], [SmtpHost], [SmtpUsername], [SmtpPassword], [SmtpPort], [SmtpFrom], [SmtpTo], [Author], [Keyword], [Description], [CustomFields], [CustomFieldsValue], [Canonical], [HtmlMetaBlock], [TimeZoneId], [MembershipId], [RepositoryId], [ParentId], [AddedDate], [ModifiedDate]) VALUES (2, N'PersianSite', N'Persian Site', N'Persian Site', 0, N'fa-IR', N'["localhost"]', N'fa', N'1.0.0.01', N'[]', 1, 1, NULL, NULL, NULL, NULL, NULL, N'[]', N'Hasan jodat shandi', N'saj,sajtech,sajtech.ir', N'sajportal', N'[]', N'[]', NULL, N'[]', N'Iran Standard Time', 2, 2, NULL, CAST(N'2016-09-15' AS Date), CAST(N'2016-11-20' AS Date))
GO
INSERT [dbo].[Site] ([ItemId], [SiteName], [Title], [Label], [Mode], [Culture], [Domains], [SitePath], [Version], [UserAgent], [IsOnline], [IsBase], [SmtpHost], [SmtpUsername], [SmtpPassword], [SmtpPort], [SmtpFrom], [SmtpTo], [Author], [Keyword], [Description], [CustomFields], [CustomFieldsValue], [Canonical], [HtmlMetaBlock], [TimeZoneId], [MembershipId], [RepositoryId], [ParentId], [AddedDate], [ModifiedDate]) VALUES (3, N'EnglishSite', N'English site', N'English site', 0, N'en-US', N'["localhost"]', N'en', NULL, N'[]', 1, 1, NULL, NULL, NULL, NULL, NULL, N'[]', NULL, NULL, NULL, N'[]', N'[]', NULL, N'[]', N'Dateline Standard Time', 3, 3, NULL, CAST(N'2016-11-05' AS Date), CAST(N'2016-11-20' AS Date))
GO
SET IDENTITY_INSERT [dbo].[Site] OFF
GO
SET IDENTITY_INSERT [dbo].[Views] ON 

GO
INSERT [dbo].[Views] ([ItemId], [ViewName], [VirtualPath], [Template], [RouteFields], [RouteFieldsValue], [Parameters], [ParametersValue], [SiteId], [AddedDate], [ModifiedDate]) VALUES (1, N'Test', N'Test.cshtml', NULL, N'["id","name"]', N'["1","d"]', N'["id","name"]', N'["1","d"]', 2, CAST(N'2010-01-01' AS Date), CAST(N'2016-11-20' AS Date))
GO
INSERT [dbo].[Views] ([ItemId], [ViewName], [VirtualPath], [Template], [RouteFields], [RouteFieldsValue], [Parameters], [ParametersValue], [SiteId], [AddedDate], [ModifiedDate]) VALUES (4, N'Weather', N'Weather.cshtml', NULL, N'["LocationName"]', N'["Tehran"]', N'[""]', N'[""]', 2, CAST(N'2016-10-28' AS Date), CAST(N'2016-10-29' AS Date))
GO
INSERT [dbo].[Views] ([ItemId], [ViewName], [VirtualPath], [Template], [RouteFields], [RouteFieldsValue], [Parameters], [ParametersValue], [SiteId], [AddedDate], [ModifiedDate]) VALUES (5, N'TabContent', N'TabContent.cshtml', NULL, N'["GroupName","TabHeaderBackground","TabHeaderForeColor","ActiveTabHeaderBackground","ActiveTabHeaderBorderWidth","ActiveTabHeaderBorderColor","ActiveTabHeaderForeColor","TabBorderWidth","TabBorderColor","TabContentBackground","Animation","Duration","TabContentForeColor"]', N'["Test","#FFF","#000","#EEE","1","#CCC","#000","1","#CCC","#FFF","default","500","#000"]', N'[]', N'[]', 2, CAST(N'2016-10-29' AS Date), CAST(N'2016-10-29' AS Date))
GO
INSERT [dbo].[Views] ([ItemId], [ViewName], [VirtualPath], [Template], [RouteFields], [RouteFieldsValue], [Parameters], [ParametersValue], [SiteId], [AddedDate], [ModifiedDate]) VALUES (6, N'BootstrapSlider', N'BootstrapSlider.cshtml', NULL, N'["GroupName","Pagger","Navigation","Interval"]', N'["Test","false","false","5000"]', N'[]', N'[]', 2, CAST(N'2016-10-30' AS Date), CAST(N'2016-10-30' AS Date))
GO
INSERT [dbo].[Views] ([ItemId], [ViewName], [VirtualPath], [Template], [RouteFields], [RouteFieldsValue], [Parameters], [ParametersValue], [SiteId], [AddedDate], [ModifiedDate]) VALUES (7, N'Gallery', N'Gallery.cshtml', NULL, N'["GroupName","Width","Height","BorderWidth","BorderColor","BorderRadius"]', N'["test","150","150","1","#ccc","3"]', N'[]', N'[]', 2, CAST(N'2016-10-31' AS Date), CAST(N'2016-10-31' AS Date))
GO
INSERT [dbo].[Views] ([ItemId], [ViewName], [VirtualPath], [Template], [RouteFields], [RouteFieldsValue], [Parameters], [ParametersValue], [SiteId], [AddedDate], [ModifiedDate]) VALUES (8, N'GridGallery', N'GridGallery.cshtml', NULL, N'["GroupName","Width","ItemBackground","BorderColor","BorderWidth","BorderRadius","ForeColor"]', N'["test","150","#fff","#ccc","1","1","#000"]', N'[]', N'[]', 2, CAST(N'2016-10-31' AS Date), NULL)
GO
INSERT [dbo].[Views] ([ItemId], [ViewName], [VirtualPath], [Template], [RouteFields], [RouteFieldsValue], [Parameters], [ParametersValue], [SiteId], [AddedDate], [ModifiedDate]) VALUES (9, N'Accordion', N'Accordion.cshtml', NULL, N'["GroupName","BorderColor","BorderWidth","ForeColor","ContentBorderColor"]', N'["test","#ccc","1","#000","#fff"]', N'[]', N'[]', 2, CAST(N'2016-10-31' AS Date), CAST(N'2016-10-31' AS Date))
GO
INSERT [dbo].[Views] ([ItemId], [ViewName], [VirtualPath], [Template], [RouteFields], [RouteFieldsValue], [Parameters], [ParametersValue], [SiteId], [AddedDate], [ModifiedDate]) VALUES (10, N'Faq', N'Faq.cshtml', NULL, N'["GroupName","Title","HeaderBackground","HeaderForeColor","QuestionBackground","QuestionForeColor","AnswerBackground","AnswerForeColor"]', N'["test","_","#fff","#000","#fff","#000","#fff","#000"]', N'[]', N'[]', 2, CAST(N'2016-10-31' AS Date), CAST(N'2016-10-31' AS Date))
GO
INSERT [dbo].[Views] ([ItemId], [ViewName], [VirtualPath], [Template], [RouteFields], [RouteFieldsValue], [Parameters], [ParametersValue], [SiteId], [AddedDate], [ModifiedDate]) VALUES (1010, N'Calculator', N'Calculator.cshtml', NULL, N'[]', N'[]', N'[]', N'[]', 2, CAST(N'2016-10-31' AS Date), NULL)
GO
INSERT [dbo].[Views] ([ItemId], [ViewName], [VirtualPath], [Template], [RouteFields], [RouteFieldsValue], [Parameters], [ParametersValue], [SiteId], [AddedDate], [ModifiedDate]) VALUES (1011, N'Dictionary', N'Dictionary.cshtml', NULL, N'[]', N'[]', N'[]', N'[]', 2, CAST(N'2016-10-31' AS Date), NULL)
GO
INSERT [dbo].[Views] ([ItemId], [ViewName], [VirtualPath], [Template], [RouteFields], [RouteFieldsValue], [Parameters], [ParametersValue], [SiteId], [AddedDate], [ModifiedDate]) VALUES (1012, N'Phonebook', N'Phonebook.cshtml', NULL, N'[]', N'[]', N'[]', N'[]', 2, CAST(N'2016-10-31' AS Date), NULL)
GO
INSERT [dbo].[Views] ([ItemId], [ViewName], [VirtualPath], [Template], [RouteFields], [RouteFieldsValue], [Parameters], [ParametersValue], [SiteId], [AddedDate], [ModifiedDate]) VALUES (1013, N'TabNews', N'TabNews.cshtml', NULL, N'["BorderWidth","BorderColor","BorderRadius","HeaderBorderWidth","HeaderBorderColor","HeaderBackgroundColor","HeaderFontColor","BackgroundColor","HeaderBorderRadius","DetailPage"]', N'["1","#ccc","1","1","#ccc","#fff","#000","#fff","1","detail"]', N'[]', N'[]', 2, CAST(N'2016-11-01' AS Date), NULL)
GO
INSERT [dbo].[Views] ([ItemId], [ViewName], [VirtualPath], [Template], [RouteFields], [RouteFieldsValue], [Parameters], [ParametersValue], [SiteId], [AddedDate], [ModifiedDate]) VALUES (1014, N'NewsDetail', N'NewsDetail.cshtml', NULL, N'["SearchTagPage"]', N'["newssearch"]', N'[]', N'[]', 2, CAST(N'2016-11-01' AS Date), NULL)
GO
INSERT [dbo].[Views] ([ItemId], [ViewName], [VirtualPath], [Template], [RouteFields], [RouteFieldsValue], [Parameters], [ParametersValue], [SiteId], [AddedDate], [ModifiedDate]) VALUES (1015, N'TickerNews', N'TickerNews.cshtml', NULL, N'["BorderRadius","BackgroundColor","HeaderFontColor","HeaderBackgroundColor","FontColor","ChangeSpeed","BorderWidth","BorderColor","Title","DetailPage","GroupName"]', N'["1","#fff","#000","#fff","#000","4000","1","#ccc","TickerNews","detail","test"]', N'[]', N'[]', 2, CAST(N'2016-11-02' AS Date), CAST(N'2016-11-02' AS Date))
GO
INSERT [dbo].[Views] ([ItemId], [ViewName], [VirtualPath], [Template], [RouteFields], [RouteFieldsValue], [Parameters], [ParametersValue], [SiteId], [AddedDate], [ModifiedDate]) VALUES (1016, N'TickerNewsDetail', N'TickerNewsDetail.cshtml', NULL, N'["SearchTagPage"]', N'["newssearch"]', N'[]', N'[]', 2, CAST(N'2016-11-02' AS Date), NULL)
GO
INSERT [dbo].[Views] ([ItemId], [ViewName], [VirtualPath], [Template], [RouteFields], [RouteFieldsValue], [Parameters], [ParametersValue], [SiteId], [AddedDate], [ModifiedDate]) VALUES (1017, N'BoxNews', N'BoxNews.cshtml', NULL, N'["GroupName","DetailPage","Title","NewsPerPage","Autoplay","PauseOnHover","Direction","NewsTickerInterval"]', N'["test","detail","BoxNews","5","true","true","up","4000"]', N'[]', N'[]', 2, CAST(N'2016-11-02' AS Date), CAST(N'2016-11-02' AS Date))
GO
INSERT [dbo].[Views] ([ItemId], [ViewName], [VirtualPath], [Template], [RouteFields], [RouteFieldsValue], [Parameters], [ParametersValue], [SiteId], [AddedDate], [ModifiedDate]) VALUES (1018, N'BoxNewsDetail', N'BoxNewsDetail.cshtml', NULL, N'["SearchTagPage"]', N'["newssearch"]', N'[]', N'[]', 2, CAST(N'2016-11-02' AS Date), NULL)
GO
INSERT [dbo].[Views] ([ItemId], [ViewName], [VirtualPath], [Template], [RouteFields], [RouteFieldsValue], [Parameters], [ParametersValue], [SiteId], [AddedDate], [ModifiedDate]) VALUES (1019, N'Person', N'Person.cshtml', NULL, N'[]', N'[]', N'[]', N'[]', 3, CAST(N'2016-11-07' AS Date), CAST(N'2016-11-20' AS Date))
GO
SET IDENTITY_INSERT [dbo].[Views] OFF
GO
SET IDENTITY_INSERT [dbo].[ZoneContent] ON 

GO
INSERT [dbo].[ZoneContent] ([ItemId], [ZoneName], [OrderId], [Hidden], [RouteFields], [RouteFieldsValue], [HtmlString], [ViewId], [ModuleId], [PageId], [AddedDate], [ModifiedDate]) VALUES (1, N'Content', 0, 0, N'[]', N'[]', N'dddd', NULL, NULL, 6, CAST(N'2016-11-05' AS Date), CAST(N'2016-11-07' AS Date))
GO
INSERT [dbo].[ZoneContent] ([ItemId], [ZoneName], [OrderId], [Hidden], [RouteFields], [RouteFieldsValue], [HtmlString], [ViewId], [ModuleId], [PageId], [AddedDate], [ModifiedDate]) VALUES (2, N'Content', 0, 0, N'[]', N'[]', NULL, 1019, NULL, 12, CAST(N'2016-11-05' AS Date), CAST(N'2016-11-07' AS Date))
GO
SET IDENTITY_INSERT [dbo].[ZoneContent] OFF
GO
ALTER TABLE [dbo].[CustomerPermission]  WITH NOCHECK ADD  CONSTRAINT [FK_CustomerPermission_Customer] FOREIGN KEY([CustomerId])
REFERENCES [dbo].[Customer] ([ItemId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[CustomerPermission] NOCHECK CONSTRAINT [FK_CustomerPermission_Customer]
GO
ALTER TABLE [dbo].[CustomerPermission]  WITH NOCHECK ADD  CONSTRAINT [FK_CustomerPermission_Permission] FOREIGN KEY([PermissionId])
REFERENCES [dbo].[Permission] ([ItemId])
GO
ALTER TABLE [dbo].[CustomerPermission] NOCHECK CONSTRAINT [FK_CustomerPermission_Permission]
GO
ALTER TABLE [dbo].[CustomerPosition]  WITH NOCHECK ADD  CONSTRAINT [FK_CustomerPosition_Customer] FOREIGN KEY([CustomerId])
REFERENCES [dbo].[Customer] ([ItemId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[CustomerPosition] NOCHECK CONSTRAINT [FK_CustomerPosition_Customer]
GO
ALTER TABLE [dbo].[CustomerPosition]  WITH NOCHECK ADD  CONSTRAINT [FK_CustomerPosition_Position] FOREIGN KEY([PositionId])
REFERENCES [dbo].[Position] ([ItemId])
GO
ALTER TABLE [dbo].[CustomerPosition] NOCHECK CONSTRAINT [FK_CustomerPosition_Position]
GO
ALTER TABLE [dbo].[CustomerRole]  WITH NOCHECK ADD  CONSTRAINT [FK_CustomerRole_Customer] FOREIGN KEY([CustomerId])
REFERENCES [dbo].[Customer] ([ItemId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[CustomerRole] NOCHECK CONSTRAINT [FK_CustomerRole_Customer]
GO
ALTER TABLE [dbo].[CustomerRole]  WITH NOCHECK ADD  CONSTRAINT [FK_CustomerRole_Role] FOREIGN KEY([RoleId])
REFERENCES [dbo].[Role] ([ItemId])
GO
ALTER TABLE [dbo].[CustomerRole] NOCHECK CONSTRAINT [FK_CustomerRole_Role]
GO
ALTER TABLE [dbo].[Page]  WITH NOCHECK ADD  CONSTRAINT [FK_Page_Site] FOREIGN KEY([SiteId])
REFERENCES [dbo].[Site] ([ItemId])
GO
ALTER TABLE [dbo].[Page] NOCHECK CONSTRAINT [FK_Page_Site]
GO
ALTER TABLE [dbo].[RolePermission]  WITH NOCHECK ADD  CONSTRAINT [FK_RolePermission_Permission] FOREIGN KEY([PermissionId])
REFERENCES [dbo].[Permission] ([ItemId])
GO
ALTER TABLE [dbo].[RolePermission] NOCHECK CONSTRAINT [FK_RolePermission_Permission]
GO
ALTER TABLE [dbo].[RolePermission]  WITH NOCHECK ADD  CONSTRAINT [FK_RolePermission_Role] FOREIGN KEY([RoleId])
REFERENCES [dbo].[Role] ([ItemId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[RolePermission] NOCHECK CONSTRAINT [FK_RolePermission_Role]
GO
ALTER TABLE [dbo].[Site]  WITH NOCHECK ADD  CONSTRAINT [FK_Site_Membership] FOREIGN KEY([MembershipId])
REFERENCES [dbo].[Membership] ([ItemId])
GO
ALTER TABLE [dbo].[Site] NOCHECK CONSTRAINT [FK_Site_Membership]
GO
ALTER TABLE [dbo].[Site]  WITH NOCHECK ADD  CONSTRAINT [FK_Site_Repository] FOREIGN KEY([RepositoryId])
REFERENCES [dbo].[Repository] ([ItemId])
GO
ALTER TABLE [dbo].[Site] NOCHECK CONSTRAINT [FK_Site_Repository]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "CustomerPermission"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 125
               Right = 198
            End
            DisplayFlags = 280
            TopColumn = 2
         End
         Begin Table = "Permission"
            Begin Extent = 
               Top = 6
               Left = 236
               Bottom = 125
               Right = 396
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'CustomerPermissionView'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'CustomerPermissionView'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "CustomerPosition"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 125
               Right = 198
            End
            DisplayFlags = 280
            TopColumn = 1
         End
         Begin Table = "Position"
            Begin Extent = 
               Top = 6
               Left = 236
               Bottom = 125
               Right = 396
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'CustomerPositionView'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'CustomerPositionView'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "CustomerRole"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 125
               Right = 198
            End
            DisplayFlags = 280
            TopColumn = 1
         End
         Begin Table = "Role"
            Begin Extent = 
               Top = 6
               Left = 236
               Bottom = 125
               Right = 396
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'CustomerRoleView'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'CustomerRoleView'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Folder"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 135
               Right = 208
            End
            DisplayFlags = 280
            TopColumn = 1
         End
         Begin Table = "FolderType"
            Begin Extent = 
               Top = 6
               Left = 246
               Bottom = 135
               Right = 416
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Site"
            Begin Extent = 
               Top = 6
               Left = 454
               Bottom = 135
               Right = 626
            End
            DisplayFlags = 280
            TopColumn = 1
         End
         Begin Table = "Content"
            Begin Extent = 
               Top = 6
               Left = 664
               Bottom = 125
               Right = 825
            End
            DisplayFlags = 280
            TopColumn = 9
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'FolderView'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'FolderView'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "RolePermission"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 125
               Right = 198
            End
            DisplayFlags = 280
            TopColumn = 1
         End
         Begin Table = "Permission"
            Begin Extent = 
               Top = 6
               Left = 236
               Bottom = 125
               Right = 396
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'RolePermissionView'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'RolePermissionView'
GO
