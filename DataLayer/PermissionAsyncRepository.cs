﻿namespace DataLayer
{

    #region Assembly

    using System.Threading.Tasks;
    using System.Collections.Generic;
    using Dapper;
    using ORM.Model;
    using Utility;
    using static Dapper.SqlBuilder;

    #endregion

    public class PermissionAsyncRepository : DynamicFilterAsyncRepository
    {

        #region Methods

        public static async Task<IEnumerable<CustomerPermissions>> GetList(string condition, dynamic parameter)
        {
            OpenConnection();
            var builder = new SqlBuilder();
            Template template = builder.AddTemplate(@"
SELECT
    [CustomerRole].[CustomerID], [RolePermission].PermissionID, Permission.DisplayName, Permission.DevName
FROM
    [CustomerRole]
CROSS JOIN
    [RolePermission]
INNER JOIN
    Permission
ON
    RolePermission.PermissionId = Permission.ItemID
WHERE
    [CustomerRole].RoleId = [RolePermission].RoleId AND
    [RolePermission].PermissionId NOT IN (
		SELECT
			[CustomerPermission].PermissionID
		FROM
			[CustomerPermission]
		WHERE
			[CustomerPermission].IsHave = 0" + (CommonUtil.IsNotEmpty(condition) ? " AND " + condition : string.Empty) + @"
	)" + (CommonUtil.IsNotEmpty(condition) ? " AND " + condition : string.Empty) + @"
UNION ALL
SELECT
    [CustomerPermission].[CustomerID], [CustomerPermission].PermissionID, Permission.DisplayName, Permission.DevName
FROM
    [CustomerPermission]
INNER JOIN
	Permission
ON
	CustomerPermission.PermissionId = Permission.ItemID
WHERE
    [CustomerPermission].IsHave = 1" + (CommonUtil.IsNotEmpty(condition) ? " AND " + condition : string.Empty), parameter);

            return await Connection.QueryAsync<CustomerPermissions>(CommonUtil.PersianFix(template.RawSql), template.Parameters);
        }

        public static async Task<CustomerPermissions> Get(string condition, dynamic parameter)
        {
            OpenConnection();
            var builder = new SqlBuilder();
            Template template = builder.AddTemplate(@"
SELECT
    [CustomerRole].[CustomerID], [RolePermission].PermissionID, Permission.DisplayName, Permission.DevName
FROM
    [CustomerRole]
CROSS JOIN
    [RolePermission]
INNER JOIN
    Permission
ON
    RolePermission.PermissionId = Permission.ItemID
WHERE
    [CustomerRole].RoleId = [RolePermission].RoleId AND
    [RolePermission].PermissionId NOT IN (
		SELECT
			[CustomerPermission].PermissionID
		FROM
			[CustomerPermission]
		WHERE
			[CustomerPermission].IsHave = 0" + (CommonUtil.IsNotEmpty(condition) ? " AND " + condition : string.Empty) + @"
	)" + (CommonUtil.IsNotEmpty(condition) ? " AND " + condition : string.Empty) + @"
UNION ALL
SELECT
    [CustomerPermission].[CustomerID], [CustomerPermission].PermissionID, Permission.DisplayName, Permission.DevName
FROM
    [CustomerPermission]
INNER JOIN
	Permission
ON
	CustomerPermission.PermissionId = Permission.ItemID
WHERE
    [CustomerPermission].IsHave = 1" + (CommonUtil.IsNotEmpty(condition) ? " AND " + condition : string.Empty), parameter);

            return await Connection.QueryFirstOrDefaultAsync<CustomerPermissions>(CommonUtil.PersianFix(template.RawSql), template.Parameters);
        }

        #endregion

    }
}
