﻿namespace DataLayer
{

    #region Assembly

    using System.Collections.Generic;
    using System.Linq;
    using ORM.Condition;
    using ORM.Model;
    using System;
    using Utility;
    using ORM.Enums;

    #endregion

    /// <summary>
    /// از این کلاس برای مدیریت مجوزهای کاربران و سمت ها و نقش های آن استفاده می شود
    /// </summary>
    public class MembershipRepository
    {

        #region Attribute

        private static MembershipRepository _current;

        #endregion

        #region Property

        /// <summary>
        /// نسخه ایستا از کلاس
        /// </summary>
        public static MembershipRepository Current
        {
            get
            {
                if (_current != null)
                    return _current;
                else
                    return _current = new MembershipRepository();
            }
        }

        #endregion

        #region Method

        /// <summary>
        /// اضافه کردن مجوز به نقش
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// MembershipRepository.Current.AddRolePermission(1, 2);
        /// </code>
        /// </example>
        /// <param name="roleId">: int</param>
        /// <param name="permissionId">: int</param>
        /// <returns>bool</returns>
        public bool AddRolePermission(int roleId, int permissionId)
        {
            try
            {
                if (DynamicFilterRepository.Current.CreateItem(new RolePermission { RoleId = roleId, PermissionId = permissionId, AddedDate = DateTime.Today }) != -1)
                    return true;
            }
            catch (Exception ex) { Logger.Log(ex); }
            return false;
        }

        /// <summary>
        /// اضافه کردن مجوز به نقش
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// MembershipRepository.Current.AddRolePermission(1, "ورود به سیستم");
        /// </code>
        /// </example>
        /// <param name="roleId">: int</param>
        /// <param name="displayName">: string</param>
        /// <returns>bool</returns>
        public bool AddRolePermission(int roleId, string displayName)
        {
            try
            {
                var item = DynamicFilterRepository.Current.GetItem<Permission>(new FilterExpressionList(new[] { new Filter("DisplayName", SqlOperators.Equal, displayName) }));
                if (item != null)
                {
                    var permissionId = item.ItemId;
                    if (DynamicFilterRepository.Current.CreateItem(new RolePermission { RoleId = roleId, PermissionId = permissionId, AddedDate = DateTime.Today }) != -1)
                        return true;
                }
            }
            catch (Exception ex) { Logger.Log(ex); }
            return false;
        }

        /// <summary>
        /// اضافه کردن مجوز به نقش
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// MembershipRepository.Current.AddRolePermissionExtra(1, "Login");
        /// </code>
        /// </example>
        /// <param name="roleId">: int</param>
        /// <param name="devName">: string</param>
        /// <returns>bool</returns>
        public bool AddRolePermissionExtra(int roleId, string devName)
        {
            try
            {
                var item = DynamicFilterRepository.Current.GetItem<Permission>(new FilterExpressionList(new[] { new Filter("DevName", SqlOperators.Equal, devName) }));
                if (item != null)
                {
                    var permissionId = item.ItemId;
                    if (DynamicFilterRepository.Current.CreateItem(new RolePermission { RoleId = roleId, PermissionId = permissionId, AddedDate = DateTime.Today }) != -1)
                        return true;
                }
            }
            catch (Exception ex) { Logger.Log(ex); }
            return false;
        }

        /// <summary>
        /// اضافه کردن مجوز به کاربر
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// MembershipRepository.Current.AddUserPermission(1, 2);
        /// </code>
        /// </example>
        /// <param name="customerId">: int</param>
        /// <param name="permissionId">: int</param>
        /// <returns>bool</returns>
        public bool AddUserPermission(int customerId, int permissionId)
        {
            try
            {
                var item = DynamicFilterRepository.Current.GetItem<CustomerPermission>(new FilterExpressionList(new[] { new Filter("CustomerId", SqlOperators.Equal, customerId), new Filter("PermissionId", SqlOperators.Equal, permissionId), new Filter("IsHave", SqlOperators.Equal, false) }));
                if (item != null)
                {
                    return DynamicFilterRepository.Current.DeleteItem<CustomerPermission>(item.ItemId);
                }
                if (DynamicFilterRepository.Current.CreateItem(new CustomerPermission { CustomerId = customerId, PermissionId = permissionId, IsHave = Toggle.True, AddedDate = DateTime.Today }) != -1)
                    return true;
            }
            catch (Exception ex) { Logger.Log(ex); }
            return false;
        }

        /// <summary>
        /// اضافه کردن مجوز به کاربر
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// MembershipRepository.Current.AddUserPermission(1, "ورود به سیستم");
        /// </code>
        /// </example>
        /// <param name="customerId">: int</param>
        /// <param name="displayName">: string</param>
        /// <returns>bool</returns>
        public bool AddUserPermission(int customerId, string displayName)
        {
            try
            {
                var permissionItem = DynamicFilterRepository.Current.GetItem<Permission>(new FilterExpressionList(new[] { new Filter("DisplayName", SqlOperators.Equal, displayName) }));
                if (permissionItem != null)
                {
                    var permissionId = permissionItem.ItemId;
                    var item = DynamicFilterRepository.Current.GetItem<CustomerPermission>(new FilterExpressionList(new[] { new Filter("CustomerId", SqlOperators.Equal, customerId), new Filter("PermissionId", SqlOperators.Equal, permissionId), new Filter("IsHave", SqlOperators.Equal, false) }));
                    if (item != null)
                    {
                        return DynamicFilterRepository.Current.DeleteItem<CustomerPermission>(item.ItemId);
                    }
                    if (DynamicFilterRepository.Current.CreateItem(new CustomerPermission { CustomerId = customerId, PermissionId = permissionId, IsHave = Toggle.True, AddedDate = DateTime.Today }) != -1)
                        return true;
                }
            }
            catch (Exception ex) { Logger.Log(ex); }
            return false;
        }

        /// <summary>
        /// اضافه کردن مجوز به کاربر
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// MembershipRepository.Current.AddUserPermissionExtra(1, "Login");
        /// </code>
        /// </example>
        /// <param name="customerId">: int</param>
        /// <param name="devName">: string</param>
        /// <returns>bool</returns>
        public bool AddUserPermissionExtra(int customerId, string devName)
        {
            try
            {
                var permissionItem = DynamicFilterRepository.Current.GetItem<Permission>(new FilterExpressionList(new[] { new Filter("DevName", SqlOperators.Equal, devName) }));
                if (permissionItem != null)
                {
                    var permissionId = permissionItem.ItemId;
                    var item = DynamicFilterRepository.Current.GetItem<CustomerPermission>(new FilterExpressionList(new[] { new Filter("CustomerId", SqlOperators.Equal, customerId), new Filter("PermissionId", SqlOperators.Equal, permissionId), new Filter("IsHave", SqlOperators.Equal, false) }));
                    if (item != null)
                    {
                        return DynamicFilterRepository.Current.DeleteItem<CustomerPermission>(item.ItemId);
                    }
                    if (DynamicFilterRepository.Current.CreateItem(new CustomerPermission { CustomerId = customerId, PermissionId = permissionId, IsHave = Toggle.True, AddedDate = DateTime.Today }) != -1)
                        return true;
                }
            }
            catch (Exception ex) { Logger.Log(ex); }
            return false;
        }

        /// <summary>
        /// گرفتن مجوز از کاربر
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// MembershipRepository.Current.DeleteUserPermission(1, 2);
        /// </code>
        /// </example>
        /// <param name="customerId">: int</param>
        /// <param name="permissionId">: int</param>
        /// <returns>bool</returns>
        public bool DeleteUserPermission(int customerId, int permissionId)
        {
            try
            {
                var item = DynamicFilterRepository.Current.GetItem<CustomerPermission>(new FilterExpressionList(new[] { new Filter("CustomerId", SqlOperators.Equal, customerId), new Filter("PermissionId", SqlOperators.Equal, permissionId), new Filter("IsHave", SqlOperators.Equal, true) }));
                if (item != null)
                {
                    return DynamicFilterRepository.Current.DeleteItem<CustomerPermission>(item.ItemId);
                }
                if (DynamicFilterRepository.Current.CreateItem(new CustomerPermission { CustomerId = customerId, PermissionId = permissionId, IsHave = Toggle.False, AddedDate = DateTime.Today }) != -1)
                    return true;
            }
            catch (Exception ex) { Logger.Log(ex); }
            return false;
        }

        /// <summary>
        /// گرفتن مجوز از کاربر
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// MembershipRepository.Current.DeleteUserPermission(1, "ورود به سیستم");
        /// </code>
        /// </example>
        /// <param name="customerId">: int</param>
        /// <param name="displayName">: string</param>
        /// <returns>bool</returns>
        public bool DeleteUserPermission(int customerId, string displayName)
        {
            try
            {
                var permissionItem = DynamicFilterRepository.Current.GetItem<Permission>(new FilterExpressionList(new[] { new Filter("DisplayName", SqlOperators.Equal, displayName) }));
                if (permissionItem != null)
                {
                    var permissionId = permissionItem.ItemId;
                    var item = DynamicFilterRepository.Current.GetItem<CustomerPermission>(new FilterExpressionList(new[] { new Filter("CustomerId", SqlOperators.Equal, customerId), new Filter("PermissionId", SqlOperators.Equal, permissionId), new Filter("IsHave", SqlOperators.Equal, true) }));
                    if (item != null)
                    {
                        return DynamicFilterRepository.Current.DeleteItem<CustomerPermission>(item.ItemId);
                    }
                    if (DynamicFilterRepository.Current.CreateItem(new CustomerPermission { CustomerId = customerId, PermissionId = permissionId, IsHave = Toggle.False, AddedDate = DateTime.Today }) != -1)
                        return true;
                }
            }
            catch (Exception ex) { Logger.Log(ex); }
            return false;
        }

        /// <summary>
        /// گرفتن مجوز از کاربر
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// MembershipRepository.Current.DeleteUserPermissionExtra(1, "Login");
        /// </code>
        /// </example>
        /// <param name="customerId">: int</param>
        /// <param name="devName">: string</param>
        /// <returns>bool</returns>
        public bool DeleteUserPermissionExtra(int customerId, string devName)
        {
            try
            {
                var permissionItem = DynamicFilterRepository.Current.GetItem<Permission>(new FilterExpressionList(new[] { new Filter("DevName", SqlOperators.Equal, devName) }));
                if (permissionItem != null)
                {
                    var permissionId = permissionItem.ItemId;
                    var item = DynamicFilterRepository.Current.GetItem<CustomerPermission>(new FilterExpressionList(new[] { new Filter("CustomerId", SqlOperators.Equal, customerId), new Filter("PermissionId", SqlOperators.Equal, permissionId), new Filter("IsHave", SqlOperators.Equal, true) }));
                    if (item != null)
                    {
                        return DynamicFilterRepository.Current.DeleteItem<CustomerPermission>(item.ItemId);
                    }
                    if (DynamicFilterRepository.Current.CreateItem(new CustomerPermission { CustomerId = customerId, PermissionId = permissionId, IsHave = Toggle.False, AddedDate = DateTime.Today }) != -1)
                        return true;
                }
            }
            catch (Exception ex) { Logger.Log(ex); }
            return false;
        }

        /// <summary>
        /// حذف تمامی مجوزهای نقش
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// MembershipRepository.Current.DeleteRolePermissions(1);
        /// </code>
        /// </example>
        /// <param name="roleId">: int</param>
        /// <returns>int</returns>
        public int DeleteRolePermissions(int roleId)
        {
            try
            {

                return DynamicFilterRepository.Current.DeleteList<RolePermission>(new FilterExpressionList(new[] { new Filter("RoleId", SqlOperators.Equal, roleId) }));
            }
            catch (Exception ex) { Logger.Log(ex); }
            return -1;
        }

        /// <summary>
        /// حذف تمامی نقش های کاربر
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// MembershipRepository.Current.DeleteCustomerRoles(1);
        /// </code>
        /// </example>
        /// <param name="customerId">: int</param>
        /// <returns>int</returns>
        public int DeleteCustomerRoles(int customerId)
        {
            try
            {

                return DynamicFilterRepository.Current.DeleteList<CustomerRole>(new FilterExpressionList(new[] { new Filter("CustomerId", SqlOperators.Equal, customerId) }));
            }
            catch (Exception ex) { Logger.Log(ex); }
            return -1;
        }

        /// <summary>
        /// حذف تمامی سمت های کاربر
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// MembershipRepository.Current.DeleteCustomerPositions(1);
        /// </code>
        /// </example>
        /// <param name="customerId">: int</param>
        /// <returns>int</returns>
        public int DeleteCustomerPositions(int customerId)
        {
            try
            {

                return DynamicFilterRepository.Current.DeleteList<CustomerPosition>(new FilterExpressionList(new[] { new Filter("CustomerId", SqlOperators.Equal, customerId) }));
            }
            catch (Exception ex) { Logger.Log(ex); }
            return -1;
        }

        /// <summary>
        /// اضافه کردن نقش برای کاربر
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// MembershipRepository.Current.AddUserRole(1, 2);
        /// </code>
        /// </example>
        /// <param name="customerId">: int</param>
        /// <param name="roleId">: int</param>
        /// <returns>int</returns>
        public bool AddUserRole(int customerId, int roleId)
        {
            try
            {
                if (DynamicFilterRepository.Current.CreateItem(new CustomerRole { CustomerId = customerId, RoleId = roleId, AddedDate = DateTime.Today }) != -1)
                    return true;
            }
            catch (Exception ex) { Logger.Log(ex); }
            return false;
        }

        /// <summary>
        /// اضافه کردن سمت برای کاربر
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// MembershipRepository.Current.AddUserPosition(1, 2);
        /// </code>
        /// </example>
        /// <param name="customerId">: int</param>
        /// <param name="positionId">: int</param>
        /// <returns>int</returns>
        public bool AddUserPosition(int customerId, int positionId)
        {
            try
            {
                if (DynamicFilterRepository.Current.CreateItem(new CustomerPosition { CustomerId = customerId, PositionId = positionId, AddedDate = DateTime.Today }) != -1)
                    return true;
            }
            catch (Exception ex) { Logger.Log(ex); }
            return false;
        }

        /// <summary>
        /// حذف مجوز از نقش
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// MembershipRepository.Current.DeleteRolePermission(1, 2);
        /// </code>
        /// </example>
        /// <param name="roleId">: int</param>
        /// <param name="permissionId">: int</param>
        /// <returns>int</returns>
        public bool DeleteRolePermission(int roleId, int permissionId)
        {
            try
            {
                var item = DynamicFilterRepository.Current.GetItem<RolePermission>(new FilterExpressionList(new[] { new Filter("RoleId", SqlOperators.Equal, roleId), new Filter("PermissionId", SqlOperators.Equal, permissionId) }));
                if (item != null)
                {
                    return DynamicFilterRepository.Current.DeleteItem<RolePermission>(item.ItemId);
                }
            }
            catch (Exception ex) { Logger.Log(ex); }
            return false;
        }

        /// <summary>
        /// حذف مجوز از نقش
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// MembershipRepository.Current.DeleteRolePermission(1, "ورود به سیستم");
        /// </code>
        /// </example>
        /// <param name="roleId">: int</param>
        /// <param name="displayName">: int</param>
        /// <returns>int</returns>
        public bool DeleteRolePermission(int roleId, string displayName)
        {
            try
            {
                var permissionItem = DynamicFilterRepository.Current.GetItem<Permission>(new FilterExpressionList(new[] { new Filter("DisplayName", SqlOperators.Equal, displayName) }));
                if (permissionItem != null)
                {
                    var permissionId = permissionItem.ItemId;
                    var item = DynamicFilterRepository.Current.GetItem<RolePermission>(new FilterExpressionList(new[] { new Filter("RoleId", SqlOperators.Equal, roleId), new Filter("PermissionId", SqlOperators.Equal, permissionId) }));
                    if (item != null)
                    {
                        return DynamicFilterRepository.Current.DeleteItem<RolePermission>(item.ItemId);
                    }
                }
            }
            catch (Exception ex) { Logger.Log(ex); }
            return false;
        }

        /// <summary>
        /// حذف مجوز از نقش
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// MembershipRepository.Current.DeleteRolePermissionExtra(1, "Login");
        /// </code>
        /// </example>
        /// <param name="roleId">: int</param>
        /// <param name="devName">: int</param>
        /// <returns>int</returns>
        public bool DeleteRolePermissionExtra(int roleId, string devName)
        {
            try
            {
                var permissionItem = DynamicFilterRepository.Current.GetItem<Permission>(new FilterExpressionList(new[] { new Filter("DevName", SqlOperators.Equal, devName) }));
                if (permissionItem != null)
                {
                    var permissionId = permissionItem.ItemId;
                    var item = DynamicFilterRepository.Current.GetItem<RolePermission>(new FilterExpressionList(new[] { new Filter("RoleId", SqlOperators.Equal, roleId), new Filter("PermissionId", SqlOperators.Equal, permissionId) }));
                    if (item != null)
                    {
                        return DynamicFilterRepository.Current.DeleteItem<RolePermission>(item.ItemId);
                    }
                }
            }
            catch (Exception ex) { Logger.Log(ex); }
            return false;
        }

        /// <summary>
        /// دریافت اطلاعات کاربر بر اساس نام کاربری
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// MembershipRepository.Current.GetUser("John Doue");
        /// </code>
        /// </example>
        /// <param name="username">: string</param>
        /// <returns>Customer</returns>
        public Customer GetUser(string username)
        {
            try
            {
                var item = DynamicFilterRepository.Current.GetItem<Customer>(new FilterExpressionList(new[] { new Filter("Username", SqlOperators.Equal, username) }));
                if (item != null)
                    return item;
            }
            catch (Exception ex) { Logger.Log(ex); }
            return null;
        }

        /// <summary>
        /// بررسی صحت نام کاربری و رمزعبور
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// MembershipRepository.Current.Login("John Doue", "******");
        /// </code>
        /// </example>
        /// <param name="username">: string</param>
        /// <param name="password">: string</param>
        /// <returns>bool</returns>
        public bool Login(string username, string password)
        {
            try
            {
                var item = DynamicFilterRepository.Current.GetItem<Customer>(new FilterExpressionList(new[] { new Filter("Username", SqlOperators.Equal, username), new Filter("Password", SqlOperators.Equal, password) }));
                if (item != null)
                    return true;
            }
            catch (Exception ex) { Logger.Log(ex); }
            return false;
        }

        /// <summary>
        /// بررسی دارا بودن مجوز برای نقش خاص
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// MembershipRepository.Current.RoleHasPermission(1, 2);
        /// </code>
        /// </example>
        /// <param name="roleId">: int</param>
        /// <param name="permissionId">: int</param>
        /// <returns>bool</returns>
        public bool RoleHasPermission(int roleId, int permissionId)
        {
            try
            {
                var item = DynamicFilterRepository.Current.GetItem<RolePermission>(new FilterExpressionList(new[] { new Filter("RoleId", SqlOperators.Equal, roleId), new Filter("PermissionId", SqlOperators.Equal, permissionId) }));
                if (item != null)
                    return true;
            }
            catch (Exception ex) { Logger.Log(ex); }
            return false;
        }

        /// <summary>
        /// بررسی دارا بودن مجوز برای نقش خاص
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// MembershipRepository.Current.RoleHasPermission(1, "ورود به سیستم");
        /// </code>
        /// </example>
        /// <param name="roleId">: int</param>
        /// <param name="displayName">: string</param>
        /// <returns>bool</returns>
        public bool RoleHasPermission(int roleId, string displayName)
        {
            try
            {
                var item = DynamicFilterRepository.Current.GetItem<RolePermissionView>(new FilterExpressionList(new[] { new Filter("RoleId", SqlOperators.Equal, roleId), new Filter("PermissionDisplayName", SqlOperators.Equal, displayName) }));
                if (item != null)
                    return true;
            }
            catch (Exception ex) { Logger.Log(ex); }
            return false;
        }

        /// <summary>
        /// بررسی دارا بودن مجوز برای نقش خاص
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// MembershipRepository.Current.RoleHasPermissionExtra(1, "Login");
        /// </code>
        /// </example>
        /// <param name="roleId">: int</param>
        /// <param name="devName">: string</param>
        /// <returns>bool</returns>
        public bool RoleHasPermissionExtra(int roleId, string devName)
        {
            try
            {
                var item = DynamicFilterRepository.Current.GetItem<RolePermissionView>(new FilterExpressionList(new[] { new Filter("RoleId", SqlOperators.Equal, roleId), new Filter("PermissionDevName", SqlOperators.Equal, devName) }));
                if (item != null)
                    return true;
            }
            catch (Exception ex) { Logger.Log(ex); }
            return false;
        }

        /// <summary>
        /// بررسی دارا بودن مجوز برای کاربر خاص
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// MembershipRepository.Current.UserHasPermission(1, 2);
        /// </code>
        /// </example>
        /// <param name="customerId">: int</param>
        /// <param name="permissionId">: int</param>
        /// <returns>bool</returns>
        public bool UserHasPermission(int customerId, int permissionId)
        {
            try
            {
                var filters = new FilterExpressionList(new[] { new Filter("CustomerId", SqlOperators.Equal, customerId), new Filter("PermissionId", SqlOperators.Equal, permissionId) });
                var item = PermissionRepository.Current.GetList(filters.FilterString, filters.FilterValueParameter).First();
                if (item != null)
                    return true;
            }
            catch (Exception ex) { Logger.Log(ex); }
            return false;
        }

        /// <summary>
        /// بررسی دارا بودن مجوز برای کاربر خاص
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// MembershipRepository.Current.UserHasPermission(1, "ورود به سیستم");
        /// </code>
        /// </example>
        /// <param name="customerId">: int</param>
        /// <param name="displayName">: string</param>
        /// <returns>bool</returns>
        public bool UserHasPermission(int customerId, string displayName)
        {
            try
            {
                var filters = new FilterExpressionList(new[] { new Filter("CustomerId", SqlOperators.Equal, customerId), new Filter("DisplayName", SqlOperators.Equal, displayName) });
                var item = PermissionRepository.Current.GetList(filters.FilterString, filters.FilterValueParameter).First();
                if (item != null)
                    return true;
            }
            catch (Exception ex) { Logger.Log(ex); }
            return false;
        }

        /// <summary>
        /// بررسی دارا بودن مجوز برای کاربر خاص
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// MembershipRepository.Current.UserHasPermissionExtra(1, "Login");
        /// </code>
        /// </example>
        /// <param name="customerId">: int</param>
        /// <param name="devName">: string</param>
        /// <returns>bool</returns>
        public bool UserHasPermissionExtra(int customerId, string devName)
        {
            try
            {
                var filters = new FilterExpressionList(new[] { new Filter("CustomerId", SqlOperators.Equal, customerId), new Filter("DevName", SqlOperators.Equal, devName) });
                var item = PermissionRepository.Current.GetList(filters.FilterString, filters.FilterValueParameter).First();
                if (item != null)
                    return true;
            }
            catch (Exception ex) { Logger.Log(ex); }
            return false;
        }

        /// <summary>
        /// دریافت مجوزهای اضافه کاربر
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// MembershipRepository.Current.UserPermissions(1);
        /// </code>
        /// </example>
        /// <param name="customerId">: int</param>
        /// <returns>IEnumerable&lt;CustomerPermissionView&gt;</returns>
        public IEnumerable<CustomerPermissionView> UserPermissions(int customerId)
        {
            try
            {
                return DynamicFilterRepository.Current.GetList<CustomerPermissionView>(new FilterExpressionList(new[] { new Filter("CustomerId", SqlOperators.Equal, customerId) }));
            }
            catch (Exception ex) { Logger.Log(ex); }
            return null;
        }

        /// <summary>
        /// دریافت مجوزهای کاربر
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// MembershipRepository.Current.UserExistPermissions(1);
        /// </code>
        /// </example>
        /// <param name="customerId">: int</param>
        /// <returns>IEnumerable&lt;CustomerPermissions&gt;</returns>
        public IEnumerable<CustomerPermissions> UserExistPermissions(int customerId)
        {
            try
            {
                var filters = new FilterExpressionList(new[] { new Filter("CustomerId", SqlOperators.Equal, customerId) });
                return PermissionRepository.Current.GetList(filters.FilterString, filters.FilterValueParameter);
            }
            catch (Exception ex) { Logger.Log(ex); }
            return null;
        }

        /// <summary>
        /// دریافت مجوزهای نقش
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// MembershipRepository.Current.RolePermissions(1);
        /// </code>
        /// </example>
        /// <param name="roleId">: int</param>
        /// <returns>IEnumerable&lt;RolePermissionView&gt;</returns>
        public IEnumerable<RolePermissionView> RolePermissions(int roleId)
        {
            try
            {
                return DynamicFilterRepository.Current.GetList<RolePermissionView>(new FilterExpressionList(new[] { new Filter("RoleId", SqlOperators.Equal, roleId) }));
            }
            catch (Exception ex) { Logger.Log(ex); }
            return null;
        }

        /// <summary>
        /// دریافت سمت های کاربر
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// MembershipRepository.Current.UserPositions(1);
        /// </code>
        /// </example>
        /// <param name="customerId">: int</param>
        /// <returns>IEnumerable&lt;CustomerPositionView&gt;</returns>
        public IEnumerable<CustomerPositionView> UserPositions(int customerId)
        {
            try
            {
                return DynamicFilterRepository.Current.GetList<CustomerPositionView>(new FilterExpressionList(new[] { new Filter("CustomerId", SqlOperators.Equal, customerId) }));
            }
            catch (Exception ex) { Logger.Log(ex); }
            return null;
        }

        /// <summary>
        /// دریافت نقش های کاربر
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// MembershipRepository.Current.UserRoles(1);
        /// </code>
        /// </example>
        /// <param name="customerId">: int</param>
        /// <returns>IEnumerable&lt;CustomerRoleView&gt;</returns>
        public IEnumerable<CustomerRoleView> UserRoles(int customerId)
        {
            try
            {
                return DynamicFilterRepository.Current.GetList<CustomerRoleView>(new FilterExpressionList(new[] { new Filter("CustomerId", SqlOperators.Equal, customerId) }));
            }
            catch (Exception ex) { Logger.Log(ex); }
            return null;
        }

        /// <summary>
        /// بررسی دارا بودن سمت توسط کاربر
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// MembershipRepository.Current.UserHasPosition(1, "مدیر سیستم");
        /// </code>
        /// </example>
        /// <param name="customerId">: int</param>
        /// <param name="displayName">: string</param>
        /// <returns>bool</returns>
        public bool UserHasPosition(int customerId, string displayName)
        {
            try
            {
                var item = DynamicFilterRepository.Current.GetItem<CustomerPositionView>(new FilterExpressionList(new[] { new Filter("CustomerId", SqlOperators.Equal, customerId), new Filter("PositionDisplayName", SqlOperators.Equal, displayName) }));
                if (item != null)
                    return true;
            }
            catch (Exception ex) { Logger.Log(ex); }
            return false;
        }

        /// <summary>
        /// تخصیص سمت به کاربر
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// MembershipRepository.Current.AddUserPosition(1, "مدیر سیستم");
        /// </code>
        /// </example>
        /// <param name="customerId">: int</param>
        /// <param name="displayName">: string</param>
        /// <returns>bool</returns>
        public bool AddUserPosition(int customerId, string displayName)
        {
            try
            {
                var positionItem = DynamicFilterRepository.Current.GetItem<Position>(new FilterExpressionList(new[] { new Filter("DisplayName", SqlOperators.Equal, displayName) }));
                if (positionItem != null)
                {
                    var positionId = positionItem.ItemId;
                    if (DynamicFilterRepository.Current.CreateItem(new CustomerPosition { CustomerId = customerId, PositionId = positionId, AddedDate = DateTime.Today }) != -1)
                        return true;
                }
            }
            catch (Exception ex) { Logger.Log(ex); }
            return false;
        }

        /// <summary>
        /// حذف سمت کاربر
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// MembershipRepository.Current.DeleteUserPosition(1, "مدیر سیستم");
        /// </code>
        /// </example>
        /// <param name="customerId">: int</param>
        /// <param name="displayName">: string</param>
        /// <returns>bool</returns>
        public bool DeleteUserPosition(int customerId, string displayName)
        {
            try
            {
                var positionItem = DynamicFilterRepository.Current.GetItem<Position>(new FilterExpressionList(new[] { new Filter("DisplayName", SqlOperators.Equal, displayName) }));
                if (positionItem != null)
                {
                    var positionId = positionItem.ItemId;
                    var item = DynamicFilterRepository.Current.GetItem<CustomerPosition>(new FilterExpressionList(new[] { new Filter("CustomerId", SqlOperators.Equal, customerId), new Filter("PositionId", SqlOperators.Equal, positionId) }));
                    if (item != null)
                    {
                        return DynamicFilterRepository.Current.DeleteItem<CustomerPosition>(item.ItemId);
                    }
                }
            }
            catch (Exception ex) { Logger.Log(ex); }
            return false;
        }

        /// <summary>
        /// بررسی دارا بودن نقش برای کاربر
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// MembershipRepository.Current.UserHasRole(1, "راهبر");
        /// </code>
        /// </example>
        /// <param name="customerId">: int</param>
        /// <param name="displayName">: string</param>
        /// <returns>bool</returns>
        public bool UserHasRole(int customerId, string displayName)
        {
            try
            {
                var item = DynamicFilterRepository.Current.GetItem<CustomerRoleView>(new FilterExpressionList(new[] { new Filter("CustomerId", SqlOperators.Equal, customerId), new Filter("RoleDisplayName", SqlOperators.Equal, displayName) }));
                if (item != null)
                    return true;
            }
            catch (Exception ex) { Logger.Log(ex); }
            return false;
        }

        /// <summary>
        /// تخصیص نقش به کاربر
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// MembershipRepository.Current.AddUserRole(1, "راهبر");
        /// </code>
        /// </example>
        /// <param name="customerId">: int</param>
        /// <param name="displayName">: string</param>
        /// <returns>bool</returns>
        public bool AddUserRole(int customerId, string displayName)
        {
            try
            {
                var roleItem = DynamicFilterRepository.Current.GetItem<Role>(new FilterExpressionList(new[] { new Filter("DisplayName", SqlOperators.Equal, displayName) }));
                if (roleItem != null)
                {
                    var roleId = roleItem.ItemId;
                    if (DynamicFilterRepository.Current.CreateItem(new CustomerRole { CustomerId = customerId, RoleId = roleId, AddedDate = DateTime.Today }) != -1)
                        return true;
                }
            }
            catch (Exception ex) { Logger.Log(ex); }
            return false;
        }

        /// <summary>
        /// حذف نقش کاربر
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// MembershipRepository.Current.DeleteUserRole(1, "راهبر");
        /// </code>
        /// </example>
        /// <param name="customerId">: int</param>
        /// <param name="displayName">: string</param>
        /// <returns>bool</returns>
        public bool DeleteUserRole(int customerId, string displayName)
        {
            try
            {
                var roleItem = DynamicFilterRepository.Current.GetItem<Role>(new FilterExpressionList(new[] { new Filter("DisplayName", SqlOperators.Equal, displayName) }));
                if (roleItem != null)
                {
                    var roleId = roleItem.ItemId;
                    var item = DynamicFilterRepository.Current.GetItem<CustomerRoleView>(new FilterExpressionList(new[] { new Filter("CustomerId", SqlOperators.Equal, customerId), new Filter("RoleId", SqlOperators.Equal, roleId) }));
                    if (item != null)
                    {
                        return DynamicFilterRepository.Current.DeleteItem<CustomerRole>(item.ItemId);
                    }
                }
            }
            catch (Exception ex) { Logger.Log(ex); }
            return false;
        }

        #endregion

    }
}
