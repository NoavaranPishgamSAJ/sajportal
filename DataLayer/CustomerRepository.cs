﻿namespace DataLayer
{

    #region Assembly

    using System;
    using Dapper.Contrib.Extensions;
    using ORM.Model;

    #endregion


    /// <summary>
    /// این کلاس برای ثبت وقایع ورود به سیستم کاربران و پاک کردن پیام عمومی دریافتی آن ها و همچنین عملیات CURD کاربران در دیتابیس استفاده می شود
    /// </summary>
    public class CustomerRepository : DynamicFilterRepository
    {

        #region Attribute

        private static CustomerRepository _current;

        #endregion

        #region Property

        /// <summary>
        /// نسخه ایستا از کلاس
        /// </summary>
        public new static CustomerRepository Current
        {
            get
            {
                if (_current != null)
                    return _current;
                return _current = new CustomerRepository();
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// از این متد برای ثبت ورود کاربر به سیستم استفاده می شود
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// bool isReported = CustomerRepository.Current.UpdateLogout(12, DateTime.Now, "DOOB-PC");
        /// </code>
        /// </example>
        /// <param name="itemId">شناسه رکورد کاربر: int</param>
        /// <param name="lastLoginDate">آخرین تاریخ ورود: System.DateTime</param>
        /// <param name="lastLoginIdentification">آخرین شناسه ورود: string</param>
        /// <returns>bool</returns>
        public bool UpdateLogout(int itemId, DateTime? lastLoginDate, string lastLoginIdentification)
        {
            OpenConnection();
            var item = Connection.Get<Customer>(itemId);
            item.LastLoginDate = lastLoginDate;
            item.LastLoginIdentification = lastLoginIdentification;
            return Connection.Update(item);
        }

        /// <summary>
        /// از این متد برای پاک کردن پیام عمومی دریافتی استفاده می شود
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// bool isCleared = CustomerRepository.Current.ClearPublicMassage(12);
        /// </code>
        /// </example>
        /// <param name="itemId">شناسه رکورد کاربر: int</param>
        /// <returns>bool</returns>
        public bool ClearPublicMassage(int itemId)
        {
            OpenConnection();
            var item = Connection.Get<Customer>(itemId);
            item.PublicMassage = string.Empty;
            return Connection.Update(item);
        }

        #endregion

    }
}
