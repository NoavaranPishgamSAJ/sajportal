﻿namespace DataLayer
{

    #region Assembly

    using System.Collections.Generic;
    using System.Data;
    using Dapper;
    using Dapper.Contrib.Extensions;
    using ORM.Model;
    using Utility;
    using System;
    using System.Threading.Tasks;

    #endregion

    public class DynamicAsyncRepository
    {

        #region Attribute

        public static string ConnectionString = string.Empty;

        public static IDbConnection Connection = null;

        private const string SelectStr = "SELECT {0} FROM {1} /**where**/ /**orderby**/";

        private const string SelectPaggedStr = "SELECT * FROM (SELECT {0}, ROW_NUMBER() OVER(/**orderby**/) AS row_number FROM {1} /**where**/) AS PaginationTable WHERE row_number BETWEEN @start and @end";

        public static readonly string CountStr = "SELECT COUNT({0}) FROM {1} /**where**/";

        public static readonly string DeleteStr = "DELETE {0} /**where**/";

        #endregion

        #region Method

        public static string GetTableName<T>() where T : BaseModel<T>
        {
            try
            {
                return new BaseModel<T>().GetTableName();
            }
            catch (Exception ex) { Logger.Log(ex); }
            return string.Empty;
        }

        public static void OpenConnection()
        {
            if (Connection != null)
            {
                if (Connection.State != ConnectionState.Open)
                    Connection.Open();
            }
        }

        public static async Task<long> GetCount<T>(string condition = "", string orderBy = "", dynamic parameter = null) where T : BaseModel<T>
        {
            OpenConnection();
            var builder = new SqlBuilder();
            var template = builder.AddTemplate(string.Format(CountStr, "ItemId", GetTableName<T>()));

            if (CommonUtil.IsNotEmpty(condition))
                builder.Where(condition, parameter);

            builder.OrderBy(CommonUtil.IsEmpty(orderBy) ? "ItemId ASC" : orderBy);

            return await Connection.QueryFirstOrDefaultAsync<int>(CommonUtil.PersianFix(template.RawSql), template.Parameters);
        }

        public static async Task<IEnumerable<T>> GetList<T>(string condition = "", string orderBy = "", dynamic parameter = null) where T : BaseModel<T>
        {
            OpenConnection();
            var builder = new SqlBuilder();
            var template = builder.AddTemplate(string.Format(SelectStr, "*", GetTableName<T>()));

            if (CommonUtil.IsNotEmpty(condition))
                builder.Where(condition, parameter);

            builder.OrderBy(CommonUtil.IsEmpty(orderBy) ? "ItemId ASC" : orderBy);

            return await Connection.QueryAsync<T>(CommonUtil.PersianFix(template.RawSql), template.Parameters);
        }

        public static async Task<IEnumerable<T>> GetList<T>(long page, int itemPerPage, string condition = "", string orderBy = "", dynamic parameter = null) where T : BaseModel<T>
        {
            OpenConnection();
            var builder = new SqlBuilder();
            var template = builder.AddTemplate(string.Format(SelectPaggedStr, "*", GetTableName<T>()));

            if (CommonUtil.IsNotEmpty(condition))
                builder.Where(condition, parameter);

            builder.OrderBy(CommonUtil.IsEmpty(orderBy) ? "ItemId ASC" : orderBy);

            builder.AddParameters(new { start = page * itemPerPage + 1, end = (page + 1) * itemPerPage });

            return await Connection.QueryAsync<T>(CommonUtil.PersianFix(template.RawSql), template.Parameters);
        }

        public static async Task<T> GetItem<T>(int itemId) where T : BaseModel<T>
        {
            OpenConnection();
            return await Connection.GetAsync<T>(itemId);
        }

        public static async Task<T> GetItem<T>(string condition, string orderBy = "", dynamic parameter = null) where T : BaseModel<T>
        {
            OpenConnection();
            var builder = new SqlBuilder();
            var template = builder.AddTemplate(string.Format(SelectStr, "*", GetTableName<T>()));

            if (CommonUtil.IsNotEmpty(condition))
                builder.Where(condition, parameter);

            builder.OrderBy(CommonUtil.IsEmpty(orderBy) ? "ItemId ASC" : orderBy);

            return await Connection.QueryFirstOrDefaultAsync<T>(CommonUtil.PersianFix(template.RawSql), template.Parameters);
        }

        public static async Task<long> CreateItem<T>(T item) where T : BaseModel<T>
        {
            OpenConnection();
            return await Connection.InsertAsync(item);
        }

        public static async Task<bool> UpdateItem<T>(T item) where T : BaseModel<T>
        {
            OpenConnection();
            return await Connection.UpdateAsync(item);
        }

        public static async Task<bool> DeleteItem<T>(T item) where T : BaseModel<T>
        {
            OpenConnection();
            return await Connection.DeleteAsync(item);
        }

        public static async Task<bool> DeleteItem<T>(int itemId) where T : BaseModel<T>
        {
            OpenConnection();
            var item = await Connection.GetAsync<T>(itemId);
            return await Connection.DeleteAsync(item);
        }

        public static async Task<bool> DeleteAllItem<T>() where T : BaseModel<T>
        {
            OpenConnection();
            return await Connection.DeleteAllAsync<T>();
        }

        public static async Task<int> DeleteList<T>(string condition, dynamic parameter = null) where T : BaseModel<T>
        {
            OpenConnection();
            var builder = new SqlBuilder();
            var template = builder.AddTemplate(string.Format(DeleteStr, GetTableName<T>()));

            if (CommonUtil.IsNotEmpty(condition))
                builder.Where(condition, parameter);

            return await Connection.ExecuteAsync(CommonUtil.PersianFix(template.RawSql), template.Parameters);
        }

        public static async Task<int> Execute(string command, DynamicParameters param = null, CommandType type = CommandType.Text)
        {
            OpenConnection();
            return await Connection.ExecuteAsync(command, param, null, null, type);
        }

        public static async Task<IEnumerable<T>> StoredProcedures<T>(string procedureName, DynamicParameters param, CommandType type) where T : BaseModel<T>
        {
            OpenConnection();
            return await Connection.QueryAsync<T>(procedureName, param, null, null, type);
        }

        #endregion

    }
}
