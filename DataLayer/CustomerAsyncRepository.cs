﻿namespace DataLayer
{
    #region Assembly

    using System;
    using Dapper.Contrib.Extensions;
    using ORM.Model;
    using System.Threading.Tasks;
    #endregion


    public class CustomerAsyncRepository : DynamicFilterAsyncRepository
    {

        #region Methods

        public static async Task<bool> UpdateLogout(int itemId, DateTime? lastLoginDate, string lastLoginIdentification)
        {
            OpenConnection();
            var item = await Connection.GetAsync<Customer>(itemId);
            item.LastLoginDate = lastLoginDate;
            item.LastLoginIdentification = lastLoginIdentification;
            return await Connection.UpdateAsync(item);
        }

        public static async Task<bool> ClearPublicMassage(int itemId)
        {
            OpenConnection();
            var item = await Connection.GetAsync<Customer>(itemId);
            item.PublicMassage = string.Empty;
            return await Connection.UpdateAsync(item);
        }

        #endregion

    }
}
