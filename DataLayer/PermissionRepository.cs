﻿namespace DataLayer
{

    #region Assembly

    using System.Collections.Generic;
    using Dapper;
    using ORM.Model;
    using Utility;
    using static Dapper.SqlBuilder;

    #endregion

    /// <summary>
    /// از این کلاس برای دریافت لیست تمامی مجمزهای کاربر استفاده می شود
    /// </summary>
    public class PermissionRepository : DynamicFilterRepository
    {

        #region Attribute

        private static PermissionRepository _current;

        #endregion

        #region Property

        /// <summary>
        /// نسخه ایستا از کلاس
        /// </summary>
        public new static PermissionRepository Current
        {
            get
            {
                if (_current != null)
                    return _current;
                return _current = new PermissionRepository();
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// از این متد برای دریافت لیست مجوزهای کاربر استفاده می شود
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// DynamicParameters params = new DynamicParameters();
        /// params.Add("CustomerId", 23);
        /// IEnumerable&lt;CustomerPermissions&gt; permissionList = PermissionRepository.Current.GetList("CustomerId = @CustomerId", parameter);
        /// </code>
        /// </example>
        /// <param name="condition">شرط های اعمال شده در پرسجو: string</param>
        /// <param name="parameter">مقدار پارامترها: DynamicParameters</param>
        /// <returns>IEnumerable&lt;CustomerPermissions&gt;</returns>
        public IEnumerable<CustomerPermissions> GetList(string condition, dynamic parameter)
        {
            OpenConnection();
            var builder = new SqlBuilder();
            Template template = builder.AddTemplate(@"
SELECT
    [CustomerRole].[CustomerID], [RolePermission].PermissionID, Permission.DisplayName, Permission.DevName
FROM
    [CustomerRole]
CROSS JOIN
    [RolePermission]
INNER JOIN
    Permission
ON
    RolePermission.PermissionId = Permission.ItemID
WHERE
    [CustomerRole].RoleId = [RolePermission].RoleId AND
    [RolePermission].PermissionId NOT IN (
		SELECT
			[CustomerPermission].PermissionID
		FROM
			[CustomerPermission]
		WHERE
			[CustomerPermission].IsHave = 0" + (CommonUtil.IsNotEmpty(condition) ? " AND " + condition : string.Empty) + @"
	)" + (CommonUtil.IsNotEmpty(condition) ? " AND " + condition : string.Empty) + @"
UNION ALL
SELECT
    [CustomerPermission].[CustomerID], [CustomerPermission].PermissionID, Permission.DisplayName, Permission.DevName
FROM
    [CustomerPermission]
INNER JOIN
	Permission
ON
	CustomerPermission.PermissionId = Permission.ItemID
WHERE
    [CustomerPermission].IsHave = 1" + (CommonUtil.IsNotEmpty(condition) ? " AND " + condition : string.Empty), parameter);

            return Connection.Query<CustomerPermissions>(CommonUtil.PersianFix(template.RawSql), template.Parameters);
        }

        /// <summary>
        /// از این متد برای دریافت یکی از مجوزهای مورد نظر کاربر استفاده می شود
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// DynamicParameters params = new DynamicParameters();
        /// params.Add("CustomerId", 23);
        /// params.Add("DevName", "Login");
        /// CustomerPermissions permission = PermissionRepository.Current.GetList("CustomerId = @CustomerId", parameter);
        /// </code>
        /// </example>
        /// <param name="condition">شرط های اعمال شده در پرسجو: string</param>
        /// <param name="parameter">مقدار پارامترها: DynamicParameters</param>
        /// <returns>CustomerPermissions</returns>
        public CustomerPermissions Get(string condition, dynamic parameter)
        {
            OpenConnection();
            var builder = new SqlBuilder();
            Template template = builder.AddTemplate(@"
SELECT
    [CustomerRole].[CustomerID], [RolePermission].PermissionID, Permission.DisplayName, Permission.DevName
FROM
    [CustomerRole]
CROSS JOIN
    [RolePermission]
INNER JOIN
    Permission
ON
    RolePermission.PermissionId = Permission.ItemID
WHERE
    [CustomerRole].RoleId = [RolePermission].RoleId AND
    [RolePermission].PermissionId NOT IN (
		SELECT
			[CustomerPermission].PermissionID
		FROM
			[CustomerPermission]
		WHERE
			[CustomerPermission].IsHave = 0" + (CommonUtil.IsNotEmpty(condition) ? " AND " + condition : string.Empty) + @"
	)" + (CommonUtil.IsNotEmpty(condition) ? " AND " + condition : string.Empty) + @"
UNION ALL
SELECT
    [CustomerPermission].[CustomerID], [CustomerPermission].PermissionID, Permission.DisplayName, Permission.DevName
FROM
    [CustomerPermission]
INNER JOIN
	Permission
ON
	CustomerPermission.PermissionId = Permission.ItemID
WHERE
    [CustomerPermission].IsHave = 1" + (CommonUtil.IsNotEmpty(condition) ? " AND " + condition : string.Empty), parameter);

            return Connection.QueryFirstOrDefault<CustomerPermissions>(CommonUtil.PersianFix(template.RawSql), template.Parameters);
        }

        #endregion

    }
}
