/****** Object:  Table [dbo].[View]    Script Date: 08/04/2016 19:24:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[View]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[View](
	[ItemId] [int] IDENTITY(1,1) NOT NULL,
	[ViewName] [nvarchar](50) NOT NULL,
	[VirtualPath] [nvarchar](max) NOT NULL,
	[Template] [nvarchar](max) NOT NULL,
	[RouteValues] [ntext] NULL,
	[FolderId] [int] NOT NULL,
	[SiteId] [int] NOT NULL,
 CONSTRAINT [PK_View] PRIMARY KEY CLUSTERED 
(
	[ItemId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[Validation]    Script Date: 08/04/2016 19:24:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Validation]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Validation](
	[ItemId] [int] IDENTITY(1,1) NOT NULL,
	[ValidationName] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_Validation] PRIMARY KEY CLUSTERED 
(
	[ItemId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[Site]    Script Date: 08/04/2016 19:24:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Site]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Site](
	[ItemId] [int] IDENTITY(1,1) NOT NULL,
	[SiteName] [nvarchar](50) NOT NULL,
	[Title] [nvarchar](50) NULL,
	[Mode] [bit] NOT NULL,
	[MembershipId] [int] NOT NULL,
	[RepositoryId] [int] NOT NULL,
 CONSTRAINT [PK_Site] PRIMARY KEY CLUSTERED 
(
	[ItemId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[SearchSetting]    Script Date: 08/04/2016 19:24:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SearchSetting]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[SearchSetting](
	[ItemId] [int] IDENTITY(1,1) NOT NULL,
	[ContentId] [int] NOT NULL,
	[LinkPage] [nvarchar](max) NOT NULL,
	[RouteField] [ntext] NOT NULL,
	[RepositoryId] [int] NOT NULL,
 CONSTRAINT [PK_SearchSetting] PRIMARY KEY CLUSTERED 
(
	[ItemId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[Role]    Script Date: 08/04/2016 19:24:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Role]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Role](
	[ItemId] [int] IDENTITY(1,1) NOT NULL,
	[DisplayName] [nvarchar](100) NOT NULL,
 CONSTRAINT [PK_Role] PRIMARY KEY CLUSTERED 
(
	[ItemId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[Repository]    Script Date: 08/04/2016 19:24:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Repository]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Repository](
	[ItemId] [int] IDENTITY(1,1) NOT NULL,
	[RepositoryName] [nvarchar](50) NOT NULL,
	[ShowHiddenFolder] [bit] NOT NULL,
	[EnableWorkFlow] [bit] NOT NULL,
 CONSTRAINT [PK_Repository] PRIMARY KEY CLUSTERED 
(
	[ItemId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[Position]    Script Date: 08/04/2016 19:24:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Position]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Position](
	[ItemId] [int] IDENTITY(1,1) NOT NULL,
	[DisplayName] [nvarchar](150) NOT NULL,
 CONSTRAINT [PK_Position] PRIMARY KEY CLUSTERED 
(
	[ItemId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[Permission]    Script Date: 08/04/2016 19:24:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Permission]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Permission](
	[ItemId] [int] IDENTITY(1,1) NOT NULL,
	[DisplayName] [nvarchar](150) NOT NULL,
	[DevName] [nvarchar](150) NOT NULL,
 CONSTRAINT [PK_Permission] PRIMARY KEY CLUSTERED 
(
	[ItemId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[Page]    Script Date: 08/04/2016 19:24:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Page]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Page](
	[ItemId] [int] IDENTITY(1,1) NOT NULL,
	[PageName] [nvarchar](50) NOT NULL,
	[VirtualPath] [nvarchar](max) NOT NULL,
	[Template] [nvarchar](max) NOT NULL,
	[Title] [nvarchar](max) NULL,
	[MenuTitle] [nvarchar](50) NULL,
	[RouteValues] [ntext] NULL,
	[Action] [nvarchar](50) NOT NULL,
	[Controller] [nvarchar](50) NOT NULL,
	[Area] [nvarchar](50) NULL,
	[ParentId] [int] NOT NULL,
	[SiteId] [int] NOT NULL,
 CONSTRAINT [PK_Page] PRIMARY KEY CLUSTERED 
(
	[ItemId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[Membership]    Script Date: 08/04/2016 19:24:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Membership]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Membership](
	[ItemId] [int] IDENTITY(1,1) NOT NULL,
	[MembershipName] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_Membership] PRIMARY KEY CLUSTERED 
(
	[ItemId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[FolderType]    Script Date: 08/04/2016 19:24:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[FolderType]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[FolderType](
	[ItemId] [int] IDENTITY(1,1) NOT NULL,
	[TypeName] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_FolderType] PRIMARY KEY CLUSTERED 
(
	[ItemId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[Folder]    Script Date: 08/04/2016 19:24:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Folder]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Folder](
	[ItemId] [int] IDENTITY(1,1) NOT NULL,
	[FolderName] [nvarchar](50) NOT NULL,
	[VirtualPath] [nvarchar](max) NOT NULL,
	[FolderTypeId] [int] NOT NULL,
	[ParentId] [int] NOT NULL,
	[SiteId] [int] NOT NULL,
 CONSTRAINT [PK_Folder] PRIMARY KEY CLUSTERED 
(
	[ItemId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[File]    Script Date: 08/04/2016 19:24:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[File]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[File](
	[ItemId] [int] IDENTITY(1,1) NOT NULL,
	[FileName] [nvarchar](50) NOT NULL,
	[FileExtension] [nvarchar](5) NOT NULL,
	[VirtualPath] [nvarchar](max) NOT NULL,
	[FolderId] [int] NOT NULL,
	[SiteId] [int] NOT NULL,
 CONSTRAINT [PK_File] PRIMARY KEY CLUSTERED 
(
	[ItemId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[DataType]    Script Date: 08/04/2016 19:24:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DataType]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[DataType](
	[ItemId] [int] IDENTITY(1,1) NOT NULL,
	[DataName] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_DataType] PRIMARY KEY CLUSTERED 
(
	[ItemId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[Customer]    Script Date: 08/04/2016 19:24:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Customer]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Customer](
	[ItemId] [int] IDENTITY(1,1) NOT NULL,
	[Username] [nvarchar](150) NOT NULL,
	[Password] [nvarchar](50) NOT NULL,
	[FirstName] [nvarchar](150) NULL,
	[LastName] [nvarchar](150) NULL,
	[FatherName] [nvarchar](150) NULL,
	[NationalCode] [nvarchar](20) NULL,
	[RegisterDate] [date] NOT NULL,
	[RetrievalText] [nvarchar](150) NULL,
	[IsAdmin] [bit] NOT NULL,
	[BoycottUser] [bit] NOT NULL,
	[LastLoginDate] [date] NULL,
	[LastLoginIdentification] [nvarchar](150) NULL,
	[UseOperationDate] [bit] NOT NULL,
	[OperationDate] [date] NULL,
	[PublicMassage] [ntext] NULL,
	[IdentificationNumber] [nvarchar](50) NULL,
 CONSTRAINT [PK_Customer] PRIMARY KEY CLUSTERED 
(
	[ItemId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[ControlType]    Script Date: 08/04/2016 19:24:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ControlType]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ControlType](
	[ItemId] [int] IDENTITY(1,1) NOT NULL,
	[ControlName] [nvarchar](50) NOT NULL,
	[VirtualPath] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_FieldType] PRIMARY KEY CLUSTERED 
(
	[ItemId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[ContentTypeFieldValidation]    Script Date: 08/04/2016 19:24:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ContentTypeFieldValidation]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ContentTypeFieldValidation](
	[ItemId] [int] IDENTITY(1,1) NOT NULL,
	[ContentTypeFieldId] [int] NOT NULL,
	[ValidationId] [int] NOT NULL,
	[FirstParam] [nvarchar](max) NULL,
	[SecondParam] [nvarchar](max) NULL,
	[ThirdParam] [nvarchar](max) NULL,
 CONSTRAINT [PK_ContentTypeFieldValidation] PRIMARY KEY CLUSTERED 
(
	[ItemId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[ContentTypeField]    Script Date: 08/04/2016 19:24:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ContentTypeField]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ContentTypeField](
	[ItemId] [int] IDENTITY(1,1) NOT NULL,
	[FieldName] [nvarchar](100) NOT NULL,
	[Label] [nvarchar](100) NULL,
	[ControlTypeId] [int] NOT NULL,
	[DataTypeId] [int] NOT NULL,
	[DefaultValue] [ntext] NULL,
	[ShowInList] [bit] NOT NULL,
	[Tooltip] [nvarchar](max) NULL,
	[Length] [int] NULL,
	[AllowNull] [bit] NULL,
	[CustomSetting] [ntext] NULL,
	[ContentTypeId] [int] NOT NULL,
 CONSTRAINT [PK_ContentTypeField] PRIMARY KEY CLUSTERED 
(
	[ItemId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[ContentType]    Script Date: 08/04/2016 19:24:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ContentType]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ContentType](
	[ItemId] [int] IDENTITY(1,1) NOT NULL,
	[TypeName] [nvarchar](50) NOT NULL,
	[RepositoryId] [int] NOT NULL,
 CONSTRAINT [PK_ContentType] PRIMARY KEY CLUSTERED 
(
	[ItemId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[ContentRole]    Script Date: 08/04/2016 19:24:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ContentRole]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ContentRole](
	[ItemId] [int] IDENTITY(1,1) NOT NULL,
	[ContentId] [int] NOT NULL,
	[RoleId] [int] NOT NULL,
 CONSTRAINT [PK_ContentRole] PRIMARY KEY CLUSTERED 
(
	[ItemId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[ContentEmbedded]    Script Date: 08/04/2016 19:24:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ContentEmbedded]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ContentEmbedded](
	[ItemId] [int] IDENTITY(1,1) NOT NULL,
	[ContentId] [int] NOT NULL,
	[EmbeddedContentId] [int] NOT NULL,
 CONSTRAINT [PK_ContentEmbedded] PRIMARY KEY CLUSTERED 
(
	[ItemId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[ContentCategory]    Script Date: 08/04/2016 19:24:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ContentCategory]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[ContentCategory](
	[ItemId] [int] IDENTITY(1,1) NOT NULL,
	[ContentId] [int] NOT NULL,
	[CategoryContentId] [int] NOT NULL,
 CONSTRAINT [PK_ContentCategory] PRIMARY KEY CLUSTERED 
(
	[ItemId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[Content]    Script Date: 08/04/2016 19:24:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Content]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Content](
	[ItemId] [int] IDENTITY(1,1) NOT NULL,
	[ContentName] [nvarchar](100) NOT NULL,
	[Label] [nvarchar](100) NULL,
	[ContentTypeId] [int] NOT NULL,
	[Hidden] [bit] NOT NULL,
	[Sortable] [bit] NOT NULL,
	[Paging] [bit] NOT NULL,
	[PageSize] [int] NOT NULL,
	[RepositoryId] [int] NOT NULL,
 CONSTRAINT [PK_Content] PRIMARY KEY CLUSTERED 
(
	[ItemId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[CustomerPosition]    Script Date: 08/04/2016 19:24:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CustomerPosition]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[CustomerPosition](
	[ItemId] [int] IDENTITY(1,1) NOT NULL,
	[CustomerId] [int] NOT NULL,
	[PositionId] [int] NOT NULL,
 CONSTRAINT [PK_CustomerPosition] PRIMARY KEY CLUSTERED 
(
	[ItemId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[CustomerRole]    Script Date: 08/04/2016 19:24:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CustomerRole]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[CustomerRole](
	[ItemId] [int] IDENTITY(1,1) NOT NULL,
	[CustomerId] [int] NOT NULL,
	[RoleId] [int] NOT NULL,
 CONSTRAINT [PK_CustomerRole] PRIMARY KEY CLUSTERED 
(
	[ItemId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[RolePermission]    Script Date: 08/04/2016 19:24:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RolePermission]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[RolePermission](
	[ItemId] [int] IDENTITY(1,1) NOT NULL,
	[RoleId] [int] NOT NULL,
	[PermissionId] [int] NOT NULL,
 CONSTRAINT [PK_RolePermission] PRIMARY KEY CLUSTERED 
(
	[ItemId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  Table [dbo].[CustomerPermission]    Script Date: 08/04/2016 19:24:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CustomerPermission]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[CustomerPermission](
	[ItemId] [int] IDENTITY(1,1) NOT NULL,
	[CustomerId] [int] NOT NULL,
	[PermissionId] [int] NOT NULL,
	[IsHave] [bit] NOT NULL,
 CONSTRAINT [PK_CustomerPermission] PRIMARY KEY CLUSTERED 
(
	[ItemId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
/****** Object:  View [dbo].[RolePermissionView]    Script Date: 08/04/2016 19:24:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[RolePermissionView]'))
EXEC dbo.sp_executesql @statement = N'CREATE VIEW [dbo].[RolePermissionView] AS 
SELECT
RolePermission.ItemId,
RolePermission.RoleId,
RolePermission.PermissionId,
Permission.DisplayName AS PermissionDisplayName,
Permission.DevName AS PermissionDevName
FROM
RolePermission
INNER JOIN Permission ON RolePermission.PermissionId = Permission.ItemId
'
GO
/****** Object:  View [dbo].[CustomerRoleView]    Script Date: 08/04/2016 19:24:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[CustomerRoleView]'))
EXEC dbo.sp_executesql @statement = N'CREATE VIEW [dbo].[CustomerRoleView] AS 
SELECT
CustomerRole.ItemId,
CustomerRole.CustomerId,
CustomerRole.RoleId,
Role.DisplayName AS RoleDisplayName
FROM
CustomerRole
INNER JOIN Role ON CustomerRole.RoleId = Role.ItemId
'
GO
/****** Object:  View [dbo].[CustomerPositionView]    Script Date: 08/04/2016 19:24:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[CustomerPositionView]'))
EXEC dbo.sp_executesql @statement = N'CREATE VIEW [dbo].[CustomerPositionView] AS 
SELECT
CustomerPosition.ItemId,
CustomerPosition.CustomerId,
CustomerPosition.PositionId,
Position.DisplayName AS PositionDisplayName
FROM
CustomerPosition
INNER JOIN Position ON CustomerPosition.PositionId = Position.ItemId
'
GO
/****** Object:  View [dbo].[CustomerPermissionView]    Script Date: 08/04/2016 19:24:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[CustomerPermissionView]'))
EXEC dbo.sp_executesql @statement = N'CREATE VIEW [dbo].[CustomerPermissionView] AS 
SELECT
CustomerPermission.ItemId,
CustomerPermission.CustomerId,
CustomerPermission.PermissionId,
CustomerPermission.IsHave,
CASE WHEN CustomerPermission.IsHave = 1 THEN ''بله'' ELSE ''خیر'' END AS IsHaveStr,
Permission.DisplayName AS PermissionDisplayName,
Permission.DevName AS PermissionDevName
FROM
CustomerPermission
INNER JOIN Permission ON CustomerPermission.PermissionId = Permission.ItemId
;
'
GO
/****** Object:  View [dbo].[CustomerPermissionsView]    Script Date: 08/04/2016 19:24:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[CustomerPermissionsView]'))
EXEC dbo.sp_executesql @statement = N'CREATE VIEW [dbo].[CustomerPermissionsView] AS 
SELECT
    [CustomerRole].[CustomerId], [RolePermission].PermissionId, Permission.DisplayName, Permission.DevName
FROM
    [CustomerRole]
CROSS JOIN
    [RolePermission]
INNER JOIN
    Permission
ON
    RolePermission.PermissionId = Permission.ItemId
WHERE
    [CustomerRole].RoleId = [RolePermission].RoleId AND
    [RolePermission].PermissionId NOT IN (
		SELECT
			[CustomerPermission].PermissionId
		FROM
			[CustomerPermission]
		WHERE
			[CustomerPermission].IsHave = 0
	)
UNION ALL
SELECT
    [CustomerPermission].[CustomerId], [CustomerPermission].PermissionId, Permission.DisplayName, Permission.DevName
FROM
    [CustomerPermission]
INNER JOIN
	Permission
ON
	CustomerPermission.PermissionId = Permission.ItemId
WHERE
    [CustomerPermission].IsHave = 1
'
GO
/****** Object:  ForeignKey [FK_CustomerPermission_Customer]    Script Date: 08/04/2016 19:24:13 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_CustomerPermission_Customer]') AND parent_object_id = OBJECT_ID(N'[dbo].[CustomerPermission]'))
ALTER TABLE [dbo].[CustomerPermission]  WITH CHECK ADD  CONSTRAINT [FK_CustomerPermission_Customer] FOREIGN KEY([CustomerId])
REFERENCES [dbo].[Customer] ([ItemId])
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_CustomerPermission_Customer]') AND parent_object_id = OBJECT_ID(N'[dbo].[CustomerPermission]'))
ALTER TABLE [dbo].[CustomerPermission] CHECK CONSTRAINT [FK_CustomerPermission_Customer]
GO
/****** Object:  ForeignKey [FK_CustomerPermission_Permission]    Script Date: 08/04/2016 19:24:13 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_CustomerPermission_Permission]') AND parent_object_id = OBJECT_ID(N'[dbo].[CustomerPermission]'))
ALTER TABLE [dbo].[CustomerPermission]  WITH CHECK ADD  CONSTRAINT [FK_CustomerPermission_Permission] FOREIGN KEY([PermissionId])
REFERENCES [dbo].[Permission] ([ItemId])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_CustomerPermission_Permission]') AND parent_object_id = OBJECT_ID(N'[dbo].[CustomerPermission]'))
ALTER TABLE [dbo].[CustomerPermission] CHECK CONSTRAINT [FK_CustomerPermission_Permission]
GO
/****** Object:  ForeignKey [FK_CustomerPosition_Customer]    Script Date: 08/04/2016 19:24:13 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_CustomerPosition_Customer]') AND parent_object_id = OBJECT_ID(N'[dbo].[CustomerPosition]'))
ALTER TABLE [dbo].[CustomerPosition]  WITH CHECK ADD  CONSTRAINT [FK_CustomerPosition_Customer] FOREIGN KEY([CustomerId])
REFERENCES [dbo].[Customer] ([ItemId])
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_CustomerPosition_Customer]') AND parent_object_id = OBJECT_ID(N'[dbo].[CustomerPosition]'))
ALTER TABLE [dbo].[CustomerPosition] CHECK CONSTRAINT [FK_CustomerPosition_Customer]
GO
/****** Object:  ForeignKey [FK_CustomerPosition_Position]    Script Date: 08/04/2016 19:24:13 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_CustomerPosition_Position]') AND parent_object_id = OBJECT_ID(N'[dbo].[CustomerPosition]'))
ALTER TABLE [dbo].[CustomerPosition]  WITH CHECK ADD  CONSTRAINT [FK_CustomerPosition_Position] FOREIGN KEY([PositionId])
REFERENCES [dbo].[Position] ([ItemId])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_CustomerPosition_Position]') AND parent_object_id = OBJECT_ID(N'[dbo].[CustomerPosition]'))
ALTER TABLE [dbo].[CustomerPosition] CHECK CONSTRAINT [FK_CustomerPosition_Position]
GO
/****** Object:  ForeignKey [FK_CustomerRole_Customer]    Script Date: 08/04/2016 19:24:13 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_CustomerRole_Customer]') AND parent_object_id = OBJECT_ID(N'[dbo].[CustomerRole]'))
ALTER TABLE [dbo].[CustomerRole]  WITH CHECK ADD  CONSTRAINT [FK_CustomerRole_Customer] FOREIGN KEY([CustomerId])
REFERENCES [dbo].[Customer] ([ItemId])
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_CustomerRole_Customer]') AND parent_object_id = OBJECT_ID(N'[dbo].[CustomerRole]'))
ALTER TABLE [dbo].[CustomerRole] CHECK CONSTRAINT [FK_CustomerRole_Customer]
GO
/****** Object:  ForeignKey [FK_CustomerRole_Role]    Script Date: 08/04/2016 19:24:13 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_CustomerRole_Role]') AND parent_object_id = OBJECT_ID(N'[dbo].[CustomerRole]'))
ALTER TABLE [dbo].[CustomerRole]  WITH CHECK ADD  CONSTRAINT [FK_CustomerRole_Role] FOREIGN KEY([RoleId])
REFERENCES [dbo].[Role] ([ItemId])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_CustomerRole_Role]') AND parent_object_id = OBJECT_ID(N'[dbo].[CustomerRole]'))
ALTER TABLE [dbo].[CustomerRole] CHECK CONSTRAINT [FK_CustomerRole_Role]
GO
/****** Object:  ForeignKey [FK_RolePermission_Permission]    Script Date: 08/04/2016 19:24:13 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_RolePermission_Permission]') AND parent_object_id = OBJECT_ID(N'[dbo].[RolePermission]'))
ALTER TABLE [dbo].[RolePermission]  WITH CHECK ADD  CONSTRAINT [FK_RolePermission_Permission] FOREIGN KEY([PermissionId])
REFERENCES [dbo].[Permission] ([ItemId])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_RolePermission_Permission]') AND parent_object_id = OBJECT_ID(N'[dbo].[RolePermission]'))
ALTER TABLE [dbo].[RolePermission] CHECK CONSTRAINT [FK_RolePermission_Permission]
GO
/****** Object:  ForeignKey [FK_RolePermission_Role]    Script Date: 08/04/2016 19:24:13 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_RolePermission_Role]') AND parent_object_id = OBJECT_ID(N'[dbo].[RolePermission]'))
ALTER TABLE [dbo].[RolePermission]  WITH CHECK ADD  CONSTRAINT [FK_RolePermission_Role] FOREIGN KEY([RoleId])
REFERENCES [dbo].[Role] ([ItemId])
ON DELETE CASCADE
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_RolePermission_Role]') AND parent_object_id = OBJECT_ID(N'[dbo].[RolePermission]'))
ALTER TABLE [dbo].[RolePermission] CHECK CONSTRAINT [FK_RolePermission_Role]
GO
