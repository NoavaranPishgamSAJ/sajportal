﻿namespace DataLayer
{

    #region Assembly

    using System.Threading.Tasks;
    using System.Collections.Generic;
    using System.Data;
    using Dapper;
    using Dapper.Contrib.Extensions;
    using ORM.Condition;
    using ORM.Model;
    using Utility;
    using System;
    using System.Linq;

    #endregion

    public class DynamicFilterAsyncRepository
    {

        #region Attribute

        public static string ConnectionString = string.Empty;

        public static IDbConnection Connection = null;

        public static readonly string SelectStr = "SELECT {0} FROM {1} /**where**/ /**orderby**/";

        public static readonly string SelectPaggedStr = "SELECT * FROM (SELECT {0}, ROW_NUMBER() OVER(/**orderby**/) AS row_number FROM {1} /**where**/) AS PaginationTable WHERE row_number BETWEEN @start and @end";

        public static readonly string CountStr = "SELECT COUNT({0}) FROM {1} /**where**/";

        public static readonly string DeleteStr = "DELETE {0} /**where**/";

        #endregion

        #region Method

        public static string GetTableName<T>() where T : class
        {
            try
            {
                return new BaseModel<T>().GetTableName();
            }
            catch (Exception ex) { Logger.Log(ex); }
            return string.Empty;
        }

        protected static void OpenConnection()
        {
            if (Connection != null)
            {
                if (Connection.State != ConnectionState.Open)
                    Connection.Open();
            }
        }

        public static async Task<long> GetCount<T>(FilterExpressionList condition = null, string orderBy = "") where T : class
        {
            OpenConnection();
            var builder = new SqlBuilder();
            var template = builder.AddTemplate(string.Format(CountStr, "ItemId", GetTableName<T>()));

            if (condition != null && condition.Count > 0)
                builder.Where(condition.FilterString, condition.FilterValueParameter);

            builder.OrderBy(CommonUtil.IsEmpty(orderBy) ? "ItemId ASC" : orderBy);

            return await Connection.QueryFirstOrDefaultAsync<int>(CommonUtil.PersianFix(template.RawSql), template.Parameters);
        }

        public static async Task<long> GetCount(string table, FilterExpressionList condition = null, string orderBy = "")
        {
            OpenConnection();
            var builder = new SqlBuilder();
            var template = builder.AddTemplate(string.Format(CountStr, "ItemId", table));

            if (condition != null && condition.Count > 0)
                builder.Where(condition.FilterString, condition.FilterValueParameter);

            builder.OrderBy(CommonUtil.IsEmpty(orderBy) ? "ItemId ASC" : orderBy);

            return await Connection.QueryFirstOrDefaultAsync<int>(CommonUtil.PersianFix(template.RawSql), template.Parameters);
        }

        public static async Task<IEnumerable<T>> GetList<T>(FilterExpressionList condition = null, string orderBy = "", string columns = "*") where T : class
        {
            OpenConnection();
            var builder = new SqlBuilder();
            var template = builder.AddTemplate(string.Format(SelectStr, columns, GetTableName<T>()));

            if (condition != null && condition.Count > 0)
                builder.Where(condition.FilterString, condition.FilterValueParameter);

            builder.OrderBy(CommonUtil.IsEmpty(orderBy) ? "ItemId ASC" : orderBy);

            return await Connection.QueryAsync<T>(CommonUtil.PersianFix(template.RawSql), template.Parameters);
        }

        public static async Task<IEnumerable<dynamic>> GetList(string table, FilterExpressionList condition = null, string orderBy = "", string columns = "*")
        {
            OpenConnection();
            var builder = new SqlBuilder();
            var template = builder.AddTemplate(string.Format(SelectStr, columns, table));

            if (condition != null && condition.Count > 0)
                builder.Where(condition.FilterString, condition.FilterValueParameter);

            builder.OrderBy(CommonUtil.IsEmpty(orderBy) ? "ItemId ASC" : orderBy);

            return await Connection.QueryAsync(CommonUtil.PersianFix(template.RawSql), template.Parameters);
        }

        public static async Task<IEnumerable<T>> GetList<T>(long page, int itemPerPage, FilterExpressionList condition = null, string orderBy = "", string columns = "*") where T : class
        {
            OpenConnection();
            var builder = new SqlBuilder();
            var template = builder.AddTemplate(string.Format(SelectPaggedStr, columns, GetTableName<T>()));

            if (condition != null && condition.Count > 0)
                builder.Where(condition.FilterString, condition.FilterValueParameter);

            builder.OrderBy(CommonUtil.IsEmpty(orderBy) ? "ItemId ASC" : orderBy);

            builder.AddParameters(new { start = page * itemPerPage + 1, end = (page + 1) * itemPerPage });

            return await Connection.QueryAsync<T>(CommonUtil.PersianFix(template.RawSql), template.Parameters);
        }

        public static async Task<IEnumerable<dynamic>> GetList(string table, long page, int itemPerPage, FilterExpressionList condition = null, string orderBy = "", string columns = "*")
        {
            OpenConnection();
            var builder = new SqlBuilder();
            var template = builder.AddTemplate(string.Format(SelectPaggedStr, columns, table));

            if (condition != null && condition.Count > 0)
                builder.Where(condition.FilterString, condition.FilterValueParameter);

            builder.OrderBy(CommonUtil.IsEmpty(orderBy) ? "ItemId ASC" : orderBy);

            builder.AddParameters(new { start = page * itemPerPage + 1, end = (page + 1) * itemPerPage });

            return await Connection.QueryAsync(CommonUtil.PersianFix(template.RawSql), template.Parameters);
        }

        public static async Task<T> GetItem<T>(int itemId) where T : class
        {
            OpenConnection();
            return await Connection.GetAsync<T>(itemId);
        }

        public static async Task<dynamic> GetItem(string table, int itemId)
        {
            var condition = new FilterExpressionList();
            condition.Add(new Filter("ItemId", SqlOperators.Equal, itemId));
            var list = await GetList(table, condition);
            return list.FirstOrDefault();
        }

        public static async Task<T> GetItem<T>(FilterExpressionList condition = null, string orderBy = "", string columns = "*") where T : class
        {
            OpenConnection();
            var builder = new SqlBuilder();
            var template = builder.AddTemplate(string.Format(SelectStr, columns, GetTableName<T>()));

            if (condition != null && condition.Count > 0)
                builder.Where(condition.FilterString, condition.FilterValueParameter);

            builder.OrderBy(CommonUtil.IsEmpty(orderBy) ? "ItemId ASC" : orderBy);

            return await Connection.QueryFirstOrDefaultAsync<T>(CommonUtil.PersianFix(template.RawSql), template.Parameters);
        }

        public static async Task<dynamic> GetItem<dynamic>(string table, FilterExpressionList condition = null, string orderBy = "", string columns = "*")
        {
            var list = await GetList(table, condition, orderBy, columns);
            return list.FirstOrDefault();
        }

        public static async Task<long> CreateItem<T>(T item) where T : class
        {
            OpenConnection();
            return await Connection.InsertAsync(item);
        }

        public static async Task<bool> UpdateItem<T>(T item) where T : class
        {
            OpenConnection();
            return await Connection.UpdateAsync(item);
        }

        public static async Task<bool> DeleteItem<T>(T item) where T : class
        {
            OpenConnection();
            return await Connection.DeleteAsync(item);
        }

        public static async Task<bool> DeleteItem<T>(int itemId) where T : class
        {
            OpenConnection();
            var item = await Connection.GetAsync<T>(itemId);
            return await Connection.DeleteAsync(item);
        }

        public static async Task<bool> DeleteAllItem<T>() where T : class
        {
            OpenConnection();
            return await Connection.DeleteAllAsync<T>();
        }

        public static async Task<int> DeleteList<T>(FilterExpressionList condition = null) where T : class
        {
            OpenConnection();
            var builder = new SqlBuilder();
            var template = builder.AddTemplate(string.Format(DeleteStr, GetTableName<T>()));

            if (condition != null && condition.Count > 0)
                builder.Where(condition.FilterString, condition.FilterValueParameter);

            return await Connection.ExecuteAsync(CommonUtil.PersianFix(template.RawSql), template.Parameters);
        }

        public static async Task<int> Execute(string command, DynamicParameters param = null, CommandType type = CommandType.Text)
        {
            OpenConnection();
            return await Connection.ExecuteAsync(command, param, null, null, type);
        }

        public static async Task<IEnumerable<T>> StoredProcedures<T>(string procedureName, DynamicParameters param, CommandType type) where T : class
        {
            OpenConnection();
            return await Connection.QueryAsync<T>(procedureName, param, null, null, type);
        }

        #endregion

    }
}
