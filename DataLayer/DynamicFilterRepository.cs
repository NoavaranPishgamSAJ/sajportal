﻿namespace DataLayer
{

    #region Assembly

    using System.Collections.Generic;
    using System.Data;
    using System.Linq;
    using Dapper;
    using Dapper.Contrib.Extensions;
    using ORM.Condition;
    using ORM.Model;
    using Utility;
    using System;
    using System.Configuration;

    #endregion

    /// <summary>
    /// این کلاس برای انجام عملیات CURD دیتابیس استفاده می شود
    /// </summary>
    public class DynamicFilterRepository : IDisposable
    {

        #region Attribute

        /// <summary>
        /// رشته تماس با دیتابیس
        /// </summary>
        public string ConnectionString;

        /// <summary>
        /// رابط تماس با دیتابیس
        /// </summary>
        public IDbConnection Connection;

        /// <summary>
        /// قالب پرسجو دریافت اطلاعات
        /// </summary>
        public readonly string SelectStr = "SELECT {0} FROM {1} /**where**/ /**orderby**/";

        /// <summary>
        /// قالب پرسجو دریافت اطلاعات در حالت صفحه بندی
        /// </summary>
        private const string SelectPaggedStr = "SELECT * FROM (SELECT {0}, ROW_NUMBER() OVER(/**orderby**/) AS row_number FROM {1} /**where**/) AS PaginationTable WHERE row_number BETWEEN @start and @end";

        /// <summary>
        /// قالب پرسجو دریافت تعداد رکورد بازگردانده شده
        /// </summary>
        private const string CountStr = "SELECT COUNT({0}) FROM {1} /**where**/";

        /// <summary>
        /// قالب پرسجو حذف اطلاعات
        /// </summary>
        public readonly string DeleteStr = "DELETE {0} /**where**/";

        /// <summary>
        /// نسخه ایستا از کلاس
        /// </summary>
        private static DynamicFilterRepository _current;

        #endregion

        #region Property

        /// <summary>
        /// نسخه ایستا از کلاس
        /// </summary>
        public static DynamicFilterRepository Current
        {
            get
            {
                if (_current != null)
                    return _current;
                return _current = new DynamicFilterRepository();
            }
        }

        #endregion

        #region Method

        /// <summary>
        /// سازنده کلاس
        /// </summary>
        public DynamicFilterRepository()
        {
            ConnectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            Connection = new System.Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
        }

        /// <summary>
        /// دریافت نام جدول موجودیتی که در حال انجام عملیات بر روی آن است
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// string tableName = DynamicFilterRepository.Current.GetTableName&lt;Person&gt;();
        /// </code>
        /// </example>
        /// <returns>string</returns>
        public string GetTableName<T>() where T : class
        {
            try
            {
                var table = new BaseModel<T>().GetTableName();
                if (table.IsEmpty())
                    table = GetType().Name;
                return table;
            }
            catch (Exception ex) { Logger.Log(ex); }
            return string.Empty;
        }

        /// <summary>
        /// برقراری ارتباط با دیتابیس
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// OpenConnection();
        /// </code>
        /// </example>
        protected void OpenConnection()
        {
            if (Connection != null)
            {
                if (Connection.State != ConnectionState.Open)
                    Connection.Open();
            }
        }

        /// <summary>
        /// از این متد برای دریافت تعداد رکورد بازگردانده شده بر اساس شرط خاص استفاده می شود
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// long count = DynamicFilterRepository.Current.GetCount&lt;Person&gt;([condition], ["ID ASC"]);
        /// </code>
        /// </example>
        /// <param name="condition">شرط های اعمال شده در پرسجو: FilterExpressionList</param>
        /// <param name="orderBy">مرتب سازی بر اساس: string</param>
        /// <returns>long</returns>
        public long GetCount<T>(FilterExpressionList condition = null, string orderBy = "") where T : class
        {
            OpenConnection();
            var builder = new SqlBuilder();
            var template = builder.AddTemplate(string.Format(CountStr, "ItemId", GetTableName<T>()));

            if (condition != null && condition.Count > 0)
                builder.Where(condition.FilterString, condition.FilterValueParameter);

            builder.OrderBy(CommonUtil.IsEmpty(orderBy) ? "ItemId ASC" : orderBy);

            return Connection.Query<int>(CommonUtil.PersianFix(template.RawSql), template.Parameters).FirstOrDefault();
        }

        /// <summary>
        /// از این متد برای دریافت تعداد رکورد بازگردانده شده بر اساس شرط خاص استفاده می شود
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// long count = DynamicFilterRepository.Current.GetCount("Person", [condition], ["ID ASC"]);
        /// </code>
        /// </example>
        /// <param name="table">نام جدول در دیتابیس: string</param>
        /// <param name="condition">شرط های اعمال شده در پرسجو: FilterExpressionList</param>
        /// <param name="orderBy">مرتب سازی بر اساس: string</param>
        /// <returns>long</returns>
        public long GetCount(string table, FilterExpressionList condition = null, string orderBy = "")
        {
            OpenConnection();
            var builder = new SqlBuilder();
            var template = builder.AddTemplate(string.Format(CountStr, "ItemId", table));

            if (condition != null && condition.Count > 0)
                builder.Where(condition.FilterString, condition.FilterValueParameter);

            builder.OrderBy(CommonUtil.IsEmpty(orderBy) ? "ItemId ASC" : orderBy);

            return Connection.QueryFirstOrDefault<int>(CommonUtil.PersianFix(template.RawSql), template.Parameters);
        }

        /// <summary>
        /// از این متد برای دریافت رکوردهای جدول بر اساس شرط خاص استفاده می شود
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// IEnumerable&lt;Person&gt; list = DynamicFilterRepository.Current.GetList&lt;Person&gt;([condition], ["ID ASC"], ["*"]);
        /// </code>
        /// </example>
        /// <param name="condition">شرط های اعمال شده در پرسجو: FilterExpressionList</param>
        /// <param name="orderBy">مرتب سازی بر اساس: string</param>
        /// <param name="columns">فیلدهای مورد نظر جهت برگرداندن: string</param>
        /// <returns>IEnumerable&lt;GenericType&gt;</returns>
        public IEnumerable<T> GetList<T>(FilterExpressionList condition = null, string orderBy = "", string columns = "*") where T : class
        {
            OpenConnection();
            var builder = new SqlBuilder();
            var template = builder.AddTemplate(string.Format(SelectStr, columns, GetTableName<T>()));

            if (condition != null && condition.Count > 0)
                builder.Where(condition.FilterString, condition.FilterValueParameter);

            builder.OrderBy(CommonUtil.IsEmpty(orderBy) ? "ItemId ASC" : orderBy);

            return Connection.Query<T>(CommonUtil.PersianFix(template.RawSql), template.Parameters);
        }

        /// <summary>
        /// از این متد برای دریافت رکوردهای جدول بر اساس شرط خاص استفاده می شود
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// IEnumerable&lt;dynamic&gt; list = DynamicFilterRepository.Current.GetList("Person", [condition], ["ID ASC"], ["*"]);
        /// </code>
        /// </example>
        /// <param name="table">نام جدول در دیتابیس: string</param>
        /// <param name="condition">شرط های اعمال شده در پرسجو: FilterExpressionList</param>
        /// <param name="orderBy">مرتب سازی بر اساس: string</param>
        /// <param name="columns">فیلدهای مورد نظر جهت برگرداندن: string</param>
        /// <returns>IEnumerable&lt;dynamic&gt;</returns>
        public IEnumerable<dynamic> GetList(string table, FilterExpressionList condition = null, string orderBy = "", string columns = "*")
        {
            OpenConnection();
            var builder = new SqlBuilder();
            var template = builder.AddTemplate(string.Format(SelectStr, columns, table));

            if (condition != null && condition.Count > 0)
                builder.Where(condition.FilterString, condition.FilterValueParameter);

            builder.OrderBy(CommonUtil.IsEmpty(orderBy) ? "ItemId ASC" : orderBy);

            return Connection.Query(CommonUtil.PersianFix(template.RawSql), template.Parameters);
        }

        /// <summary>
        /// از این متد برای دریافت رکوردهای جدول بر اساس شرط خاص استفاده می شود
        /// </summary>
        /// <remarks>
        /// این متد برای دریافت رکوردها در یک ساختار با قابلیت صفحه بندی استفاده می شود
        /// </remarks>
        /// <typeparam name="T"></typeparam>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// IEnumerable&lt;Person&gt; list = DynamicFilterRepository.Current.GetList&lt;Person&gt;(2, 10, [condition], ["ID ASC"], ["*"]);
        /// </code>
        /// </example>
        /// <param name="condition">شرط های اعمال شده در پرسجو: FilterExpressionList</param>
        /// <param name="orderBy">مرتب سازی بر اساس: string</param>
        /// <param name="columns">فیلدهای مورد نظر جهت برگرداندن: string</param>
        /// <param name="page">اندیس صفحه مورد نظر: long</param>
        /// <param name="itemPerPage">تعداد رکورد در هر صفحه: int</param>
        /// <returns>IEnumerable&lt;GenericType&gt;</returns>
        public IEnumerable<T> GetList<T>(long page, int itemPerPage, FilterExpressionList condition = null, string orderBy = "", string columns = "*") where T : class
        {
            OpenConnection();
            var builder = new SqlBuilder();
            var template = builder.AddTemplate(string.Format(SelectPaggedStr, columns, GetTableName<T>()));

            if (condition != null && condition.Count > 0)
                builder.Where(condition.FilterString, condition.FilterValueParameter);

            builder.OrderBy(CommonUtil.IsEmpty(orderBy) ? "ItemId ASC" : orderBy);

            builder.AddParameters(new { start = page * itemPerPage + 1, end = ((page + 1) * itemPerPage) });

            return Connection.Query<T>(CommonUtil.PersianFix(template.RawSql), template.Parameters);
        }

        /// <summary>
        /// از این متد برای دریافت رکوردهای جدول بر اساس شرط خاص استفاده می شود
        /// </summary>
        /// <remarks>
        /// این متد برای دریافت رکوردها در یک ساختار با قابلیت صفحه بندی استفاده می شود
        /// </remarks>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// IEnumerable&lt;dynamic&gt; list = DynamicFilterRepository.Current.GetList("Person", 2, 10, [condition], ["ID ASC"], ["*"]);
        /// </code>
        /// </example>
        /// <param name="table">نام جدول در دیتابیس: string</param>
        /// <param name="condition">شرط های اعمال شده در پرسجو: FilterExpressionList</param>
        /// <param name="orderBy">مرتب سازی بر اساس: string</param>
        /// <param name="columns">فیلدهای مورد نظر جهت برگرداندن: string</param>
        /// <param name="page">اندیس صفحه مورد نظر: long</param>
        /// <param name="itemPerPage">تعداد رکورد در هر صفحه: int</param>
        /// <returns>IEnumerable&lt;dynamic&gt;</returns>
        public IEnumerable<dynamic> GetList(string table, long page, int itemPerPage, FilterExpressionList condition = null, string orderBy = "", string columns = "*")
        {
            OpenConnection();
            var builder = new SqlBuilder();
            var template = builder.AddTemplate(string.Format(SelectPaggedStr, columns, table));

            if (condition != null && condition.Count > 0)
                builder.Where(condition.FilterString, condition.FilterValueParameter);

            builder.OrderBy(CommonUtil.IsEmpty(orderBy) ? "ItemId ASC" : orderBy);

            builder.AddParameters(new { start = page * itemPerPage + 1, end = (page + 1) * itemPerPage });

            return Connection.Query(CommonUtil.PersianFix(template.RawSql), template.Parameters);
        }

        /// <summary>
        /// از این متد برای دریافت یک رکورد از جدول بر اساس کلید اصلی استفاده می شود
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// Person item = DynamicFilterRepository.Current.GetItem&lt;Person&gt;(24);
        /// </code>
        /// </example>
        /// <param name="itemId">شناسه رکورد مورد نظر: int</param>
        /// <returns>GenericType</returns>
        public T GetItem<T>(int itemId) where T : class
        {
            OpenConnection();
            return Connection.Get<T>(itemId);
        }

        /// <summary>
        /// از این متد برای دریافت یک رکورد از جدول بر اساس کلید اصلی استفاده می شود
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// Person item = DynamicFilterRepository.Current.GetItem("Person", 24);
        /// </code>
        /// </example>
        /// <param name="table">نام جدول در دیتابیس: string</param>
        /// <param name="itemId">شناسه رکورد مورد نظر: int</param>
        /// <returns>dynamic</returns>
        public dynamic GetItem(string table, int itemId)
        {
            var condition = new FilterExpressionList();
            condition.Add(new Filter("ItemId", SqlOperators.Equal, itemId));
            var list = GetList(table, condition);
            return list.FirstOrDefault();
        }

        /// <summary>
        /// از این متد برای دریافت یک رکورد از جدول بر اساس شرط خاص استفاده می شود
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// Person item = DynamicFilterRepository.Current.GetItem&lt;Person&gt;([condition], ["ID ASC"], ["*"]);
        /// </code>
        /// </example>
        /// <param name="condition">شرط های اعمال شده در پرسجو: FilterExpressionList</param>
        /// <param name="orderBy">مرتب سازی بر اساس: string</param>
        /// <param name="columns">فیلدهای مورد نظر جهت برگرداندن: string</param>
        /// <returns>GenericType</returns>
        public T GetItem<T>(FilterExpressionList condition = null, string orderBy = "", string columns = "*") where T : class
        {
            return GetList<T>(condition, orderBy, columns).FirstOrDefault();
        }

        /// <summary>
        /// از این متد برای دریافت یک رکورد از جدول بر اساس شرط خاص استفاده می شود
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// Person item = DynamicFilterRepository.Current.GetItem("Person", [condition], ["ID ASC"], ["*"]);
        /// </code>
        /// </example>
        /// <param name="table">نام جدول در دیتابیس: string</param>
        /// <param name="condition">شرط های اعمال شده در پرسجو: FilterExpressionList</param>
        /// <param name="orderBy">مرتب سازی بر اساس: string</param>
        /// <param name="columns">فیلدهای مورد نظر جهت برگرداندن: string</param>
        /// <returns>dynamic</returns>
        public dynamic GetItem(string table, FilterExpressionList condition = null, string orderBy = "", string columns = "*")
        {
            var list = GetList(table, condition, orderBy, columns);
            return list.FirstOrDefault();
        }

        /// <summary>
        /// از این متد برای ذخیره یک رکورد در جدول خاص استفاده می شود
        /// </summary>
        /// <remarks>
        /// بعد از ذخیره رکورد شناسه آن بازگردانده می شود
        /// </remarks>
        /// <typeparam name="T"></typeparam>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// class Person {
        ///     public int ID;
        ///     public string Name;
        /// }
        /// DynamicFilterRepository.Current.CreateItem(new Person { ID = 1, Name = "John" });
        /// </code>
        /// </example>
        /// <param name="item">موجودیت مورد نظر جهت ذخیره: GenericType</param>
        /// <returns>long</returns>
        public long CreateItem<T>(T item) where T : class
        {
            OpenConnection();
            return Connection.Insert(item);
        }

        /// <summary>
        /// از این متد برای ویرایش یک رکورد در جدول خاص استفاده می شود
        /// </summary>
        /// <remarks>
        /// بعد از ویرایش رکورد در صورت موفقیت آمیز بودن مقدار صحیح بازگردانده می شود
        /// </remarks>
        /// <typeparam name="T"></typeparam>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// class Person {
        ///     public int ID;
        ///     public string Name;
        /// }
        /// var person = DynamicFilterRepository.Current.GetItem&lt;Person&gt;(23);
        /// person.Name = "John Doue";
        /// DynamicFilterRepository.Current.UpdateItem(person);
        /// </code>
        /// </example>
        /// <param name="item">موجودیت مورد نظر جهت ویرایش: GenericType</param>
        /// <returns>bool</returns>
        public bool UpdateItem<T>(T item) where T : class
        {
            OpenConnection();
            return Connection.Update(item);
        }

        /// <summary>
        /// از این متد برای حذف یک رکورد در جدول خاص استفاده می شود
        /// </summary>
        /// <remarks>
        /// بعد از حذف رکورد در صورت موفقیت آمیز بودن مقدار صحیح بازگردانده می شود
        /// </remarks>
        /// <typeparam name="T"></typeparam>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// class Person {
        ///     public int ID;
        ///     public string Name;
        /// }
        /// var person = DynamicFilterRepository.Current.GetItem&lt;Person&gt;(23);
        /// DynamicFilterRepository.Current.DeleteItem(person);
        /// </code>
        /// </example>
        /// <param name="item">موجودیت مورد نظر جهت ویرایش: GenericType</param>
        /// <returns>bool</returns>
        public bool DeleteItem<T>(T item) where T : class
        {
            OpenConnection();
            return Connection.Delete(item);
        }

        /// <summary>
        /// از این متد برای حذف یک رکورد در جدول خاص استفاده می شود
        /// </summary>
        /// <remarks>
        /// بعد از حذف رکورد در صورت موفقیت آمیز بودن مقدار صحیح بازگردانده می شود
        /// </remarks>
        /// <typeparam name="T"></typeparam>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// DynamicFilterRepository.Current.DeleteItem&lt;Person&gt;(23);
        /// </code>
        /// </example>
        /// <param name="itemId">شناسه رکورد مورد نظر: int</param>
        /// <returns>bool</returns>
        public bool DeleteItem<T>(int itemId) where T : class
        {
            OpenConnection();
            var item = Connection.Get<T>(itemId);
            return Connection.Delete(item);
        }

        /// <summary>
        /// از این متد برای حذف کلیه رکوردهای جدول خاص استفاده می شود
        /// </summary>
        /// <remarks>
        /// بعد از حذف رکورد در صورت موفقیت آمیز بودن مقدار صحیح بازگردانده می شود
        /// </remarks>
        /// <typeparam name="T"></typeparam>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// DynamicFilterRepository.Current.DeleteAllItem&lt;Person&gt;();
        /// </code>
        /// </example>
        /// <returns>bool</returns>
        public bool DeleteAllItem<T>() where T : class
        {
            OpenConnection();
            return Connection.DeleteAll<T>();
        }

        /// <summary>
        /// از این متد برای حذف یکسری رکورد در جدول براساس شرط خاص استفاده می شود
        /// </summary>
        /// <remarks>
        /// بعد از حذف رکورد در صورت موفقیت آمیز بودن یک عدد بازگردانده می شود
        /// </remarks>
        /// <typeparam name="T"></typeparam>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// DynamicFilterRepository.Current.DeleteList&lt;Person&gt;([condition]);
        /// </code>
        /// </example>
        /// <param name="condition">شرط های اعمال شده در پرسجو: FilterExpressionList</param>
        /// <returns>int</returns>
        public int DeleteList<T>(FilterExpressionList condition = null) where T : class
        {
            OpenConnection();
            var builder = new SqlBuilder();
            var template = builder.AddTemplate(string.Format(DeleteStr, GetTableName<T>()));

            if (condition != null && condition.Count > 0)
                builder.Where(condition.FilterString, condition.FilterValueParameter);

            return Connection.Execute(CommonUtil.PersianFix(template.RawSql), template.Parameters);
        }

        /// <summary>
        /// از این متد برای اجرای دستورات sql به صورت مستقیم استفاده می شود
        /// </summary>
        /// <remarks>
        /// بعد از انجام عملیات در صورت موفقیت آمیز بودن یک عدد بازگردانده می شود
        /// </remarks>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// DynamicParameters params = new DynamicParameters();
        /// params.Add("ID", 23);
        /// int return = DynamicFilterRepository.Current.Execute("SELECT * FROM Person WHERE ID = @ID", [params], [CommandType.Text]);
        /// </code>
        /// </example>
        /// <param name="command">رشته پرسجو مورد نظر: string</param>
        /// <param name="param">پارامترهای ارسالی: DynamicParameters</param>
        /// <param name="type">نوع پرسجو: CommandType</param>
        /// <returns>int</returns>
        public int Execute(string command, DynamicParameters param = null, CommandType type = CommandType.Text)
        {
            OpenConnection();
            return Connection.Execute(command, param, null, null, type);
        }

        /// <summary>
        /// از این متد برای اجرای یک رویه در دیتابیس استفاده می شود
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// DynamicParameters params = new DynamicParameters();
        /// params.Add("ID", 23);
        /// IEnumerable&lt;Person&gt; list = DynamicFilterRepository.Current.StoredProcedures&lt;Person&gt;("PesronList", params, CommandType.StoredProcedure);
        /// </code>
        /// </example>
        /// <param name="procedureName">نام رویه: string</param>
        /// <param name="param">پارامترهای ارسالی: DynamicParameters</param>
        /// <param name="type">نوع پرسجو: CommandType</param>
        /// <returns>IEnumerable&lt;GenericType&gt;</returns>
        public IEnumerable<T> StoredProcedures<T>(string procedureName, DynamicParameters param, CommandType type) where T : class
        {
            OpenConnection();
            return Connection.Query<T>(procedureName, param, null, true, null, type);
        }

        /// <summary>
        /// از این متد برای اجرای دستورات sql به صورت مستقیم استفاده می شود
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// IEnumerable&lt;dynamic&gt; list = DynamicFilterRepository.Current.Query("SELECT * FROM Person");
        /// </code>
        /// </example>
        /// <param name="command">رشته پرسجو مورد نظر: string</param>
        /// <returns>IEnumerable&lt;dynamic&gt;</returns>
        public IEnumerable<dynamic> Query(string command)
        {
            OpenConnection();
            return Connection.Query(command);
        }

        /// <summary>
        /// ازبین برنده کلاس
        /// </summary>
        /// <param name="disposing">حذف ارتباط با دیتابیس: bool</param>
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                Connection.Dispose();
            }
        }

        /// <summary>
        /// ازبین برنده کلاس
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        #endregion

    }

}
