﻿namespace DataLayer
{

    #region Assembly

    using System.Threading.Tasks;
    using System.Collections.Generic;
    using ORM.Condition;
    using ORM.Model;
    using System;
    using Utility;
    using ORM.Enums;

    #endregion

    public static class MembershipAsyncRepository
    {

        #region Method

        public static async Task<bool> AddRolePermission(int roleId, int permissionId)
        {
            try
            {
                if (await DynamicFilterAsyncRepository.CreateItem(new RolePermission { RoleId = roleId, PermissionId = permissionId, AddedDate = DateTime.Today }) != -1)
                    return true;
            }
            catch (Exception ex) { Logger.Log(ex); }
            return false;
        }

        public static async Task<bool> AddRolePermission(int roleId, string displayName)
        {
            try
            {
                var item = await DynamicFilterAsyncRepository.GetItem<Permission>(new FilterExpressionList(new[] { new Filter("DisplayName", SqlOperators.Equal, displayName) }));
                if (item != null)
                {
                    var permissionId = item.ItemId;
                    if (await DynamicFilterAsyncRepository.CreateItem(new RolePermission { RoleId = roleId, PermissionId = permissionId, AddedDate = DateTime.Today }) != -1)
                        return true;
                }
            }
            catch (Exception ex) { Logger.Log(ex); }
            return false;
        }

        public static async Task<bool> AddRolePermissionExtra(int roleId, string devName)
        {
            try
            {
                var item = await DynamicFilterAsyncRepository.GetItem<Permission>(new FilterExpressionList(new[] { new Filter("DevName", SqlOperators.Equal, devName) }));
                if (item != null)
                {
                    var permissionId = item.ItemId;
                    if (await DynamicFilterAsyncRepository.CreateItem(new RolePermission { RoleId = roleId, PermissionId = permissionId, AddedDate = DateTime.Today }) != -1)
                        return true;
                }
            }
            catch (Exception ex) { Logger.Log(ex); }
            return false;
        }

        public static async Task<bool> AddUserPermission(int customerId, int permissionId)
        {
            try
            {
                var item = await DynamicFilterAsyncRepository.GetItem<CustomerPermission>(new FilterExpressionList(new[] { new Filter("CustomerId", SqlOperators.Equal, customerId), new Filter("PermissionId", SqlOperators.Equal, permissionId), new Filter("IsHave", SqlOperators.Equal, false) }));
                if (item != null)
                {
                    return await DynamicFilterAsyncRepository.DeleteItem<CustomerPermission>(item.ItemId);
                }
                if (await DynamicFilterAsyncRepository.CreateItem(new CustomerPermission { CustomerId = customerId, PermissionId = permissionId, IsHave = Toggle.True, AddedDate = DateTime.Today }) != -1)
                    return true;
            }
            catch (Exception ex) { Logger.Log(ex); }
            return false;
        }

        public static async Task<bool> AddUserPermission(int customerId, string displayName)
        {
            try
            {
                var permissionItem = await DynamicFilterAsyncRepository.GetItem<Permission>(new FilterExpressionList(new[] { new Filter("DisplayName", SqlOperators.Equal, displayName) }));
                if (permissionItem != null)
                {
                    var permissionId = permissionItem.ItemId;
                    var item = await DynamicFilterAsyncRepository.GetItem<CustomerPermission>(new FilterExpressionList(new[] { new Filter("CustomerId", SqlOperators.Equal, customerId), new Filter("PermissionId", SqlOperators.Equal, permissionId), new Filter("IsHave", SqlOperators.Equal, false) }));
                    if (item != null)
                    {
                        return await DynamicFilterAsyncRepository.DeleteItem<CustomerPermission>(item.ItemId);
                    }
                    if (await DynamicFilterAsyncRepository.CreateItem(new CustomerPermission { CustomerId = customerId, PermissionId = permissionId, IsHave = Toggle.True, AddedDate = DateTime.Today }) != -1)
                        return true;
                }
            }
            catch (Exception ex) { Logger.Log(ex); }
            return false;
        }

        public static async Task<bool> AddUserPermissionExtra(int customerId, string devName)
        {
            try
            {
                var permissionItem = await DynamicFilterAsyncRepository.GetItem<Permission>(new FilterExpressionList(new[] { new Filter("DevName", SqlOperators.Equal, devName) }));
                if (permissionItem != null)
                {
                    var permissionId = permissionItem.ItemId;
                    var item = await DynamicFilterAsyncRepository.GetItem<CustomerPermission>(new FilterExpressionList(new[] { new Filter("CustomerId", SqlOperators.Equal, customerId), new Filter("PermissionId", SqlOperators.Equal, permissionId), new Filter("IsHave", SqlOperators.Equal, false) }));
                    if (item != null)
                    {
                        return await DynamicFilterAsyncRepository.DeleteItem<CustomerPermission>(item.ItemId);
                    }
                    if (await DynamicFilterAsyncRepository.CreateItem(new CustomerPermission { CustomerId = customerId, PermissionId = permissionId, IsHave = Toggle.True, AddedDate = DateTime.Today }) != -1)
                        return true;
                }
            }
            catch (Exception ex) { Logger.Log(ex); }
            return false;
        }

        public static async Task<bool> DeleteUserPermission(int customerId, int permissionId)
        {
            try
            {
                var item = await DynamicFilterAsyncRepository.GetItem<CustomerPermission>(new FilterExpressionList(new[] { new Filter("CustomerId", SqlOperators.Equal, customerId), new Filter("PermissionId", SqlOperators.Equal, permissionId), new Filter("IsHave", SqlOperators.Equal, true) }));
                if (item != null)
                {
                    return await DynamicFilterAsyncRepository.DeleteItem<CustomerPermission>(item.ItemId);
                }
                if (await DynamicFilterAsyncRepository.CreateItem(new CustomerPermission { CustomerId = customerId, PermissionId = permissionId, IsHave = Toggle.False, AddedDate = DateTime.Today }) != -1)
                    return true;
            }
            catch (Exception ex) { Logger.Log(ex); }
            return false;
        }

        public static async Task<bool> DeleteUserPermission(int customerId, string displayName)
        {
            try
            {
                var permissionItem = await DynamicFilterAsyncRepository.GetItem<Permission>(new FilterExpressionList(new[] { new Filter("DisplayName", SqlOperators.Equal, displayName) }));
                if (permissionItem != null)
                {
                    var permissionId = permissionItem.ItemId;
                    var item = await DynamicFilterAsyncRepository.GetItem<CustomerPermission>(new FilterExpressionList(new[] { new Filter("CustomerId", SqlOperators.Equal, customerId), new Filter("PermissionId", SqlOperators.Equal, permissionId), new Filter("IsHave", SqlOperators.Equal, true) }));
                    if (item != null)
                    {
                        return await DynamicFilterAsyncRepository.DeleteItem<CustomerPermission>(item.ItemId);
                    }
                    if (await DynamicFilterAsyncRepository.CreateItem(new CustomerPermission { CustomerId = customerId, PermissionId = permissionId, IsHave = Toggle.False, AddedDate = DateTime.Today }) != -1)
                        return true;
                }
            }
            catch (Exception ex) { Logger.Log(ex); }
            return false;
        }

        public static async Task<bool> DeleteUserPermissionExtra(int customerId, string devName)
        {
            try
            {
                var permissionItem = await DynamicFilterAsyncRepository.GetItem<Permission>(new FilterExpressionList(new[] { new Filter("DevName", SqlOperators.Equal, devName) }));
                if (permissionItem != null)
                {
                    var permissionId = permissionItem.ItemId;
                    var item = await DynamicFilterAsyncRepository.GetItem<CustomerPermission>(new FilterExpressionList(new[] { new Filter("CustomerId", SqlOperators.Equal, customerId), new Filter("PermissionId", SqlOperators.Equal, permissionId), new Filter("IsHave", SqlOperators.Equal, true) }));
                    if (item != null)
                    {
                        return await DynamicFilterAsyncRepository.DeleteItem<CustomerPermission>(item.ItemId);
                    }
                    if (await DynamicFilterAsyncRepository.CreateItem(new CustomerPermission { CustomerId = customerId, PermissionId = permissionId, IsHave = Toggle.False, AddedDate = DateTime.Today }) != -1)
                        return true;
                }
            }
            catch (Exception ex) { Logger.Log(ex); }
            return false;
        }

        public static async Task<int> DeleteRolePermissions(int roleId)
        {
            try
            {

                return await DynamicFilterAsyncRepository.DeleteList<RolePermission>(new FilterExpressionList(new[] { new Filter("RoleId", SqlOperators.Equal, roleId) }));
            }
            catch (Exception ex) { Logger.Log(ex); }
            return -1;
        }

        public static async Task<bool> DeleteRolePermission(int roleId, int permissionId)
        {
            try
            {
                var item = await DynamicFilterAsyncRepository.GetItem<RolePermission>(new FilterExpressionList(new[] { new Filter("RoleId", SqlOperators.Equal, roleId), new Filter("PermissionId", SqlOperators.Equal, permissionId) }));
                if (item != null)
                {
                    return await DynamicFilterAsyncRepository.DeleteItem<RolePermission>(item.ItemId);
                }
            }
            catch (Exception ex) { Logger.Log(ex); }
            return false;
        }

        public static async Task<bool> DeleteRolePermission(int roleId, string displayName)
        {
            try
            {
                var permissionItem = await DynamicFilterAsyncRepository.GetItem<Permission>(new FilterExpressionList(new[] { new Filter("DisplayName", SqlOperators.Equal, displayName) }));
                if (permissionItem != null)
                {
                    var permissionId = permissionItem.ItemId;
                    var item = await DynamicFilterAsyncRepository.GetItem<RolePermission>(new FilterExpressionList(new[] { new Filter("RoleId", SqlOperators.Equal, roleId), new Filter("PermissionId", SqlOperators.Equal, permissionId) }));
                    if (item != null)
                    {
                        return await DynamicFilterAsyncRepository.DeleteItem<RolePermission>(item.ItemId);
                    }
                }
            }
            catch (Exception ex) { Logger.Log(ex); }
            return false;
        }

        public static async Task<bool> DeleteRolePermissionExtra(int roleId, string devName)
        {
            try
            {
                var permissionItem = await DynamicFilterAsyncRepository.GetItem<Permission>(new FilterExpressionList(new[] { new Filter("DevName", SqlOperators.Equal, devName) }));
                if (permissionItem != null)
                {
                    var permissionId = permissionItem.ItemId;
                    var item = await DynamicFilterAsyncRepository.GetItem<RolePermission>(new FilterExpressionList(new[] { new Filter("RoleId", SqlOperators.Equal, roleId), new Filter("PermissionId", SqlOperators.Equal, permissionId) }));
                    if (item != null)
                    {
                        return await DynamicFilterAsyncRepository.DeleteItem<RolePermission>(item.ItemId);
                    }
                }
            }
            catch (Exception ex) { Logger.Log(ex); }
            return false;
        }

        public static async Task<Customer> GetUser(string username)
        {
            try
            {
                var item = await DynamicFilterAsyncRepository.GetItem<Customer>(new FilterExpressionList(new[] { new Filter("Username", SqlOperators.Equal, username) }));
                if (item != null)
                    return item;
            }
            catch (Exception ex) { Logger.Log(ex); }
            return null;
        }

        public static async Task<bool> Login(string username, string password)
        {
            try
            {
                var item = await DynamicFilterAsyncRepository.GetItem<Customer>(new FilterExpressionList(new[] { new Filter("Username", SqlOperators.Equal, username), new Filter("Password", SqlOperators.Equal, password) }));
                if (item != null)
                    return true;
            }
            catch (Exception ex) { Logger.Log(ex); }
            return false;
        }

        public static async Task<bool> RoleHasPermission(int roleId, int permissionId)
        {
            try
            {
                var item = await DynamicFilterAsyncRepository.GetItem<RolePermission>(new FilterExpressionList(new[] { new Filter("RoleId", SqlOperators.Equal, roleId), new Filter("PermissionId", SqlOperators.Equal, permissionId) }));
                if (item != null)
                    return true;
            }
            catch (Exception ex) { Logger.Log(ex); }
            return false;
        }

        public static async Task<bool> RoleHasPermission(int roleId, string displayName)
        {
            try
            {
                var item = await DynamicFilterAsyncRepository.GetItem<RolePermissionView>(new FilterExpressionList(new[] { new Filter("RoleId", SqlOperators.Equal, roleId), new Filter("PermissionDisplayName", SqlOperators.Equal, displayName) }));
                if (item != null)
                    return true;
            }
            catch (Exception ex) { Logger.Log(ex); }
            return false;
        }

        public static async Task<bool> RoleHasPermissionExtra(int roleId, string devName)
        {
            try
            {
                var item = await DynamicFilterAsyncRepository.GetItem<RolePermissionView>(new FilterExpressionList(new[] { new Filter("RoleId", SqlOperators.Equal, roleId), new Filter("PermissionDevName", SqlOperators.Equal, devName) }));
                if (item != null)
                    return true;
            }
            catch (Exception ex) { Logger.Log(ex); }
            return false;
        }

        public static async Task<bool> UserHasPermission(int customerId, int permissionId)
        {
            try
            {
                var filters = new FilterExpressionList(new[] { new Filter("CustomerId", SqlOperators.Equal, customerId), new Filter("PermissionId", SqlOperators.Equal, permissionId) });
                var item = await PermissionAsyncRepository.Get(filters.FilterString, filters.FilterValueParameter);
                if (item != null)
                    return true;
            }
            catch (Exception ex) { Logger.Log(ex); }
            return false;
        }

        public static async Task<bool> UserHasPermission(int customerId, string displayName)
        {
            try
            {
                var filters = new FilterExpressionList(new[] { new Filter("CustomerId", SqlOperators.Equal, customerId), new Filter("DisplayName", SqlOperators.Equal, displayName) });
                var item = await PermissionAsyncRepository.Get(filters.FilterString, filters.FilterValueParameter);
                if (item != null)
                    return true;
            }
            catch (Exception ex) { Logger.Log(ex); }
            return false;
        }

        public static async Task<bool> UserHasPermissionExtra(int customerId, string devName)
        {
            try
            {
                var filters = new FilterExpressionList(new[] { new Filter("CustomerId", SqlOperators.Equal, customerId), new Filter("DevName", SqlOperators.Equal, devName) });
                var item = await PermissionAsyncRepository.Get(filters.FilterString, filters.FilterValueParameter);
                if (item != null)
                    return true;
            }
            catch (Exception ex) { Logger.Log(ex); }
            return false;
        }

        public static async Task<IEnumerable<CustomerPermissionView>> UserPermissions(int customerId)
        {
            try
            {
                return await DynamicFilterAsyncRepository.GetList<CustomerPermissionView>(new FilterExpressionList(new[] { new Filter("CustomerId", SqlOperators.Equal, customerId) }));
            }
            catch (Exception ex) { Logger.Log(ex); }
            return null;
        }

        public static async Task<IEnumerable<CustomerPermissions>> UserExistPermissions(int customerId)
        {
            try
            {
                var filters = new FilterExpressionList(new[] { new Filter("CustomerId", SqlOperators.Equal, customerId) });
                return await PermissionAsyncRepository.GetList(filters.FilterString, filters.FilterValueParameter);
            }
            catch (Exception ex) { Logger.Log(ex); }
            return null;
        }

        public static async Task<IEnumerable<RolePermissionView>> RolePermissions(int roleId)
        {
            try
            {
                return await DynamicFilterAsyncRepository.GetList<RolePermissionView>(new FilterExpressionList(new[] { new Filter("RoleId", SqlOperators.Equal, roleId) }));
            }
            catch (Exception ex) { Logger.Log(ex); }
            return null;
        }

        public static async Task<IEnumerable<CustomerPositionView>> UserPositions(int customerId)
        {
            try
            {
                return await DynamicFilterAsyncRepository.GetList<CustomerPositionView>(new FilterExpressionList(new[] { new Filter("CustomerId", SqlOperators.Equal, customerId) }));
            }
            catch (Exception ex) { Logger.Log(ex); }
            return null;
        }

        public static async Task<IEnumerable<CustomerRoleView>> UserRoles(int customerId)
        {
            try
            {
                return await DynamicFilterAsyncRepository.GetList<CustomerRoleView>(new FilterExpressionList(new[] { new Filter("CustomerId", SqlOperators.Equal, customerId) }));
            }
            catch (Exception ex) { Logger.Log(ex); }
            return null;
        }

        public static async Task<bool> UserHasPosition(int customerId, string displayName)
        {
            try
            {
                var item = await DynamicFilterAsyncRepository.GetItem<CustomerPositionView>(new FilterExpressionList(new[] { new Filter("CustomerId", SqlOperators.Equal, customerId), new Filter("PositionDisplayName", SqlOperators.Equal, displayName) }));
                if (item != null)
                    return true;
            }
            catch (Exception ex) { Logger.Log(ex); }
            return false;
        }

        public static async Task<bool> AddUserPosition(int customerId, string displayName)
        {
            try
            {
                var positionItem = await DynamicFilterAsyncRepository.GetItem<Position>(new FilterExpressionList(new[] { new Filter("DisplayName", SqlOperators.Equal, displayName) }));
                if (positionItem != null)
                {
                    var positionId = positionItem.ItemId;
                    if (await DynamicFilterAsyncRepository.CreateItem(new CustomerPosition { CustomerId = customerId, PositionId = positionId, AddedDate = DateTime.Today }) != -1)
                        return true;
                }
            }
            catch (Exception ex) { Logger.Log(ex); }
            return false;
        }

        public static async Task<bool> DeleteUserPosition(int customerId, string displayName)
        {
            try
            {
                var positionItem = await DynamicFilterAsyncRepository.GetItem<Position>(new FilterExpressionList(new[] { new Filter("DisplayName", SqlOperators.Equal, displayName) }));
                if (positionItem != null)
                {
                    var positionId = positionItem.ItemId;
                    var item = await DynamicFilterAsyncRepository.GetItem<CustomerPosition>(new FilterExpressionList(new[] { new Filter("CustomerId", SqlOperators.Equal, customerId), new Filter("PositionId", SqlOperators.Equal, positionId) }));
                    if (item != null)
                    {
                        return await DynamicFilterAsyncRepository.DeleteItem<CustomerPosition>(item.ItemId);
                    }
                }
            }
            catch (Exception ex) { Logger.Log(ex); }
            return false;
        }

        public static async Task<bool> UserHasRole(int customerId, string displayName)
        {
            try
            {
                var item = await DynamicFilterAsyncRepository.GetItem<CustomerRoleView>(new FilterExpressionList(new[] { new Filter("CustomerId", SqlOperators.Equal, customerId), new Filter("RoleDisplayName", SqlOperators.Equal, displayName) }));
                if (item != null)
                    return true;
            }
            catch (Exception ex) { Logger.Log(ex); }
            return false;
        }

        public static async Task<bool> AddUserRole(int customerId, string displayName)
        {
            try
            {
                var roleItem = await DynamicFilterAsyncRepository.GetItem<Role>(new FilterExpressionList(new[] { new Filter("DisplayName", SqlOperators.Equal, displayName) }));
                if (roleItem != null)
                {
                    var roleId = roleItem.ItemId;
                    if (await DynamicFilterAsyncRepository.CreateItem(new CustomerRole { CustomerId = customerId, RoleId = roleId, AddedDate = DateTime.Today }) != -1)
                        return true;
                }
            }
            catch (Exception ex) { Logger.Log(ex); }
            return false;
        }

        public static async Task<bool> DeleteUserRole(int customerId, string displayName)
        {
            try
            {
                var roleItem = await DynamicFilterAsyncRepository.GetItem<Role>(new FilterExpressionList(new[] { new Filter("DisplayName", SqlOperators.Equal, displayName) }));
                if (roleItem != null)
                {
                    var roleId = roleItem.ItemId;
                    var item = await DynamicFilterAsyncRepository.GetItem<CustomerRoleView>(new FilterExpressionList(new[] { new Filter("CustomerId", SqlOperators.Equal, customerId), new Filter("RoleId", SqlOperators.Equal, roleId) }));
                    if (item != null)
                    {
                        return await DynamicFilterAsyncRepository.DeleteItem<CustomerRole>(item.ItemId);
                    }
                }
            }
            catch (Exception ex) { Logger.Log(ex); }
            return false;
        }

        #endregion

    }
}
