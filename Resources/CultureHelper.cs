﻿namespace Resources
{

    #region Assembely

    using System.Web.SessionState;
    using System.Threading;
    using System.Globalization;
    using Utility;

    #endregion

    /// <summary>
    /// از این کلاس برای بحث چند زبانه سازی استفاده می شود
    /// </summary>
    public class CultureHelper
    {

        #region Attribute

        /// <summary>
        /// جلسه کاربر فعال
        /// </summary>
        protected HttpSessionState Session;

        #endregion

        #region Property

        /// <summary>
        /// تعیین و دریافت فرهنگ اینترفیس فعال براساس عدد متناظر با آن
        /// </summary>
        public static int CurrentCulture
        {
            get
            {
                switch (Thread.CurrentThread.CurrentUICulture.Name)
                {
                    case "en-GB":
                        return 0;
                    case "fa-IR":
                        return 1;
                }
                return 0;
            }
            set
            {
                switch (value)
                {
                    case 0:
                        Thread.CurrentThread.CurrentUICulture = Thread.CurrentThread.CurrentCulture = new CultureInfo("en-GB");
                        break;
                    case 1:
                        Thread.CurrentThread.CurrentUICulture = Thread.CurrentThread.CurrentCulture = new CultureInfo("fa-IR");
                        break;
                    default:
                        Thread.CurrentThread.CurrentUICulture = Thread.CurrentThread.CurrentCulture = CultureInfo.InvariantCulture;
                        break;
                }
            }
        }

        #endregion

        #region Method

        /// <summary>
        /// سازنده کلاس
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// CultureHelper cultureHelper = new CultureHelper(HttpContext.Current.Session);
        /// </code>
        /// </example>
        /// <param name="httpSessionState">جلسه کاربر فعال: System.Web.SessionState.HttpSessionState</param>
        public CultureHelper(HttpSessionState httpSessionState)
        {
            Session = httpSessionState;
        }

        /// <summary>
        /// این متد مشخص می کند که نوشتار فرهنگ فعلی راست به چپ است یا چپ به راست
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// CultureHelper cultureHelper = new CultureHelper(HttpContext.Current.Session);
        /// bool isRighToLeft = cultureHelper.IsRighToLeft();
        /// </code>
        /// </example>
        /// <returns>bool</returns>
        public static bool IsRighToLeft()
        {
            return Thread.CurrentThread.CurrentUICulture.TextInfo.IsRightToLeft;

        }

        /// <summary>
        /// این متد نام فرهنگ کاربر فعال فعلی را باز می گرداند
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// CultureHelper cultureHelper = new CultureHelper(HttpContext.Current.Session);
        /// string currentCulture = cultureHelper.GetCurrentCulture();
        /// </code>
        /// </example>
        /// <returns>string</returns>
        public static string GetCurrentCulture()
        {
            return Thread.CurrentThread.CurrentUICulture.Name;
        }

        /// <summary>
        /// این متد نام فرهنگ خنثی کاربر فعال فعلی را باز می گرداند
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// CultureHelper cultureHelper = new CultureHelper(HttpContext.Current.Session);
        /// string currentNeutralCulture = cultureHelper.GetCurrentNeutralCulture();
        /// </code>
        /// </example>
        /// <returns>string</returns>
        public static string GetCurrentNeutralCulture()
        {
            return GetNeutralCulture(Thread.CurrentThread.CurrentUICulture.Name);
        }

        /// <summary>
        /// این متد نام فرهنگ خنثی از رشته دلخواه را باز می گرداند
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// CultureHelper cultureHelper = new CultureHelper(HttpContext.Current.Session);
        /// string neutralCulture = cultureHelper.GetNeutralCulture("fa-IR");
        /// </code>
        /// </example>
        /// <param name="name">نام فرهنگ: string</param>
        /// <returns>string</returns>
        public static string GetNeutralCulture(string name)
        {
            return !name.Contains("-") ? name : name.Split('-')[0];
        }

        /// <summary>
        /// این متد فرهنگ اینترفیس فعال را تعیین می کند
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// CultureHelper cultureHelper = new CultureHelper(HttpContext.Current.Session);
        /// cultureHelper.SetCurrentCulture("fa-IR");
        /// </code>
        /// </example>
        /// <param name="name">نام فرهنگ: string</param>
        public static void SetCurrentCulture(string name)
        {
            switch (name)
            {
                case "en-GB":
                    CurrentCulture = 0;
                    break;
                case "fa-IR":
                    CurrentCulture = 1;
                    break;
            }
        }

        /// <summary>
        /// این متد متن متناظر با کلید داده شده را از لیست واژگان بر اساس فرهنگ کاربر فعال باز می گرداند
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// string str = CultureHelper.Localize("ID");
        /// </code>
        /// </example>
        /// <param name="key">کلید واژه: string</param>
        /// <returns>string</returns>
        public static string Localize(string key)
        {
            try
            {
                var s = Global.ResourceManager.GetString(key);
                return s != null && s.Trim().IsNotEmpty() ? Global.ResourceManager.GetString(key) : key;
            }
            catch
            {
                return key;
            }
        }

        #endregion

    }
}