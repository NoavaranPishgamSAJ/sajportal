﻿namespace Utility
{

    #region Assembly

    using System;
    using System.Threading;
    using System.IO;
    using System.Runtime.Serialization.Formatters.Binary;
    using System.Collections.Generic;
    using System.Linq;
    using Newtonsoft.Json;
    using System.ComponentModel;

    #endregion

    /// <summary>
    /// از این کلاس برای توسعه کلاس های دات نت استفاده می شود
    /// </summary>
    public static class Extensions
    {

        #region Extensions

        /// <summary>
        /// از این متد برای بررسی برابر بودن دو موجودیت استفاده می شود
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// bool isEqual = "Test".Equal("test");
        /// </code>
        /// </example>
        /// <param name="o">موجودیت اول از نوع صوری می باشد: object</param>
        /// <param name="value">موجودیت دوم: object</param>
        /// <returns>bool</returns>
        public static bool Equal(this object o, object value)
        {
            return o != null && o.Equals(value);
        }

        /// <summary>
        /// از این متد برای بررسی برابر نبودن دو موجودیت استفاده می شود
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// bool isNotEqual = "Test".NotEqual("test");
        /// </code>
        /// </example>
        /// <param name="o">موجودیت اول از نوع صوری می باشد: object</param>
        /// <param name="value">موجودیت دوم: object</param>
        /// <returns>bool</returns>
        public static bool NotEqual(this object o, object value)
        {
            return o != null && !o.Equals(value);
        }

        /// <summary>
        /// از این متد برای بررسی عدد بودن مقدار یک رشته استفاده می شود
        /// </summary>
        /// <remarks>
        /// از این متد فقط در رشته ها می توان بهره برد
        /// </remarks>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// bool isNumeric = "123".IsNumeric();
        /// </code>
        /// </example>
        /// <param name="s">مقدار از نوع صوری می باشد: string</param>
        /// <returns>bool</returns>
        public static bool IsNumeric(this string s)
        {
            return Microsoft.VisualBasic.Information.IsNumeric(s);
        }

        /// <summary>
        /// از این متد برای بررسی تهی بودن مقدار یک رشته استفاده می شود
        /// </summary>
        /// <remarks>
        /// از این متد فقط در رشته ها می توان بهره برد
        /// </remarks>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// bool isEmpty = "".IsEmpty();
        /// </code>
        /// </example>
        /// <param name="s">مقدار از نوع صوری می باشد: string</param>
        /// <returns>bool</returns>
        public static bool IsEmpty(this string s)
        {
            return CommonUtil.IsEmpty(s);
        }

        /// <summary>
        /// از این متد برای بررسی تهی نبودن مقدار یک رشته استفاده می شود
        /// </summary>
        /// <remarks>
        /// از این متد فقط در رشته ها می توان بهره برد
        /// </remarks>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// bool isNotEmpty = "".IsNotEmpty();
        /// </code>
        /// </example>
        /// <param name="s">مقدار از نوع صوری می باشد: string</param>
        /// <returns>bool</returns>
        public static bool IsNotEmpty(this string s)
        {
            return CommonUtil.IsNotEmpty(s);
        }

        /// <summary>
        /// از این متد برای محاسبه تعداد روزهای یک موجودیت تاریخ استفاده می شود
        /// </summary>
        /// <remarks>
        /// از این متد فقط در تاریخ ها می توان بهره برد
        /// </remarks>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// int totalDays = DateTime.Now.TotalDays();
        /// </code>
        /// </example>
        /// <param name="date">مقدار از نوع صوری می باشد: System.DateTime</param>
        /// <returns>int</returns>
        public static int TotalDays(this DateTime date)
        {
            return int.Parse(new TimeSpan(date.Ticks).TotalDays.ToString(Thread.CurrentThread.CurrentCulture));
        }

        /// <summary>
        /// از این متد برای دریافت مقدار متغیر استفاده می شود
        /// </summary>
        /// <remarks>
        /// اگر مقدار متغیر تهی باشد به جای آن مقدار پیش فرض آن نوع داده برگردانده می شود
        /// </remarks>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// int id = 123;
        /// int initialValue = id.Default&lt;int&gt;();
        /// </code>
        /// </example>
        /// <param name="wrapped">مقدار از نوع صوری می باشد: GenericType</param>
        /// <returns>GenericType</returns>
        public static T Default<T>(this T wrapped)
        {
            var result = default(T);
            if (wrapped != null)
                result = wrapped;
            return result;
        }

        /// <summary>
        /// از این متد برای دریافت مقدار متغیر استفاده می شود
        /// </summary>
        /// <remarks>
        /// اگر مقدار متغیر تهی باشد به جای آن مقدار پیش فرض که به عنوان پارامتر ارسال شده، برگردانده می شود
        /// </remarks>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// int id = 123;
        /// int initialValue = id.Default&lt;int&gt;(0);
        /// </code>
        /// </example>
        /// <param name="wrapped">مقدار از نوع صوری می باشد: GenericType</param>
        /// <param name="defaultValue">مقدار پیش فرض: GenericType</param>
        /// <returns>GenericType</returns>
        public static T Default<T>(this T wrapped, T defaultValue)
        {
            var result = defaultValue;
            if (wrapped != null)
                result = wrapped;
            return result;
        }

        /// <summary>
        /// از این متد برای تبدیل یک موجودیت به آرایه ای از نوع بایت استفاده می شود
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// class Test
        /// {
        ///     public int ID;
        /// }
        /// var test = new Test();
        /// byte[] byteArray = test.ObjectToByteArray&lt;Test&gt;();
        /// </code>
        /// </example>
        /// <param name="wrapped">مقدار از نوع صوری می باشد: GenericType</param>
        /// <returns>byte[]</returns>
        public static byte[] ObjectToByteArray<T>(this T wrapped)
        {
            var bf = new BinaryFormatter();
            using (var ms = new MemoryStream())
            {
                bf.Serialize(ms, wrapped);
                return ms.ToArray();
            }
        }

        /// <summary>
        /// از این متد برای تبدیل آرایه ای از نوع بایت به یک موجودیت استفاده می شود
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// class Test
        /// {
        ///     public int ID;
        /// }
        /// byte[] byteArray = new byte[]{ ... }
        /// Test test = byteArray.ByteArrayToObject&lt;Test&gt;();
        /// </code>
        /// </example>
        /// <param name="wrapped">مقدار از نوع صوری می باشد: GenericType</param>
        /// <returns>object</returns>
        public static object ByteArrayToObject<T>(this T wrapped)
        {
            if (wrapped == null)
            {
                return null;
            }
            using (var memStream = new MemoryStream())
            {
                var wrapper = wrapped as byte[];
                var binForm = new BinaryFormatter();
                if (wrapper == null) return null;
                memStream.Write(wrapper, 0, wrapper.Length);
                memStream.Seek(0, SeekOrigin.Begin);
                var obj = binForm.Deserialize(memStream);
                return obj;
            }
        }

        /// <summary>
        /// از این متد برای ساخت رشته مقادیر یک لیست استفاده می شود
        /// </summary>
        /// <remarks>
        /// از این متد فقط در لیست ها می توان بهره برد
        /// </remarks>
        /// <typeparam name="T"></typeparam>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// List&lt;string&gt; list = new List&lt;string&gt;();
        /// list.Add("item1");
        /// list.Add("item2");
        /// list.Add("item3");
        /// string strItems = list.GetString&lt;string&gt;(",");
        /// </code>
        /// </example>
        /// <param name="wrapped">مقدار از نوع صوری می باشد: List of GenericType</param>
        /// <param name="delimiter">رشته جداکننده مقادیر: string</param>
        /// <returns>string</returns>
        public static string GetString<T>(this List<T> wrapped, string delimiter)
        {
            if (wrapped == null)
            {
                return null;
            }
            var result = string.Empty;
            if (wrapped.Count > 0)
            {
                result += wrapped[0];
                for (var i = 1; i < wrapped.Count; i++)
                {
                    result += delimiter + wrapped[i];
                }
            }
            return result;
        }

        /// <summary>
        /// از این متد برای ساخت رشته مقادیر یک لیست استفاده می شود
        /// </summary>
        /// <remarks>
        /// از این متد فقط در لیست ها می توان بهره برد
        /// </remarks>
        /// <typeparam name="T"></typeparam>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// List&lt;string&gt; list = new List&lt;string&gt;();
        /// list.Add("item1");
        /// list.Add("item2");
        /// list.Add("item3");
        /// string strItems = list.GetString&lt;string&gt;(',');
        /// </code>
        /// </example>
        /// <param name="wrapped">مقدار از نوع صوری می باشد: List of GenericType</param>
        /// <param name="delimiter">رشته جداکننده مقادیر: char</param>
        /// <returns>char</returns>
        public static string GetString<T>(this List<T> wrapped, char delimiter)
        {
            if (wrapped == null)
            {
                return null;
            }
            var result = "[";
            wrapped.ForEach(x => { result += x + delimiter.ToString(); });
            result = result.TrimEnd(delimiter);
            result += "]";
            return result;
        }

        /// <summary>
        /// از این متد برای ساخت رشته مقادیر یک دیکشنری استفاده می شود
        /// </summary>
        /// <remarks>
        /// از این متد فقط در دیکشنری ها می توان بهره برد
        /// </remarks>
        /// <typeparam name="T"></typeparam>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// Dictionary&lt;string, string&gt; dictionary = new Dictionary&lt;string, string&gt;();
        /// dictionary.Add("key1", "item1");
        /// dictionary.Add("key2", "item2");
        /// dictionary.Add("key3", "item3");
        /// string strItems = dictionary.GetString&lt;string&gt;(",");
        /// </code>
        /// </example>
        /// <param name="wrapped">مقدار از نوع صوری می باشد: Dictionary of GenericType</param>
        /// <param name="delimiter">رشته جداکننده مقادیر: string</param>
        /// <returns>string</returns>
        public static string GetString<T>(this Dictionary<T, T> wrapped, string delimiter)
        {
            if (wrapped == null)
            {
                return null;
            }
            var result = string.Empty;
            if (wrapped.Count > 0)
            {
                result += $"{wrapped.Keys.First()}={wrapped.Values.First()}";
                for (var i = 1; i < wrapped.Keys.Count; i++)
                {
                    result += delimiter + $"{wrapped.Keys.ElementAt(i)}={wrapped.Values.ElementAt(i)}";
                }
            }
            return result;
        }

        /// <summary>
        /// از این متد برای ساخت یک کپی از موجودیت استفاده می شود
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// class Test
        /// {
        ///     public int ID;
        /// }
        /// Test test = new Test() { ID = 1 };
        /// Test temp = test.Clone&lt;Test&gt;();
        /// </code>
        /// </example>
        /// <param name="source">مقدار از نوع صوری می باشد: GenericType</param>
        /// <returns>GenericType</returns>
        public static T Clone<T>(this T source)
        {
            var serialized = JsonConvert.SerializeObject(source);
            return JsonConvert.DeserializeObject<T>(serialized);
        }

        /// <summary>
        /// از این متد برای دریافت مقدار یک رشته در حالت تهی پذیر بودن و تبدیل آن به یک نوع خاص استفاده می شود
        /// </summary>
        /// <remarks>
        /// اگر مقدار رشته تهی باشد، مقدار تهی از نوع همان موجودیت بازگردانده می شود
        /// </remarks>
        /// <typeparam name="T"></typeparam>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// int ID = "123".ToNullable&lt;int&gt;();
        /// </code>
        /// </example>
        /// <param name="s">مقدار از نوع صوری می باشد: string</param>
        /// <returns>GenericType</returns>
        public static T? ToNullable<T>(this string s) where T : struct
        {
            var result = new T?();
            try
            {
                if (!string.IsNullOrEmpty(s) && s.Trim().Length > 0)
                {
                    var conv = TypeDescriptor.GetConverter(typeof(T));
                    var convertFrom = conv.ConvertFrom(s);
                    if (convertFrom != null)
                        result = (T) convertFrom;
                }
            }
            catch (Exception ex)
            {
                Logger.Log(ex);
            }
            return result;
        }

        #endregion

    }
}
