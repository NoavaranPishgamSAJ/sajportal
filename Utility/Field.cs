﻿namespace Utility
{

    #region Assembly

    using System;

    #endregion

    /// <summary>
    /// این کلاس در ایجاد موجودیت های داینامیک در زمان اجرا استفاده می شود
    /// </summary>
    /// <remarks>
    /// از این کلاس برای ایجاد لیست فیلدهای موجودیت داینامیک استفاده می شود
    /// </remarks>
    /// <example> 
    /// این مثال طریقه استفاده از این کلاس را نشان می دهد
    /// <code>
    /// List&lt;Field&gt; fields = new List&lt;Field&gt;();
    /// fields.Add(new Field("ID", typeof(int), 1));
    /// </code>
    /// </example>
    public class Field
    {

        #region Attribute

        /// <summary>
        /// نام فیلد
        /// </summary>
        public string FieldName { get; set; }

        /// <summary>
        /// نوع فیلد
        /// </summary>
        /// <remarks>
        /// نوع این متغیر همان نوع هایی هست که در تعریف متغیر در سی شارپ استفاده می شود
        /// </remarks>
        public Type FieldType { get; set; }

        /// <summary>
        /// مقدار فیلد
        /// </summary>
        public object FieldValue { get; set; }

        #endregion

        #region Method

        /// <summary>
        /// سازنده این کلاس سه پارامتر برای مقدار دهی اولیه خود دریافت می کند
        /// </summary>
        /// <param name="fieldName">نام فیلد: string</param>
        /// <param name="fieldType">نوع فیلد: System.Type</param>
        /// <param name="fieldValue">مقدار اولیه فیلد: object</param>
        public Field(string fieldName, Type fieldType, object fieldValue = null)
        {
            FieldName = fieldName;
            FieldType = fieldType;
            FieldValue = fieldValue;
        }

        #endregion

    }
}
