﻿namespace Utility
{

    #region Assembly

    using Microsoft.CSharp.RuntimeBinder;
    using System;
    using System.Collections.Generic;
    using System.Dynamic;
    using System.Linq.Expressions;
    using System.Reflection;
    using System.Runtime.CompilerServices;

    #endregion

    /// <summary>
    /// از این کلاس برای ایجاد موجودیت های داینامیک در زمان اجرا استفاده می شود
    /// </summary>
    /// <remarks>
    /// این موجودیت ها می تواند شامل تعداد نامحدودی فیلد باشد و بیشتر در عملیات هایی نظیر کار با دیتابیس استفاده می شود
    /// </remarks>
    public class DynamicClass : DynamicObject
    {

        #region Attribute

        private readonly Dictionary<string, KeyValuePair<Type, object>> _fields;

        #endregion

        #region Method

        /// <summary>
        /// سازنده کلاس که در زمان تعریف یک موجودیت خالی ایجاد می کند
        /// </summary>
        public DynamicClass()
        {
            _fields = new Dictionary<string, KeyValuePair<Type, object>>();
        }

        /// <summary>
        /// سازنده کلاس که در زمان تعریف یک موجودیت با فیلدهایی که برای آن توسط پارامتر ارسال می شود ایجاد می کند
        /// </summary>
        /// <param name="fields">لیست فیلدهای موجودیت: List&lt;Field&gt;</param>
        public DynamicClass(List<Field> fields)
        {
            _fields = new Dictionary<string, KeyValuePair<Type, object>>();
            fields.ForEach(x => _fields.Add(x.FieldName, new KeyValuePair<Type, object>(x.FieldType, x.FieldValue)));
        }

        /// <summary>
        /// از این متد برای مقداردهی به فیلدهای موجودیت استفاده می شود
        /// </summary>
        /// <param name="binder">بایندر فیلد: System.Dynamic.SetMemberBinder</param>
        /// <param name="value">مقدار فیلد: object</param>
        /// <returns>bool</returns>
        public override bool TrySetMember(SetMemberBinder binder, object value)
        {
            if (_fields.ContainsKey(binder.Name))
            {
                var type = _fields[binder.Name].Key;
                if (value.GetType() == type)
                {
                    _fields[binder.Name] = new KeyValuePair<Type, object>(type, value);
                    return true;
                }
                throw new Exception($"Value {value} is not of type {type.Name}");
            }
            return false;
        }

        /// <summary>
        /// از این متد برای دریافت مقادیر فیلدهای موجودیت استفاده می شود
        /// </summary>
        /// <param name="binder">بایندر فیلد: System.Dynamic.GetMemberBinder</param>
        /// <param name="result">مقدار خروجی فیلد: object</param>
        /// <returns>bool</returns>
        public override bool TryGetMember(GetMemberBinder binder, out object result)
        {
            result = _fields[binder.Name].Value;
            return true;
        }

        /// <summary>
        /// این متد براساس نام نوع متغیر، نوع متناظر با آن را بر می گرداند
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// Type type = DynamicClass.GetFieldType("string");
        /// </code>
        /// </example>
        /// <param name="dataType">نام نوع: string</param>
        /// <returns>System.Type</returns>
        public static Type GetFieldType(string dataType)
        {
            if (dataType.IsEmpty())
                return typeof(string);
            switch (dataType.ToLower())
            {
                case "bit":
                    return typeof(bool);
                case "binary":
                case "varbinary":
                    return typeof(byte);
                case "tinyint":
                case "smallint":
                    return typeof(short);
                case "int":
                    return typeof(int);
                case "bigint":
                case "money":
                case "smallmoney":
                    return typeof(long);
                case "float":
                    return typeof(float);
                case "real":
                    return typeof(double);
                case "decimal":
                    return typeof(decimal);
                case "char":
                case "nchar":
                case "varchar":
                case "nvarchar":
                case "text":
                case "ntext":
                case "uniqueidentifier":
                case "xml":
                    return typeof(string);
                case "image":
                    return typeof(byte[]);
                case "variant":
                case "udt":
                case "structured":
                    return typeof(string);
                case "date":
                case "datetime":
                case "datetime2":
                case "smalldatetime":
                    return typeof(DateTime);
                case "time":
                    return typeof(string);
                case "timestamp":
                    return typeof(string);
                case "blob":
                    return typeof(byte[]);
                default:
                    return typeof(string);
            }
        }

        /// <summary>
        /// این متد براساس نام نوع متغیر، نام نوع متناظر با آن را بر می گرداند
        /// </summary>
        /// <remarks>
        /// در این متد اگر نوع متغیر قابل تهی بودن باشد، نوع متناظر با علامت ؟ بازگردانده می شود که نشان دهنده تهی پذیر بودن نوع می باشد
        /// </remarks>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// string typeName = DynamicClass.GetFieldTypeStr("string", true);
        /// </code>
        /// </example>
        /// <param name="dataType">نام نوع: string</param>
        /// <param name="allowNull">تهی پذیر بودن: bool</param>
        /// <returns>string</returns>
        public static string GetFieldTypeStr(string dataType, bool allowNull)
        {
            if (dataType.IsEmpty())
                return "string";
            switch (dataType.ToLower())
            {
                case "bit":
                    return "bool" + (allowNull ? "?" : string.Empty);
                case "binary":
                case "varbinary":
                    return "byte" + (allowNull ? "?" : string.Empty);
                case "tinyint":
                case "smallint":
                    return "short" + (allowNull ? "?" : string.Empty);
                case "int":
                    return "int" + (allowNull ? "?" : string.Empty);
                case "bigint":
                case "money":
                case "smallmoney":
                    return "long" + (allowNull ? "?" : string.Empty);
                case "float":
                    return "float" + (allowNull ? "?" : string.Empty);
                case "real":
                    return "double" + (allowNull ? "?" : string.Empty);
                case "decimal":
                    return "decimal" + (allowNull ? "?" : string.Empty);
                case "char":
                case "nchar":
                case "varchar":
                case "nvarchar":
                case "text":
                case "ntext":
                case "uniqueidentifier":
                case "xml":
                    return "string";
                case "image":
                    return "byte[]" + (allowNull ? "?" : string.Empty);
                case "variant":
                case "udt":
                case "structured":
                    return "string";
                case "date":
                case "datetime":
                case "datetime2":
                case "smalldatetime":
                    return "DateTime" + (allowNull ? "?" : string.Empty);
                case "time":
                    return "string";
                case "timestamp":
                    return "string";
                case "blob":
                    return "byte[]" + (allowNull ? "?" : string.Empty);
                default:
                    return "string";
            }
        }

        /// <summary>
        /// این متد برای تبدیل مقدار پارامتر value بر اساس تهی بودن یا نبودن به نوع مورد نظر استفاده می شود
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// var type = DynamicClass.GetFieldValue("int", "123", false);
        /// </code>
        /// </example>
        /// <param name="dataType">نام نوع: string</param>
        /// <param name="value">مقدار: string</param>
        /// <param name="allowNull">تهی پذیر بودن: bool</param>
        /// <returns>dynamic</returns>
        public static dynamic GetFieldValue(string dataType, string value, bool allowNull)
        {
            if (!dataType.IsNotEmpty())
                return Convert.ToString(value);
            switch (dataType.ToLower())
            {
                case "bit":
                    if(value.ToLower().Equals("true,false"))
                        return allowNull ? "true".ToNullable<bool>() : Convert.ToBoolean("true");
                    if (value.ToLower().Equals("false"))
                        return allowNull ? "false".ToNullable<bool>() : Convert.ToBoolean("false");
                    return null;
                case "binary":
                case "varbinary":
                    return allowNull ? value.ToNullable<byte>() : Convert.ToByte(value);
                case "tinyint":
                case "smallint":
                    return allowNull ? value.ToNullable<short>() : Convert.ToInt16(value);
                case "int":
                    return allowNull ? value.ToNullable<int>() : Convert.ToInt32(value);
                case "bigint":
                case "money":
                case "smallmoney":
                    return allowNull ? value.ToNullable<long>() : Convert.ToInt64(value);
                case "float":
                    return allowNull ? value.ToNullable<double>() : Convert.ToDouble(value);
                case "real":
                    return allowNull ? value.ToNullable<double>() : Convert.ToDouble(value);
                case "decimal":
                    return allowNull ? value.ToNullable<decimal>() : Convert.ToDecimal(value);
                case "char":
                case "nchar":
                case "varchar":
                case "nvarchar":
                case "text":
                case "ntext":
                case "uniqueidentifier":
                case "xml":
                    return Convert.ToString(value);
                case "image":
                    return Convert.ToString(value);
                case "variant":
                case "udt":
                case "structured":
                    return Convert.ToString(value);
                case "date":
                case "datetime":
                case "datetime2":
                case "smalldatetime":
                    return allowNull ? value.ToNullable<DateTime>() : Convert.ToDateTime(value);
                case "time":
                    return Convert.ToString(value);
                case "timestamp":
                    return Convert.ToString(value);
                case "blob":
                    return Convert.ToString(value);
                default:
                    return Convert.ToString(value);
            }
        }

        /// <summary>
        /// این متد برای دریافت مقدار یک فیلد از موجودیت استفاده می شود
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// object value = DynamicClass.GetProperty(entity, "ID");
        /// </code>
        /// </example>
        /// <param name="o">موجودیت: object</param>
        /// <param name="member">نام فیلد: string</param>
        /// <returns>object</returns>
        public static object GetProperty(object o, string member)
        {
            if (o == null) throw new ArgumentNullException(nameof(o));
            if (member == null) throw new ArgumentNullException(nameof(member));
            var scope = o.GetType();
            var provider = o as IDynamicMetaObjectProvider;
            if (provider != null)
            {
                var param = Expression.Parameter(typeof(object));
                var mobj = provider.GetMetaObject(param);
                var binder = (GetMemberBinder)Microsoft.CSharp.RuntimeBinder.Binder.GetMember(0, member, scope, new [] { CSharpArgumentInfo.Create(0, null) });
                var ret = mobj.BindGetMember(binder);
                var final = Expression.Block(
                    Expression.Label(CallSiteBinder.UpdateLabel),
                    ret.Expression
                );
                var lambda = Expression.Lambda(final, param);
                var del = lambda.Compile();
                return del.DynamicInvoke(o);
            }
            else
            {
                return o.GetType().GetProperty(member, BindingFlags.Public | BindingFlags.Instance).GetValue(o, null);
            }
        }

        /// <summary>
        /// این متد برای تعیین مقدار یک فیلد از موجودیت استفاده می شود
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// DynamicClass.SetProperty(entity, "ID", 123);
        /// </code>
        /// </example>
        /// <param name="o">موجودیت: object</param>
        /// <param name="member">نام فیلد: string</param>
        /// <param name="value">مقدار فیلد: object</param>
        public static void SetProperty(object o, string member, object value)
        {
            if (o == null) throw new ArgumentNullException(nameof(o));
            if (member == null) throw new ArgumentNullException(nameof(member));
            var prop = o.GetType().GetProperty(member, BindingFlags.Public | BindingFlags.Instance);
            if (null != prop && prop.CanWrite)
            {
                prop.SetValue(o, value);
            }
        }

        public static T Create<T>(string definition, string path, List<string> referencedAssemblies)
        {
            var entity = default(T);

            var providerOptions = new Dictionary<string, string> { ["CompilerVersion"] = "v4.0" };
            var provider = new Microsoft.CSharp.CSharpCodeProvider(providerOptions);

            var parameters = new System.CodeDom.Compiler.CompilerParameters
            {
                GenerateExecutable = false,
                GenerateInMemory = true,
                IncludeDebugInformation = true
            };
            parameters.ReferencedAssemblies.AddRange(referencedAssemblies.ToArray());
            var results = provider.CompileAssemblyFromSource(parameters, definition);

            if (results.Errors.Count == 0)
            {
                var generated = results.CompiledAssembly.GetType(path);
                entity = (T)generated.GetConstructor(Type.EmptyTypes)?.Invoke(null);
            }
            return entity;
        }

        #endregion
        /*
        /// <summary>
        /// دریافت رشته ایجاد کلاس مربوط به محتوا
        /// </summary>
        /// <param name="tableName">نام جدول: string</param>
        /// <param name="fields">لیست فیلدهای کلاس: List&lt;ContentField&gt;</param>
        /// <param name="categories">لیست فیلدهای کلاس های دیگر: string[]</param>
        /// <returns>string</returns>
        public string GetClassDefinition(string tableName, List<ContentField> fields, string[] categories)
        {
            string definition = string.Empty;
            if (fields != null)
            {
                definition += "namespace ORM.Model" + Environment.NewLine;
                definition += "{" + Environment.NewLine;
                //definition += "    #region Assembly" + Environment.NewLine + Environment.NewLine;
                definition += "    using Dapper.Contrib.Extensions;" + Environment.NewLine;
                definition += "    using System;" + Environment.NewLine;
                definition += "    using System.Collections.Generic;" + Environment.NewLine;
                //definition += "    #endregion" + Environment.NewLine + Environment.NewLine;
                definition += $@"    [Table(""{tableName}"")]" + Environment.NewLine;
                definition += $"    public class {tableName}" + Environment.NewLine;
                definition += "    {" + Environment.NewLine + Environment.NewLine;
                //definition += "        #region Property" + Environment.NewLine + Environment.NewLine;
                // تعریف کلید اصلی کلاس
                definition += "        [Key]" + Environment.NewLine;
                definition += "        public int ItemId { get; set; }" + Environment.NewLine + Environment.NewLine;
                // تعریف فیلدهای کلاس
                definition = fields.Aggregate(definition, (current, field) => current + $@"        public {DynamicClass.GetFieldTypeStr(field.DataType, field.AllowNull.GetValue())} {field.FieldName} {{ get; set; }}" + Environment.NewLine + Environment.NewLine);
                // تعریف کلیدهای خارجی کلاس
                definition = categories.Where(category => category.IsNotEmpty()).Aggregate(definition, (current, category) => current + $@"        public int {category}Id {{ get; set; }}" + Environment.NewLine + Environment.NewLine);
                //definition += "        #endregion" + Environment.NewLine + Environment.NewLine;
                definition += "    }" + Environment.NewLine;
                definition += "}" + Environment.NewLine;
            }
            return definition;
        }

        /// <summary>
        /// از این متد برای ایجاد فایل کامپایل شده کلاس مورد نظر در زمان اجرای پرتال استفاده می شود
        /// </summary>
        /// <param name="assemblyPath">مسیر ایجاد فایل: string</param>
        /// <param name="classDefinition">تعریف کلاس به زبان سی شارپ: string</param>
        public void CreateAssembly(string assemblyPath, string classDefinition)
        {
            // پارامترهای کامپایلر
            var parameters = new CompilerParameters
            {
                GenerateExecutable = false,
                OutputAssembly = assemblyPath,
                IncludeDebugInformation = true
            };
            // دریافت لیست اسمبلی های مورد نیاز
            var assemblies = typeof(GenericBaseModel).Assembly.GetReferencedAssemblies().ToList();

            // دریافت لیست محل اسمبلی های مورد نیاز
            var assemblyLocations = assemblies.Select(a => Assembly.ReflectionOnlyLoad(a.FullName).Location).ToList();
            assemblyLocations.Add(typeof(GenericBaseModel).Assembly.Location);

            // اضافه کردن اسمبلی ها به پارامترهای کامپایلر
            parameters.ReferencedAssemblies.AddRange(assemblyLocations.ToArray());

            // تععین نوع کامپایلر به سی شارپ
            var r = CodeDomProvider.CreateProvider("CSharp").CompileAssemblyFromSource(parameters, classDefinition);
            if (r.Errors.Count > 0)
            {
            }
        }
        */
    }
}
