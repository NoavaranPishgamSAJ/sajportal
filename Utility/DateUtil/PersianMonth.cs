﻿namespace Utility.DateUtil
{
    /// <summary>
    /// از این کلاس برای کار با ماه های شمسی در کلاس تبدیل تاریخ استفاده می شود
    /// </summary>
    public static class PersianMonth
    {

        #region Static Method

        /// <summary>
        /// این متد تعداد روزهای هر ماه را برمی گرداند
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// byte daysInMonth = PersianMonth.DaysInMonth(2);
        /// </code>
        /// </example>
        /// <param name="ordination">شماره ماه: int</param>
        /// <returns>byte</returns>
        public static byte DaysInMonth(int ordination)
        {
            try
            {
                if (ordination >= 1 && ordination < 7)
                    return 31;
                if (ordination >= 7 && ordination < 12)
                    return 30;
                if (ordination == 0 || ordination == 12)
                    return 29;
                return 30;
            }
            catch { return 0; }
        }

        /// <summary>
        /// این متد نام ماه را براساس شماره آن برمی گرداند
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// string monthName = PersianMonth.GetPersianMonthName(2);
        /// </code>
        /// </example>
        /// <param name="month">شماره ماه: int</param>
        /// <returns>string</returns>
        public static string GetPersianMonthName(int month)
        {
            try
            {
                switch (month)
                {
                    case 1:
                        return "فروردین";
                    case 2:
                        return "اردیبهشت";
                    case 3:
                        return "خرداد";
                    case 4:
                        return "تیر";
                    case 5:
                        return "مرداد";
                    case 6:
                        return "شهریور";
                    case 7:
                        return "مهر";
                    case 8:
                        return "آبان";
                    case 9:
                        return "آذر";
                    case 10:
                        return "دی";
                    case 11:
                        return "بهمن";
                    case 12:
                        return "اسفند";
                    default:
                        return "فروردین";
                }
            }
            catch { return "فروردین"; }
        }

        #endregion

    }
}