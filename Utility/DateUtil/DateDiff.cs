﻿namespace Utility.DateUtil
{
    /// <summary>
    /// از این کلاس برای محاسبه میزان ماه ها و روزهای یک تاریخ خاص استفاده می شود
    /// </summary>
    /// <remarks>
    /// از این کلاس بیشتر در عملیات جمع یا تفریق دو تاریخ بهره برده می شود
    /// </remarks>
    public class DateDiff
    {

        #region Property

        /// <summary>
        /// سال
        /// </summary>
        public int Year
        {
            get;
            set;
        }

        /// <summary>
        /// ماه
        /// </summary>
        public int Month
        {
            get;
            set;
        }

        /// <summary>
        /// روز
        /// </summary>
        public int Day
        {
            get;
            set;
        }

        #endregion

        #region Method

        /// <summary>
        /// سال های تاریخ
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// DateDiff diff = new DateDiff() { Year = 1390, Month = 5, Day = 2  };
        /// int years = diff.Years();
        /// </code>
        /// </example>
        /// <returns>int</returns>
        public int Years()
        {
            try
            {
                return Year;
            }
            catch { return -1; }
        }

        /// <summary>
        /// ماه های تاریخ
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// DateDiff diff = new DateDiff() { Year = 1390, Month = 5, Day = 2  };
        /// int months = diff.Months();
        /// </code>
        /// </example>
        /// <returns>int</returns>
        public int Months()
        {
            try
            {
                return Years() * 12 + Month;
            }
            catch { return -1; }
        }

        /// <summary>
        /// روزهای تاریخ
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// DateDiff diff = new DateDiff() { Year = 1390, Month = 5, Day = 2  };
        /// int days = diff.Days();
        /// </code>
        /// </example>
        /// <returns>int</returns>
        public int Days()
        {
            try
            {
                return Months() * 30 + Day + Years() * 5;
            }
            catch { return -1; }
        }

        #endregion

    }
}
