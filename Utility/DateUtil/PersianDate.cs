﻿namespace Utility.DateUtil
{

    #region Assembly

    using System;

    #endregion

    /// <summary>
    /// این کلاس برای کار با تاریخ شمسی در کلاس نبدیل تاریخ می باشد
    /// </summary>
    public class PersianDate
    {

        #region Property

        /// <summary>
        /// سال
        /// </summary>
        public int Year { get; set; }

        /// <summary>
        /// ماه
        /// </summary>
        public int Month { get; set; }

        /// <summary>
        /// روز
        /// </summary>
        public int Day { get; set; }

        /// <summary>
        /// هفته در سال
        /// </summary>
        public int WeekOfYear { get; set; }

        /// <summary>
        /// سال به صورت رشته کامل
        /// </summary>
        public string YearStr => Year >= 10 ? Year.ToString() : "0" + Year;

        /// <summary>
        /// ماه به صورت رشته کامل
        /// </summary>
        public string MonthStr => Month >= 10 ? Month.ToString() : "0" + Month;

        /// <summary>
        /// روز به صورت رشته کامل
        /// </summary>
        public string DayStr => Day >= 10 ? Day.ToString() : "0" + Day;

        /// <summary>
        /// هفته در سال به صورت رشته
        /// </summary>
        public string WeekOfYearStr => WeekOfYear.ToString();

        #endregion

        #region Method

        /// <summary>
        /// از این متد سازنده کلاس بوده و کلاس را با مقدار تاریخ روز جاری مقداردهی اولیه می کند
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// PersianDate currentDate = new PersianDate();
        /// </code>
        /// </example>
        public PersianDate()
        {
            try
            {
                var converter = new System.Globalization.PersianCalendar();
                var handle = DateTime.Today;
                Year = converter.GetYear(handle);
                Month = converter.GetMonth(handle);
                Day = converter.GetDayOfMonth(handle);
                WeekOfYear = converter.GetWeekOfYear(handle, System.Globalization.CalendarWeekRule.FirstDay, DayOfWeek.Saturday);
            }
            catch (Exception ex) { Logger.Log(ex); }
        }

        /// <summary>
        /// از این متد سازنده کلاس بوده و کلاس را با مقدار تاریخ ورودی مقداردهی اولیه می کند
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// PersianDate date = new PersianDate(DateTime.Today);
        /// </code>
        /// </example>
        /// <param name="handle">تاریخ موردنظر: System.DateTime</param>
        public PersianDate(DateTime handle)
        {
            try
            {
                var converter = new System.Globalization.PersianCalendar();
                Year = converter.GetYear(handle);
                Month = converter.GetMonth(handle);
                Day = converter.GetDayOfMonth(handle);
                WeekOfYear = converter.GetWeekOfYear(handle, System.Globalization.CalendarWeekRule.FirstDay, DayOfWeek.Saturday);
            }
            catch (Exception ex) { Logger.Log(ex); }
        }

        /// <summary>
        /// از این متد سازنده کلاس بوده و کلاس را با مقدار تاریخ ورودی مقداردهی اولیه می کند
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// PersianDate date = new PersianDate(1395, 5, 2);
        /// </code>
        /// </example>
        /// <param name="year">سال: int</param>
        /// <param name="month">ماه: int</param>
        /// <param name="day">روز: int</param>
        public PersianDate(int year, int month, int day)
        {
            try
            {
                Year = year;
                Month = month;
                Day = day;
            }
            catch (Exception ex) { Logger.Log(ex); }
        }

        /// <summary>
        /// این متد تاریخ معادل میلادی، تاریخ شمسی فعلی را باز می گرداند
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// PersianDate date = new PersianDate();
        /// DateTime gregorianDate =  date.ToEnglish();
        /// </code>
        /// </example>
        /// <returns>System.DateTime</returns>
        public DateTime ToEnglish()
        {
            try
            {
                var converter = new System.Globalization.PersianCalendar();
                return converter.ToDateTime(Year, Month, Day, 0, 0, 0, 0, System.Globalization.PersianCalendar.PersianEra);
            }
            catch { return DateTime.Today; }
        }

        /// <summary>
        /// این متد تاریخ شمسی فعلی به صورت رشته باز می گرداند
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// PersianDate date = new PersianDate();
        /// string dateStr =  date.ToString();
        /// </code>
        /// </example>
        /// <returns>string</returns>
        public new string ToString()
        {
            try
            {
                return $"{YearStr}\\{MonthStr}\\{DayStr}";
            }
            catch { return null; }
        }

        /// <summary>
        /// این متد تاریخ شمسی فعلی به صورت رشته باز می گرداند
        /// </summary>
        /// <remarks>
        /// در صورتی که مقدار پارامتر برابر true باشد تاریخ برعکس برگردانده می شود
        /// </remarks>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// PersianDate date = new PersianDate();
        /// string dateStr =  date.ToString(false);
        /// </code>
        /// </example>
        /// <param name="direction">ترتیب قرار گیری: bool</param>
        /// <returns>string</returns>
        public string ToString(bool direction)
        {
            try
            {
                return !direction ? $"{YearStr}\\{MonthStr}\\{DayStr}" : $"{DayStr}\\{MonthStr}\\{YearStr}";
            }
            catch { return null; }
        }

        #endregion

    }
}
