﻿namespace Utility.DateUtil
{

    #region Assembly

    using System;
    using System.Threading;

    #endregion

    /// <summary>
    /// از این کلاس برای تبدیل تاریخ شمسی و میلادی به یکدیگر استفاده می شود
    /// </summary>
    public static class DateConvertor
    {

        #region Method

        /// <summary>
        /// از این متد برای استاندارد کردن یک رشته تاریخ جهت انجام عملیات بر روی آن استفاده می شود
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// string fixedDate = DateConvertor.FixDate("1395/01/05");
        /// </code>
        /// </example>
        /// <param name="before">رشته تاریخ: string</param>
        /// <returns>string</returns>
        public static string FixDate(string before)
        {
            try
            {
                var result = string.Empty;
                var splited = before.Split('\\');
                if (splited.Length != 3)
                    splited = before.Split('/');
                if (splited.Length != 3)
                    splited = before.Split('-');
                if (splited.Length != 3)
                    splited = before.Split(',');
                if (splited.Length == 3)
                {
                    if (int.Parse(splited[0]) > 50)
                    {
                        var year = int.Parse(splited[0]);
                        var month = int.Parse(splited[1]);
                        var day = int.Parse(splited[2]);
                        if (year < 1300)
                            result += $"13{year}\\";
                        else
                            result += $"{year}\\";
                        if (month < 10)
                            result += $"0{month}\\";
                        else
                            result += $"{month}\\";
                        if (day < 10)
                            result += $"0{day}\\";
                        else
                            result += $"{day}\\";
                        return result;
                    }
                    if (int.Parse(splited[2]) > 50)
                    {
                        var year = int.Parse(splited[2]);
                        var month = int.Parse(splited[1]);
                        var day = int.Parse(splited[0]);
                        if (year < 1300)
                            result += $"13{year}\\";
                        else
                            result += $"{year}\\";
                        if (month < 10)
                            result += $"0{month}\\";
                        else
                            result += $"{month}\\";
                        if (day < 10)
                            result += $"0{day}\\";
                        else
                            result += $"{day}\\";
                        return result;
                    }
                    return ConvertToPersianStr(DateTime.Today);
                }
                return ConvertToPersianStr(DateTime.Today);
            }
            catch { return ConvertToPersianStr(DateTime.Today); }
        }

        /// <summary>
        /// از این متد برای تبدیل تاریخ میلادی به شمسی استفاده می شود
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// PersianDate date = DateConvertor.ConvertToPersian(DateTime.Now);
        /// </code>
        /// </example>
        /// <param name="handle">تاریخ مورد نظر: System.DateTime</param>
        /// <returns>PersianDate</returns>
        public static PersianDate ConvertToPersian(DateTime handle)
        {
            try
            {
                return new PersianDate(handle);
            }
            catch { return new PersianDate(); }
        }

        /// <summary>
        /// از این متد برای تبدیل تاریخ میلادی به شمسی استفاده می شود
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// PersianDate date = DateConvertor.ConvertToPersian("2015/05/02");
        /// </code>
        /// </example>
        /// <param name="before">تاریخ مورد نظر: string</param>
        /// <returns>PersianDate</returns>
        public static PersianDate ConvertToPersian(string before)
        {
            try
            {
                var handle = DateTime.Parse(before);
                return new PersianDate(handle);
            }
            catch { return new PersianDate(); }
        }

        /// <summary>
        /// از این متد برای تبدیل تاریخ میلادی به شمسی استفاده می شود
        /// </summary>
        /// <remarks>
        /// در صورتی که مقدار پارامتر direction برابر true باشد تاریخ برعکس برگردانده می شود
        /// </remarks>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// string date = DateConvertor.ConvertToPersianStr(DateTime.Now, [false]);
        /// </code>
        /// </example>
        /// <param name="handle">تاریخ مورد نظر: string</param>
        /// <param name="direction">ترتیب قرار گیری: bool</param>
        /// <returns>string</returns>
        public static string ConvertToPersianStr(DateTime? handle, bool direction = false)
        {
            try
            {
                return handle != null ? new PersianDate(handle.Value).ToString(direction) : string.Empty;
            }
            catch { return ""; }
        }

        /// <summary>
        /// از این متد برای تبدیل تاریخ میلادی به شمسی استفاده می شود
        /// </summary>
        /// <remarks>
        /// در صورتی که مقدار پارامتر direction برابر true باشد تاریخ برعکس برگردانده می شود
        /// </remarks>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// string date = DateConvertor.ConvertToPersianStr("2015/05/02", [false]);
        /// </code>
        /// </example>
        /// <param name="before">تاریخ مورد نظر: string</param>
        /// <param name="direction">ترتیب قرار گیری: bool</param>
        /// <returns>string</returns>
        public static string ConvertToPersianStr(string before, bool direction = false)
        {
            try
            {
                var handle = DateTime.Parse(before);
                return new PersianDate(handle).ToString(direction);
            }
            catch { return ""; }
        }

        /// <summary>
        /// از این متد برای تبدیل تاریخ شمسی به میلادی استفاده می شود
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// PersianDate persianDate = DateConvertor.ConvertToPersian(DateTime.Now);
        /// DateTime date = DateConvertor.ConvertToEnglish(persianDate);
        /// </code>
        /// </example>
        /// <param name="handle">تاریخ مورد نظر: PersianDate</param>
        /// <returns>System.DateTime</returns>
        public static DateTime ConvertToEnglish(PersianDate handle)
        {
            try
            {
                return handle.ToEnglish();
            }
            catch { return DateTime.Today; }
        }

        /// <summary>
        /// از این متد برای تبدیل تاریخ شمسی به میلادی استفاده می شود
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// DateTime date = DateConvertor.ConvertToEnglish("1395/05/02");
        /// </code>
        /// </example>
        /// <param name="before">تاریخ مورد نظر: string</param>
        /// <returns>System.DateTime</returns>
        public static DateTime ConvertToEnglish(string before)
        {
            try
            {
                before = FixDate(before);
                var year = int.Parse(before.Substring(0, 4));
                var month = int.Parse(before.Substring(5, 2));
                var day = int.Parse(before.Substring(8, 2));
                return new PersianDate(year, month, day).ToEnglish();
            }
            catch { return DateTime.Today; }
        }

        /// <summary>
        /// از این متد برای تبدیل تاریخ شمسی به میلادی استفاده می شود
        /// </summary>
        /// <remarks>
        /// در صورت خالی بودن رشته تاریخ مقدار تهی برگردانده می شود
        /// </remarks>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// DateTime? date = DateConvertor.ConvertToNEnglish("1395/05/02");
        /// </code>
        /// </example>
        /// <param name="before">تاریخ مورد نظر: string</param>
        /// <returns>System.DateTime?</returns>
        public static DateTime? ConvertToNEnglish(string before)
        {
            try
            {
                before = FixDate(before);
                var year = int.Parse(before.Substring(0, 4));
                var month = int.Parse(before.Substring(5, 2));
                var day = int.Parse(before.Substring(8, 2));
                return new PersianDate(year, month, day).ToEnglish();
            }
            catch { return null; }
        }

        /// <summary>
        /// از این متد برای تبدیل تاریخ شمسی به میلادی استفاده می شود
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// DateTime date = DateConvertor.ConvertToEnglish(1395, 5, 2);
        /// </code>
        /// </example>
        /// <param name="year">سال: int</param>
        /// <param name="month">ماه: int</param>
        /// <param name="day">روز: int</param>
        /// <returns>System.DateTime</returns>
        public static DateTime ConvertToEnglish(int year, int month, int day)
        {
            try
            {
                return new PersianDate(year, month, day).ToEnglish();
            }
            catch { return DateTime.Today; }
        }

        /// <summary>
        /// از این متد برای تبدیل تاریخ شمسی به میلادی استفاده می شود
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// PersianDate persianDate = DateConvertor.ConvertToPersian(DateTime.Now);
        /// string date = DateConvertor.ConvertToEnglishStr(persianDate);
        /// </code>
        /// </example>
        /// <param name="handle">تاریخ مورد نظر: PersianDate</param>
        /// <returns>string</returns>
        public static string ConvertToEnglishStr(PersianDate handle)
        {
            try
            {
                return handle.ToEnglish().ToShortDateString();
            }
            catch { return DateTime.Today.ToShortDateString(); }
        }

        /// <summary>
        /// از این متد برای تبدیل تاریخ شمسی به میلادی استفاده می شود
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// string date = DateConvertor.ConvertToEnglishStr("1395/05/02");
        /// </code>
        /// </example>
        /// <param name="before">تاریخ مورد نظر: string</param>
        /// <returns>string</returns>
        public static string ConvertToEnglishStr(string before)
        {
            try
            {
                before = FixDate(before);
                var year = int.Parse(before.Substring(0, 4));
                var month = int.Parse(before.Substring(5, 2));
                var day = int.Parse(before.Substring(8, 2));
                return new PersianDate(year, month, day).ToEnglish().ToShortDateString();
            }
            catch { return DateTime.Today.ToShortDateString(); }
        }

        /// <summary>
        /// از این متد برای تبدیل تاریخ شمسی به میلادی استفاده می شود
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// string date = DateConvertor.ConvertToEnglishStr(1395, 5, 2);
        /// </code>
        /// </example>
        /// <param name="year">سال: int</param>
        /// <param name="month">ماه: int</param>
        /// <param name="day">روز: int</param>
        /// <returns>string</returns>
        public static string ConvertToEnglishStr(int year, int month, int day)
        {
            try
            {
                return new PersianDate(year, month, day).ToEnglish().ToShortDateString();
            }
            catch { return DateTime.Today.ToShortDateString(); }
        }

        /// <summary>
        /// از این متد برای تبدیل تاریخ میلادی به شمسی استفاده می شود
        /// </summary>
        /// <remarks>
        /// در صورت بروز خطا در عملیات مقدار تاریخ روز به عنوان نتیجه برگردانده می شود
        /// </remarks>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// string date = DateConvertor.DateLocalization(DateTime.Now);
        /// </code>
        /// </example>
        /// <param name="before">تاریخ مورد نظر: System.DateTime</param>
        /// <returns>string</returns>
        public static string DateLocalization(DateTime before)
        {
            try
            {
                var result = new PersianDate(before);
                return $"{result.YearStr}\\{result.MonthStr}\\{result.DayStr}";
            }
            catch
            {
                var result = new PersianDate();
                return $"{result.YearStr}\\{result.MonthStr}\\{result.DayStr}";
            }
        }

        /// <summary>
        /// از این متد برای تبدیل تاریخ میلادی به شمسی استفاده می شود
        /// </summary>
        /// <remarks>
        /// در صورت بروز خطا در عملیات مقدار تاریخ روز به عنوان نتیجه برگردانده می شود
        /// در صورتی که مقدار پارامتر rtl برابر true باشد تاریخ برعکس برگردانده می شود
        /// </remarks>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// string date = DateConvertor.DateLocalization(DateTime.Now, false);
        /// </code>
        /// </example>
        /// <param name="before">تاریخ مورد نظر: System.DateTime</param>
        /// <param name="rtl">برعکس بودن تاریخ: bool</param>
        /// <returns>string</returns>
        public static string DateLocalization(DateTime before, bool rtl)
        {
            try
            {
                if (!rtl)
                {
                    var result = new PersianDate(before);
                    return $"{result.YearStr}\\{result.MonthStr}\\{result.DayStr}";
                }
                else
                {
                    var result = new PersianDate(before);
                    return $"{result.DayStr}/{result.MonthStr}/{result.YearStr}";
                }
            }
            catch
            {
                if (!rtl)
                {
                    var result = new PersianDate();
                    return $"{result.YearStr}\\{result.MonthStr}\\{result.DayStr}";
                }
                else
                {
                    var result = new PersianDate();
                    return $"{result.DayStr}/{result.MonthStr}/{result.YearStr}";
                }
            }
        }

        /// <summary>
        /// از این متد برای تبدیل تاریخ میلادی به شمسی استفاده می شود
        /// </summary>
        /// <remarks>
        /// در صورت بروز خطا در عملیات مقدار تاریخ روز به عنوان نتیجه برگردانده می شود
        /// در صورتی که مقدار پارامتر rtl برابر true باشد تاریخ برعکس برگردانده می شود
        /// </remarks>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// string date = DateConvertor.DateLocalization(DateTime.Now, false, false);
        /// </code>
        /// </example>
        /// <param name="before">تاریخ مورد نظر: System.DateTime</param>
        /// <param name="rtl">برعکس بودن تاریخ: bool</param>
        /// <param name="number">برگرداندن تاریخ بدون جداکننده ها: bool</param>
        /// <returns>string</returns>
        public static string DateLocalization(DateTime before, bool rtl, bool number)
        {
            try
            {
                var result = new PersianDate(before);
                if (!number)
                {
                    return !rtl ? $"{result.YearStr}\\{result.MonthStr}\\{result.DayStr}" : $"{result.DayStr}/{result.MonthStr}/{result.YearStr}";
                }
                if (!rtl)
                {
                    return result.YearStr + result.MonthStr + result.DayStr;
                }
                return result.DayStr + result.MonthStr + result.YearStr;
            }
            catch
            {
                if (!number)
                {
                    if (!rtl)
                    {
                        var result = new PersianDate();
                        return $"{result.YearStr}\\{result.MonthStr}\\{result.DayStr}";
                    }
                    else
                    {
                        var result = new PersianDate();
                        return $"{result.DayStr}/{result.MonthStr}/{result.YearStr}";
                    }
                }
                if (!rtl)
                {
                    var result = new PersianDate();
                    return result.YearStr + result.MonthStr + result.DayStr;
                }
                else
                {
                    var result = new PersianDate();
                    return result.DayStr + result.MonthStr + result.YearStr;
                }
            }
        }

        /// <summary>
        /// از این متد برای تبدیل تاریخ میلادی به شمسی استفاده می شود
        /// </summary>
        /// <remarks>
        /// در صورت بروز خطا در عملیات مقدار تاریخ روز به عنوان نتیجه برگردانده می شود
        /// در صورتی که مقدار پارامتر rtl برابر true باشد تاریخ برعکس برگردانده می شود
        /// </remarks>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// string date = DateConvertor.DateLocalization(DateTime.Now, false, false, ',');
        /// </code>
        /// </example>
        /// <param name="before">تاریخ مورد نظر: System.DateTime</param>
        /// <param name="rtl">برعکس بودن تاریخ: bool</param>
        /// <param name="number">برگرداندن تاریخ بدون جداکننده ها: bool</param>
        /// <param name="limiter">برگرداندن تاریخ با جداکننده مورد نظر خود: char</param>
        /// <returns>string</returns>
        public static string DateLocalization(DateTime before, bool rtl, bool number, char limiter)
        {
            try
            {
                var result = new PersianDate(before);
                if (!number)
                {
                    if (!rtl)
                    {
                        return result.YearStr + limiter + result.MonthStr + limiter + result.DayStr;
                    }
                    return result.DayStr + limiter + result.MonthStr + limiter + result.YearStr;
                }
                if (!rtl)
                {
                    return result.YearStr + result.MonthStr + result.DayStr;
                }
                return result.DayStr + result.MonthStr + result.YearStr;
            }
            catch
            {
                if (!number)
                {
                    if (!rtl)
                    {
                        var result = new PersianDate();
                        return result.YearStr + limiter + result.MonthStr + limiter + result.DayStr;
                    }
                    else
                    {
                        var result = new PersianDate();
                        return result.DayStr + limiter + result.MonthStr + limiter + result.YearStr;
                    }
                }
                if (!rtl)
                {
                    var result = new PersianDate();
                    return result.YearStr + result.MonthStr + result.DayStr;
                }
                else
                {
                    var result = new PersianDate();
                    return result.DayStr + result.MonthStr + result.YearStr;
                }
            }
        }

        /// <summary>
        /// از این متد برای تبدیل تاریخ میلادی به شمسی استفاده می شود
        /// </summary>
        /// <remarks>
        /// در صورت بروز خطا در عملیات مقدار تاریخ روز به عنوان نتیجه برگردانده می شود
        /// در صورتی که مقدار پارامتر rtl برابر true باشد تاریخ برعکس برگردانده می شود
        /// </remarks>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// string date = DateConvertor.DateLocalization("2015/05/02", false, false);
        /// </code>
        /// </example>
        /// <param name="before">تاریخ مورد نظر: string</param>
        /// <param name="rtl">برعکس بودن تاریخ: bool</param>
        /// <param name="number">برگرداندن تاریخ بدون جداکننده ها: bool</param>
        /// <returns>string</returns>
        public static string DateLocalization(string before, bool rtl, bool number)
        {
            try
            {
                if (!number)
                {
                    if (!rtl)
                    {
                        var handle = DateTime.Parse(before);
                        var result = new PersianDate(handle);
                        return $"{result.YearStr}\\{result.MonthStr}\\{result.DayStr}";
                    }
                    else
                    {
                        var handle = DateTime.Parse(before);
                        var result = new PersianDate(handle);
                        return $"{result.DayStr}/{result.MonthStr}/{result.YearStr}";
                    }
                }
                if (!rtl)
                {
                    var handle = DateTime.Parse(before);
                    var result = new PersianDate(handle);
                    return result.YearStr + result.MonthStr + result.DayStr;
                }
                else
                {
                    var handle = DateTime.Parse(before);
                    var result = new PersianDate(handle);
                    return result.DayStr + result.MonthStr + result.YearStr;
                }
            }
            catch
            {
                if (!number)
                {
                    if (!rtl)
                    {
                        var result = new PersianDate();
                        return $"{result.YearStr}\\{result.MonthStr}\\{result.DayStr}";
                    }
                    else
                    {
                        var result = new PersianDate();
                        return $"{result.DayStr}/{result.MonthStr}/{result.YearStr}";
                    }
                }
                if (!rtl)
                {
                    var result = new PersianDate();
                    return result.YearStr + result.MonthStr + result.DayStr;
                }
                else
                {
                    var result = new PersianDate();
                    return result.DayStr + result.MonthStr + result.YearStr;
                }
            }
        }

        /// <summary>
        /// از این متد برای تبدیل تاریخ میلادی به شمسی استفاده می شود
        /// </summary>
        /// <remarks>
        /// در صورت بروز خطا در عملیات مقدار تاریخ روز به عنوان نتیجه برگردانده می شود
        /// در صورتی که مقدار پارامتر rtl برابر true باشد تاریخ برعکس برگردانده می شود
        /// </remarks>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// string date = DateConvertor.DateLocalization("2015/05/02", false, false, ',');
        /// </code>
        /// </example>
        /// <param name="before">تاریخ مورد نظر: string</param>
        /// <param name="rtl">برعکس بودن تاریخ: bool</param>
        /// <param name="number">برگرداندن تاریخ بدون جداکننده ها: bool</param>
        /// <param name="limiter">برگرداندن تاریخ با جداکننده مورد نظر خود: char</param>
        /// <returns>string</returns>
        public static string DateLocalization(string before, bool rtl, bool number, char limiter)
        {
            try
            {
                if (!number)
                {
                    if (!rtl)
                    {
                        var handle = DateTime.Parse(before);
                        var result = new PersianDate(handle);
                        return result.YearStr + limiter + result.MonthStr + limiter + result.DayStr;
                    }
                    else
                    {
                        var handle = DateTime.Parse(before);
                        var result = new PersianDate(handle);
                        return result.DayStr + limiter + result.MonthStr + limiter + result.YearStr;
                    }
                }
                if (!rtl)
                {
                    var handle = DateTime.Parse(before);
                    var result = new PersianDate(handle);
                    return result.YearStr + result.MonthStr + result.DayStr;
                }
                else
                {
                    var handle = DateTime.Parse(before);
                    var result = new PersianDate(handle);
                    return result.DayStr + result.MonthStr + result.YearStr;
                }
            }
            catch
            {
                if (!number)
                {
                    if (!rtl)
                    {
                        var result = new PersianDate();
                        return result.YearStr + limiter + result.MonthStr + limiter + result.DayStr;
                    }
                    else
                    {
                        var result = new PersianDate();
                        return result.DayStr + limiter + result.MonthStr + limiter + result.YearStr;
                    }
                }
                if (!rtl)
                {
                    var result = new PersianDate();
                    return result.YearStr + result.MonthStr + result.DayStr;
                }
                else
                {
                    var result = new PersianDate();
                    return result.DayStr + result.MonthStr + result.YearStr;
                }
            }
        }

        /// <summary>
        /// از این متد برای تبدیل تاریخ شمسی به شمسی از نوع رشته استفاده می شود
        /// </summary>
        /// <remarks>
        /// در صورت بروز خطا در عملیات مقدار تاریخ روز به عنوان نتیجه برگردانده می شود
        /// در صورتی که مقدار پارامتر rtl برابر true باشد تاریخ برعکس برگردانده می شود
        /// </remarks>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// PersianDate persianDate = DateConvertor.ConvertToPersian(DateTime.Now);
        /// string date = DateConvertor.DateLocalization(persianDate, false, false, ',');
        /// </code>
        /// </example>
        /// <param name="before">تاریخ مورد نظر: PersianDate</param>
        /// <param name="rtl">برعکس بودن تاریخ: bool</param>
        /// <param name="number">برگرداندن تاریخ بدون جداکننده ها: bool</param>
        /// <param name="limiter">برگرداندن تاریخ با جداکننده مورد نظر خود: char</param>
        /// <returns>string</returns>
        public static string DateLocalization(PersianDate before, bool rtl, bool number, char limiter)
        {
            try
            {
                if (!number)
                {
                    if (!rtl)
                    {
                        return before.YearStr + limiter + before.MonthStr + limiter + before.DayStr;
                    }
                    return before.DayStr + limiter + before.MonthStr + limiter + before.YearStr;
                }
                if (!rtl)
                {
                    return before.YearStr + before.MonthStr + before.DayStr;
                }
                return before.DayStr + before.MonthStr + before.YearStr;
            }
            catch
            {
                if (!number)
                {
                    if (!rtl)
                    {
                        var result = new PersianDate();
                        return result.YearStr + limiter + result.MonthStr + limiter + result.DayStr;
                    }
                    else
                    {
                        var result = new PersianDate();
                        return result.DayStr + limiter + result.MonthStr + limiter + result.YearStr;
                    }
                }
                if (!rtl)
                {
                    var result = new PersianDate();
                    return result.YearStr + result.MonthStr + result.DayStr;
                }
                else
                {
                    var result = new PersianDate();
                    return result.DayStr + result.MonthStr + result.YearStr;
                }
            }
        }

        /// <summary>
        /// بررسی معتبر بورن تاریخ
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// bool valid = DateConvertor.Validate(DateTime.Now);
        /// </code>
        /// </example>
        /// <param name="date">تاریخ مورد نظر: System.DateTime</param>
        /// <returns>bool</returns>
        public static bool Validate(DateTime date)
        {
            try
            {
                if (ConvertToPersianStr(date).IsNotEmpty())
                    return true;
            }
            catch { return false; }
            return false;
        }

        /// <summary>
        /// بررسی معتبر بورن تاریخ
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// bool valid = DateConvertor.Validate("2015/05/02");
        /// </code>
        /// </example>
        /// <param name="date">تاریخ مورد نظر: string</param>
        /// <returns>bool</returns>
        public static bool Validate(string date)
        {
            try
            {
                ConvertToEnglish(date);
                return true;
            }
            catch { return false; }
        }

        /// <summary>
        /// بدست آوردن فاصله مابین دو تاریخ
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// DateDiff diff = DateConvertor.Difference("2015/05/02", "2016/05/02");
        /// </code>
        /// </example>
        /// <param name="start">تاریخ شروع: string</param>
        /// <param name="end">تاریخ پایان: string</param>
        /// <returns>DateDiff</returns>
        public static DateDiff Difference(string start, string end)
        {
            try
            {
                return Difference(ConvertToEnglish(start), ConvertToEnglish(end));
            }
            catch { return null; }
        }

        /// <summary>
        /// بدست آوردن فاصله مابین دو تاریخ
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// DateDiff diff = DateConvertor.Difference(DateTime.Now, DateTime.Now);
        /// </code>
        /// </example>
        /// <param name="start">تاریخ شروع: System.DateTime</param>
        /// <param name="end">تاریخ پایان: System.DateTime</param>
        /// <returns>DateDiff</returns>
        public static DateDiff Difference(DateTime start, DateTime end)
        {
            try
            {
                if (start.CompareTo(end) < 0)
                {
                    var s = new PersianDate(start);
                    var e = new PersianDate(end);
                    var diff = new DateDiff() { Year = 0, Month = 12 * (end.Year - start.Year) + (end.Month - start.Month) };
                    var day = e.Day - s.Day;
                    if (day < 0)
                    {
                        diff.Month--;
                        diff.Day = PersianMonth.DaysInMonth(diff.Month);
                        diff.Day += day;
                    }
                    if (diff.Month > 12)
                    {
                        diff.Year += byte.Parse((Math.Abs(diff.Month) / 12).ToString());
                        var month = diff.Month;
                        Math.DivRem(month, 12, out month);
                        diff.Month = month;
                    }
                    return diff;
                }
                return null;
            }
            catch { return null; }
        }

        /// <summary>
        /// بدست آوردن اختلاف سال مابین دو تاریخ
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// DateDiff diff = DateConvertor.DifferenceY("2015/05/02", "2016/05/02");
        /// </code>
        /// </example>
        /// <param name="start">تاریخ شروع: string</param>
        /// <param name="end">تاریخ پایان: string</param>
        /// <returns>DateDiff</returns>
        public static DateDiff DifferenceY(string start, string end)
        {
            try
            {
                return DifferenceY(ConvertToEnglish(start), ConvertToEnglish(end));
            }
            catch { return null; }
        }

        /// <summary>
        /// بدست آوردن اختلاف سال مابین دو تاریخ
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// DateDiff diff = DateConvertor.DifferenceY(DateTime.Now, DateTime.Now);
        /// </code>
        /// </example>
        /// <param name="start">تاریخ شروع: System.DateTime</param>
        /// <param name="end">تاریخ پایان: System.DateTime</param>
        /// <returns>DateDiff</returns>
        public static DateDiff DifferenceY(DateTime start, DateTime end)
        {
            try
            {
                if (start.CompareTo(end) < 0)
                {
                    var s = new PersianDate(start);
                    var e = new PersianDate(end);
                    var diff = new DateDiff() { Year = 0, Month = 12 * (end.Year - start.Year) + (end.Month - start.Month) };
                    var day = e.Day - s.Day;
                    if (day < 0)
                    {
                        diff.Month--;
                        diff.Day = PersianMonth.DaysInMonth(diff.Month);
                        diff.Day += day;
                    }
                    return diff;
                }
                return null;
            }
            catch { return null; }
        }

        /// <summary>
        /// بدست آوردن اختلاف سال و ماه مابین دو تاریخ
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// DateDiff diff = DateConvertor.DifferenceYearMonth("2015/05/02", "2016/05/02");
        /// </code>
        /// </example>
        /// <param name="start">تاریخ شروع: string</param>
        /// <param name="end">تاریخ پایان: string</param>
        /// <returns>DateDiff</returns>
        public static DateDiff DifferenceYearMonth(string start, string end)
        {
            try
            {
                return DifferenceYearMonth(ConvertToEnglish(start), ConvertToEnglish(end));
            }
            catch { return null; }
        }

        /// <summary>
        /// بدست آوردن اختلاف سال و ماه مابین دو تاریخ
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// DateDiff diff = DateConvertor.DifferenceYearMonth(DateTime.Now, DateTime.Now);
        /// </code>
        /// </example>
        /// <param name="start">تاریخ شروع: System.DateTime</param>
        /// <param name="end">تاریخ پایان: System.DateTime</param>
        /// <returns>DateDiff</returns>
        public static DateDiff DifferenceYearMonth(DateTime start, DateTime end)
        {
            try
            {
                if (start.CompareTo(end) < 0)
                {
                    var diff = new DateDiff { Year = 0, Month = 0, Day = int.Parse(end.Subtract(start).TotalDays.ToString(Thread.CurrentThread.CurrentCulture)) };
                    return diff;
                }
                return null;
            }
            catch { return null; }
        }

        /// <summary>
        /// بدست آوردن اختلاف مابین دو تاریخ شمسی به روز
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// long days = DateConvertor.PersianDayDiff(DateTime.Now, DateTime.Now);
        /// </code>
        /// </example>
        /// <param name="start">تاریخ شروع: System.DateTime</param>
        /// <param name="end">تاریخ پایان: System.DateTime</param>
        /// <returns>long</returns>
        public static long PersianDayDiff(DateTime start, DateTime end)
        {
            try
            {
                if (start.CompareTo(end) < 0)
                {
                    var sDate = new PersianDate(start);
                    var eDate = new PersianDate(end);
                    long days = 0;
                    if (sDate.Year == eDate.Year)
                    {
                        if (sDate.Month == eDate.Month)
                        {
                            days += eDate.Day - sDate.Day;
                        }
                        else
                        {
                            for (var i = sDate.Month; i < eDate.Month; i++)
                            {
                                days += PersianMonth.DaysInMonth(i);
                            }
                            days += eDate.Day - sDate.Day;
                        }
                    }
                    else
                    {
                        days += (eDate.Year - sDate.Year) * 365;
                        if (sDate.Month == eDate.Month)
                        {
                            days += eDate.Day - sDate.Day;
                        }
                        else
                        {
                            for (var i = sDate.Month; i < eDate.Month; i++)
                            {
                                days += PersianMonth.DaysInMonth(i);
                            }
                            days += eDate.Day - sDate.Day;
                        }
                    }
                    return days;
                }
                return 0;
            }
            catch { return 0; }
        }

        /// <summary>
        /// این متد به تاریخ مورد نظر به تعداد ماه دلخواه اضافه می کند
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// DateTime date = DateConvertor.AddMonth(DateTime.Now, 5);
        /// </code>
        /// </example>
        /// <param name="date">تاریخ: System.DateTime</param>
        /// <param name="value">تعداد ماه: int</param>
        /// <returns>System.DateTime</returns>
        public static DateTime AddMonth(DateTime date, int value)
        {
            try
            {
                var pDate = new PersianDate(date);
                var year = pDate.Year;
                var month = pDate.Month;
                var day = pDate.Day;
                for (var i = 0; i < value; i++)
                {
                    if (month < 12)
                    {
                        month++;
                    }
                    else if (month == 12)
                    {
                        year++;
                        month = 1;
                    }
                }
                pDate.Year = year;
                pDate.Month = month;
                pDate.Day = day;
                return pDate.ToEnglish();
            }
            catch { return date; }
        }

        /// <summary>
        /// این متد به تاریخ مورد نظر به تعداد روز دلخواه اضافه می کند
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// DateTime date = DateConvertor.AddDay(DateTime.Now, 50);
        /// </code>
        /// </example>
        /// <param name="date">تاریخ: System.DateTime</param>
        /// <param name="value">تعداد روز: int</param>
        /// <returns>System.DateTime</returns>
        public static DateTime AddDay(DateTime date, int value)
        {
            try
            {
                var pDate = new PersianDate(date);
                var year = pDate.Year;
                var month = pDate.Month;
                var day = pDate.Day;
                if (value >= 0)
                {
                    for (var i = 0; i < value; i++)
                    {
                        if (day < PersianMonth.DaysInMonth(month))
                        {
                            day++;
                        }
                        else if (day == PersianMonth.DaysInMonth(month))
                        {
                            if (month < 12)
                            {
                                month++;
                                day = 1;
                            }
                            else if (month == 12)
                            {
                                year++;
                                month = 1;
                                day = 1;
                            }
                        }
                    }
                }
                else
                {
                    for (var i = value; i < 0; i++)
                    {
                        if (day > 1)
                        {
                            day--;
                        }
                        else if (day == 1)
                        {
                            if (month > 1 && month <= 6)
                            {
                                month--;
                                day = 31;
                            }
                            if (month > 6 && month <= 12)
                            {
                                month--;
                                day = 30;
                            }
                            else if (month == 1)
                            {
                                year--;
                                month = 12;
                                day = 29;
                            }
                        }
                    }
                }
                pDate.Year = year;
                pDate.Month = month;
                pDate.Day = day;
                return pDate.ToEnglish();
            }
            catch { return date; }
        }

        /// <summary>
        /// بدست آوردن فاصله مابین دو تاریخ
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// DateDiff diff = DateConvertor.Diff("2015/02/05", "2016/02/05");
        /// </code>
        /// </example>
        /// <param name="start">تاریخ شروع: string</param>
        /// <param name="end">تاریخ پایان: string</param>
        /// <returns>DateDiff</returns>
        public static DateDiff Diff(string start, string end)
        {
            try
            {
                start = FixDate(start);
                end = FixDate(end);
                var sYear = int.Parse(start.Substring(0, 4));
                var sMonth = int.Parse(start.Substring(5, 2));
                var sDay = int.Parse(start.Substring(8, 2));
                var eYear = int.Parse(end.Substring(0, 4));
                var eMonth = int.Parse(end.Substring(5, 2));
                var eDay = int.Parse(end.Substring(8, 2));
                var sEnglish = new PersianDate(sYear, sMonth, sDay).ToEnglish();
                var eEnglish = new PersianDate(eYear, eMonth, eDay).ToEnglish();
                return Diff(sEnglish, eEnglish);
            }
            catch { return null; }
        }

        /// <summary>
        /// بدست آوردن فاصله مابین دو تاریخ
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// DateDiff diff = DateConvertor.Diff(DateTime.Now, DateTime.Now);
        /// </code>
        /// </example>
        /// <param name="start">تاریخ شروع: System.DateTime</param>
        /// <param name="end">تاریخ پایان: System.DateTime</param>
        /// <returns>DateDiff</returns>
        public static DateDiff Diff(DateTime start, DateTime end)
        {
            try
            {
                var sEnglish = start;
                var eEnglish = end;
                var sDate = new PersianDate(start);
                var eDate = new PersianDate(end);
                if (sEnglish.CompareTo(eEnglish) < 0)
                {
                    var subtract = new DateDiff { Year = eDate.Year - sDate.Year >= 0 ? eDate.Year - sDate.Year : 0 };
                    if (eDate.Month - sDate.Month >= 0)
                    {
                        subtract.Month = eDate.Month - sDate.Month;
                        if (subtract.Month == 0)
                        {
                            subtract.Year--;
                            subtract.Month = 12;
                        }
                    }
                    else
                    {
                        subtract.Month = 12 + (eDate.Month - sDate.Month);
                        subtract.Year--;
                    }
                    if (eDate.Day - sDate.Day >= 0)
                    {
                        subtract.Day = eDate.Day - sDate.Day;
                    }
                    else
                    {
                        subtract.Day = 30 + (eDate.Day - sDate.Day);
                        if (subtract.Month > 1)
                        {
                            subtract.Month--;
                        }
                        else
                        {
                            subtract.Year--;
                            subtract.Month = 12;
                        }
                    }
                    return subtract;
                }
                return null;
            }
            catch { return null; }
        }

        /// <summary>
        /// دریافت تاریخ شمسی در قالب PersianDate
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// PersianDate date = DateConvertor.ParsePersianDate("1395/05/02");
        /// </code>
        /// </example>
        /// <param name="before">تاریخ: string</param>
        /// <returns>PersianDate</returns>
        public static PersianDate ParsePersianDate(string before)
        {
            try
            {
                before = FixDate(before);
                var year = int.Parse(before.Substring(0, 4));
                var month = int.Parse(before.Substring(5, 2));
                var day = int.Parse(before.Substring(8, 2));
                return new PersianDate(year, month, day);
            }
            catch { return new PersianDate(); }
        }

        #endregion

    }
}

