﻿namespace Utility
{

    #region Assembly

    using System;
    using System.Collections.ObjectModel;
    using System.Linq;

    #endregion

    /// <summary>
    /// از این کلاس برای تبدیل اعداد و حروف به فارسی و انگلیسی استفاده می شود
    /// </summary>
    public class CommonUtil
    {

        #region Attribute

        /// <summary>
        /// لیست حروف فارسی
        /// </summary>
        public static Collection<char> PersianCharachter = new Collection<char> { 'و', 'ئ', 'ء', 'د', 'أ', 'ذ', 'إ', 'ر', 'ؤ', 'ز', 'ژ', 'ط', 'ي', 'ظ', 'ة', 'گ', 'ک', 'م', 'ن', 'ت', 'ا', 'آ', 'ل', 'ۀ', 'ب', 'ی', 'س', 'ش', 'پ', 'چ', 'ج', 'ح', 'خ', 'ه', 'ع', 'غ', 'ف', 'ق', 'ث', 'ص', 'ض', 'ك', 'ي', ' ' };

        /// <summary>
        /// لیست حروف فارسی مپ کیبورد
        /// </summary>
        public static Collection<char> PersianCharachterE = new Collection<char> { 'ش', 'ذ', 'ز', 'ی', 'ث', 'ب', 'ل', 'ا', 'ه', 'ت', 'ن', 'م', 'ئ', 'د', 'خ', 'ح', 'ض', 'ق', 'س', 'ف', 'ع', 'ر', 'ص', 'ط', 'غ', 'ظ', '\0', 'إ', 'ژ', '\0', '\0', '\0', 'ۀ', 'آ', '\0', '\0', '\0', '\0', 'ء', 'أ', '\0', '\0', '\0', '\0', '\0', '\0', '\0', 'ؤ', '\0', 'ي', '\0', 'ة', 'پ', 'چ', 'ج', 'پ', 'گ', 'ک', 'و', ' ' };

        /// <summary>
        /// لیست اعداد فارسی
        /// </summary>
        public static Collection<char> PersianNumber = new Collection<char> { '٠', '١', '٢', '٣', '۴', '۵', '۶', '٧', '٨', '٩' };

        /// <summary>
        /// لیست اعداد فارسی
        /// </summary>
        public static Collection<char> PersianNumberE = new Collection<char> { '۰', '۱', '۲', '۳', '۴', '۵', '۶', '۷', '۸', '۹' };

        /// <summary>
        /// لیست حروف انگلیسی
        /// </summary>
        public static Collection<char> EnglishCharachter = new Collection<char> { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', ' ' };

        /// <summary>
        /// لیست حروف انگلیسی مپ کیبورد
        /// </summary>
        public static Collection<char> EnglishCharachterE = new Collection<char> { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', '`', ']', '[', '\\', '\'', ';', ',', ' ' };

        /// <summary>
        /// لیست اعداد انگلیسی
        /// </summary>
        public static Collection<char> EnglishNumber = new Collection<char> { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' };

        #endregion

        #region Method

        /// <summary>
        /// از این متد برای بررسی عدد صحیح بودن یک مقدار استفاده می شود
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// bool isInteger = CommonUtil.IsInteger(123);
        /// </code>
        /// </example>
        /// <param name="value">مقدار مورد نظر: dynamic</param>
        /// <returns>bool</returns>
        public static bool IsInteger(dynamic value)
        {
            try
            {
                return
                    value is sbyte ||
                    value is byte ||
                    value is short ||
                    value is ushort ||
                    value is int ||
                    value is uint ||
                    value is long ||
                    value is ulong;
            }
            catch (Exception ex) { Logger.Log(ex); }
            return false;
        }

        /// <summary>
        /// از این متد برای بررسی عدد اعشاری بودن یک مقدار استفاده می شود
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// float isFloat = CommonUtil.IsFloat(123.2);
        /// </code>
        /// </example>
        /// <param name="value">مقدار مورد نظر: dynamic</param>
        /// <returns>bool</returns>
        public static bool IsFloat(dynamic value)
        {
            try
            {
                return
                    value is float ||
                    value is double ||
                    value is decimal;
            }
            catch (Exception ex) { Logger.Log(ex); }
            return false;
        }

        /// <summary>
        /// از این متد برای بررسی عدد بودن یک مقدار استفاده می شود
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// bool isNumeric = CommonUtil.IsNumeric(123);
        /// </code>
        /// </example>
        /// <param name="value">مقدار مورد نظر: dynamic</param>
        /// <returns>bool</returns>
        public static bool IsNumeric(dynamic value)
        {
            try
            {
                return IsInteger(value) || IsFloat(value);
            }
            catch (Exception ex) { Logger.Log(ex); }
            return false;
        }

        /// <summary>
        /// از این متد برای بررسی تهی بودن مقدار یک رشته استفاده می شود
        /// </summary>
        /// <remarks>
        /// از این متد فقط در رشته ها می توان بهره برد
        /// </remarks>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// bool isEmpty = CommonUtil.IsEmpty("");
        /// </code>
        /// </example>
        /// <param name="value">رشته مورد نظر: string</param>
        /// <returns>bool</returns>
        public static bool IsEmpty(string value)
        {
            return string.IsNullOrEmpty(value);
        }

        /// <summary>
        /// از این متد برای بررسی تهی نبودن مقدار یک رشته استفاده می شود
        /// </summary>
        /// <remarks>
        /// از این متد فقط در رشته ها می توان بهره برد
        /// </remarks>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// bool isNotEmpty = CommonUtil.IsNotEmpty("");
        /// </code>
        /// </example>
        /// <param name="value">رشته مورد نظر: string</param>
        /// <returns>bool</returns>
        public static bool IsNotEmpty(string value)
        {
            return !string.IsNullOrEmpty(value);
        }

        /// <summary>
        /// از این متد برای تصحیح کاراکترهای فارسی استفاده می شود
        /// </summary>
        /// <remarks>
        /// این متد بعضی کاراکترهای فارسی را که عربی نوشته می شود تبدیل به فارسی می کند مثل: ك به ک
        /// </remarks>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// char fixedChar = CommonUtil.PersianFix('ك');
        /// </code>
        /// </example>
        /// <param name="ch">کاراکتر مورد نظر: char</param>
        /// <returns>char</returns>
        public static char PersianFix(char ch)
        {
            try
            {
                if (ch == 'ك')
                    ch = 'ک';
                if (ch == 'ي')
                    ch = 'ی';

                if (ch == 1610)
                    ch = (char)1740;
                if (ch == 1603)
                    ch = (char)1705;

                if (ch == 1610)
                    ch = (char)1740;
                if (ch == 1603)
                    ch = (char)1705;

                return ch;
            }
            catch { return ch; }
        }

        /// <summary>
        /// از این متد برای تصحیح کاراکترهای فارسی یک رشته استفاده می شود
        /// </summary>
        /// <remarks>
        /// این متد بعضی کاراکترهای فارسی را که عربی نوشته می شود تبدیل به فارسی می کند مثل: ك به ک
        /// </remarks>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// string fixedString = CommonUtil.PersianFix("كمك");
        /// </code>
        /// </example>
        /// <param name="str">رشته مورد نظر: string</param>
        /// <returns>string</returns>
        public static string PersianFix(string str)
        {
            try
            {
                return str.Aggregate(string.Empty, (current, t) => current + PersianFix(t));
            }
            catch { return str; }
        }

        /// <summary>
        /// از این متد برای دریافت داده شمارشی براساس اندیس عددی آن استفاده می شود
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// public enum Toggle : byte { False = 0, True = 1 }
        /// Toggle enum = CommonUtil.GetEnum&lt;Toggle&gt;(0);
        /// </code>
        /// </example>
        /// <param name="number">اندیس: int</param>
        /// <returns>GenericType</returns>
        public T GetEnum<T>(int number)
        {
            if (Enum.IsDefined(typeof(T), number))
                return (T)Enum.ToObject(typeof(T), number);
            throw new ArgumentException(typeof(T) + "does not contain a value member = " + number);

        }

        /// <summary>
        /// از این متد برای دریافت داده شمارشی براساس نام آن استفاده می شود
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// public enum Toggle : byte { False = 0, True = 1 }
        /// Toggle enum = CommonUtil.GetEnum&lt;Toggle&gt;("False");
        /// </code>
        /// </example>
        /// <param name="name">نام: string</param>
        /// <returns>GenericType</returns>
        public static T GetEnum<T>(string name)
        {
            if (Enum.IsDefined(typeof(T), name))
                return (T)Enum.Parse(typeof(T), name);
            throw new ArgumentException(typeof(T) + "does not contain a value member = " + name);
        }

        /// <summary>
        /// این متد بررسی می کند که آیا کاراکتر موردنظر یک کاراکتر فارسی هست یا نه
        /// </summary>
        /// <remarks>
        /// براساس لیست حروف فارسی مپ کیبورد
        /// </remarks>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// bool isPersianCharachter = CommonUtil.IsPersianCharachter('ل');
        /// </code>
        /// </example>
        /// <param name="ch">کاراکتر مورد نظر: char</param>
        /// <returns>bool</returns>
        public static bool IsPersianCharachter(char ch)
        {
            return PersianCharachterE.Contains(ch);
        }

        /// <summary>
        /// این متد بررسی می کند که آیا کاراکتر موردنظر یک عدد فارسی هست یا نه
        /// </summary>
        /// <remarks>
        /// براساس لیست اعداد فارسی
        /// </remarks>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// bool isPersianNumber = CommonUtil.IsPersianNumber('١');
        /// </code>
        /// </example>
        /// <param name="ch">کاراکتر مورد نظر: char</param>
        /// <returns>bool</returns>
        public static bool IsPersianNumber(char ch)
        {
            return PersianNumber.Contains(ch) || PersianNumberE.Contains(ch);
        }

        /// <summary>
        /// این متد بررسی می کند که آیا کاراکتر موردنظر یک کاراکتر انگلیسی هست یا نه
        /// </summary>
        /// <remarks>
        /// براساس لیست حروف انگلیسی مپ کیبورد
        /// </remarks>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// bool isEnglishCharachter = CommonUtil.IsEnglishCharachter('a');
        /// </code>
        /// </example>
        /// <param name="ch">کاراکتر مورد نظر: char</param>
        /// <returns>bool</returns>
        public static bool IsEnglishCharachter(char ch)
        {
            return EnglishCharachter.Contains(ch);
        }

        /// <summary>
        /// این متد بررسی می کند که آیا کاراکتر موردنظر یک عدد انگلیسی هست یا نه
        /// </summary>
        /// <remarks>
        /// براساس لیست اعداد انگلیسی
        /// </remarks>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// bool isEnglishNumber = CommonUtil.IsEnglishNumber('1');
        /// </code>
        /// </example>
        /// <param name="ch">کاراکتر مورد نظر: char</param>
        /// <returns>bool</returns>
        public static bool IsEnglishNumber(char ch)
        {
            return EnglishNumber.Contains(ch);
        }

        #endregion

    }
}
