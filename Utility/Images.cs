﻿namespace Utility
{

    #region Assembly

    using System;
    using System.Drawing;
    using System.Drawing.Drawing2D;
    using System.Drawing.Imaging;
    using System.Drawing.Printing;
    using System.IO;
    using System.Windows.Forms;
    using System.Linq;

    #endregion

    /// <summary>
    /// این کلاس برای کار، بر روی تصاویر می باشد
    /// </summary>
    public static class Images
    {

        #region Method

        /// <summary>
        /// این متد پارامتری از نوع تصویر دریافت کرده و آن را به آرایه ای از نوع بایت تبدیل می کند
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// byte[] imageBytes = Images.ImageToArray(image);
        /// </code>
        /// </example>
        /// <param name="image">تصویر اصلی: System.Drawing.Image</param>
        /// <returns>byte[]</returns>
        public static byte[] ImageToArray(Image image)
        {
            try
            {
                var ms = new MemoryStream();
                image.Save(ms, ImageFormat.Jpeg);
                return ms.ToArray();
            }
            catch { return null; }
        }

        /// <summary>
        /// این متد پارامتری از نوع آرایه ای از بایت دریافت کرده و آن را به تصویر تبدیل می کند
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// Image image = Images.ArrayToImage(imageBytes);
        /// </code>
        /// </example>
        /// <param name="byteArrayIn">آرایه ای از نوع بایت: byte[]</param>
        /// <returns>System.Drawing.Image</returns>
        public static Image ArrayToImage(byte[] byteArrayIn)
        {
            try
            {
                var ms = new MemoryStream(byteArrayIn);
                var returnImage = Image.FromStream(ms);
                return returnImage;
            }
            catch { return null; }
        }

        /// <summary>
        /// این متد پارامتری از نوع تصویر دریافت کرده و آن را به تصویر بندانگشتی تبدیل می کند
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// Image image = Images.GenerateThumbnails(0.5, image);
        /// </code>
        /// </example>
        /// <param name="scaleFactor">مقیاس عکس بندانگشتی نسبت به عکس اصلی: double</param>
        /// <param name="original">تصویر اصلی: System.Drawing.Image</param>
        /// <returns>System.Drawing.Image</returns>
        public static Image GenerateThumbnails(double scaleFactor, Image original)
        {
            try
            {
                var newWidth = (int)(original.Width * scaleFactor);
                var newHeight = (int)(original.Height * scaleFactor);
                var thumbnailImg = new Bitmap(newWidth, newHeight);
                var thumbGraph = Graphics.FromImage(thumbnailImg);
                thumbGraph.CompositingQuality = CompositingQuality.HighQuality;
                thumbGraph.SmoothingMode = SmoothingMode.HighQuality;
                thumbGraph.InterpolationMode = InterpolationMode.HighQualityBicubic;
                var imageRectangle = new Rectangle(0, 0, newWidth, newHeight);
                thumbGraph.DrawImage(original, imageRectangle);
                return thumbnailImg;
            }
            catch { return original; }
        }

        /// <summary>
        /// این متد پارامتری از نوع تصویر دریافت کرده و آن را به تصویر بندانگشتی تبدیل می کند
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// Image image = Images.GenerateThumbnails(100, 75, image);
        /// </code>
        /// </example>
        /// <param name="width">عرض عکس بندانگشتس : int</param>
        /// <param name="height">ارتفاع عکس بندانگشتس : int</param>
        /// <param name="original">تصویر اصلی: System.Drawing.Image</param>
        /// <returns>System.Drawing.Image</returns>
        public static Image GenerateThumbnails(int width, int height, Image original)
        {
            try
            {
                var newWidth = width;
                var newHeight = original.Height * width / original.Width;
                if (newHeight > original.Height)
                {
                    newWidth = original.Width * original.Height / original.Height;
                    newHeight = original.Height;
                }
                var thumbnailImg = new Bitmap(newWidth, newHeight);
                var thumbGraph = Graphics.FromImage(thumbnailImg);
                thumbGraph.CompositingQuality = CompositingQuality.HighQuality;
                thumbGraph.SmoothingMode = SmoothingMode.HighQuality;
                thumbGraph.InterpolationMode = InterpolationMode.HighQualityBicubic;
                var imageRectangle = new Rectangle(0, 0, newWidth, newHeight);
                thumbGraph.DrawImage(original, imageRectangle);
                return thumbnailImg;
            }
            catch { return original; }
        }

        /// <summary>
        /// این متد پارامتری از نوع تصویر دریافت کرده و آن را به پرینتر برای چاپ ارسال می کند
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// Images.PrintImage(image);
        /// </code>
        /// </example>
        /// <param name="image">تصویر اصلی: System.Drawing.Image</param>
        public static void PrintImage(Image image)
        {
            try
            {
                new ImagePrinter().PrintImage(image);
            }
            catch (Exception ex) { Logger.Log(ex); }
        }

        /// <summary>
        /// این متد پارامتری از نوع تصویر دریافت کرده و فشرده کرده و حجمش را پایین می آورد 
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// Image image = Images.CompressImage(image, 80);
        /// </code>
        /// </example>
        /// <param name="sourceImage">تصویر اصلی: System.Drawing.Image</param>
        /// <param name="imageQuality">درصد کیفیت عکس : int</param>
        /// <returns>System.Drawing.Image</returns>
        public static Image CompressImage(Image sourceImage, int imageQuality)
        {
            try
            {
                //Create an ImageCodecInfo-object for the codec information

                //Set quality factor for compression
                var imageQualitysParameter = new EncoderParameter(Encoder.Quality, imageQuality);

                //List all avaible codecs (system wide)
                var alleCodecs = ImageCodecInfo.GetImageEncoders();

                var codecParameter = new EncoderParameters(1) {Param = {[0] = imageQualitysParameter}};

                //Find and choose JPEG codec
                var jpegCodec = alleCodecs.FirstOrDefault(codec => codec.MimeType == "image/jpeg");
                //Save compressed image
                var stream = new MemoryStream();
                if (jpegCodec != null)
                    sourceImage.Save(stream, jpegCodec, codecParameter);
                else
                    throw new Exception("Jpeg codec is null.");

                return Image.FromStream(stream);
            }
            catch (Exception ex) { Logger.Log(ex); }
            return sourceImage;
        }

        #endregion

    }

    /// <summary>
    /// این کلاس برای چاپ تصاویر می باشد
    /// </summary>
    internal class ImagePrinter
    {

        #region Attribute

        private readonly Image.GetThumbnailImageAbort _thumbnailCallback;

        private Image _image;

        #endregion

        #region Method

        public ImagePrinter()
        {
            _thumbnailCallback += ThumbnailAbort;
        }

        private static bool ThumbnailAbort()
        {
            return false;
        }

        private void Print_Handler(object sender, PrintPageEventArgs e)
        {
            try
            {
                var rect = e.Graphics.VisibleClipBounds;
                var margin = Math.Min(rect.Height / _image.Height, rect.Width / _image.Width);
                _image = _image.GetThumbnailImage((int)(_image.Height * margin), (int)(_image.Width * margin), _thumbnailCallback, IntPtr.Zero);
                e.Graphics.DrawImage(_image, new Rectangle(0, 0, _image.Height, _image.Width));
            }
            catch (Exception ex) { Logger.Log(ex); }
        }

        /// <summary>
        /// این متد پارامتری از نوع تصویر دریافت کرده و آن را برای چاپ به پرینتر ارسال می کند
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// ImagePrinter.PrintImage(image);
        /// </code>
        /// </example>
        /// <param name="img">تصویر اصلی: System.Drawing.Image</param>
        public void PrintImage(Image img)
        {
            try
            {
                _image = img;
                var printer = new PrintDocument();
                printer.PrintPage += Print_Handler;
                var printDialog = new PrintDialog
                {
                    Document = printer
                };
                if (printDialog.ShowDialog() != DialogResult.OK)
                    return;
                var previewDialog = new PrintPreviewDialog
                {
                    Document = printer
                };
                if (previewDialog.ShowDialog() == DialogResult.OK)
                    printer.Print();
            }
            catch (Exception ex) { Logger.Log(ex); }
        }

        #endregion

    }
}
