﻿namespace Utility
{

    #region Assembly

    using System.Globalization;
    using System.IO;
    using System;
    using System.Web;
    using System.Text;

    #endregion

    /// <summary>
    /// این کلاس برای گزارش کردن خطا های برنامه می باشد که توسط برنامه نویس استفاده می شود
    /// </summary>
    /// <remarks>
    /// خطاهای تولیدی در برنامه در زمان اجرا توسط پکیچ elmah پوشش داده می شود.
    /// </remarks>
    public static class Logger
    {

        #region Method

        /// <summary>
        /// این متد پارامتری را دریافت و خطای تولیدی براساس آن را تولید و در محل تعریف شده ذخیره می کند
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// try {
        /// }
        /// catch (Exception ex) {
        ///     Logger.Log(ex);
        /// }
        /// Logger.Log(new Exception("The error has occured."));
        /// </code>
        /// </example>
        /// <param name="ex">خطای ارسالی: System.Exception</param>
        public static void Log(Exception ex)
        {
            try
            {
                var builder = new StringBuilder();
                builder
                    .AppendLine("----------")
                    .AppendLine(DateTime.Now.ToString(CultureInfo.InvariantCulture))
                    .AppendFormat("Source:\t{0}", ex.Source)
                    .AppendLine()
                    .AppendFormat("Target:\t{0}", ex.TargetSite)
                    .AppendLine()
                    .AppendFormat("Type:\t{0}", ex.GetType().Name)
                    .AppendLine()
                    .AppendFormat("Message:\t{0}", ex.Message)
                    .AppendLine()
                    .AppendFormat("Stack:\t{0}", ex.StackTrace)
                    .AppendLine();

                var filePath = HttpContext.Current?.Server.MapPath("~/App_Data/Error.log");
                if (filePath != null && filePath.IsNotEmpty())
                {
                    using (var writer = File.AppendText(filePath))
                    {
                        writer.Write(builder.ToString());
                        writer.Flush();
                    }
                }
            }
            catch (Exception)
            {
                // ignored
            }
        }

        #endregion

    }
}
