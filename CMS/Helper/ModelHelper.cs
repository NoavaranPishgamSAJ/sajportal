﻿namespace CMS.Helper
{

    #region Assembly

    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Web.Mvc;
    using DataService;
    using ORM.Model;

    #endregion

    /// <summary>
    /// کلاس راهنمای مدل، شامل یکسری متدها برای استفاده در کنترل کننده ها و نماها جهت سهولت کار می باشد
    /// </summary>
    public class ModelHelper
    {

        #region Method

        /// <summary>
        /// این متد با دریافت یک داده شمارشی یک لیست از نوع SelectListItem برای استفاده در ساخت DropDownList برمی گرداند
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// IEnumerable&lt;SelectListItem&gt; list = ModelHelper.EnumToSelectList&lt;PersonType&gt;();
        /// </code>
        /// </example>
        /// <returns>IEnumerable&lt;SelectListItem&gt;</returns>
        public static IEnumerable<SelectListItem> EnumToSelectList<T>()
        {
            return Enum.GetValues(typeof(T)).Cast<T>().Select(e => new SelectListItem() { Text = e.ToString(), Value = e.ToString() }).ToList();
        }

        /// <summary>
        /// از این متد برای دریافت منبع مورد نظر استفاده می شود
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// Repository currentRepository = ModelHelper.GetRepository(2);
        /// </code>
        /// </example>
        /// <param name="id">شناسه منبع: int</param>
        /// <returns>Repository</returns>
        public static Repository GetRepository(int id)
        {
            return DynamicService.Current.GetItem<Repository>(id);
        }

        /// <summary>
        /// از این متد برای دریافت محتوا مورد نظر استفاده می شود
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// Content currentContent = ModelHelper.GetContent(2);
        /// </code>
        /// </example>
        /// <param name="id">شناسه محتوا: int</param>
        /// <returns>Content</returns>
        public static Content GetContent(int id)
        {
            return DynamicService.Current.GetItem<Content>(id);
        }

        /// <summary>
        /// از این متد برای دریافت پوشه مورد نظر استفاده می شود
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// FolderView currentFolder = ModelHelper.GetFolder(2);
        /// </code>
        /// </example>
        /// <param name="id">شناسه محتوا: int</param>
        /// <returns>FolderView</returns>
        public static FolderView GetFolder(int id)
        {
            return DynamicService.Current.GetItem<FolderView>(id);
        }

        /// <summary>
        /// از این متد برای دریافت عضویت مورد نظر استفاده می شود
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// Membership currentMembership = ModelHelper.GetMembership(2);
        /// </code>
        /// </example>
        /// <param name="id">شناسه محتوا: int</param>
        /// <returns>Membership</returns>
        public static Membership GetMembership(int id)
        {
            return DynamicService.Current.GetItem<Membership>(id);
        }

        /// <summary>
        /// از این متد برای دریافت نام جدول در دیتابیس استفاده می شود
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// string table = ModelHelper.GetTableName("ActiveSite", "Person");
        /// </code>
        /// </example>
        /// <param name="repositoryName">نام مخزن: string</param>
        /// <param name="contentName">نام محتوا: string</param>
        /// <returns>string</returns>
        public static string GetTableName(string repositoryName, string contentName)
        {
            return $@"{repositoryName}_{contentName}";
        }

        /// <summary>
        /// از این متد برای دریافت یک موجودیت خام از محتوای مورد نظر استفاده می شود
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// dynamic item = ModelHelper.GetEntity("~", "ActiveSite", "Person");
        /// </code>
        /// </example>
        /// <param name="path">مسیر فایل dll موجودیت جدول: string</param>
        /// <param name="repositoryName">نام مخزن: string</param>
        /// <param name="contentName">نام محتوا: string</param>
        /// <returns>dynamic</returns>
        public static dynamic GetEntity(string path, string repositoryName, string contentName)
        {
            var tableName = GetTableName(repositoryName, contentName);
            return CreateInstance($"{path}\\CmsData\\Repository\\{repositoryName}\\{tableName}.dll", tableName);
        }

        /// <summary>
        /// از این متد برای دریافت یک نمونه خام از محتوای مورد نظر استفاده می شود
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// dynamic item = CreateInstance("~\\CmsData\\Repository\\ActiveSite\\ActiveSite_Person.dll", "ActiveSite_Person");
        /// </code>
        /// </example>
        /// <param name="path">مسیر فایل dll موجودیت جدول: string</param>
        /// <param name="classname">نام کلاس جدول: string</param>
        /// <returns>dynamic</returns>
        private static dynamic CreateInstance(string path, string classname)
        {
            return Activator.CreateInstance(Assembly.LoadFrom(path).GetType($"ORM.Model.{classname}"));
        }

        /// <summary>
        /// این متد نوع کنترل های ورودی داده برای فرم را در یک لیست از نوع SelectListItem برای استفاده در ساخت DropDownList برمی گرداند
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// IEnumerable&lt;SelectListItem&gt; list = ModelHelper.GetControlType();
        /// </code>
        /// </example>
        /// <returns>IEnumerable&lt;SelectListItem&gt;</returns>
        public static List<SelectListItem> GetControlType()
        {
            return new List<SelectListItem>
            {
                new SelectListItem { Text = @"Text box", Value = "TextBox" },
                new SelectListItem { Text = @"Text area", Value = "TextArea" },
                new SelectListItem { Text = @"Text editor", Value = "TextEditor" },
                new SelectListItem { Text = @"Drop down list", Value = "DropDownList" },
                new SelectListItem { Text = @"Check box", Value = "CheckBox" },
                new SelectListItem { Text = @"Radio button", Value = "RadioButton" },
                new SelectListItem { Text = @"Image", Value = "Image" },
                new SelectListItem { Text = @"Date", Value = "Date" }
            };
        }

        /// <summary>
        /// این متد نوع فیلدهای فرم را در یک لیست از نوع SelectListItem برای استفاده در ساخت DropDownList برمی گرداند
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// IEnumerable&lt;SelectListItem&gt; list = ModelHelper.GetDataType();
        /// </code>
        /// </example>
        /// <returns>IEnumerable&lt;SelectListItem&gt;</returns>
        public static List<SelectListItem> GetDataType()
        {
            return new List<SelectListItem>
            {
                new SelectListItem {Text = @"bit", Value = "bit"},
                new SelectListItem {Text = @"binary", Value = "binary"},
                new SelectListItem {Text = @"varbinary", Value = "varbinary"},
                new SelectListItem {Text = @"tinyint", Value = "tinyint"},
                new SelectListItem {Text = @"smallint", Value = "smallint"},
                new SelectListItem {Text = @"int", Value = "int"},
                new SelectListItem {Text = @"bigint", Value = "bigint"},
                new SelectListItem {Text = @"money", Value = "money"},
                new SelectListItem {Text = @"smallmoney", Value = "smallmoney"},
                new SelectListItem {Text = @"float", Value = "float"},
                new SelectListItem {Text = @"real", Value = "real"},
                new SelectListItem {Text = @"decimal", Value = "decimal"},
                new SelectListItem {Text = @"char", Value = "char"},
                new SelectListItem {Text = @"nchar", Value = "nchar"},
                new SelectListItem {Text = @"varchar", Value = "varchar"},
                new SelectListItem {Text = @"nvarchar", Value = "nvarchar"},
                new SelectListItem {Text = @"text", Value = "text"},
                new SelectListItem {Text = @"ntext", Value = "ntext"},
                new SelectListItem {Text = @"date", Value = "date"},
                new SelectListItem {Text = @"datetime", Value = "datetime"},
                new SelectListItem {Text = @"datetime2", Value = "datetime2"},
                new SelectListItem {Text = @"smalldatetime", Value = "smalldatetime"},
                new SelectListItem {Text = @"time", Value = "time"},
                new SelectListItem {Text = @"timestamp", Value = "timestamp"}
            };
        }

        /// <summary>
        /// این متد نوع اعتبارسنجی فیلدهای فرم را در یک لیست از نوع SelectListItem برای استفاده در ساخت DropDownList برمی گرداند
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// IEnumerable&lt;SelectListItem&gt; list = ModelHelper.GetValidations();
        /// </code>
        /// </example>
        /// <returns>IEnumerable&lt;SelectListItem&gt;</returns>
        public static List<SelectListItem> GetValidations()
        {
            return new List<SelectListItem>
            {
                new SelectListItem {Text = @"Required", Value = "Required"},
                new SelectListItem {Text = @"Length", Value = "Length"},
                new SelectListItem {Text = @"Rang", Value = "Rang"},
                new SelectListItem {Text = @"Email", Value = "Email"},
                new SelectListItem {Text = @"Date", Value = "Date"},
                new SelectListItem {Text = @"Date time", Value = "DateTime"},
                new SelectListItem {Text = @"Phone", Value = "Phone"},
                new SelectListItem {Text = @"National code", Value = "NationalCode"},
                new SelectListItem {Text = @"Postal code", Value = "PostalCode"},
                new SelectListItem {Text = @"Custom", Value = "Custom"}
            };
        }

        #endregion

    }
}
