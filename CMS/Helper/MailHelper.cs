﻿namespace CMS.Helper
{

    #region Assembly

    using Utility;
    using System;
    using System.Configuration;
    using System.IO;
    using System.Net;
    using System.Net.Mail;

    #endregion

    /// <summary>
    /// کلاس راهنمای صندوق پستی، شامل یکسری متدها برای ارسال ایمیل می باشد
    /// </summary>
    public class MailHelper
    {

        #region Attribute

        private readonly string _host;

        private readonly int _port;

        private readonly string _user;

        private readonly string _pass;

        private readonly bool _ssl;

        #endregion

        #region Property

        /// <summary>
        /// ایمیل فرستنده
        /// </summary>
        public string Sender { get; set; }

        /// <summary>
        /// ایمیل گیرنده
        /// </summary>
        public string Recipient { get; set; }

        /// <summary>
        /// ایمیل گیرنده کپی
        /// </summary>
        public string RecipientCc { get; set; }

        /// <summary>
        /// موضوع
        /// </summary>
        public string Subject { get; set; }

        /// <summary>
        /// پیام
        /// </summary>
        public string Body { get; set; }

        /// <summary>
        /// ضمائم
        /// </summary>
        public string AttachmentFile { get; set; }

        #endregion

        #region Method

        /// <summary>
        /// سازنده کلاس
        /// </summary>
        /// <remarks>
        /// اگر خصیصه های مربوط به سرور ارسال کننده ایمیل را تنظیم نکنید به صورت پیش فرض از تنظیمات ایمیل موجود در فایل web.config استفاده می کند.
        /// </remarks>
        public MailHelper()
        {
            var baseSite = ModuleHelper.GetBaseSite();
            if (baseSite != null)
            {
                _host = baseSite.SmtpHost.IsNotEmpty() ? baseSite.SmtpHost : ConfigurationManager.AppSettings["MailServer"];
                _port = baseSite.SmtpPort.IsNotEmpty() ? int.Parse(baseSite.SmtpPort) : int.Parse(ConfigurationManager.AppSettings["MailPort"]);
                _user = baseSite.SmtpUsername.IsNotEmpty() ? baseSite.SmtpUsername : ConfigurationManager.AppSettings["MailAuthUser"];
                _pass = baseSite.SmtpPassword.IsNotEmpty() ? baseSite.SmtpPassword : ConfigurationManager.AppSettings["MailAuthPass"];
                _ssl = Convert.ToBoolean(ConfigurationManager.AppSettings["EnableSSL"]);
            }
            else
            {
                _host = ConfigurationManager.AppSettings["MailServer"];
                _port = int.Parse(ConfigurationManager.AppSettings["MailPort"]);
                _user = ConfigurationManager.AppSettings["MailAuthUser"];
                _pass = ConfigurationManager.AppSettings["MailAuthPass"];
                _ssl = Convert.ToBoolean(ConfigurationManager.AppSettings["EnableSSL"]);
            }
        }

        /// <summary>
        /// از این متد برای ارسال پیام استفاده می شود
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// MailHelper mail = new MailHelper { Sender = "info@sajtech.ir", Recipient = "hasanjodat@gmail.com", Subject = "Message", body = "Salam" };
        /// bool isSend = MailHelper.Send();
        /// </code>
        /// </example>
        /// <returns>bool</returns>
        public bool Send()
        {
            var baseSite = ModuleHelper.GetBaseSite();
            if (baseSite != null)
            {
                // We do not catch the error here... let it pass direct to the caller
                Attachment att = null;
                var message = new MailMessage(baseSite.SmtpFrom.IsNotEmpty() ? baseSite.SmtpFrom : Sender, Recipient, Subject, Body) { IsBodyHtml = true };
                if (RecipientCc != null)
                {
                    message.Bcc.Add(RecipientCc);
                }
                if (baseSite.SmtpToList != null && baseSite.SmtpToList.Length > 0)
                {
                    foreach (var to in baseSite.SmtpToList)
                    {
                        message.Bcc.Add(to);
                    }
                }
                var smtp = new SmtpClient(_host, _port);

                if (!string.IsNullOrEmpty(AttachmentFile))
                {
                    if (File.Exists(AttachmentFile))
                    {
                        att = new Attachment(AttachmentFile);
                        message.Attachments.Add(att);
                    }
                }

                if (_user.Length > 0 && _pass.Length > 0)
                {
                    smtp.UseDefaultCredentials = false;
                    smtp.Credentials = new NetworkCredential(_user, _pass);
                    smtp.EnableSsl = _ssl;
                }

                smtp.Send(message);

                att?.Dispose();
                message.Dispose();
                smtp.Dispose();
                return true;
            }
            return false;
        }

        #endregion

    }
}
