﻿namespace CMS.Helper
{

    #region Assembly

    using System.Collections.Generic;
    using SessionHelper;
    using SessionHelper.Models;
    using ORM.Model;
    using Sitemap;
    using Utility;
    using DataService;
    using ORM.Condition;
    using System.Web;
    using ORM.Enums;
    using System.Security.Principal;

    #endregion

    /// <summary>
    /// در این کلاس متغیرهای مربوط به پرتال که در تمام پرتال استفاده می شود قرار دارد
    /// </summary>
    public static class CmsVariables
    {

        #region Attribute

        private static List<Site> _activeSite = new List<Site>();

        #endregion

        #region Variables

        /// <summary>
        /// سایت های فعال کاربر که براساس تنظیمات دامنه در دسترس هستند
        /// </summary>
        public static List<Site> ActiveSite
        {
            get
            {
                if (_activeSite != null)
                    return _activeSite;

                var das = DynamicService.Current;
                var host = HttpContext.Current.Request.ServerVariables["HTTP_HOST"];
                var criterias = new FilterExpressionList();
                var siteList = das.GetList<Site>(criterias);
                if (siteList != null)
                {
                    foreach (var site in siteList)
                    {
                        foreach (var domain in site.DomainsList)
                        {
                            if (domain.IsNotEmpty())
                            {
                                var domainPath = domain;
                                if (host.ToLower().Contains("localhost") && domainPath.Contains("localhost") && site.IsOnline == Toggle.True)
                                {
                                    ConfigureSite(site);
                                }
                                else if (domainPath.Equals(host) && site.IsOnline == Toggle.True)
                                {
                                    ConfigureSite(site);
                                }
                            }
                        }
                    }
                }
                return null;
            }
            set
            {
                _activeSite = value;
            }
        }

        /// <summary>
        /// سایت فعال که در حال نمایش به کاربر است
        /// </summary>
        public static Site RenderSite { get; set; }

        /// <summary>
        /// جلسه کاربر بعد از ورود به سیستم
        /// </summary>
        public static SessionProperty<GenericPrincipal> FormsAuthentication = Session.CreateProperty<GenericPrincipal>("FormsAuthentication");

        #endregion

        #region Method

        /// <summary>
        /// هر سایت برای در دسترس قرار گرفتن باید توسط این متد پیکربندی اولیه شود
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// CmsVariables.ConfigureSite(site);
        /// </code>
        /// </example>
        /// <param name="site">سایت مورد نظر: Site</param>
        public static void ConfigureSite(Site site)
        {
            if (site != null && !ActiveSite.Contains(site))
            {
                ActiveSite.Add(site);
                SitemapProvider.BindNodeFromDatabase(site.ItemId, site.SitePath);
            }
        }

        #endregion

    }
}