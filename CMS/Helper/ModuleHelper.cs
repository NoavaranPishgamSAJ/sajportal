﻿namespace CMS.Helper
{

    #region Assembly

    using System.Net;
    using System.Web;
    using Utility;
    using System;
    using System.Web.Mvc;
    using System.Linq;
    using ORM.Enums;
    using ORM.Model;

    #endregion

    /// <summary>
    /// کلاس راهنمای ماژول، شامل یکسری متدها برای استفاده در تعریف ماژول ها جهت سهولت کار می باشد
    /// </summary>
    public static class ModuleHelper
    {

        #region Method

        /// <summary>
        /// از این متد برای دریافت یک شناسه یکتا در کل پرتال استفاده می شود
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// string uniqueId = ModuleHelper.GetUniqueId();
        /// </code>
        /// </example>
        /// <returns>string</returns>
        public static string GetUniqueId()
        {
            return Guid.NewGuid().ToString().Replace("-", "");
        }

        /// <summary>
        /// بررسی موجود بودن یک پارامتر در پارامترهای مسیریابی
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// ViewContext.HasRouteDataKey("GroupName");
        /// </code>
        /// </example>
        /// <param name="context">این پارامتر از نوع صوری می باشد: System.Web.Mvc.ViewContext</param>
        /// <param name="key">کلید پارامتر: string</param>
        /// <returns>bool</returns>
        public static bool HasRouteDataKey(this ViewContext context, string key)
        {
            return context.RouteData.Values.ContainsKey(key);
        }

        /// <summary>
        /// دریافت یک پارامتر از پارامترهای مسیریابی
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// string item = ViewContext.GetRouteData&lt;string&gt;("GroupName");
        /// </code>
        /// </example>
        /// <param name="context">این پارامتر از نوع صوری می باشد: System.Web.Mvc.ViewContext</param>
        /// <param name="key">کلید پارامتر: string</param>
        /// <returns>GenericType</returns>
        public static T GetRouteData<T>(this ViewContext context, string key)
        {
            if (typeof(T) == typeof(bool))
                return (T)(Convert.ToBoolean((string)context.RouteData.Values[key]) as object);
            if (typeof(T) == typeof(short))
                return (T)(Convert.ToInt16((string)context.RouteData.Values[key]) as object);
            if (typeof(T) == typeof(int))
                return (T)(Convert.ToInt32((string)context.RouteData.Values[key]) as object);
            if (typeof(T) == typeof(long))
                return (T)(Convert.ToInt64((string)context.RouteData.Values[key]) as object);
            if (typeof(T) == typeof(float) || typeof(T) == typeof(double))
                return (T)(Convert.ToDouble((string)context.RouteData.Values[key]) as object);
            if (typeof(T) == typeof(decimal))
                return (T)(Convert.ToDecimal((string)context.RouteData.Values[key]) as object);
            if (typeof(T) == typeof(byte))
                return (T)(Convert.ToByte((string)context.RouteData.Values[key]) as object);
            return (T)context.RouteData.Values[key];
        }

        /// <summary>
        /// بررسی موجود بودن یک داده در داده های نما
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// ViewContext.HasViewDataKey("GroupName");
        /// </code>
        /// </example>
        /// <param name="context">این پارامتر از نوع صوری می باشد: System.Web.Mvc.ViewContext</param>
        /// <param name="key">کلید پارامتر: string</param>
        /// <returns>bool</returns>
        public static bool HasViewDataKey(this ViewContext context, string key)
        {
            return context.ViewData.ContainsKey(key);
        }

        /// <summary>
        /// دریافت یک داده از داده های نما
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// string item = ViewContext.GetViewData&lt;string&gt;("GroupName");
        /// </code>
        /// </example>
        /// <param name="context">این پارامتر از نوع صوری می باشد: System.Web.Mvc.ViewContext</param>
        /// <param name="key">کلید پارامتر: string</param>
        /// <returns>GenericType</returns>
        public static T GetViewData<T>(this ViewContext context, string key)
        {
            if (typeof(T) == typeof(bool))
                return (T)(Convert.ToBoolean((string)context.ViewData[key]) as object);
            if (typeof(T) == typeof(short))
                return (T)(Convert.ToInt16((string)context.ViewData[key]) as object);
            if (typeof(T) == typeof(int))
                return (T)(Convert.ToInt32((string)context.ViewData[key]) as object);
            if (typeof(T) == typeof(long))
                return (T)(Convert.ToInt64((string)context.ViewData[key]) as object);
            if (typeof(T) == typeof(float) || typeof(T) == typeof(double))
                return (T)(Convert.ToDouble((string)context.ViewData[key]) as object);
            if (typeof(T) == typeof(decimal))
                return (T)(Convert.ToDecimal((string)context.ViewData[key]) as object);
            if (typeof(T) == typeof(byte))
                return (T)(Convert.ToByte((string)context.ViewData[key]) as object);
            return (T)context.ViewData[key];
        }

        /// <summary>
        /// بررسی موجود بودن یک پارامتر در پارامترهای درخواست
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// Request.HasQueryStringKey("GroupName");
        /// </code>
        /// </example>
        /// <param name="context">این پارامتر از نوع صوری می باشد: System.Web.HttpRequestBase</param>
        /// <param name="key">کلید پارامتر: string</param>
        /// <returns>bool</returns>
        public static bool HasQueryStringKey(this HttpRequestBase context, string key)
        {
            return context.QueryString.AllKeys.Contains(key);
        }

        /// <summary>
        /// دریافت یک پارامتر از پارامترهای درخواست
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// string item = Request.GetQueryString&lt;string&gt;("GroupName");
        /// </code>
        /// </example>
        /// <param name="context">این پارامتر از نوع صوری می باشد: System.Web.HttpRequestBase</param>
        /// <param name="key">کلید پارامتر: string</param>
        /// <returns>GenericType</returns>
        public static string GetQueryString(this HttpRequestBase context, string key)
        {
            return context.QueryString[key];
        }

        /// <summary>
        /// بررسی موجود بودن یک پارامتر در پارامترهای فرم
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// Request.HasFormKey("GroupName");
        /// </code>
        /// </example>
        /// <param name="context">این پارامتر از نوع صوری می باشد: System.Web.HttpRequestBase</param>
        /// <param name="key">کلید پارامتر: string</param>
        /// <returns>bool</returns>
        public static bool HasFormKey(this HttpRequestBase context, string key)
        {
            return context.Form.AllKeys.Contains(key);
        }

        /// <summary>
        /// دریافت یک پارامتر از پارامترهای فرم
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// string item = Request.GetForm&lt;string&gt;("GroupName");
        /// </code>
        /// </example>
        /// <param name="context">این پارامتر از نوع صوری می باشد: System.Web.HttpRequestBase</param>
        /// <param name="key">کلید پارامتر: string</param>
        /// <returns>GenericType</returns>
        public static string GetForm(this HttpRequestBase context, string key)
        {
            return context.Form[key];
        }

        /// <summary>
        /// دریافت آی پی کاربر فعال
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// string ip = Request.GetUserIp([false]);
        /// </code>
        /// </example>
        /// <param name="context">این پارامتر از نوع صوری می باشد: System.Web.HttpRequestBase</param>
        /// <param name="getLan">مشخص کردن نوع شبکه: bool</param>
        /// <returns>string</returns>
        public static string GetUserIp(this HttpRequestBase context, bool getLan = false)
        {
            try
            {
                var visitorIpAddress = context.ServerVariables["HTTP_X_FORWARDED_FOR"];

                if (visitorIpAddress.IsNotEmpty())
                    visitorIpAddress = context.ServerVariables["REMOTE_ADDR"];

                if (visitorIpAddress.IsNotEmpty())
                    visitorIpAddress = context.UserHostAddress;

                if (visitorIpAddress != null && (visitorIpAddress.IsNotEmpty() || visitorIpAddress.Trim() == "::1"))
                {
                    getLan = true;
                    visitorIpAddress = string.Empty;
                }

                if (!getLan || !string.IsNullOrEmpty(visitorIpAddress))
                    return visitorIpAddress;

                //This is for Local(LAN) Connected ID Address
                var stringHostName = Dns.GetHostName();
                //Get Ip Host Entry
                var ipHostEntries = Dns.GetHostEntry(stringHostName);
                //Get Ip Address From The Ip Host Entry Address List
                var arrIpAddress = ipHostEntries.AddressList;

                try
                {
                    visitorIpAddress = arrIpAddress[arrIpAddress.Length - 2].ToString();
                }
                catch
                {
                    try
                    {
                        visitorIpAddress = arrIpAddress[0].ToString();
                    }
                    catch
                    {
                        try
                        {
                            arrIpAddress = Dns.GetHostAddresses(stringHostName);
                            visitorIpAddress = arrIpAddress[0].ToString();
                        }
                        catch
                        {
                            visitorIpAddress = "127.0.0.1";
                        }
                    }
                }
                return visitorIpAddress;
            }
            catch (Exception ex)
            {
                Logger.Log(ex);
            }
            return string.Empty;
        }

        /// <summary>
        /// دریافت سایت اصلی پرتال
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// Site baseSite = ModuleHelper.GetBaseSite();
        /// </code>
        /// </example>
        /// <returns>Site</returns>
        public static Site GetBaseSite()
        {
            return CmsVariables.RenderSite != null ? CmsVariables.RenderSite : CmsVariables.ActiveSite.FirstOrDefault(p => p.IsBase == Toggle.True);
        }

        #endregion

    }
}
