﻿namespace CMS.Helper
{

    #region Assembly

    using System.Web.Mvc;
    using Utility;
    using System;
    using System.Linq;

    #endregion

    /// <summary>
    /// کلاس راهنمای قالب، شامل یکسری متدها برای استفاده در تعریف قالب ها جهت سهولت کار می باشد
    /// </summary>
    public class LayoutHelper
    {

        #region Method

        /// <summary>
        /// از این متد برای دریافت بلاک های متا که در تعریف سایت گنجانده شده استفاده می شود
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// LayoutHelper.GetSiteTags();
        /// </code>
        /// </example>
        /// <returns>MvcHtmlString</returns>
        public static MvcHtmlString GetSiteTags()
        {
            var tags = string.Empty;
            var baseSite = ModuleHelper.GetBaseSite();
            if (baseSite != null && baseSite.Culture.IsNotEmpty())
            {
                if (baseSite.Author.IsNotEmpty())
                {
                    tags += $@"<meta name=""author"" content=""{baseSite.Author}"">" + Environment.NewLine;
                }
                if (baseSite.Keyword.IsNotEmpty())
                {
                    tags += $@"<meta name=""keywords"" content=""{baseSite.Keyword }"">" + Environment.NewLine;
                }
                if (baseSite.Description.IsNotEmpty())
                {
                    tags += $@"<meta name=""description"" content=""{baseSite.Description }"">" + Environment.NewLine;
                }
                if (baseSite.HtmlMetaBlockList != null && baseSite.HtmlMetaBlockList.Length > 0)
                {
                    tags = baseSite.HtmlMetaBlockList.Aggregate(tags, (current, meta) => current + meta + Environment.NewLine);
                }
            }
            return new MvcHtmlString(tags);
        }

        #endregion

    }
}
