﻿namespace CMS.Configuration
{

    #region Assembly

    using System.Configuration;

    #endregion

    /// <summary>
    /// این کلاس برای تعریف بخش تنظیمات بیدار نگه داشتن پرتال در فایل web.config استفاده می شود.
    /// </summary>
    public class UpTimeConfigurationSection : ConfigurationSection
    {

        #region Property

        /// <summary>
        /// آدرس های مورد نظر جهت بیدار نگه داشتن
        /// </summary>
        [ConfigurationProperty("Urls", IsDefaultCollection = false)]
        [ConfigurationCollection(typeof(UrlCollection), AddItemName = "add", ClearItemsName = "clear", RemoveItemName = "remove")]
        public UrlCollection Urls => (UrlCollection)base["Urls"];

        #endregion

    }
}