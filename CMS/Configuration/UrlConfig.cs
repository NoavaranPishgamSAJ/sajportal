﻿namespace CMS.Configuration
{

    #region Assembly

    using System.Configuration;

    #endregion

    /// <summary>
    /// این کلاس برای تعریف عناصر مخصوص بیدار نگه داشتن پرتال در فایل web.config استفاده می شود.
    /// </summary>
    public class UrlConfig : ConfigurationElement
    {

        #region Property

        /// <summary>
        /// مسیر مورد نظر جهت بیدار نگه داشتن
        /// </summary>
        [ConfigurationProperty("Path", DefaultValue = "", IsRequired = true, IsKey = true)]
        public string Path
        {
            get { return (string)this["Path"]; }
            set { this["Path"] = value; }
        }

        #endregion

        #region Method

        /// <summary>
        /// سازنده کلاس
        /// </summary>
        public UrlConfig() { }

        /// <summary>
        /// سازنده کلاس همراه با مقدار دهی اولیه
        /// </summary>
        public UrlConfig(string path)
        {
            Path = path;
        }

        #endregion

    }
}
