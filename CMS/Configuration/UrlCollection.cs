﻿namespace CMS.Configuration
{

    #region Assembly

    using System.Configuration;

    #endregion

    /// <summary>
    /// این کلاس برای تعریف مجموعه عناصر مخصوص بیدار نگه داشتن پرتال در فایل web.config استفاده می شود.
    /// </summary>
    public class UrlCollection : ConfigurationElementCollection
    {

        #region Property

        /// <summary>
        /// دسترسی به عناصر مجموعه از طریق اندیس
        /// </summary>
        public UrlConfig this[int index]
        {
            get { return (UrlConfig)BaseGet(index); }
            set
            {
                if (BaseGet(index) != null)
                {
                    BaseRemoveAt(index);
                }
                BaseAdd(index, value);
            }
        }

        #endregion

        #region Method

        /// <summary>
        /// اضافه کردن عنصر به مجموعه
        /// </summary>
        public void Add(UrlConfig urlConfig)
        {
            BaseAdd(urlConfig);
        }

        /// <summary>
        /// خالی کردن مجموعه
        /// </summary>
        public void Clear()
        {
            BaseClear();
        }

        /// <summary>
        /// ایجاد یک عنصر جدید
        /// </summary>
        protected override ConfigurationElement CreateNewElement()
        {
            return new UrlConfig();
        }

        /// <summary>
        /// دریاقت کلید دسترسی به عنصر بر اساس موجودیت عنصر
        /// </summary>
        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((UrlConfig)element).Path;
        }

        /// <summary>
        /// حذف عنصر از مجموعه بر اساس خود مجودیت
        /// </summary>
        public void Remove(UrlConfig serviceConfig)
        {
            BaseRemove(serviceConfig.Path);
        }

        /// <summary>
        /// حذف عنصر از مجموعه بر اساس اندیس آن
        /// </summary>
        public void RemoveAt(int index)
        {
            BaseRemoveAt(index);
        }

        /// <summary>
        /// حذف عنصر از مجموعه بر اساس نام آن
        /// </summary>
        public void Remove(string name)
        {
            BaseRemove(name);
        }

        #endregion

    }
}
