﻿namespace CMS.Sitemap
{

    #region Assembly

    using Newtonsoft.Json;
    using ORM.Enums;
    using System.Collections.Generic;
    using Utility;

    #endregion

    /// <summary>
    /// از این کلاس برای ساخت درخت منو پرتال استفاده می شود
    /// </summary>
    public class SitemapNode
    {

        #region Property

        /// <summary>
        /// کلید منو
        /// </summary>
        public int Key { get; set; }

        /// <summary>
        /// مسیر نسبی منو
        /// </summary>
        public string VirtualPath { get; set; }

        /// <summary>
        /// مسیر نسبی منو برای دسترسی در بخش Render نما
        /// </summary>
        public string ViewPath
        {
            get
            {
                if (VirtualPath[0].Equals('/'))
                    return VirtualPath.Substring(1, VirtualPath.Length - 1).ToLower();
                return VirtualPath.ToLower();
            }
        }

        /// <summary>
        /// قالب صفحه
        /// </summary>
        public string Template { get; set; }

        /// <summary>
        /// عنوان صفحه
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// عنوان منو
        /// </summary>
        public string MenuTitle { get; set; }

        /// <summary>
        /// نام پارامترهای مسیریابی در قالب JSON
        /// </summary>
        public string RouteFields { get; set; }

        /// <summary>
        /// مقدار پارامترهای مسیریابی در قالب JSON
        /// </summary>
        public string RouteFieldsValue { get; set; }

        /// <summary>
        /// نام پارامترهای مسیریابی
        /// </summary>
        public List<string> RouteFieldsList
        {
            get { return RouteFields.IsNotEmpty() && RouteFields.Length > 4 ? JsonConvert.DeserializeObject<List<string>>(RouteFields) : new List<string>(); }
            set { RouteFields = JsonConvert.SerializeObject(value); }
        }

        /// <summary>
        /// مقدار پارامترهای مسیریابی
        /// </summary>
        public List<string> RouteFieldsValueList
        {
            get { return RouteFieldsValue.IsNotEmpty() && RouteFieldsValue.Length > 4 ? JsonConvert.DeserializeObject<List<string>>(RouteFieldsValue) : new List<string>(); }
            set { RouteFieldsValue = JsonConvert.SerializeObject(value); }
        }

        /// <summary>
        /// نام پارامترهای صفحه در قالب JSON
        /// </summary>
        public string Parameters { get; set; }

        /// <summary>
        /// مقدار پارامترهای صفحه در قالب JSON
        /// </summary>
        public string ParametersValue { get; set; }

        /// <summary>
        /// نام پارامترهای صفحه
        /// </summary>
        public List<string> ParametersList
        {
            get { return Parameters.IsNotEmpty() && Parameters.Length > 4 ? JsonConvert.DeserializeObject<List<string>>(Parameters) : new List<string>(); }
            set { Parameters = JsonConvert.SerializeObject(value); }
        }

        /// <summary>
        /// مقدار پارامترهای صفحه
        /// </summary>
        public List<string> ParametersValueList
        {
            get { return ParametersValue.IsNotEmpty() && ParametersValue.Length > 4 ? JsonConvert.DeserializeObject<List<string>>(ParametersValue) : new List<string>(); }
            set { ParametersValue = JsonConvert.SerializeObject(value); }
        }

        /// <summary>
        /// نام رویداد
        /// </summary>
        public string Action { get; set; }

        /// <summary>
        /// نمایش در منو
        /// </summary>
        public bool ShowInMenu { get; set; }

        /// <summary>
        /// نمایش در ردپا
        /// </summary>
        public bool ShowInBreadcrumb { get; set; }

        /// <summary>
        /// شناسه والد
        /// </summary>
        public int ParentId { get; set; }

        /// <summary>
        /// نوع صفحه
        /// </summary>
        public PageTypes PageType { get; set; }

        /// <summary>
        /// نام مجوز برای دستیابی به صفحه
        /// </summary>
        public string RoleName { get; set; }

        /// <summary>
        /// شناسه سایت
        /// </summary>
        public int SiteId { get; set; }

        /// <summary>
        /// مسیر اضافی صفحه
        /// </summary>
        public string SitePath { get; set; }

        #endregion

    }
}
