﻿namespace CMS.Sitemap
{

    #region Assembly

    using DataService;
    using ORM.Model;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text.RegularExpressions;

    #endregion

    public static class RouteProvider
    {

        #region Attribute

        public static List<Route> Routes;

        #endregion

        #region Method

        public static void BindRouteFromDatabase()
        {
            Routes = new DynamicService().GetList<Route>(null, "Priority ASC").ToList();
        }

        public static void AddRoute(Route route)
        {
            if (Routes != null && !Routes.Contains(route))
                Routes.Add(route);
        }

        public static Route GetRouteByName(string name)
        {
            if (Routes != null && Routes.Count > 0)
            {
                return Routes.SingleOrDefault(r => r.Name.Equals(name));
            }
            return null;
        }

        public static Route GetRouteByUrl(string url)
        {
            if (Routes != null && Routes.Count > 0)
            {
                return Routes.SingleOrDefault(r => r.Url.Equals(url));
            }
            return null;
        }

        public static string GetRouteUrl(string request)
        {
            foreach (var route in Routes)
            {
                var regex = new Regex(route.Url);
                var match = regex.Match(request);
                if (match.Success)
                {
                    var url = string.Empty;
                    if (route.ParameterCount > 0)
                    {
                        for (var i = 1; i < match.Groups.Count && i < route.ParameterCount + 1; i++)
                        {
                            var key = regex.GroupNameFromNumber(i);
                            var group = match.Groups[i];
                            if (!string.IsNullOrEmpty(key))
                            {
                                url += @group.Value;
                            }
                        }
                    }
                    else
                        url = @match.Groups[0].Value;
                    return url;
                }
            }
            return string.Empty;
        }

        #endregion

    }
}
