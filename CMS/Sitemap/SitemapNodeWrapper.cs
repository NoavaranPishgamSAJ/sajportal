﻿namespace CMS.Sitemap
{

    #region Assembly

    using System.Collections.Generic;

    #endregion

    /// <summary>
    /// از این کلاس برای نگه داشتن لیست تمامی منوهای پرتال استفاده می شود
    /// </summary>
    public class SitemapNodeWrapper : SitemapNode
    {

        #region Property

        /// <summary>
        /// منوی ریشه سایت که فقط نقش دستگیره دسترسی به منو های اصلی را دارد
        /// </summary>
        public SitemapNodeWrapper Parent;

        /// <summary>
        /// لیست منوهای پرتال
        /// </summary>
        public List<SitemapNodeWrapper> Children;

        #endregion

        #region Method

        /// <summary>
        /// سازنده کلاس
        /// </summary>
        public SitemapNodeWrapper()
        {
            Children = new List<SitemapNodeWrapper>();
        }

        #endregion

    }
}
