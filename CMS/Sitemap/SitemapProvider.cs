﻿namespace CMS.Sitemap
{

    #region Assembly

    using DataService;
    using ORM.Model;
    using System.Collections.Generic;
    using System.Linq;
    using ORM.Enums;
    using ORM.Condition;
    using Utility;

    #endregion

    /// <summary>
    /// از این کلاس برای انجام عملیات ساخت و دسترسی به منو های پرتال استفاده می شود
    /// </summary>
    public static class SitemapProvider
    {

        #region Attribute

        /// <summary>
        /// منوی ریشه پرتال
        /// </summary>
        public static SitemapNodeWrapper Root = new SitemapNodeWrapper { Key = 0, VirtualPath = string.Empty };

        /// <summary>
        /// لیست صفحات پرتال
        /// </summary>
        public static List<Page> Pages = new List<Page>();

        #endregion

        #region Method

        /// <summary>
        /// از این متد برای ساخت منوهای پرتال از دیتابیس استفاده می شود
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// SitemapProvider.BindNodeFromDatabase(1, {"fa"});
        /// </code>
        /// </example>
        /// <param name="siteId">شناسه سایت: int</param>
        /// <param name="sitePath">مسیر اضافی سایت: string</param>
        public static void BindNodeFromDatabase(int siteId, string sitePath)
        {
            var criterias = new FilterExpressionList();
            criterias.Add(new Filter("SiteId", SqlOperators.Equal, siteId));
            var pages = DynamicService.Current.GetList<Page>(criterias).ToList();
            Pages.AddRange(pages);
            foreach (var page in pages)
                AddNode(new SitemapNodeWrapper
                {
                    Key = page.ItemId,
                    VirtualPath = (sitePath.IsNotEmpty() ? sitePath : string.Empty) + page.VirtualPath.ToLower(),
                    Template = page.Template,
                    Title = page.Title,
                    MenuTitle = page.MenuTitle,
                    RouteFields = page.RouteFields,
                    Parameters = page.Parameters,
                    ShowInMenu = page.ShowInMenu.GetValue(),
                    ShowInBreadcrumb = page.ShowInBreadcrumb.GetValue(),
                    PageType = page.PageType,
                    ParentId = page.ParentId ?? 0,
                    RoleName = page.RoleName,
                    SiteId = page.SiteId,
                    SitePath = sitePath
                });
        }

        /// <summary>
        /// از این متد برای اضافه کردن یک منو به لیست منو ها استفاده می شود
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// SitemapProvider.AddNode(node);
        /// </code>
        /// </example>
        /// <param name="node">منوی مورد نظر: SitemapNodeWrapper</param>
        public static void AddNode(SitemapNodeWrapper node)
        {
            if (node.ParentId != 0)
            {
                var parent = GetNodeByKey(node.ParentId, Root);
                if (parent != null &&
                    parent.Children.Count(p => p.Key.Equals(node.Key) && p.SiteId.Equals(node.SiteId)) <= 0 &&
                    parent.Children.Count(p => p.VirtualPath.Equals(node.VirtualPath) && p.SiteId.Equals(node.SiteId)) <= 0 &&
                    !parent.Children.Contains(node) && GetNodeByKey(node.Key, parent) == null)
                {
                    node.Parent = parent;
                    parent.Children.Add(node);
                }
            }
            else if (Root != null &&
                    Root.Children.Count(p => p.Key.Equals(node.Key) && p.SiteId.Equals(node.SiteId)) <= 0 &&
                    Root.Children.Count(p => p.VirtualPath.Equals(node.VirtualPath) && p.SiteId.Equals(node.SiteId)) <= 0 &&
                    !Root.Children.Contains(node) && GetNodeByKey(node.Key, Root) == null)
            {
                Root.Children.Add(node);
            }
        }

        /// <summary>
        /// از این متد برای پیدا کردن منویی خاص در لیست منو ها استفاده می شود که بصورت تابع بازگشی عمل می کند
        /// </summary>
        /// <remarks>
        /// این متد براساس شناسه منو جستجو می کند
        /// </remarks>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// SitemapNodeWrapper targetNode = GetNodeByKey(2, node);
        /// </code>
        /// </example>
        /// <param name="id">شناسه منو: int</param>
        /// <param name="node">منو مورد هدف برای جستجو منوی مورد نظر در آن: SitemapNodeWrapper</param>
        /// <returns>SitemapNodeWrapper</returns>
        private static SitemapNodeWrapper GetNodeByKey(int id, SitemapNodeWrapper node)
        {
            if (node.Key.Equals(id))
                return node;
            return node.Children.Count > 0 ? node.Children.Select(child => GetNodeByKey(id, child)).FirstOrDefault(r => r != null) : null;
        }

        /// <summary>
        /// از این متد برای پیدا کردن منویی خاص در لیست منو ها استفاده می شود که بصورت تابع بازگشی عمل می کند
        /// </summary>
        /// <remarks>
        /// این متد براساس مسیر منو جستجو می کند
        /// </remarks>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// SitemapNodeWrapper targetNode = GetNodeByKey(1, "Home", node);
        /// </code>
        /// </example>
        /// <param name="siteId">شناسه سایت: int</param>
        /// <param name="path">مسیر منو مورد نظر: string</param>
        /// <param name="node">منو مورد هدف برای جستجو منوی مورد نظر در آن: SitemapNodeWrapper</param>
        /// <returns>SitemapNodeWrapper</returns>
        private static SitemapNodeWrapper GetNodeByPath(int siteId, string path, SitemapNodeWrapper node)
        {
            var virtualPath = node.VirtualPath;
            if (node.SitePath.IsNotEmpty())
            {
                virtualPath = virtualPath.Substring(node.SitePath.Length);
            }
            if (node.SiteId.Equals(siteId) && virtualPath.ToLower().Equals(path.ToLower()))
                return node;
            return node.Children.Count > 0 ? node.Children.Select(child => GetNodeByPath(siteId, path, child)).FirstOrDefault(r => r != null) : null;
        }

        /// <summary>
        /// از این متد برای پیدا کردن منویی خاص در لیست منو ها استفاده می شود
        /// </summary>
        /// <remarks>
        /// این متد براساس شناسه منو جستجو می کند
        /// </remarks>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// SitemapNodeWrapper targetNode = GetNodeByKey(2);
        /// </code>
        /// </example>
        /// <param name="id">شناسه منو: int</param>
        /// <returns>SitemapNodeWrapper</returns>
        public static SitemapNodeWrapper GetNodeByKey(int id)
        {
            return GetNodeByKey(id, Root);
        }

        /// <summary>
        /// از این متد برای پیدا کردن منویی خاص در لیست منو ها استفاده می شود
        /// </summary>
        /// <remarks>
        /// این متد براساس مسیر منو جستجو می کند
        /// </remarks>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// SitemapNodeWrapper targetNode = GetNodeByPath(1, "Home");
        /// </code>
        /// </example>
        /// <param name="siteId">شناسه سایت: int</param>
        /// <param name="path">مسیر منو مورد نظر: string</param>
        /// <returns>SitemapNodeWrapper</returns>
        public static SitemapNodeWrapper GetNodeByPath(int siteId, string path)
        {
            return GetNodeByPath(siteId, path, Root);
        }

        #endregion

    }
}
