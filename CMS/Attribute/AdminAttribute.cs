﻿namespace CMS.Attribute
{

    #region Assembly

    using System.Web.Mvc;
    using Utility;
    using System;

    #endregion

    /// <summary>
    /// از این کلاس در تعریف رویداد های کنترل کننده های نماها برای بررسی راهبر بودن کاربر استفاده می شود
    /// </summary>
    public class AdminAttribute : ActionFilterAttribute
    {

        #region Method

        /// <summary>
        /// این متد قبل از اجرای رویداد اجرا می شود و بررسی می کند که آیا کاربر دسترسی راهبر را دارد یا خیر و درصورتی که دارای این دسترسی نباشد کاربر را به صفحه عدم دسترسی هدایت می کند
        /// </summary>
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            try
            {
                if (!filterContext.HttpContext.User.IsInRole("IsAdmin"))
                {
                    filterContext.HttpContext.Response.Redirect(Uri.EscapeUriString("~/error/1001"));
                }
                base.OnActionExecuting(filterContext);
            }
            catch (Exception ex)
            {
                Logger.Log(ex);
                filterContext.HttpContext.Response.Redirect(Uri.EscapeUriString("~/error/1001"));
            }
        }

        #endregion

    }
}
