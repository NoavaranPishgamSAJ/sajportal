﻿namespace CMS.Attribute
{

    #region Assembly

    using System;
    using System.Web.Mvc;
    using Utility;

    #endregion

    /// <summary>
    /// از این کلاس در تعریف رویداد های کنترل کننده های نماها برای بررسی دارا بودن مجوزهای مورد نظر توسط کاربر برای اجرای رویداد مورد نظر استفاده می شود
    /// </summary>
    public class DapperAuthorizeAttribute : ActionFilterAttribute
    {

        #region Attribute

        /// <summary>
        /// لیست مجوزهای موردنظر
        /// </summary>
        public string Roles { get; set; }

        #endregion

        #region Method

        /// <summary>
        /// این متد قبل از اجرای رویداد اجرا می شود و بررسی می کند که آیا کاربر مجوزهای مورد نظر را دارد یا خیر و درصورتی که دارای این مجوزها نباشد کاربر را به صفحه عدم دسترسی هدایت می کند
        /// </summary>
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            try
            {
                if (filterContext.HttpContext.User.IsInRole("IsAdmin"))
                    base.OnActionExecuting(filterContext);
                foreach (var role in Roles.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
                {
                    if (!filterContext.HttpContext.User.IsInRole(role))
                    {
                        filterContext.HttpContext.Response.Redirect(Uri.EscapeUriString("~/error/1001"));
                    }
                }
                base.OnActionExecuting(filterContext);
            }
            catch (Exception ex)
            {
                Logger.Log(ex);
                filterContext.HttpContext.Response.Redirect(Uri.EscapeUriString("~/error/1001"));
            }
        }

        #endregion

    }
}
