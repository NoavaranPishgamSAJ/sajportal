﻿namespace CMS.Attribute
{

    #region Assembly

    using System.Web.Mvc;
    using Helper;
    using Utility;
    using System;

    #endregion

    /// <summary>
    /// از این کلاس در تعریف رویداد های کنترل کننده های نماها برای ورود کردن کاربر به سیستم استفاده می شود
    /// </summary>
    public class LoggedOrAuthorizedAttribute : AuthorizeAttribute
    {

        #region Method

        /// <summary>
        /// این متد قبل از اجرای رویداد اجرا می شود و بررسی می کند که آیا کاربر به سیستم ورود کرده یا خیر و درصورتی که عدم ورود کاربر را به صفحه عدم ورود به سیستم هدایت می کند
        /// </summary>
        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            try
            {
                if (CmsVariables.FormsAuthentication.Get() != null && CmsVariables.FormsAuthentication.Get().Identity.Name.IsNotEmpty())
                {
                    filterContext.HttpContext.User = CmsVariables.FormsAuthentication.Get();
                }
                // اگر مقدار result تهی باشد یعنی کاربر به سیستم ورود کرده و معتبر است
                if (filterContext.Result == null)
                    return;

                // اگر به این خط رسیده اید یعنی شما وارد وضعیت HTTP_401 شده اید.
                // به خصوص که filterContext.Result از نوع HttpUnauthorizedResult باشد.
                // اینجا درخواست های آزاکس را بررسی کنید
                if (!filterContext.HttpContext.User.Identity.IsAuthenticated)
                {
                    filterContext.HttpContext.Response.Redirect(Uri.EscapeUriString("~/error/1002"));
                }
            }
            catch (Exception ex)
            {
                Logger.Log(ex);
            }
        }

        #endregion

    }
}
