﻿namespace CMS.Extension
{

    #region Assembly

    using System;
    using Helper;

    #endregion

    /// <summary>
    /// از این کلاس برای توسعه کلاس تاریخ دات نت استفاده می شود
    /// </summary>
    public static class DateTimeExtensions
    {

        #region Method

        /// <summary>
        /// از این متد برای تبدیل تاریخ استاندارد به تاریخ محلی استفاده می شود
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// DateTime localTime = DateTime.Now.UtcToLocal();
        /// </code>
        /// </example>
        /// <param name="source">موجودیت اول از نوع صوری می باشد: System.DateTime</param>
        /// <returns>System.DateTime</returns>
        public static DateTime UtcToLocal(this DateTime source)
        {
            var baseSite = ModuleHelper.GetBaseSite();
            if (baseSite != null)
            {
                var localTimeZone = TimeZoneInfo.FindSystemTimeZoneById(baseSite.TimeZoneId);
                return TimeZoneInfo.ConvertTimeFromUtc(source, localTimeZone);
            }
            return TimeZoneInfo.ConvertTimeFromUtc(source, TimeZoneInfo.Local);
        }

        /// <summary>
        /// از این متد برای تبدیل تاریخ محلی به تاریخ استاندارد استفاده می شود
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// DateTime utcTime = DateTime.Now.LocalToUtc();
        /// </code>
        /// </example>
        /// <param name="source">موجودیت اول از نوع صوری می باشد: System.DateTime</param>
        /// <returns>System.DateTime</returns>
        public static DateTime LocalToUtc(this DateTime source)
        {
            var baseSite = ModuleHelper.GetBaseSite();
            if (baseSite != null)
            {
                var localTimeZone = TimeZoneInfo.FindSystemTimeZoneById(baseSite.TimeZoneId);
                source = DateTime.SpecifyKind(source, DateTimeKind.Unspecified);
                return TimeZoneInfo.ConvertTimeToUtc(source, localTimeZone);
            }
            return TimeZoneInfo.ConvertTimeToUtc(source, TimeZoneInfo.Utc);
        }

        #endregion

    }
}
