﻿namespace CMS.Extension
{

    #region Assembly

    using System.Web;
    using System.Web.SessionState;

    #endregion

    /// <summary>
    /// از این کلاس برای توسعه کلاس جلسه دات نت استفاده می شود
    /// </summary>
    public static class SessionExtensions
    {

        #region Method

        /// <summary>
        /// از این متد برای اضافه کردن مقداری به جلسه استفاده می شود
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// Session.AddItem&lt;string&gt;("Hello", "Message");
        /// </code>
        /// </example>
        /// <param name="session">موجودیت اول از نوع صوری می باشد: System.Web.HttpSessionStateBase</param>
        /// <param name="item">مقدار مورد نظر جهت اضافه کردن به جلسه: GenericType</param>
        /// <param name="key">کلید جهت دسترسی به مقدار: string</param>
        public static void AddItem<T>(this HttpSessionStateBase session, T item, string key)
        {
            session[key] = item;
        }

        /// <summary>
        /// از این متد برای دریافت مقداری از جلسه استفاده می شود
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// string message = Session.GetItem&lt;string&gt;("Message");
        /// </code>
        /// </example>
        /// <param name="session">موجودیت اول از نوع صوری می باشد: System.Web.HttpSessionStateBase</param>
        /// <param name="key">کلید جهت دسترسی به مقدار: string</param>
        /// <returns>GenericType</returns>
        public static T GetItem<T>(this HttpSessionStateBase session, string key)
        {
            return (T)session[key];
        }

        /// <summary>
        /// از این متد برای حذف مقداری از جلسه استفاده می شود
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// Session.ClearItem("Message");
        /// </code>
        /// </example>
        /// <param name="session">موجودیت اول از نوع صوری می باشد: System.Web.HttpSessionStateBase</param>
        /// <param name="key">کلید جهت دسترسی به مقدار: string</param>
        public static void ClearItem(this HttpSessionStateBase session, string key)
        {
            session.Remove(key);
        }

        /// <summary>
        /// از این متد برای اضافه کردن مقداری به جلسه استفاده می شود
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// Session.AddItem&lt;string&gt;("Hello", "Message");
        /// </code>
        /// </example>
        /// <param name="session">موجودیت اول از نوع صوری می باشد: System.Web.HttpSessionState</param>
        /// <param name="item">مقدار مورد نظر جهت اضافه کردن به جلسه: GenericType</param>
        /// <param name="key">کلید جهت دسترسی به مقدار: string</param>
        public static void AddItem<T>(this HttpSessionState session, T item, string key)
        {
            session[key] = item;
        }

        /// <summary>
        /// از این متد برای دریافت مقداری از جلسه استفاده می شود
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// string message = Session.GetItem&lt;string&gt;("Message");
        /// </code>
        /// </example>
        /// <param name="session">موجودیت اول از نوع صوری می باشد: System.Web.HttpSessionState</param>
        /// <param name="key">کلید جهت دسترسی به مقدار: string</param>
        /// <returns>GenericType</returns>
        public static T GetItem<T>(this HttpSessionState session, string key)
        {
            return (T)session[key];
        }

        /// <summary>
        /// از این متد برای حذف مقداری از جلسه استفاده می شود
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// Session.ClearItem("Message");
        /// </code>
        /// </example>
        /// <param name="session">موجودیت اول از نوع صوری می باشد: System.Web.HttpSessionState</param>
        /// <param name="key">کلید جهت دسترسی به مقدار: string</param>
        public static void ClearItem(this HttpSessionState session, string key)
        {
            session.Remove(key);
        }

        #endregion

    }
}
