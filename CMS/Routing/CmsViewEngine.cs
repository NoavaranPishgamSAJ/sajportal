﻿namespace CMS.Routing
{

    #region Assembly

    using System;
    using Sitemap;
    using System.Linq;
    using System.Web.Mvc;

    #endregion

    /// <summary>
    /// کلاس موتور پردازش  نماها
    /// </summary>
    public class CmsViewEngine : RazorViewEngine
    {

        #region Method

        /// <summary>
        /// این متد بعد از هر درخواست تولید صفحه اجرا می شود و قالب هر صفحه را تنظیم می کند
        /// </summary>
        /// <param name="controllerContext">مخزن کنترل کننده: System.Web.Mvc.ControllerContext</param>
        /// <param name="viewPath">مسیر نما: string</param>
        /// <param name="masterPath">مسیر قالب نما: string</param>
        /// <returns>IView</returns>
        protected override IView CreateView(ControllerContext controllerContext, string viewPath, string masterPath)
        {
            // اگر نمای مورد نظر یک صفحه معتبر پرتال باشد
            if (controllerContext.RouteData.DataTokens.ContainsKey("Url"))
            {
                var node = SitemapProvider.GetNodeByPath(Convert.ToInt32(controllerContext.RouteData.DataTokens.Single(p => p.Key.Equals("SiteId")).Value), controllerContext.RouteData.DataTokens.Single(p => p.Key.Equals("Url")).Value.ToString());
                if (node != null)
                    masterPath = node.Template;
            }
            return base.CreateView(controllerContext, viewPath, masterPath);
        }

        #endregion

    }
}