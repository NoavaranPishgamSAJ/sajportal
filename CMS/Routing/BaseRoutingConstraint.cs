﻿namespace CMS.Routing
{

    #region Assembly

    using System.Web;
    using System.Web.Routing;
    using Sitemap;
    using System.Collections.Generic;
    using System.Text.RegularExpressions;
    using Utility;
    using Helper;
    using ORM.Model;

    #endregion

    /// <summary>
    /// از این کلاس برای بررسی موجود بودن مسیر درخواستی کاربر به عنوان صفحه ای از پرتال استفاده می شود
    /// </summary>
    public class BaseRoutingConstraint : IRouteConstraint
    {

        #region Method

        /// <summary>
        /// این متد بررسی می کند که آیا آدرس وارد شده برای یک صفحه در پرتال هست یا نه
        /// </summary>
        /// <remarks>
        /// اگر آدرس معتبر باشد مقدار True و گرنه مقدار False برمی گرداند
        /// </remarks>
        /// <param name="httpContext">موجودیتی که اطلاعات مربوط به درخواست HTTP را در خود نگه می دارد: System.Web.HttpContextBase</param>
        /// <param name="route">موجودیتی مسیریابی که این درخواست به آن تعلق دارد: System.Web.Routing.Route</param>
        /// <param name="parameterName">نام پارامتر آدرس مسیریابی: string</param>
        /// <param name="values">مقدار آدرس های URL: System.Web.Routing.RouteValueDictionary</param>
        /// <param name="routeDirection">این پارامتر بررسی می کند که آیا درخواست، یک درخواست ورودی است یا یک درخواست بعد از دسترسی به صفحه: System.Web.Routing.RouteDirection</param>
        /// <returns>bool</returns>
        public bool Match(HttpContextBase httpContext, Route route, string parameterName, RouteValueDictionary values, RouteDirection routeDirection)
        {
            // بررسی موجود بودن پارامتر مسیریابی در مقادیر آدرس
            if (values[parameterName] == null)
                return false;

            var permalink = $"{values[parameterName]}";
            Dictionary<string, string> page = null;
            Site activeSite = null;
            // بررسی موجود بودن سایت فعال جهت جستجو در صفحات آن
            if (CmsVariables.ActiveSite != null && CmsVariables.ActiveSite.Count > 0)
            {
                foreach (var site in CmsVariables.ActiveSite)
                {
                    // اگر سایت دارای مسیر اضافی بعد از نامه دامنه باشد
                    // این مورد بیشتر در چند زبانه سازی سایت استفاده می شود. مثل: sajtech.ir[/fa]
                    if (site.SitePath.IsNotEmpty())
                    {
                        var pattern = "^(" + site.SitePath + "{1})/(.*)$";
                        var rgx = new Regex(pattern, RegexOptions.IgnoreCase);
                        var matches = rgx.Matches(permalink);
                        if (matches.Count > 0)
                        {
                            foreach (Match match in matches)
                            {
                                // بررسی موجود بودن صفحه درخواستی
                                page = RouteProvider.GetRouteUrl(site.ItemId, match.Groups[2].Value, httpContext.Request);
                                activeSite = site;
                            }
                        }
                    }
                    else
                    {
                        // بررسی موجود بودن صفحه درخواستی
                        page = RouteProvider.GetRouteUrl(site.ItemId, permalink, httpContext.Request);
                        activeSite = site;
                    }
                }
                if (page == null)
                    return false;

                // تنظیم کردن پارامترهای مورد نظر پرتال برای هر صفحه در قسمت پارامترهای مسیریابی صفحه
                var node = SitemapProvider.GetNodeByPath(activeSite.ItemId, page["Url"]);
                foreach (var param in page.Keys)
                {
                    route.DataTokens[param] = page[param];
                }
                // تععین سایت فعال جهت نمایش به کاربر براساس آدرس درخواستی کاربر
                if (node != null)
                    CmsVariables.RenderSite = activeSite;
                return node != null;
            }
            return false;
        }

        #endregion

    }

}