﻿namespace CMS.Routing
{

    #region Assembly

    using System.Web.Mvc;
    using System.IO;
    using System.Web.Routing;
    using System;
    using System.Web;
    using Sitemap;
    using System.Linq;
    using DataService;
    using ORM.Condition;
    using ORM.Model;
    using ORM.Enums;
    using Utility;

    #endregion

    /// <summary>
    /// از این کلاس برای نمایش محتوای صفحات پرتال استفاده می شود
    /// </summary>
    /// <typeparam name="TModel"></typeparam>
    public abstract class CmsViewPage<TModel> : WebViewPage<TModel>
    {

        #region Method

        /// <summary>
        /// این متد کار نمایش محتوای ناحیه های صفحات را به عهده دارد
        /// </summary>
        /// <param name="zone">نام ناحیه: string</param>
        /// <returns>MvcHtmlString</returns>
        public MvcHtmlString RenderZone(string zone)
        {
            // دریافت اطلاعات صفحه
            var node = SitemapProvider.GetNodeByPath(Convert.ToInt32(Request.RequestContext.RouteData.DataTokens.Single(p => p.Key.Equals("SiteId")).Value), Request.RequestContext.RouteData.DataTokens.Single(p => p.Key.Equals("Url")).Value.ToString());
            // دریافت ناحیه های صفحه
            var filters = new FilterExpressionList(new[] { new ORM.Condition.Filter("PageId", SqlOperators.Equal, node.Key), new ORM.Condition.Filter("ZoneName", SqlOperators.Equal, zone) });
            var contents = DynamicService.Current.GetList<ZoneContent>(filters, "OrderId ASC");

            var result = string.Empty;

            foreach (var content in contents.Where(content => content.Hidden == Toggle.False))
            {
                // نمایش ماژول های ناحیه
                if (content.ModuleId.HasValue)
                {
                }
                // نمایش نماهای ناحیه
                else if (content.ViewId.HasValue)
                {
                    var view = DynamicService.Current.GetItem<View>(content.ViewId.Value);
                    var routeData = new RouteData();
                    var viewData = new ViewDataDictionary();
                    if (view.RouteFieldsList != null && view.RouteFieldsList.Length > 0)
                    {
                        for (var i = 0; i < view.RouteFieldsList.Length; i++)
                        {
                            if (routeData.Values.ContainsKey(view.RouteFieldsList[i]))
                                routeData.Values[view.RouteFieldsList[i]] = view.RouteFieldsValueList[i];
                            else
                                routeData.Values.Add(view.RouteFieldsList[i], view.RouteFieldsValueList[i]);
                        }
                    }
                    if (view.ParametersList != null && view.ParametersList.Length > 0)
                    {
                        for (var i = 0; i < view.ParametersList.Length; i++)
                        {
                            if (viewData.ContainsKey(view.ParametersList[i]))
                                viewData[view.ParametersList[i]] = view.ParametersValueList[i];
                            else
                                viewData.Add(view.ParametersList[i], view.ParametersValueList[i]);
                        }
                    }
                    if (content.RouteFieldsList != null && content.RouteFieldsList.Length > 0)
                    {
                        for (var i = 0; i < content.RouteFieldsList.Length; i++)
                        {
                            if (routeData.Values.ContainsKey(content.RouteFieldsList[i]))
                                routeData.Values[content.RouteFieldsList[i]] = content.RouteFieldsValueList[i];
                            else
                                routeData.Values.Add(content.RouteFieldsList[i], content.RouteFieldsValueList[i]);
                        }
                    }
                    result += GetRazorViewAsString(viewData, $@"~/CmsData/Views/{view.VirtualPath}", routeData) + "<br />";
                }
                // نمایش متن های HTML ناحیه
                else if (content.HtmlString.IsNotEmpty())
                {
                    result += HttpUtility.UrlDecode(content.HtmlString) + "<br />";
                }
            }
            return new MvcHtmlString(result);
        }

        /// <summary>
        /// از این متد جهت ادغام کردن نماها در صفحات پرتال استفاده می شود
        /// </summary>
        /// <param name="viewData">داده های نما: System.Web.Mvc.ViewDataDictionary</param>
        /// <param name="filePath">مسیر نما: string</param>
        /// <param name="routeData">داده های مسیریابی: System.Web.Routing.RouteData</param>
        /// <returns>string</returns>
        public static string GetRazorViewAsString(ViewDataDictionary viewData, string filePath, RouteData routeData)
        {
            var st = new StringWriter();
            var context = new HttpContextWrapper(HttpContext.Current);
            var controllerContext = new ControllerContext(new RequestContext(context, routeData), new RenderController());
            var razor = new RazorView(controllerContext, filePath, null, false, null);
            razor.Render(new ViewContext(controllerContext, razor, viewData, new TempDataDictionary(), st), st);
            return st.ToString();
        }

        private class RenderController : ControllerBase
        {
            protected override void ExecuteCore()
            {
            }
        }

        /*private static string RenderViewToString(ControllerContext context, string viewPath, object model = null, bool partial = false)
        {
            string result = "404";
            // first find the ViewEngine for this view
            var viewEngineResult = partial ? ViewEngines.Engines.FindPartialView(context, viewPath) : ViewEngines.Engines.FindView(context, viewPath, null);

            if (viewEngineResult == null)
                throw new FileNotFoundException("View cannot be found.");

            // get the view and attach the model to view data
            var view = viewEngineResult.View;
            context.Controller.ViewData.Model = model;

            using (var sw = new StringWriter())
            {
                var ctx = new ViewContext(context, view, context.Controller.ViewData, context.Controller.TempData, sw);
                view.Render(ctx, sw);
                result = sw.ToString();
            }
            return result;
        }

        public static string RenderViewToString(string controllerName, string viewName, object viewModel)
        {
            var context = new HttpContextWrapper(HttpContext.Current);
            var routeData = new RouteData();
            routeData.Values.Add("controller", controllerName);
            var controllerContext = new ControllerContext(context, routeData, new RenderController());

            var razorViewEngine = new RazorViewEngine();
            var razorViewResult = razorViewEngine.FindView(controllerContext, viewName, string.Empty, false);

            using (var writer = new StringWriter())
            {
                var viewData = new ViewDataDictionary(viewModel);
                var viewContext = new ViewContext(controllerContext, razorViewResult.View, viewData, new TempDataDictionary(), writer);
                razorViewResult.View.Render(viewContext, writer);
                writer.Flush();
                return writer.GetStringBuilder().ToString();
            }
        }

        public static T CreateController<T>(RouteData routeData = null) where T : Controller, new()
        {
            // create a disconnected controller instance
            var controller = new T();

            // get context wrapper from HttpContext if available
            HttpContextBase wrapper;
            if (HttpContext.Current != null)
                wrapper = new HttpContextWrapper(HttpContext.Current);
            else
                throw new InvalidOperationException(@"Can't create Controller Context if no active HttpContext instance is available.");

            if (routeData == null)
                routeData = new RouteData();

            // add the controller routing if not existing
            if (!routeData.Values.ContainsKey("controller") && !routeData.Values.ContainsKey("Controller"))
                routeData.Values.Add("controller", controller.GetType().Name.ToLower().Replace("controller", ""));

            controller.ControllerContext = new ControllerContext(wrapper, routeData, controller);
            return controller;
        }

        protected string RenderPartialViewToString(ControllerContext context, string viewName, object model)
        {
            if (string.IsNullOrEmpty(viewName))
                viewName = context.RouteData.GetRequiredString("action");

            //ViewData.Model = model;

            using (var sw = new StringWriter())
            {
                var viewResult = ViewEngines.Engines.FindPartialView(context, viewName);
                var viewContext = new ViewContext(context, viewResult.View, ViewData, TempData, sw);
                viewResult.View.Render(viewContext, sw);
                return sw.GetStringBuilder().ToString();
            }
        }
        */

        #endregion

    }

    /// <summary>
    /// از این کلاس برای نمایش محتوای صفحات پرتال استفاده می شود
    /// </summary>
    public abstract class CmsViewPage : WebViewPage<dynamic>
    {
    }

}