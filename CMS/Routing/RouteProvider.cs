﻿namespace CMS.Routing
{

    #region Assembly

    using ORM.Model;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text.RegularExpressions;
    using Sitemap;
    using System.Web;
    using Helper;
    using ORM.Enums;
    using Utility;

    #endregion

    /// <summary>
    /// از این کلاس برای بررسی موجودن بودن صفحه درخواستی در سایت مورد نظر استفاده می شود
    /// </summary>
    public static class RouteProvider
    {

        #region Method

        /// <summary>
        /// این متد بررسی می کند که صفحه درخواستی در سایت مورد نظر موجود باشد و اطلاعات آن از قبیل پارامترها و آدرس صفحه و شناسه سایت آن را برمی گرداند
        /// </summary>
        /// <param name="siteId">شناسه سایت: int</param>
        /// <param name="pageAddress">آدرس صفحه: string</param>
        /// <param name="httpRequest">موجودیت درخواست HTTP: System.Web.HttpRequestBase</param>
        /// <returns>string</returns>
        public static Dictionary<string, string> GetRouteUrl(int siteId, string pageAddress, HttpRequestBase httpRequest)
        {
            // بدست آوردن نام دامنه پرتال
            var host = httpRequest.ServerVariables["HTTP_HOST"];
            var paths = pageAddress.Split('/').Where(p => p.Trim() != string.Empty).ToList();
            var path = string.Empty;
            var result = new Dictionary<string, string>();
            Page targetPage = null;
            var site = CmsVariables.ActiveSite.Find(p => p.ItemId.Equals(siteId));
            if (site != null)
            {
                foreach (var domain in site.DomainsList)
                {
                    // اگر دامنه درخواست کننده صفحه با دامنه سایت برابر بود، یعنی صفحه جزء این سایت بود
                    if ((domain.IsNotEmpty() && host.ToLower().Contains("localhost") && domain.Contains("localhost") && site.IsOnline == Toggle.True) || (domain.IsNotEmpty() && domain.Equals(host) && site.IsOnline == Toggle.True))
                    {
                        for (var i = 0; i < paths.Count; i++)
                        {
                            path += $"/{paths[i]}";
                            var tPath = path;
                            // جستجو در بین صفحات موجود درپرتال مربوط به سایت مورد نظر و که آدرس آن ها با آدرس مورد نظر یکی است
                            foreach (var page in SitemapProvider.Pages.Where(page => page.VirtualPath.ToLower().Equals(tPath.ToLower()) && page.SiteId.Equals(siteId)))
                            {
                                targetPage = page;
                                result.Add("Url", targetPage.VirtualPath);
                                result.Add("SiteId", siteId.ToString());
                                if (page.ParametersList != null)
                                {
                                    // اگر صفحه دارای پارامتر باشد و آن پارامترها در پارامترهای مسیریابی موجو نباشد، صفحه قابل دستیابی نمی باشد
                                    if (page.ParametersList.Length > 0 && i + 1 >= paths.Count && page.ParametersList.Length > page.RouteFieldsList.Length)
                                    {
                                        targetPage = null;
                                        result = new Dictionary<string, string>();
                                        break;
                                    }
                                    // اگر صفحه دارای پارامتر باشد و آن پارامترها در پارامترهای مسیریابی صفحه موجود باشد، صفحه قابل دستیابی می باشد
                                    if (page.ParametersList.Length > 0 && i + 1 >= paths.Count && page.ParametersList.Length == page.RouteFieldsList.Length)
                                    {
                                        foreach (var parameter in page.ParametersList)
                                        {
                                            // اگر پارامتر مورد نظر در پارامترهای مسیریابی نباشد یعنی پارامتربدون مقدار بماند صفحه قابل دستبابی نیست
                                            if (!page.RouteFieldsList.Contains(parameter))
                                            {
                                                targetPage = null;
                                                result = new Dictionary<string, string>();
                                                break;
                                            }
                                            // مقدار دهی پارامتر توسط پارامتر مسیریابی 
                                            result.Add(parameter, page.RouteFieldsValueList[page.RouteFieldsList.ToList().IndexOf(parameter)]);
                                        }
                                    }
                                    // اگر پارامترها در آدرس درخواستی کاربر موجود باشد
                                    else
                                    {
                                        for (var j = 0; j < page.ParametersList.Length && i + j + 1 < paths.Count; j++)
                                        {
                                            var parameterRegex = new Regex(page.ParametersValueList[j]);
                                            var parameterMatch = parameterRegex.Match(paths[i + j + 1]);
                                            // اگر پارامتر در آدرس نباشد صفحه قابل دستیابی نیست
                                            if (!parameterMatch.Success)
                                            {
                                                targetPage = null;
                                                result = new Dictionary<string, string>();
                                                break;
                                            }
                                            // واکشی مقدار پارامتر و اضافه کردن آن به پارامتر
                                            result.Add(page.ParametersList[j], paths[i + j + 1]);
                                        }
                                    }
                                    if (targetPage != null)
                                        break;
                                }
                            }
                            if (targetPage != null)
                                break;
                        }
                    }
                }
            }
            return targetPage != null ? result : null;
        }

        #endregion

    }
}
