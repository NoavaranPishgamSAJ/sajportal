﻿namespace FluentScheduler.Model
{
	public class MinuteUnit
	{
		internal Schedule Schedule { get; }
		internal int Duration { get; }

		public MinuteUnit(Schedule schedule, int duration)
		{
			Schedule = schedule;
			Duration = duration;

			Schedule.CalculateNextRun = x => x.AddMinutes(Duration);
		}
	}
}
