﻿namespace FluentScheduler.Model
{
	public class SecondUnit
	{
		internal Schedule Schedule { get; }
		internal int Duration { get; }

		public SecondUnit(Schedule schedule, int duration)
		{
			Schedule = schedule;
			Duration = duration;

			Schedule.CalculateNextRun = x => x.AddSeconds(Duration);
		}
	}
}
