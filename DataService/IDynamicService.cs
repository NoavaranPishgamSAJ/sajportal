﻿namespace DataService
{

    #region Assembly

    using System.Collections.Generic;
    using System.Data;
    using Dapper;
    using ORM.Condition;
    using System.ServiceModel;

    #endregion

    /// <summary>
    /// از این رابط برای انجام عملیات CURD دیتابیس استفاده می شود
    /// </summary>
    [ServiceContract]
    public interface IDynamicService
    {

        #region Method

        /// <summary>
        /// از این متد برای دریافت تعداد رکورد بازگردانده شده بر اساس شرط خاص استفاده می شود
        /// </summary>
        /// <param name="condition">شرط های اعمال شده در پرسجو: FilterExpressionList</param>
        /// <param name="orderBy">مرتب سازی بر اساس: string</param>
        /// <returns>long</returns>
        [OperationContract]
        long GetCount<T>(FilterExpressionList condition = null, string orderBy = "") where T : class;

        /// <summary>
        /// از این متد برای دریافت رکوردهای جدول بر اساس شرط خاص استفاده می شود
        /// </summary>
        /// <param name="condition">شرط های اعمال شده در پرسجو: FilterExpressionList</param>
        /// <param name="orderBy">مرتب سازی بر اساس: string</param>
        /// <param name="columns">فیلدهای مورد نظر جهت برگرداندن: string</param>
        /// <returns>IEnumerable&lt;GenericType&gt;</returns>
        [OperationContract]
        IEnumerable<T> GetList<T>(FilterExpressionList condition = null, string orderBy = "", string columns = "*") where T : class;

        /// <summary>
        /// از این متد برای دریافت رکوردهای جدول بر اساس شرط خاص استفاده می شود
        /// </summary>
        /// <remarks>
        /// این متد برای دریافت رکوردها در یک ساختار با قابلیت صفحه بندی استفاده می شود
        /// </remarks>
        /// <param name="condition">شرط های اعمال شده در پرسجو: FilterExpressionList</param>
        /// <param name="orderBy">مرتب سازی بر اساس: string</param>
        /// <param name="columns">فیلدهای مورد نظر جهت برگرداندن: string</param>
        /// <param name="page">اندیس صفحه مورد نظر: long</param>
        /// <param name="itemPerPage">تعداد رکورد در هر صفحه: int</param>
        /// <returns>IEnumerable&lt;GenericType&gt;</returns>
        [OperationContract]
        IEnumerable<T> GetList<T>(long page, int itemPerPage, FilterExpressionList condition = null, string orderBy = "", string columns = "*") where T : class;

        /// <summary>
        /// از این متد برای دریافت یک رکورد از جدول بر اساس کلید اصلی استفاده می شود
        /// </summary>
        /// <param name="itemId">شناسه رکورد مورد نظر: int</param>
        /// <returns>GenericType</returns>
        [OperationContract]
        T GetItem<T>(int itemId) where T : class;

        /// <summary>
        /// از این متد برای دریافت یک رکورد از جدول بر اساس شرط خاص استفاده می شود
        /// </summary>
        /// <param name="condition">شرط های اعمال شده در پرسجو: FilterExpressionList</param>
        /// <param name="orderBy">مرتب سازی بر اساس: string</param>
        /// <param name="columns">فیلدهای مورد نظر جهت برگرداندن: string</param>
        /// <returns>GenericType</returns>
        [OperationContract]
        T GetItem<T>(FilterExpressionList condition = null, string orderBy = "", string columns = "*") where T : class;

        /// <summary>
        /// از این متد برای دریافت یک رکورد از جدول بر اساس شرط خاص استفاده می شود
        /// </summary>
        /// <param name="table">نام جدول در دیتابیس: string</param>
        /// <param name="condition">شرط های اعمال شده در پرسجو: FilterExpressionList</param>
        /// <param name="orderBy">مرتب سازی بر اساس: string</param>
        /// <param name="columns">فیلدهای مورد نظر جهت برگرداندن: string</param>
        /// <returns>dynamic</returns>
        dynamic GetItem(string table, FilterExpressionList condition = null, string orderBy = "", string columns = "*");

        /// <summary>
        /// از این متد برای ذخیره یک رکورد در جدول خاص استفاده می شود
        /// </summary>
        /// <remarks>
        /// بعد از ذخیره رکورد شناسه آن بازگردانده می شود
        /// </remarks>
        /// <param name="item">موجودیت مورد نظر جهت ذخیره: GenericType</param>
        /// <returns>long</returns>
        [OperationContract]
        long CreateItem<T>(T item) where T : class;

        /// <summary>
        /// از این متد برای ویرایش یک رکورد در جدول خاص استفاده می شود
        /// </summary>
        /// <remarks>
        /// بعد از ویرایش رکورد در صورت موفقیت آمیز بودن مقدار صحیح بازگردانده می شود
        /// </remarks>
        /// <param name="item">موجودیت مورد نظر جهت ویرایش: GenericType</param>
        /// <returns>bool</returns>
        [OperationContract]
        bool UpdateItem<T>(T item) where T : class;

        /// <summary>
        /// از این متد برای حذف یک رکورد در جدول خاص استفاده می شود
        /// </summary>
        /// <remarks>
        /// بعد از حذف رکورد در صورت موفقیت آمیز بودن مقدار صحیح بازگردانده می شود
        /// </remarks>
        /// <param name="item">موجودیت مورد نظر جهت ویرایش: GenericType</param>
        /// <returns>bool</returns>
        [OperationContract]
        bool DeleteItem<T>(T item) where T : class;

        /// <summary>
        /// از این متد برای حذف یک رکورد در جدول خاص استفاده می شود
        /// </summary>
        /// <remarks>
        /// بعد از حذف رکورد در صورت موفقیت آمیز بودن مقدار صحیح بازگردانده می شود
        /// </remarks>
        /// <param name="itemId">شناسه رکورد مورد نظر: int</param>
        /// <returns>bool</returns>
        [OperationContract]
        bool DeleteItem<T>(int itemId) where T : class;

        /// <summary>
        /// از این متد برای حذف کلیه رکوردهای جدول خاص استفاده می شود
        /// </summary>
        /// <remarks>
        /// بعد از حذف رکورد در صورت موفقیت آمیز بودن مقدار صحیح بازگردانده می شود
        /// </remarks>
        /// <returns>bool</returns>
        [OperationContract]
        bool DeleteAllItem<T>() where T : class;

        /// <summary>
        /// از این متد برای حذف یکسری رکورد در جدول براساس شرط خاص استفاده می شود
        /// </summary>
        /// <remarks>
        /// بعد از حذف رکورد در صورت موفقیت آمیز بودن یک عدد بازگردانده می شود
        /// </remarks>
        /// <param name="condition">شرط های اعمال شده در پرسجو: FilterExpressionList</param>
        /// <returns>int</returns>
        [OperationContract]
        int DeleteList<T>(FilterExpressionList condition = null) where T : class;

        /// <summary>
        /// از این متد برای اجرای دستورات sql به صورت مستقیم استفاده می شود
        /// </summary>
        /// <remarks>
        /// بعد از انجام عملیات در صورت موفقیت آمیز بودن یک عدد بازگردانده می شود
        /// </remarks>
        /// <param name="command">رشته پرسجو مورد نظر: string</param>
        /// <param name="param">پارامترهای ارسالی: DynamicParameters</param>
        /// <param name="type">نوع پرسجو: CommandType</param>
        /// <returns>int</returns>
        [OperationContract]
        int Execute(string command, DynamicParameters param = null, CommandType type = CommandType.Text);

        /// <summary>
        /// از این متد برای اجرای یک رویه در دیتابیس استفاده می شود
        /// </summary>
        /// <param name="procedureName">نام رویه: string</param>
        /// <param name="param">پارامترهای ارسالی: DynamicParameters</param>
        /// <param name="type">نوع پرسجو: CommandType</param>
        /// <returns>IEnumerable&lt;GenericType&gt;</returns>
        [OperationContract]
        IEnumerable<T> StoredProcedures<T>(string procedureName, DynamicParameters param, CommandType type) where T : class;

        /// <summary>
        /// از این متد برای اجرای دستورات sql به صورت مستقیم استفاده می شود
        /// </summary>
        /// <param name="command">رشته پرسجو مورد نظر: string</param>
        /// <returns>IEnumerable&lt;dynamic&gt;</returns>
        IEnumerable<dynamic> Query(string command);

        #endregion

    }
}
