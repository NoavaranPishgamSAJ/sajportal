﻿namespace DataService
{

    #region Assembly

    using System.Collections.Generic;
    using DataLayer;
    using System.ServiceModel;
    using ORM.Model;

    #endregion

    /// <summary>
    /// از این کلاس برای مدیریت مجوزهای کاربران و سمت ها و نقش های آن استفاده می شود
    /// </summary>
    [ServiceContract]
    public class MembershipService : IMembershipService
    {

        #region Attribute

        private static MembershipService _current;

        #endregion

        #region Property

        /// <summary>
        /// نسخه ایستا از کلاس
        /// </summary>
        public static MembershipService Current
        {
            get
            {
                if (_current != null)
                    return _current;
                return _current = new MembershipService();
            }
        }

        #endregion

        #region Method

        /// <summary>
        /// اضافه کردن مجوز به نقش
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// MembershipService.Current.AddRolePermission(1, 2);
        /// </code>
        /// </example>
        /// <param name="roleId">: int</param>
        /// <param name="permissionId">: int</param>
        /// <returns>bool</returns>
        [OperationContract]
        public bool AddRolePermission(int roleId, int permissionId)
        {
            return MembershipRepository.Current.AddRolePermission(roleId, permissionId);
        }

        /// <summary>
        /// اضافه کردن مجوز به نقش
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// MembershipService.Current.AddRolePermission(1, "ورود به سیستم");
        /// </code>
        /// </example>
        /// <param name="roleId">: int</param>
        /// <param name="displayName">: string</param>
        /// <returns>bool</returns>
        [OperationContract]
        public bool AddRolePermission(int roleId, string displayName)
        {
            return MembershipRepository.Current.AddRolePermission(roleId, displayName);
        }

        /// <summary>
        /// اضافه کردن مجوز به نقش
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// MembershipService.Current.AddRolePermissionExtra(1, "Login");
        /// </code>
        /// </example>
        /// <param name="roleId">: int</param>
        /// <param name="devName">: string</param>
        /// <returns>bool</returns>
        [OperationContract]
        public bool AddRolePermissionExtra(int roleId, string devName)
        {
            return MembershipRepository.Current.AddRolePermissionExtra(roleId, devName);
        }

        /// <summary>
        /// اضافه کردن مجوز به کاربر
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// MembershipService.Current.AddUserPermission(1, 2);
        /// </code>
        /// </example>
        /// <param name="customerId">: int</param>
        /// <param name="permissionId">: int</param>
        /// <returns>bool</returns>
        [OperationContract]
        public bool AddUserPermission(int customerId, int permissionId)
        {
            return MembershipRepository.Current.AddUserPermission(customerId, permissionId);
        }

        /// <summary>
        /// اضافه کردن مجوز به کاربر
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// MembershipService.Current.AddUserPermission(1, "ورود به سیستم");
        /// </code>
        /// </example>
        /// <param name="customerId">: int</param>
        /// <param name="displayName">: string</param>
        /// <returns>bool</returns>
        [OperationContract]
        public bool AddUserPermission(int customerId, string displayName)
        {
            return MembershipRepository.Current.AddUserPermission(customerId, displayName);
        }

        /// <summary>
        /// اضافه کردن مجوز به کاربر
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// MembershipService.Current.AddUserPermissionExtra(1, "Login");
        /// </code>
        /// </example>
        /// <param name="customerId">: int</param>
        /// <param name="devName">: string</param>
        /// <returns>bool</returns>
        [OperationContract]
        public bool AddUserPermissionExtra(int customerId, string devName)
        {
            return MembershipRepository.Current.AddUserPermissionExtra(customerId, devName);
        }

        /// <summary>
        /// گرفتن مجوز از کاربر
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// MembershipService.Current.DeleteUserPermission(1, 2);
        /// </code>
        /// </example>
        /// <param name="customerId">: int</param>
        /// <param name="permissionId">: int</param>
        /// <returns>bool</returns>
        [OperationContract]
        public bool DeleteUserPermission(int customerId, int permissionId)
        {
            return MembershipRepository.Current.DeleteUserPermission(customerId, permissionId);
        }

        /// <summary>
        /// گرفتن مجوز از کاربر
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// MembershipService.Current.DeleteUserPermission(1, "ورود به سیستم");
        /// </code>
        /// </example>
        /// <param name="customerId">: int</param>
        /// <param name="displayName">: string</param>
        /// <returns>bool</returns>
        [OperationContract]
        public bool DeleteUserPermission(int customerId, string displayName)
        {
            return MembershipRepository.Current.DeleteUserPermission(customerId, displayName);
        }

        /// <summary>
        /// گرفتن مجوز از کاربر
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// MembershipService.Current.DeleteUserPermissionExtra(1, "Login");
        /// </code>
        /// </example>
        /// <param name="customerId">: int</param>
        /// <param name="devName">: string</param>
        /// <returns>bool</returns>
        [OperationContract]
        public bool DeleteUserPermissionExtra(int customerId, string devName)
        {
            return MembershipRepository.Current.DeleteUserPermissionExtra(customerId, devName);
        }

        /// <summary>
        /// حذف تمامی مجوزهای نقش
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// MembershipService.Current.DeleteRolePermissions(1);
        /// </code>
        /// </example>
        /// <param name="roleId">: int</param>
        /// <returns>int</returns>
        [OperationContract]
        public int DeleteRolePermissions(int roleId)
        {
            return MembershipRepository.Current.DeleteRolePermissions(roleId);
        }

        /// <summary>
        /// حذف تمامی نقش های کاربر
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// MembershipService.Current.DeleteCustomerRoles(1);
        /// </code>
        /// </example>
        /// <param name="customerId">: int</param>
        /// <returns>int</returns>
        [OperationContract]
        public int DeleteCustomerRoles(int customerId)
        {
            return MembershipRepository.Current.DeleteCustomerRoles(customerId);
        }

        /// <summary>
        /// حذف تمامی سمت های کاربر
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// MembershipService.Current.DeleteCustomerPositions(1);
        /// </code>
        /// </example>
        /// <param name="customerId">: int</param>
        /// <returns>int</returns>
        [OperationContract]
        public int DeleteCustomerPositions(int customerId)
        {
            return MembershipRepository.Current.DeleteCustomerPositions(customerId);
        }

        /// <summary>
        /// اضافه کردن نقش برای کاربر
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// MembershipService.Current.AddUserRole(1, 2);
        /// </code>
        /// </example>
        /// <param name="customerId">: int</param>
        /// <param name="roleId">: int</param>
        /// <returns>int</returns>
        [OperationContract]
        public bool AddUserRole(int customerId, int roleId)
        {
            return MembershipRepository.Current.AddUserRole(customerId, roleId);
        }

        /// <summary>
        /// اضافه کردن سمت برای کاربر
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// MembershipService.Current.AddUserPosition(1, 2);
        /// </code>
        /// </example>
        /// <param name="customerId">: int</param>
        /// <param name="positionId">: int</param>
        /// <returns>int</returns>
        [OperationContract]
        public bool AddUserPosition(int customerId, int positionId)
        {
            return MembershipRepository.Current.AddUserPosition(customerId, positionId);
        }

        /// <summary>
        /// حذف مجوز از نقش
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// MembershipService.Current.DeleteRolePermission(1, 2);
        /// </code>
        /// </example>
        /// <param name="roleId">: int</param>
        /// <param name="permissionId">: int</param>
        /// <returns>int</returns>
        [OperationContract]
        public bool DeleteRolePermission(int roleId, int permissionId)
        {
            return MembershipRepository.Current.DeleteRolePermission(roleId, permissionId);
        }

        /// <summary>
        /// حذف مجوز از نقش
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// MembershipService.Current.DeleteRolePermission(1, "ورود به سیستم");
        /// </code>
        /// </example>
        /// <param name="roleId">: int</param>
        /// <param name="displayName">: int</param>
        /// <returns>int</returns>
        [OperationContract]
        public bool DeleteRolePermission(int roleId, string displayName)
        {
            return MembershipRepository.Current.DeleteRolePermission(roleId, displayName);
        }

        /// <summary>
        /// حذف مجوز از نقش
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// MembershipService.Current.DeleteRolePermissionExtra(1, "Login");
        /// </code>
        /// </example>
        /// <param name="roleId">: int</param>
        /// <param name="devName">: int</param>
        /// <returns>int</returns>
        [OperationContract]
        public bool DeleteRolePermissionExtra(int roleId, string devName)
        {
            return MembershipRepository.Current.DeleteRolePermissionExtra(roleId, devName);
        }

        /// <summary>
        /// دریافت اطلاعات کاربر بر اساس نام کاربری
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// MembershipService.Current.GetUser("John Doue");
        /// </code>
        /// </example>
        /// <param name="username">: string</param>
        /// <returns>Customer</returns>
        [OperationContract]
        public Customer GetUser(string username)
        {
            return MembershipRepository.Current.GetUser(username);
        }

        /// <summary>
        /// بررسی صحت نام کاربری و رمزعبور
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// MembershipService.Current.Login("John Doue", "******");
        /// </code>
        /// </example>
        /// <param name="username">: string</param>
        /// <param name="password">: string</param>
        /// <returns>bool</returns>
        [OperationContract]
        public bool Login(string username, string password)
        {
            return MembershipRepository.Current.Login(username, password);
        }

        /// <summary>
        /// بررسی دارا بودن مجوز برای نقش خاص
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// MembershipService.Current.RoleHasPermission(1, 2);
        /// </code>
        /// </example>
        /// <param name="roleId">: int</param>
        /// <param name="permissionId">: int</param>
        /// <returns>bool</returns>
        [OperationContract]
        public bool RoleHasPermission(int roleId, int permissionId)
        {
            return MembershipRepository.Current.RoleHasPermission(roleId, permissionId);
        }

        /// <summary>
        /// بررسی دارا بودن مجوز برای نقش خاص
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// MembershipService.Current.RoleHasPermission(1, "ورود به سیستم");
        /// </code>
        /// </example>
        /// <param name="roleId">: int</param>
        /// <param name="displayName">: string</param>
        /// <returns>bool</returns>
        [OperationContract]
        public bool RoleHasPermission(int roleId, string displayName)
        {
            return MembershipRepository.Current.RoleHasPermission(roleId, displayName);
        }

        /// <summary>
        /// بررسی دارا بودن مجوز برای نقش خاص
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// MembershipService.Current.RoleHasPermissionExtra(1, "Login");
        /// </code>
        /// </example>
        /// <param name="roleId">: int</param>
        /// <param name="devName">: string</param>
        /// <returns>bool</returns>
        [OperationContract]
        public bool RoleHasPermissionExtra(int roleId, string devName)
        {
            return MembershipRepository.Current.RoleHasPermissionExtra(roleId, devName);
        }

        /// <summary>
        /// بررسی دارا بودن مجوز برای کاربر خاص
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// MembershipService.Current.UserHasPermission(1, 2);
        /// </code>
        /// </example>
        /// <param name="customerId">: int</param>
        /// <param name="permissionId">: int</param>
        /// <returns>bool</returns>
        [OperationContract]
        public bool UserHasPermission(int customerId, int permissionId)
        {
            return MembershipRepository.Current.UserHasPermission(customerId, permissionId);
        }

        /// <summary>
        /// بررسی دارا بودن مجوز برای کاربر خاص
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// MembershipService.Current.UserHasPermission(1, "ورود به سیستم");
        /// </code>
        /// </example>
        /// <param name="customerId">: int</param>
        /// <param name="displayName">: string</param>
        /// <returns>bool</returns>
        [OperationContract]
        public bool UserHasPermission(int customerId, string displayName)
        {
            return MembershipRepository.Current.UserHasPermission(customerId, displayName);
        }

        /// <summary>
        /// بررسی دارا بودن مجوز برای کاربر خاص
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// MembershipService.Current.UserHasPermissionExtra(1, "Login");
        /// </code>
        /// </example>
        /// <param name="customerId">: int</param>
        /// <param name="devName">: string</param>
        /// <returns>bool</returns>
        [OperationContract]
        public bool UserHasPermissionExtra(int customerId, string devName)
        {
            return MembershipRepository.Current.UserHasPermissionExtra(customerId, devName);
        }

        /// <summary>
        /// دریافت مجوزهای اضافه کاربر
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// MembershipService.Current.UserPermissions(1);
        /// </code>
        /// </example>
        /// <param name="customerId">: int</param>
        /// <returns>IEnumerable&lt;CustomerPermissionView&gt;</returns>
        [OperationContract]
        public IEnumerable<CustomerPermissionView> UserPermissions(int customerId)
        {
            return MembershipRepository.Current.UserPermissions(customerId);
        }

        /// <summary>
        /// دریافت مجوزهای کاربر
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// MembershipService.Current.UserExistPermissions(1);
        /// </code>
        /// </example>
        /// <param name="customerId">: int</param>
        /// <returns>IEnumerable&lt;CustomerPermissions&gt;</returns>
        [OperationContract]
        public IEnumerable<CustomerPermissions> UserExistPermissions(int customerId)
        {
            return MembershipRepository.Current.UserExistPermissions(customerId);
        }

        /// <summary>
        /// دریافت مجوزهای نقش
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// MembershipService.Current.RolePermissions(1);
        /// </code>
        /// </example>
        /// <param name="roleId">: int</param>
        /// <returns>IEnumerable&lt;RolePermissionView&gt;</returns>
        [OperationContract]
        public IEnumerable<RolePermissionView> RolePermissions(int roleId)
        {
            return MembershipRepository.Current.RolePermissions(roleId);
        }

        /// <summary>
        /// دریافت سمت های کاربر
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// MembershipService.Current.UserPositions(1);
        /// </code>
        /// </example>
        /// <param name="customerId">: int</param>
        /// <returns>IEnumerable&lt;CustomerPositionView&gt;</returns>
        [OperationContract]
        public IEnumerable<CustomerPositionView> UserPositions(int customerId)
        {
            return MembershipRepository.Current.UserPositions(customerId);
        }

        /// <summary>
        /// دریافت نقش های کاربر
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// MembershipService.Current.UserRoles(1);
        /// </code>
        /// </example>
        /// <param name="customerId">: int</param>
        /// <returns>IEnumerable&lt;CustomerRoleView&gt;</returns>
        [OperationContract]
        public IEnumerable<CustomerRoleView> UserRoles(int customerId)
        {
            return MembershipRepository.Current.UserRoles(customerId);
        }

        /// <summary>
        /// بررسی دارا بودن سمت توسط کاربر
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// MembershipService.Current.UserHasPosition(1, "مدیر سیستم");
        /// </code>
        /// </example>
        /// <param name="customerId">: int</param>
        /// <param name="displayName">: string</param>
        /// <returns>bool</returns>
        [OperationContract]
        public bool UserHasPosition(int customerId, string displayName)
        {
            return MembershipRepository.Current.UserHasPosition(customerId, displayName);
        }

        /// <summary>
        /// تخصیص سمت به کاربر
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// MembershipService.Current.AddUserPosition(1, "مدیر سیستم");
        /// </code>
        /// </example>
        /// <param name="customerId">: int</param>
        /// <param name="displayName">: string</param>
        /// <returns>bool</returns>
        [OperationContract]
        public bool AddUserPosition(int customerId, string displayName)
        {
            return MembershipRepository.Current.AddUserPosition(customerId, displayName);
        }

        /// <summary>
        /// حذف سمت کاربر
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// MembershipService.Current.DeleteUserPosition(1, "مدیر سیستم");
        /// </code>
        /// </example>
        /// <param name="customerId">: int</param>
        /// <param name="displayName">: string</param>
        /// <returns>bool</returns>
        [OperationContract]
        public bool DeleteUserPosition(int customerId, string displayName)
        {
            return MembershipRepository.Current.DeleteUserPosition(customerId, displayName);
        }

        /// <summary>
        /// بررسی دارا بودن نقش برای کاربر
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// MembershipService.Current.UserHasRole(1, "راهبر");
        /// </code>
        /// </example>
        /// <param name="customerId">: int</param>
        /// <param name="displayName">: string</param>
        /// <returns>bool</returns>
        [OperationContract]
        public bool UserHasRole(int customerId, string displayName)
        {
            return MembershipRepository.Current.UserHasRole(customerId, displayName);
        }

        /// <summary>
        /// تخصیص نقش به کاربر
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// MembershipService.Current.AddUserRole(1, "راهبر");
        /// </code>
        /// </example>
        /// <param name="customerId">: int</param>
        /// <param name="displayName">: string</param>
        /// <returns>bool</returns>
        [OperationContract]
        public bool AddUserRole(int customerId, string displayName)
        {
            return MembershipRepository.Current.AddUserRole(customerId, displayName);
        }

        /// <summary>
        /// حذف نقش کاربر
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// MembershipService.Current.DeleteUserRole(1, "راهبر");
        /// </code>
        /// </example>
        /// <param name="customerId">: int</param>
        /// <param name="displayName">: string</param>
        /// <returns>bool</returns>
        [OperationContract]
        public bool DeleteUserRole(int customerId, string displayName)
        {
            return MembershipRepository.Current.DeleteUserRole(customerId, displayName);
        }

        #endregion

    }
}
