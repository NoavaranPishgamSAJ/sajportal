﻿namespace DataService
{

    #region Assembly

    using System.Collections.Generic;
    using DataLayer;
    using System.ServiceModel;
    using ORM.Model;
    using System.Threading.Tasks;

    #endregion

    [ServiceContract]
    public class MembershipAsyncService : IMembershipAsyncService
    {

        #region Method

        [OperationContract]
        public async Task<bool> AddRolePermission(int roleId, int permissionId)
        {
            return await MembershipAsyncRepository.AddRolePermission(roleId, permissionId);
        }

        [OperationContract]
        public async Task<bool> AddRolePermission(int roleId, string displayName)
        {
            return await MembershipAsyncRepository.AddRolePermission(roleId, displayName);
        }

        [OperationContract]
        public async Task<bool> AddRolePermissionExtra(int roleId, string devName)
        {
            return await MembershipAsyncRepository.AddRolePermissionExtra(roleId, devName);
        }

        [OperationContract]
        public async Task<bool> AddUserPermission(int customerId, int permissionId)
        {
            return await MembershipAsyncRepository.AddUserPermission(customerId, permissionId);
        }

        [OperationContract]
        public async Task<bool> AddUserPermission(int customerId, string displayName)
        {
            return await MembershipAsyncRepository.AddUserPermission(customerId, displayName);
        }

        [OperationContract]
        public async Task<bool> AddUserPermissionExtra(int customerId, string devName)
        {
            return await MembershipAsyncRepository.AddUserPermissionExtra(customerId, devName);
        }

        [OperationContract]
        public async Task<bool> DeleteUserPermission(int customerId, int permissionId)
        {
            return await MembershipAsyncRepository.DeleteUserPermission(customerId, permissionId);
        }

        [OperationContract]
        public async Task<bool> DeleteUserPermission(int customerId, string displayName)
        {
            return await MembershipAsyncRepository.DeleteUserPermission(customerId, displayName);
        }

        [OperationContract]
        public async Task<bool> DeleteUserPermissionExtra(int customerId, string devName)
        {
            return await MembershipAsyncRepository.DeleteUserPermissionExtra(customerId, devName);
        }

        [OperationContract]
        public async Task<int> DeleteRolePermissions(int roleId)
        {
            return await MembershipAsyncRepository.DeleteRolePermissions(roleId);
        }

        [OperationContract]
        public async Task<bool> DeleteRolePermission(int roleId, int permissionId)
        {
            return await MembershipAsyncRepository.DeleteRolePermission(roleId, permissionId);
        }

        [OperationContract]
        public async Task<bool> DeleteRolePermission(int roleId, string displayName)
        {
            return await MembershipAsyncRepository.DeleteRolePermission(roleId, displayName);
        }

        [OperationContract]
        public async Task<bool> DeleteRolePermissionExtra(int roleId, string devName)
        {
            return await MembershipAsyncRepository.DeleteRolePermissionExtra(roleId, devName);
        }

        [OperationContract]
        public async Task<Customer> GetUser(string username)
        {
            return await MembershipAsyncRepository.GetUser(username);
        }

        [OperationContract]
        public async Task<bool> Login(string username, string password)
        {
            return await MembershipAsyncRepository.Login(username, password);
        }

        [OperationContract]
        public async Task<bool> RoleHasPermission(int roleId, int permissionId)
        {
            return await MembershipAsyncRepository.RoleHasPermission(roleId, permissionId);
        }

        [OperationContract]
        public async Task<bool> RoleHasPermission(int roleId, string displayName)
        {
            return await MembershipAsyncRepository.RoleHasPermission(roleId, displayName);
        }

        [OperationContract]
        public async Task<bool> RoleHasPermissionExtra(int roleId, string devName)
        {
            return await MembershipAsyncRepository.RoleHasPermissionExtra(roleId, devName);
        }

        [OperationContract]
        public async Task<bool> UserHasPermission(int customerId, int permissionId)
        {
            return await MembershipAsyncRepository.UserHasPermission(customerId, permissionId);
        }

        [OperationContract]
        public async Task<bool> UserHasPermission(int customerId, string displayName)
        {
            return await MembershipAsyncRepository.UserHasPermission(customerId, displayName);
        }

        [OperationContract]
        public async Task<bool> UserHasPermissionExtra(int customerId, string devName)
        {
            return await MembershipAsyncRepository.UserHasPermissionExtra(customerId, devName);
        }

        [OperationContract]
        public async Task<IEnumerable<CustomerPermissionView>> UserPermissions(int customerId)
        {
            return await MembershipAsyncRepository.UserPermissions(customerId);
        }

        [OperationContract]
        public async Task<IEnumerable<CustomerPermissions>> UserExistPermissions(int customerId)
        {
            return await MembershipAsyncRepository.UserExistPermissions(customerId);
        }

        [OperationContract]
        public async Task<IEnumerable<RolePermissionView>> RolePermissions(int roleId)
        {
            return await MembershipAsyncRepository.RolePermissions(roleId);
        }

        [OperationContract]
        public async Task<IEnumerable<CustomerPositionView>> UserPositions(int customerId)
        {
            return await MembershipAsyncRepository.UserPositions(customerId);
        }

        [OperationContract]
        public async Task<IEnumerable<CustomerRoleView>> UserRoles(int customerId)
        {
            return await MembershipAsyncRepository.UserRoles(customerId);
        }

        [OperationContract]
        public async Task<bool> UserHasPosition(int customerId, string displayName)
        {
            return await MembershipAsyncRepository.UserHasPosition(customerId, displayName);
        }

        [OperationContract]
        public async Task<bool> AddUserPosition(int customerId, string displayName)
        {
            return await MembershipAsyncRepository.AddUserPosition(customerId, displayName);
        }

        [OperationContract]
        public async Task<bool> DeleteUserPosition(int customerId, string displayName)
        {
            return await MembershipAsyncRepository.DeleteUserPosition(customerId, displayName);
        }

        [OperationContract]
        public async Task<bool> UserHasRole(int customerId, string displayName)
        {
            return await MembershipAsyncRepository.UserHasRole(customerId, displayName);
        }

        [OperationContract]
        public async Task<bool> AddUserRole(int customerId, string displayName)
        {
            return await MembershipAsyncRepository.AddUserRole(customerId, displayName);
        }

        [OperationContract]
        public async Task<bool> DeleteUserRole(int customerId, string displayName)
        {
            return await MembershipAsyncRepository.DeleteUserRole(customerId, displayName);
        }

        #endregion

    }
}
