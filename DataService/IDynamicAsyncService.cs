﻿namespace DataService
{

    #region Assembly

    using System.Collections.Generic;
    using System.Data;
    using ORM.Model;
    using Dapper;
    using ORM.Condition;
    using System.ServiceModel;
    using System.Threading.Tasks;

    #endregion

    [ServiceContract]
    interface IDynamicAsyncService
    {

        #region Method

        [OperationContract]
        Task<long> GetCount<T>(FilterExpressionList condition = null, string orderBy = "") where T : class;

        [OperationContract]
        Task<IEnumerable<T>> GetList<T>(FilterExpressionList condition = null, string orderBy = "", string columns = "*") where T : class;

        [OperationContract]
        Task<IEnumerable<T>> GetList<T>(long page, int itemPerPage, FilterExpressionList condition = null, string orderBy = "", string columns = "*") where T : class;

        [OperationContract]
        Task<T> GetItem<T>(int itemId) where T : class;

        [OperationContract]
        Task<T> GetItem<T>(FilterExpressionList condition = null, string orderBy = "", string columns = "*") where T : class;

        [OperationContract]
        Task<long> CreateItem<T>(T item) where T : class;

        [OperationContract]
        Task<bool> UpdateItem<T>(T item) where T : class;

        [OperationContract]
        Task<bool> DeleteItem<T>(T item) where T : class;

        [OperationContract]
        Task<bool> DeleteItem<T>(int itemId) where T : class;

        [OperationContract]
        Task<bool> DeleteAllItem<T>() where T : class;

        [OperationContract]
        Task<int> DeleteList<T>(FilterExpressionList condition = null) where T : class;

        [OperationContract]
        Task<int> Execute(string command, DynamicParameters param = null, CommandType type = CommandType.Text);

        [OperationContract]
        Task<IEnumerable<T>> StoredProcedures<T>(string procedureName, DynamicParameters param, CommandType type) where T : class;

        #endregion

    }
}
