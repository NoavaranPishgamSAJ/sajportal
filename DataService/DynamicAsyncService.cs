﻿namespace DataService
{

    #region Assembly

    using System.Collections.Generic;
    using System.Data;
    using DataLayer;
    using Dapper;
    using ORM.Condition;
    using System.ServiceModel;
    using System.Threading.Tasks;

    #endregion

    [ServiceContract]
    public class DynamicAsyncService : IDynamicAsyncService
    {

        #region Method

        [OperationContract]
        public Task<long> GetCount<T>(FilterExpressionList condition = null, string orderBy = "") where T : class
        {
            return DynamicFilterAsyncRepository.GetCount<T>(condition, orderBy);
        }

        [OperationContract]
        public Task<long> GetCount(string table, FilterExpressionList condition = null, string orderBy = "")
        {
            return DynamicFilterAsyncRepository.GetCount(table, condition, orderBy);
        }

        [OperationContract]
        public Task<IEnumerable<T>> GetList<T>(FilterExpressionList condition = null, string orderBy = "", string columns = "*") where T : class
        {
            return DynamicFilterAsyncRepository.GetList<T>(condition, orderBy, columns);
        }

        [OperationContract]
        public Task<IEnumerable<dynamic>> GetList(string table, FilterExpressionList condition = null, string orderBy = "", string columns = "*")
        {
            return DynamicFilterAsyncRepository.GetList(table, condition, orderBy, columns);
        }

        [OperationContract]
        public Task<IEnumerable<T>> GetList<T>(long page, int itemPerPage, FilterExpressionList condition = null, string orderBy = "", string columns = "*") where T : class
        {
            return DynamicFilterAsyncRepository.GetList<T>(page, itemPerPage, condition, orderBy, columns);
        }

        [OperationContract]
        public Task<IEnumerable<dynamic>> GetList(string table, long page, int itemPerPage, FilterExpressionList condition = null, string orderBy = "", string columns = "*")
        {
            return DynamicFilterAsyncRepository.GetList(table, page, itemPerPage, condition, orderBy, columns);
        }

        [OperationContract]
        public Task<T> GetItem<T>(int itemId) where T : class
        {
            return DynamicFilterAsyncRepository.GetItem<T>(itemId);
        }

        [OperationContract]
        public Task<dynamic> GetItem(string table, int itemId)
        {
            return DynamicFilterAsyncRepository.GetItem(table, itemId);
        }

        [OperationContract]
        public Task<T> GetItem<T>(FilterExpressionList condition = null, string orderBy = "", string columns = "*") where T : class
        {
            return DynamicFilterAsyncRepository.GetItem<T>(condition, orderBy, columns);
        }

        [OperationContract]
        public Task<long> CreateItem<T>(T item) where T : class
        {
            return DynamicFilterAsyncRepository.CreateItem(item);
        }

        [OperationContract]
        public Task<bool> UpdateItem<T>(T item) where T : class
        {
            return DynamicFilterAsyncRepository.UpdateItem(item);
        }

        [OperationContract]
        public Task<bool> DeleteItem<T>(T item) where T : class
        {
            return DynamicFilterAsyncRepository.DeleteItem(item);
        }

        [OperationContract]
        public Task<bool> DeleteItem<T>(int itemId) where T : class
        {
            return DynamicFilterAsyncRepository.DeleteItem<T>(itemId);
        }

        [OperationContract]
        public Task<bool> DeleteAllItem<T>() where T : class
        {
            return DynamicFilterAsyncRepository.DeleteAllItem<T>();
        }

        [OperationContract]
        public Task<int> DeleteList<T>(FilterExpressionList condition = null) where T : class
        {
            return DynamicFilterAsyncRepository.DeleteList<T>(condition);
        }

        [OperationContract]
        public Task<int> Execute(string command, DynamicParameters param = null, CommandType type = CommandType.Text)
        {
            return DynamicFilterAsyncRepository.Execute(command, param, type);
        }

        [OperationContract]
        public Task<IEnumerable<T>> StoredProcedures<T>(string procedureName, DynamicParameters param, CommandType type) where T : class
        {
            return DynamicFilterAsyncRepository.StoredProcedures<T>(procedureName, param, type);
        }

        #endregion

    }
}
