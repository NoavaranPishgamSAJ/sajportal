﻿namespace DataService
{

    #region Assembly

    using System.Collections.Generic;
    using ORM.Model;
    using System.ServiceModel;
    using System.Threading.Tasks;

    #endregion

    [ServiceContract]
    public interface IMembershipAsyncService
    {

        #region Method

        [OperationContract]
        Task<bool> AddRolePermission(int roleId, int permissionId);

        [OperationContract]
        Task<bool> AddRolePermission(int roleId, string displayName);

        [OperationContract]
        Task<bool> AddRolePermissionExtra(int roleId, string devName);

        [OperationContract]
        Task<bool> AddUserPermission(int customerId, int permissionId);

        [OperationContract]
        Task<bool> AddUserPermission(int customerId, string displayName);

        [OperationContract]
        Task<bool> AddUserPermissionExtra(int customerId, string devName);

        [OperationContract]
        Task<bool> DeleteUserPermission(int customerId, int permissionId);

        [OperationContract]
        Task<bool> DeleteUserPermission(int customerId, string displayName);

        [OperationContract]
        Task<bool> DeleteUserPermissionExtra(int customerId, string devName);

        [OperationContract]
        Task<int> DeleteRolePermissions(int roleId);

        [OperationContract]
        Task<bool> DeleteRolePermission(int roleId, int permissionId);

        [OperationContract]
        Task<bool> DeleteRolePermission(int roleId, string displayName);

        [OperationContract]
        Task<bool> DeleteRolePermissionExtra(int roleId, string devName);

        [OperationContract]
        Task<Customer> GetUser(string username);

        [OperationContract]
        Task<bool> Login(string username, string password);

        [OperationContract]
        Task<bool> RoleHasPermission(int roleId, int permissionId);

        [OperationContract]
        Task<bool> RoleHasPermission(int roleId, string displayName);

        [OperationContract]
        Task<bool> RoleHasPermissionExtra(int roleId, string devName);

        [OperationContract]
        Task<bool> UserHasPermission(int customerId, int permissionId);

        [OperationContract]
        Task<bool> UserHasPermission(int customerId, string displayName);

        [OperationContract]
        Task<bool> UserHasPermissionExtra(int customerId, string devName);

        [OperationContract]
        Task<IEnumerable<CustomerPermissionView>> UserPermissions(int customerId);

        [OperationContract]
        Task<IEnumerable<CustomerPermissions>> UserExistPermissions(int customerId);

        [OperationContract]
        Task<IEnumerable<RolePermissionView>> RolePermissions(int roleId);

        [OperationContract]
        Task<IEnumerable<CustomerPositionView>> UserPositions(int customerId);

        [OperationContract]
        Task<IEnumerable<CustomerRoleView>> UserRoles(int customerId);

        [OperationContract]
        Task<bool> UserHasPosition(int customerId, string displayName);

        [OperationContract]
        Task<bool> AddUserPosition(int customerId, string displayName);

        [OperationContract]
        Task<bool> DeleteUserPosition(int customerId, string displayName);

        [OperationContract]
        Task<bool> UserHasRole(int customerId, string displayName);

        [OperationContract]
        Task<bool> AddUserRole(int customerId, string displayName);

        [OperationContract]
        Task<bool> DeleteUserRole(int customerId, string displayName);

        #endregion

    }
}
