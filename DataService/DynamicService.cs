﻿namespace DataService
{

    #region Assembly

    using System;
    using System.Collections.Generic;
    using System.Data;
    using DataLayer;
    using Dapper;
    using ORM.Condition;
    using System.ServiceModel;

    #endregion

    /// <summary>
    /// این کلاس برای انجام عملیات CURD دیتابیس استفاده می شود
    /// </summary>
    [ServiceContract]
    public class DynamicService : IDynamicService, IDisposable
    {

        #region Attribute

        private static DynamicService _current;

        #endregion

        #region Property

        /// <summary>
        /// نسخه ایستا از کلاس
        /// </summary>
        public static DynamicService Current
        {
            get
            {
                if (_current != null)
                    return _current;
                return _current = new DynamicService();
            }
        }

        #endregion

        #region Method

        /// <summary>
        /// از این متد برای دریافت تعداد رکورد بازگردانده شده بر اساس شرط خاص استفاده می شود
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// long count = DynamicService.Current.GetCount&lt;Person&gt;([condition], ["ID ASC"]);
        /// </code>
        /// </example>
        /// <param name="condition">شرط های اعمال شده در پرسجو: FilterExpressionList</param>
        /// <param name="orderBy">مرتب سازی بر اساس: string</param>
        /// <returns>long</returns>
        [OperationContract]
        public long GetCount<T>(FilterExpressionList condition = null, string orderBy = "") where T : class
        {
            return DynamicFilterRepository.Current.GetCount<T>(condition, orderBy);
        }

        /// <summary>
        /// از این متد برای دریافت تعداد رکورد بازگردانده شده بر اساس شرط خاص استفاده می شود
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// long count = DynamicService.Current.GetCount("Person", [condition], ["ID ASC"]);
        /// </code>
        /// </example>
        /// <param name="table">نام جدول در دیتابیس: string</param>
        /// <param name="condition">شرط های اعمال شده در پرسجو: FilterExpressionList</param>
        /// <param name="orderBy">مرتب سازی بر اساس: string</param>
        /// <returns>long</returns>
        [OperationContract]
        public long GetCount(string table, FilterExpressionList condition = null, string orderBy = "")
        {
            return DynamicFilterRepository.Current.GetCount(table, condition, orderBy);
        }

        /// <summary>
        /// از این متد برای دریافت رکوردهای جدول بر اساس شرط خاص استفاده می شود
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// IEnumerable&lt;Person&gt; list = DynamicService.Current.GetList&lt;Person&gt;([condition], ["ID ASC"], ["*"]);
        /// </code>
        /// </example>
        /// <param name="condition">شرط های اعمال شده در پرسجو: FilterExpressionList</param>
        /// <param name="orderBy">مرتب سازی بر اساس: string</param>
        /// <param name="columns">فیلدهای مورد نظر جهت برگرداندن: string</param>
        /// <returns>IEnumerable&lt;GenericType&gt;</returns>
        [OperationContract]
        public IEnumerable<T> GetList<T>(FilterExpressionList condition = null, string orderBy = "", string columns = "*") where T : class
        {
            return DynamicFilterRepository.Current.GetList<T>(condition, orderBy, columns);
        }

        /// <summary>
        /// از این متد برای دریافت رکوردهای جدول بر اساس شرط خاص استفاده می شود
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// IEnumerable&lt;dynamic&gt; list = DynamicService.Current.GetList("Person", [condition], ["ID ASC"], ["*"]);
        /// </code>
        /// </example>
        /// <param name="table">نام جدول در دیتابیس: string</param>
        /// <param name="condition">شرط های اعمال شده در پرسجو: FilterExpressionList</param>
        /// <param name="orderBy">مرتب سازی بر اساس: string</param>
        /// <param name="columns">فیلدهای مورد نظر جهت برگرداندن: string</param>
        /// <returns>IEnumerable&lt;dynamic&gt;</returns>
        [OperationContract]
        public IEnumerable<dynamic> GetList(string table, FilterExpressionList condition = null, string orderBy = "", string columns = "*")
        {
            return DynamicFilterRepository.Current.GetList(table, condition, orderBy, columns);
        }

        /// <summary>
        /// از این متد برای دریافت رکوردهای جدول بر اساس شرط خاص استفاده می شود
        /// </summary>
        /// <remarks>
        /// این متد برای دریافت رکوردها در یک ساختار با قابلیت صفحه بندی استفاده می شود
        /// </remarks>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// IEnumerable&lt;Person&gt; list = DynamicService.Current.GetList&lt;Person&gt;(2, 10, [condition], ["ID ASC"], ["*"]);
        /// </code>
        /// </example>
        /// <param name="condition">شرط های اعمال شده در پرسجو: FilterExpressionList</param>
        /// <param name="orderBy">مرتب سازی بر اساس: string</param>
        /// <param name="columns">فیلدهای مورد نظر جهت برگرداندن: string</param>
        /// <param name="page">اندیس صفحه مورد نظر: long</param>
        /// <param name="itemPerPage">تعداد رکورد در هر صفحه: int</param>
        /// <returns>IEnumerable&lt;GenericType&gt;</returns>
        [OperationContract]
        public IEnumerable<T> GetList<T>(long page, int itemPerPage, FilterExpressionList condition = null, string orderBy = "", string columns = "*") where T : class
        {
            return DynamicFilterRepository.Current.GetList<T>(page, itemPerPage, condition, orderBy, columns);
        }

        /// <summary>
        /// از این متد برای دریافت رکوردهای جدول بر اساس شرط خاص استفاده می شود
        /// </summary>
        /// <remarks>
        /// این متد برای دریافت رکوردها در یک ساختار با قابلیت صفحه بندی استفاده می شود
        /// </remarks>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// IEnumerable&lt;dynamic&gt; list = DynamicService.Current.GetList("Person", 2, 10, [condition], ["ID ASC"], ["*"]);
        /// </code>
        /// </example>
        /// <param name="table">نام جدول در دیتابیس: string</param>
        /// <param name="condition">شرط های اعمال شده در پرسجو: FilterExpressionList</param>
        /// <param name="orderBy">مرتب سازی بر اساس: string</param>
        /// <param name="columns">فیلدهای مورد نظر جهت برگرداندن: string</param>
        /// <param name="page">اندیس صفحه مورد نظر: long</param>
        /// <param name="itemPerPage">تعداد رکورد در هر صفحه: int</param>
        /// <returns>IEnumerable&lt;dynamic&gt;</returns>
        [OperationContract]
        public IEnumerable<dynamic> GetList(string table, long page, int itemPerPage, FilterExpressionList condition = null, string orderBy = "", string columns = "*")
        {
            return DynamicFilterRepository.Current.GetList(table, page, itemPerPage, condition, orderBy, columns);
        }

        /// <summary>
        /// از این متد برای دریافت یک رکورد از جدول بر اساس کلید اصلی استفاده می شود
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// Person item = DynamicService.Current.GetItem&lt;Person&gt;(24);
        /// </code>
        /// </example>
        /// <param name="itemId">شناسه رکورد مورد نظر: int</param>
        /// <returns>GenericType</returns>
        [OperationContract]
        public T GetItem<T>(int itemId) where T : class
        {
            return DynamicFilterRepository.Current.GetItem<T>(itemId);
        }

        /// <summary>
        /// از این متد برای دریافت یک رکورد از جدول بر اساس کلید اصلی استفاده می شود
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// Person item = DynamicService.Current.GetItem("Person", 24);
        /// </code>
        /// </example>
        /// <param name="table">نام جدول در دیتابیس: string</param>
        /// <param name="itemId">شناسه رکورد مورد نظر: int</param>
        /// <returns>dynamic</returns>
        [OperationContract]
        public dynamic GetItem(string table, int itemId)
        {
            return DynamicFilterRepository.Current.GetItem(table, itemId);
        }

        /// <summary>
        /// از این متد برای دریافت یک رکورد از جدول بر اساس شرط خاص استفاده می شود
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// Person item = DynamicService.Current.GetItem&lt;Person&gt;([condition], ["ID ASC"], ["*"]);
        /// </code>
        /// </example>
        /// <param name="condition">شرط های اعمال شده در پرسجو: FilterExpressionList</param>
        /// <param name="orderBy">مرتب سازی بر اساس: string</param>
        /// <param name="columns">فیلدهای مورد نظر جهت برگرداندن: string</param>
        /// <returns>GenericType</returns>
        [OperationContract]
        public T GetItem<T>(FilterExpressionList condition = null, string orderBy = "", string columns = "*") where T : class
        {
            return DynamicFilterRepository.Current.GetItem<T>(condition, orderBy, columns);
        }

        /// <summary>
        /// از این متد برای دریافت یک رکورد از جدول بر اساس شرط خاص استفاده می شود
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// Person item = DynamicService.Current.GetItem("Person", [condition], ["ID ASC"], ["*"]);
        /// </code>
        /// </example>
        /// <param name="table">نام جدول در دیتابیس: string</param>
        /// <param name="condition">شرط های اعمال شده در پرسجو: FilterExpressionList</param>
        /// <param name="orderBy">مرتب سازی بر اساس: string</param>
        /// <param name="columns">فیلدهای مورد نظر جهت برگرداندن: string</param>
        /// <returns>dynamic</returns>
        public dynamic GetItem(string table, FilterExpressionList condition = null, string orderBy = "", string columns = "*")
        {
            return DynamicFilterRepository.Current.GetItem(table, condition, orderBy, columns);
        }

        /// <summary>
        /// از این متد برای ذخیره یک رکورد در جدول خاص استفاده می شود
        /// </summary>
        /// <remarks>
        /// بعد از ذخیره رکورد شناسه آن بازگردانده می شود
        /// </remarks>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// class Person {
        ///     public int ID;
        ///     public string Name;
        /// }
        /// DynamicService.Current.CreateItem(new Person { ID = 1, Name = "John" });
        /// </code>
        /// </example>
        /// <param name="item">موجودیت مورد نظر جهت ذخیره: GenericType</param>
        /// <returns>long</returns>
        [OperationContract]
        public long CreateItem<T>(T item) where T : class
        {
            return DynamicFilterRepository.Current.CreateItem(item);
        }

        /// <summary>
        /// از این متد برای ویرایش یک رکورد در جدول خاص استفاده می شود
        /// </summary>
        /// <remarks>
        /// بعد از ویرایش رکورد در صورت موفقیت آمیز بودن مقدار صحیح بازگردانده می شود
        /// </remarks>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// class Person {
        ///     public int ID;
        ///     public string Name;
        /// }
        /// var person = DynamicService.Current.GetItem&lt;Person&gt;(23);
        /// person.Name = "John Doue";
        /// DynamicService.Current.UpdateItem(person);
        /// </code>
        /// </example>
        /// <param name="item">موجودیت مورد نظر جهت ویرایش: GenericType</param>
        /// <returns>bool</returns>
        [OperationContract]
        public bool UpdateItem<T>(T item) where T : class
        {
            return DynamicFilterRepository.Current.UpdateItem(item);
        }

        /// <summary>
        /// از این متد برای حذف یک رکورد در جدول خاص استفاده می شود
        /// </summary>
        /// <remarks>
        /// بعد از حذف رکورد در صورت موفقیت آمیز بودن مقدار صحیح بازگردانده می شود
        /// </remarks>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// class Person {
        ///     public int ID;
        ///     public string Name;
        /// }
        /// var person = DynamicService.Current.GetItem&lt;Person&gt;(23);
        /// DynamicService.Current.DeleteItem(person);
        /// </code>
        /// </example>
        /// <param name="item">موجودیت مورد نظر جهت ویرایش: GenericType</param>
        /// <returns>bool</returns>
        [OperationContract]
        public bool DeleteItem<T>(T item) where T : class
        {
            return DynamicFilterRepository.Current.DeleteItem(item);
        }

        /// <summary>
        /// از این متد برای حذف یک رکورد در جدول خاص استفاده می شود
        /// </summary>
        /// <remarks>
        /// بعد از حذف رکورد در صورت موفقیت آمیز بودن مقدار صحیح بازگردانده می شود
        /// </remarks>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// DynamicService.Current.DeleteItem&lt;Person&gt;(23);
        /// </code>
        /// </example>
        /// <param name="itemId">شناسه رکورد مورد نظر: int</param>
        /// <returns>bool</returns>
        [OperationContract]
        public bool DeleteItem<T>(int itemId) where T : class
        {
            return DynamicFilterRepository.Current.DeleteItem<T>(itemId);
        }

        /// <summary>
        /// از این متد برای حذف کلیه رکوردهای جدول خاص استفاده می شود
        /// </summary>
        /// <remarks>
        /// بعد از حذف رکورد در صورت موفقیت آمیز بودن مقدار صحیح بازگردانده می شود
        /// </remarks>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// DynamicService.Current.DeleteAllItem&lt;Person&gt;();
        /// </code>
        /// </example>
        /// <returns>bool</returns>
        [OperationContract]
        public bool DeleteAllItem<T>() where T : class
        {
            return DynamicFilterRepository.Current.DeleteAllItem<T>();
        }

        /// <summary>
        /// از این متد برای حذف یکسری رکورد در جدول براساس شرط خاص استفاده می شود
        /// </summary>
        /// <remarks>
        /// بعد از حذف رکورد در صورت موفقیت آمیز بودن یک عدد بازگردانده می شود
        /// </remarks>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// DynamicService.Current.DeleteList&lt;Person&gt;([condition]);
        /// </code>
        /// </example>
        /// <param name="condition">شرط های اعمال شده در پرسجو: FilterExpressionList</param>
        /// <returns>int</returns>
        [OperationContract]
        public int DeleteList<T>(FilterExpressionList condition = null) where T : class
        {
            return DynamicFilterRepository.Current.DeleteList<T>(condition);
        }

        /// <summary>
        /// از این متد برای اجرای دستورات sql به صورت مستقیم استفاده می شود
        /// </summary>
        /// <remarks>
        /// بعد از انجام عملیات در صورت موفقیت آمیز بودن یک عدد بازگردانده می شود
        /// </remarks>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// DynamicParameters params = new DynamicParameters();
        /// params.Add("ID", 23);
        /// int return = DynamicService.Current.Execute("SELECT * FROM Person WHERE ID = @ID", [params], [CommandType.Text]);
        /// </code>
        /// </example>
        /// <param name="command">رشته پرسجو مورد نظر: string</param>
        /// <param name="param">پارامترهای ارسالی: DynamicParameters</param>
        /// <param name="type">نوع پرسجو: CommandType</param>
        /// <returns>int</returns>
        [OperationContract]
        public int Execute(string command, DynamicParameters param = null, CommandType type = CommandType.Text)
        {
            return DynamicFilterRepository.Current.Execute(command, param, type);
        }

        /// <summary>
        /// از این متد برای اجرای یک رویه در دیتابیس استفاده می شود
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// DynamicParameters params = new DynamicParameters();
        /// params.Add("ID", 23);
        /// IEnumerable&lt;Person&gt; list = DynamicService.Current.StoredProcedures&lt;Person&gt;("PesronList", params, CommandType.StoredProcedure);
        /// </code>
        /// </example>
        /// <param name="procedureName">نام رویه: string</param>
        /// <param name="param">پارامترهای ارسالی: DynamicParameters</param>
        /// <param name="type">نوع پرسجو: CommandType</param>
        /// <returns>IEnumerable&lt;GenericType&gt;</returns>
        [OperationContract]
        public IEnumerable<T> StoredProcedures<T>(string procedureName, DynamicParameters param, CommandType type) where T : class
        {
            return DynamicFilterRepository.Current.StoredProcedures<T>(procedureName, param, type);
        }

        /// <summary>
        /// از این متد برای اجرای دستورات sql به صورت مستقیم استفاده می شود
        /// </summary>
        /// <example> 
        /// این مثال طریقه استفاده از این متد را نشان می دهد
        /// <code>
        /// IEnumerable&lt;dynamic&gt; list = DynamicService.Current.Query("SELECT * FROM Person");
        /// </code>
        /// </example>
        /// <param name="command">رشته پرسجو مورد نظر: string</param>
        /// <returns>IEnumerable&lt;dynamic&gt;</returns>
        public IEnumerable<dynamic> Query(string command)
        {
            return DynamicFilterRepository.Current.Query(command);
        }

        /// <summary>
        /// ازبین برنده کلاس
        /// </summary>
        /// <param name="disposing">حذف ارتباط با دیتابیس: bool</param>
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (DynamicFilterRepository.Current.Connection.State == ConnectionState.Open)
                    DynamicFilterRepository.Current.Connection.Close();
                DynamicFilterRepository.Current.Connection.Dispose();
            }
        }

        /// <summary>
        /// ازبین برنده کلاس
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        #endregion

    }
}
