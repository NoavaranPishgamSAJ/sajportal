﻿namespace DataService
{

    #region Assembly

    using System.Collections.Generic;
    using ORM.Model;
    using System.ServiceModel;

    #endregion

    /// <summary>
    /// از این رابط برای مدیریت مجوزهای کاربران و سمت ها و نقش های آن استفاده می شود
    /// </summary>
    [ServiceContract]
    public interface IMembershipService
    {

        #region Method

        /// <summary>
        /// اضافه کردن مجوز به نقش
        /// </summary>
        /// <param name="roleId">: int</param>
        /// <param name="permissionId">: int</param>
        /// <returns>bool</returns>
        [OperationContract]
        bool AddRolePermission(int roleId, int permissionId);

        /// <summary>
        /// اضافه کردن مجوز به نقش
        /// </summary>
        /// <param name="roleId">: int</param>
        /// <param name="displayName">: string</param>
        /// <returns>bool</returns>
        [OperationContract]
        bool AddRolePermission(int roleId, string displayName);

        /// <summary>
        /// اضافه کردن مجوز به نقش
        /// </summary>
        /// <param name="roleId">: int</param>
        /// <param name="devName">: string</param>
        /// <returns>bool</returns>
        [OperationContract]
        bool AddRolePermissionExtra(int roleId, string devName);

        /// <summary>
        /// اضافه کردن مجوز به کاربر
        /// </summary>
        /// <param name="customerId">: int</param>
        /// <param name="permissionId">: int</param>
        /// <returns>bool</returns>
        [OperationContract]
        bool AddUserPermission(int customerId, int permissionId);

        /// <summary>
        /// اضافه کردن مجوز به کاربر
        /// </summary>
        /// <param name="customerId">: int</param>
        /// <param name="displayName">: string</param>
        /// <returns>bool</returns>
        [OperationContract]
        bool AddUserPermission(int customerId, string displayName);

        /// <summary>
        /// اضافه کردن مجوز به کاربر
        /// </summary>
        /// <param name="customerId">: int</param>
        /// <param name="devName">: string</param>
        /// <returns>bool</returns>
        [OperationContract]
        bool AddUserPermissionExtra(int customerId, string devName);

        /// <summary>
        /// گرفتن مجوز از کاربر
        /// </summary>
        /// <param name="customerId">: int</param>
        /// <param name="permissionId">: int</param>
        /// <returns>bool</returns>
        [OperationContract]
        bool DeleteUserPermission(int customerId, int permissionId);

        /// <summary>
        /// گرفتن مجوز از کاربر
        /// </summary>
        /// <param name="customerId">: int</param>
        /// <param name="displayName">: string</param>
        /// <returns>bool</returns>
        [OperationContract]
        bool DeleteUserPermission(int customerId, string displayName);

        /// <summary>
        /// گرفتن مجوز از کاربر
        /// </summary>
        /// <param name="customerId">: int</param>
        /// <param name="devName">: string</param>
        /// <returns>bool</returns>
        [OperationContract]
        bool DeleteUserPermissionExtra(int customerId, string devName);

        /// <summary>
        /// حذف تمامی مجوزهای نقش
        /// </summary>
        /// <param name="roleId">: int</param>
        /// <returns>int</returns>
        [OperationContract]
        int DeleteRolePermissions(int roleId);

        /// <summary>
        /// حذف تمامی نقش های کاربر
        /// </summary>
        /// <param name="customerId">: int</param>
        /// <returns>int</returns>
        [OperationContract]
        int DeleteCustomerRoles(int customerId);

        /// <summary>
        /// حذف تمامی سمت های کاربر
        /// </summary>
        /// <param name="customerId">: int</param>
        /// <returns>int</returns>
        [OperationContract]
        int DeleteCustomerPositions(int customerId);

        /// <summary>
        /// اضافه کردن نقش برای کاربر
        /// </summary>
        /// <param name="customerId">: int</param>
        /// <param name="roleId">: int</param>
        /// <returns>int</returns>
        [OperationContract]
        bool AddUserRole(int customerId, int roleId);

        /// <summary>
        /// اضافه کردن سمت برای کاربر
        /// </summary>
        /// <param name="customerId">: int</param>
        /// <param name="positionId">: int</param>
        /// <returns>int</returns>
        [OperationContract]
        bool AddUserPosition(int customerId, int positionId);

        /// <summary>
        /// حذف مجوز از نقش
        /// </summary>
        /// <param name="roleId">: int</param>
        /// <param name="permissionId">: int</param>
        /// <returns>int</returns>
        [OperationContract]
        bool DeleteRolePermission(int roleId, int permissionId);

        /// <summary>
        /// حذف مجوز از نقش
        /// </summary>
        /// <param name="roleId">: int</param>
        /// <param name="displayName">: int</param>
        /// <returns>int</returns>
        [OperationContract]
        bool DeleteRolePermission(int roleId, string displayName);

        /// <summary>
        /// حذف مجوز از نقش
        /// </summary>
        /// <param name="roleId">: int</param>
        /// <param name="devName">: int</param>
        /// <returns>int</returns>
        [OperationContract]
        bool DeleteRolePermissionExtra(int roleId, string devName);

        /// <summary>
        /// دریافت اطلاعات کاربر بر اساس نام کاربری
        /// </summary>
        /// <param name="username">: string</param>
        /// <returns>Customer</returns>
        [OperationContract]
        Customer GetUser(string username);

        /// <summary>
        /// بررسی صحت نام کاربری و رمزعبور
        /// </summary>
        /// <param name="username">: string</param>
        /// <param name="password">: string</param>
        /// <returns>bool</returns>
        [OperationContract]
        bool Login(string username, string password);

        /// <summary>
        /// بررسی دارا بودن مجوز برای نقش خاص
        /// </summary>
        /// <param name="roleId">: int</param>
        /// <param name="permissionId">: int</param>
        /// <returns>bool</returns>
        [OperationContract]
        bool RoleHasPermission(int roleId, int permissionId);

        /// <summary>
        /// بررسی دارا بودن مجوز برای نقش خاص
        /// </summary>
        /// <param name="roleId">: int</param>
        /// <param name="displayName">: string</param>
        /// <returns>bool</returns>
        [OperationContract]
        bool RoleHasPermission(int roleId, string displayName);

        /// <summary>
        /// بررسی دارا بودن مجوز برای نقش خاص
        /// </summary>
        /// <param name="roleId">: int</param>
        /// <param name="devName">: string</param>
        /// <returns>bool</returns>
        [OperationContract]
        bool RoleHasPermissionExtra(int roleId, string devName);

        /// <summary>
        /// بررسی دارا بودن مجوز برای کاربر خاص
        /// </summary>
        /// <param name="customerId">: int</param>
        /// <param name="permissionId">: int</param>
        /// <returns>bool</returns>
        [OperationContract]
        bool UserHasPermission(int customerId, int permissionId);

        /// <summary>
        /// بررسی دارا بودن مجوز برای کاربر خاص
        /// </summary>
        /// <param name="customerId">: int</param>
        /// <param name="displayName">: string</param>
        /// <returns>bool</returns>
        [OperationContract]
        bool UserHasPermission(int customerId, string displayName);

        /// <summary>
        /// بررسی دارا بودن مجوز برای کاربر خاص
        /// </summary>
        /// <param name="customerId">: int</param>
        /// <param name="devName">: string</param>
        /// <returns>bool</returns>
        [OperationContract]
        bool UserHasPermissionExtra(int customerId, string devName);

        /// <summary>
        /// دریافت مجوزهای اضافه کاربر
        /// </summary>
        /// <param name="customerId">: int</param>
        /// <returns>IEnumerable&lt;CustomerPermissionView&gt;</returns>
        [OperationContract]
        IEnumerable<CustomerPermissionView> UserPermissions(int customerId);

        /// <summary>
        /// دریافت مجوزهای کاربر
        /// </summary>
        /// <param name="customerId">: int</param>
        /// <returns>IEnumerable&lt;CustomerPermissions&gt;</returns>
        [OperationContract]
        IEnumerable<CustomerPermissions> UserExistPermissions(int customerId);

        /// <summary>
        /// دریافت مجوزهای نقش
        /// </summary>
        /// <param name="roleId">: int</param>
        /// <returns>IEnumerable&lt;RolePermissionView&gt;</returns>
        [OperationContract]
        IEnumerable<RolePermissionView> RolePermissions(int roleId);

        /// <summary>
        /// دریافت سمت های کاربر
        /// </summary>
        /// <param name="customerId">: int</param>
        /// <returns>IEnumerable&lt;CustomerPositionView&gt;</returns>
        [OperationContract]
        IEnumerable<CustomerPositionView> UserPositions(int customerId);

        /// <summary>
        /// دریافت نقش های کاربر
        /// </summary>
        /// <param name="customerId">: int</param>
        /// <returns>IEnumerable&lt;CustomerRoleView&gt;</returns>
        [OperationContract]
        IEnumerable<CustomerRoleView> UserRoles(int customerId);

        /// <summary>
        /// بررسی دارا بودن سمت توسط کاربر
        /// </summary>
        /// <param name="customerId">: int</param>
        /// <param name="displayName">: string</param>
        /// <returns>bool</returns>
        [OperationContract]
        bool UserHasPosition(int customerId, string displayName);

        /// <summary>
        /// تخصیص سمت به کاربر
        /// </summary>
        /// <param name="customerId">: int</param>
        /// <param name="displayName">: string</param>
        /// <returns>bool</returns>
        [OperationContract]
        bool AddUserPosition(int customerId, string displayName);

        /// <summary>
        /// حذف سمت کاربر
        /// </summary>
        /// <param name="customerId">: int</param>
        /// <param name="displayName">: string</param>
        /// <returns>bool</returns>
        [OperationContract]
        bool DeleteUserPosition(int customerId, string displayName);

        /// <summary>
        /// بررسی دارا بودن نقش برای کاربر
        /// </summary>
        /// <param name="customerId">: int</param>
        /// <param name="displayName">: string</param>
        /// <returns>bool</returns>
        [OperationContract]
        bool UserHasRole(int customerId, string displayName);

        /// <summary>
        /// تخصیص نقش به کاربر
        /// </summary>
        /// <param name="customerId">: int</param>
        /// <param name="displayName">: string</param>
        /// <returns>bool</returns>
        [OperationContract]
        bool AddUserRole(int customerId, string displayName);

        /// <summary>
        /// حذف نقش کاربر
        /// </summary>
        /// <param name="customerId">: int</param>
        /// <param name="displayName">: string</param>
        /// <returns>bool</returns>
        [OperationContract]
        bool DeleteUserRole(int customerId, string displayName);

        #endregion

    }
}
